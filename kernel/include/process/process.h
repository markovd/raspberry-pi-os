/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#ifndef KIV_OS_RPIOS_PROCESS_H
#define KIV_OS_RPIOS_PROCESS_H

enum class NTask_State {
    New = 0,
    Runnable,
    Running,
    Blocked,
    Zombie
};

struct TCPU_Context {
    unsigned long lr;
    unsigned long sp;
    unsigned long pc;
};

struct TTask_Struct {
    TCPU_Context cpu_context;
    unsigned int pid;
    NTask_State state;
    unsigned int sched_counter;
    unsigned int sched_static_priority;
};

class CProcess_List_Node {
public:
    TTask_Struct* m_task = nullptr;
    CProcess_List_Node* m_next = nullptr;
};

class CProcess_Manager {
public:

    unsigned int Create_Process(unsigned long funcptr);
    CProcess_List_Node* Select_Next_Process();
    void Schedule();
    unsigned int Create_Main_Process();
    CProcess_List_Node* Get_Current_Process();

    ~CProcess_Manager();
private:
    unsigned int m_last_pid = 0;
    CProcess_List_Node* m_first = nullptr;
    CProcess_List_Node* m_last = nullptr;
    CProcess_List_Node* m_current = nullptr;

    void Switch_To(CProcess_List_Node* next);
    CProcess_List_Node* Get_Next_Process_In_List();
    void Push_To_Process_List(TTask_Struct* task);
    void Set_Current_Task(CProcess_List_Node* next);
};

extern "C" {
void process_bootstrap();
void context_switch(TCPU_Context* ctx_to,
                    TCPU_Context* ctx_from);
void context_switch_first(TCPU_Context* ctx_to,
                          TCPU_Context* ctx_from);
};
#endif //KIV_OS_RPIOS_PROCESS_H
