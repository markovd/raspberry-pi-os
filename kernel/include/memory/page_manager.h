/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#ifndef KIV_OS_RPIOS_PAGE_MANAGER_H
#define KIV_OS_RPIOS_PAGE_MANAGER_H

#include <cstdint>
#include <memory/mem_map.h>

class CPage_Manager {
public:
    CPage_Manager();

    uint32_t Alloc_Page();
    void Free_Page(uint32_t address);
private:
    uint8_t m_page_bitmap[mem::Page_Count / 8];
};

extern CPage_Manager sPage_Manager;

#endif //KIV_OS_RPIOS_PAGE_MANAGER_H
