/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#ifndef KIV_OS_RPIOS_KERNEL_HEAP_H
#define KIV_OS_RPIOS_KERNEL_HEAP_H

//#include <cstdint>
#include <memory/page_manager.h>

class IHeap_Manager {
public:
    virtual void* Alloc(uint32_t size) { return nullptr; }
    virtual void Free(void* memory) {}

    template<class T>
    T *Alloc() {
        return reinterpret_cast<T*>(Alloc(sizeof(T)));
    }
};

class CSimple_Kernel_Heap : public IHeap_Manager {
public:
    void *Alloc(uint32_t size) override {
        if (size > mem::Page_Size) {
            return nullptr;
        }

        if (m_page_offset + size > mem::Page_Size || m_page == 0) {
            m_page = sPage_Manager.Alloc_Page();
            if (m_page == 0) {
                return nullptr; // došla paměť
            }
            m_page_offset = 0;
        }

        uint32_t old = m_page_offset;
        m_page_offset += size;
        return (void *)(m_page + old);
    }

    void Free(void *memory) override {
        // :( neumíme
    }

private:
    uint32_t m_page = 0;
    uint32_t m_page_offset = 0;
};


enum class NMemory_type : uint8_t {
    Process = 0,
    Hole = 1
};

struct THeap_List_Node {
    NMemory_type type;  // typ oblasti
    uint32_t base;      // počáteční adresa
    uint32_t size;      // velikost oblasti
    THeap_List_Node* next;
};



/**
 * Implementace správy paměti pomocí seznamu procesů a děr. Využívá algoritmu first-fit.
 */
class CList_Kernel_Heap : public IHeap_Manager {
public:
    void *Alloc(uint32_t size) override;

    void Free(void *memory) override;
private:
    THeap_List_Node* m_first;
};

extern CSimple_Kernel_Heap sKernelMem;
#endif //KIV_OS_RPIOS_KERNEL_HEAP_H
