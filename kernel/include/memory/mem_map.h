/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#ifndef KIV_OS_RPIOS_MEM_MAP_H
#define KIV_OS_RPIOS_MEM_MAP_H

#include <cstdint>

namespace mem {
    constexpr uint32_t Low_Memory = 0x20000;
    constexpr uint32_t High_Memory = 0x20000000;
    constexpr uint32_t Page_Size = 0x4000;

    constexpr uint32_t Paging_Memory_Size = High_Memory - Low_Memory;
    constexpr uint32_t Page_Count = Paging_Memory_Size / Page_Size;

}
#endif //KIV_OS_RPIOS_MEM_MAP_H
