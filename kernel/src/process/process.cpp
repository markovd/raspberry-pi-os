/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#include <process/process.h>
#include <memory/kernel_heap.h>

unsigned int CProcess_Manager::Create_Process(unsigned long funcptr) {
    auto* task = (TTask_Struct *) sKernelMem.Alloc(sizeof(TTask_Struct));

    task->pid = ++m_last_pid;
    task->sched_static_priority = 5;
    task->sched_counter = task->sched_static_priority;
    task->state = NTask_State::New;

    task->cpu_context.lr = funcptr;
    task->cpu_context.pc = reinterpret_cast<unsigned long>(&process_bootstrap);
    task->cpu_context.sp = static_cast<unsigned long>(sPage_Manager.Alloc_Page()) + mem::Page_Size;

    Push_To_Process_List(task);

    return task->pid;
}

void CProcess_Manager::Schedule() {
    CProcess_List_Node* current_process = Get_Current_Process();
    CProcess_List_Node* next_process = nullptr;
    current_process->m_task->sched_counter--;
    if (current_process->m_task->sched_counter == 0) {
        next_process = Select_Next_Process();
        if (next_process == current_process) {
            current_process->m_task->sched_counter =
                    current_process->m_task->sched_static_priority;
            return;
        }
    }
    if (next_process)
        Switch_To(next_process);
}

CProcess_List_Node* CProcess_Manager::Select_Next_Process() {
    CProcess_List_Node *candidate;
    do {
        candidate = Get_Next_Process_In_List();
    } while (candidate->m_task->state != NTask_State::Running &&
             candidate->m_task->state != NTask_State::Runnable &&
             candidate->m_task->state != NTask_State::New);
    return candidate;
}

void CProcess_Manager::Switch_To(CProcess_List_Node* next) {
    CProcess_List_Node* current_process = Get_Current_Process();
    TTask_Struct* old_task = current_process->m_task;
    TTask_Struct* new_task = next->m_task;
    if (old_task->state == NTask_State::Running)
        old_task->state = NTask_State::Runnable;
    bool is_first_time = (new_task->state == NTask_State::New);
    Set_Current_Task(next);
    new_task->sched_counter = new_task->sched_static_priority;
    new_task->state = NTask_State::Running;
    if (is_first_time)
        context_switch_first(&new_task->cpu_context, &old_task->cpu_context);
    else
        context_switch(&new_task->cpu_context, &old_task->cpu_context);
}

unsigned int CProcess_Manager::Create_Main_Process() {
    if (m_first != nullptr) {
        return 0;   // vytvoření hlavního procesu povolíme pouze, pokud žádný neexistuje
    }

    auto* task = (TTask_Struct *) sKernelMem.Alloc(sizeof(TTask_Struct));
    task->pid = ++m_last_pid;
    task->sched_static_priority = 1;
    task->sched_counter = task->sched_static_priority;
    task->state = NTask_State::Running;

    Push_To_Process_List(task);
    // nemusíme volat Switch_To - již je aktuální
    return task->pid;
}

CProcess_Manager::~CProcess_Manager() {
    // TODO vymazat data procesů
}

CProcess_List_Node *CProcess_Manager::Get_Current_Process() {
    if (m_current == nullptr) return m_first;
    return m_current->m_next;   // vracíme vždy prvek napravo - ukazujeme "mezi"
}

CProcess_List_Node *CProcess_Manager::Get_Next_Process_In_List() {
    if (m_first == nullptr) {   // seznam procesů je prázdný
        return nullptr;
    }

    if (m_current == m_last) {
        m_current = nullptr;    // jdeme zpět na začátek
    } else {
        m_current = m_current->m_next;  // jdeme na další prvek
    }
    return Get_Current_Process();
}

void CProcess_Manager::Push_To_Process_List(TTask_Struct* task) {
    if (m_first == nullptr) {
        m_first = (CProcess_List_Node *) sKernelMem.Alloc(sizeof(CProcess_List_Node));
        m_first->m_task = task;

        m_first->m_next = nullptr;
        m_last = m_first;
    } else {
        auto new_node = (CProcess_List_Node *) sKernelMem.Alloc(sizeof(CProcess_List_Node));
        new_node->m_task = task;

        m_last->m_next = new_node;
        m_last = m_last->m_next;
    }
}

void CProcess_Manager::Set_Current_Task(CProcess_List_Node *next) {
    if (m_first == nullptr) {
        return; // máme prázdný seznam, asi došlo k chybě
    }

    if (Get_Current_Process() == next) {
        return;
    }

    while (Get_Next_Process_In_List() != next) {    // posouváme se tak dlouho, dokud neukazujeme "mezi" předchozí a ten,
                                                    // který nastavujeme jako aktuální
    }
}
