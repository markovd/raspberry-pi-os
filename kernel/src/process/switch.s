;@ r0 = prvni parametr -- PCB noveho procesu
;@ r1 = druhy parametr -- PCB stareho procesu
context_switch:
    mrs r12, cpsr
    push {r14}
    push {r13}
    push {r0-r12}
    str sp, [r1, #4]

    ldr sp, [r0, #4]
    pop {r0-r12}
    msr cpsr_c, r12
    pop {lr, pc}

context_switch_first:
    mrs r12, cpsr
    push {r14}
    push {r13}
    push {r0-r12}
    str sp, [r1, #4]

    ldr r3, [r0, #0]
    ldr r2, [r0, #8]
    ldr sp, [r0, #4]
    push {r3}
    push {r2}
    cpsie i
    pop {pc}

process_bootstrap:
    ;@ prepnuti stavu procesoru
    add lr, pc, #0
    pop {pc}
    ;@ TODO: terminate

fin_loop:
    b fin_loop
