/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#include <memory/kernel_heap.h>

CSimple_Kernel_Heap sKernelMem;

void *CList_Kernel_Heap::Alloc(const uint32_t size) {
    if (size == 0) {
        return nullptr;
    }

    if (m_first == nullptr) {
        m_first = (THeap_List_Node *) mem::Low_Memory;
        m_first->type = NMemory_type::Hole;
        m_first->next = nullptr;
        m_first->base = mem::Low_Memory + sizeof(*m_first); // počátek je až za projovacím prvkem
        m_first->size = mem::Paging_Memory_Size - sizeof(*m_first); // velikost je snížena o velikost prvku
    }

    THeap_List_Node* node = m_first;
    while (node != nullptr){
        if (node->type == NMemory_type::Hole && node->size >= size) {
            // TODO
        }
        node = node->next;
    }

    return nullptr; // došla paměť
}

void CList_Kernel_Heap::Free(void *memory) {
    IHeap_Manager::Free(memory);
}
