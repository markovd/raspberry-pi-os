/**
 * Created by <a href='mailto:markov.david@seznam.cz'>David Markov</a> on 22.10.21.
 */

#include <memory/page_manager.h>

CPage_Manager sPage_Manager;

CPage_Manager::CPage_Manager() {
    for (unsigned char & i : m_page_bitmap) {
        i = 0;
    }
}

uint32_t CPage_Manager::Alloc_Page() {
    for (uint32_t i = 0; i < mem::Page_Count / 8; ++i) {
        if (m_page_bitmap[i] == 0xFF) {
            continue;
        }

        for (uint32_t j = 0; j < 8; ++j) {
            if ((m_page_bitmap[i] & (1 << j)) == 0) {
                const uint32_t page_index = i * 8 + j;
                m_page_bitmap[i] |= 1 << j;
                return mem::Low_Memory + page_index * mem::Page_Size;
            }
        }
    }

    return 0;
}

void CPage_Manager::Free_Page(const uint32_t address) {
    const uint32_t page_index = (address - mem::Low_Memory) / mem::Page_Size;
    m_page_bitmap[page_index / 8] &= ~(1 << (page_index % 8));
}
