
./kernel:     file format elf32-littlearm


Disassembly of section .text:

00008000 <_start>:
_start():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:14
	;@	- sem skoci bootloader, prvni na co narazi je "ldr pc, _reset_ptr" -> tedy se chova jako kdyby slo o reset a skoci na zacatek provadeni
	;@	- v cele svoji krase (vsechny "ldr" instrukce) slouzi jako predloha skutecne tabulce vektoru preruseni
	;@ na dany offset procesor skoci, kdyz je vyvolano libovolne preruseni
	;@ ARM nastavuje rovnou registr PC na tuto adresu, tzn. na teto adrese musi byt kodovana 4B instrukce skoku nekam jinam
	;@ oproti tomu napr. x86 (x86_64) obsahuje v tabulce rovnou adresu a procesor nastavuje PC (CS:IP) na adresu kterou najde v tabulce
	ldr pc, _reset_ptr						;@ 0x00 - reset - vyvolano pri resetu procesoru
    8000:	e59ff018 	ldr	pc, [pc, #24]	; 8020 <_reset_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:15
	ldr pc, _undefined_instruction_ptr		;@ 0x04 - undefined instruction - vyjimka, vyvolana pri dekodovani nezname instrukce
    8004:	e59ff018 	ldr	pc, [pc, #24]	; 8024 <_undefined_instruction_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:16
	ldr pc, _software_interrupt_ptr			;@ 0x08 - software interrupt - vyvolano, kdyz procesor provede instrukci swi
    8008:	e59ff018 	ldr	pc, [pc, #24]	; 8028 <_software_interrupt_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:17
	ldr pc, _prefetch_abort_ptr				;@ 0x0C - prefetch abort - vyvolano, kdyz se procesor snazi napr. nacist instrukci z mista, odkud nacist nejde
    800c:	e59ff018 	ldr	pc, [pc, #24]	; 802c <_prefetch_abort_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:18
	ldr pc, _data_abort_ptr					;@ 0x10 - data abort - vyvolano, kdyz se procesor snazi napr. nacist data z mista, odkud nacist nejdou
    8010:	e59ff018 	ldr	pc, [pc, #24]	; 8030 <_data_abort_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:19
	ldr pc, _unused_handler_ptr				;@ 0x14 - unused - ve specifikaci ARM neni uvedeno zadne vyuziti
    8014:	e59ff018 	ldr	pc, [pc, #24]	; 8034 <_unused_handler_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:20
	ldr pc, _irq_ptr						;@ 0x18 - IRQ - hardwarove preruseni (general purpose)
    8018:	e59ff018 	ldr	pc, [pc, #24]	; 8038 <_irq_ptr>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
	ldr pc, _fast_interrupt_ptr				;@ 0x1C - fast interrupt request - prioritni IRQ pro vysokorychlostni zalezitosti
    801c:	e59ff018 	ldr	pc, [pc, #24]	; 803c <_fast_interrupt_ptr>

00008020 <_reset_ptr>:
_reset_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8020:	00008040 	andeq	r8, r0, r0, asr #32

00008024 <_undefined_instruction_ptr>:
_undefined_instruction_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8024:	00009114 	andeq	r9, r0, r4, lsl r1

00008028 <_software_interrupt_ptr>:
_software_interrupt_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8028:	00008d9c 	muleq	r0, ip, sp

0000802c <_prefetch_abort_ptr>:
_prefetch_abort_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    802c:	00009118 	andeq	r9, r0, r8, lsl r1

00008030 <_data_abort_ptr>:
_data_abort_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8030:	0000911c 	andeq	r9, r0, ip, lsl r1

00008034 <_unused_handler_ptr>:
_unused_handler_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8034:	00008040 	andeq	r8, r0, r0, asr #32

00008038 <_irq_ptr>:
_irq_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    8038:	00008db4 			; <UNDEFINED> instruction: 0x00008db4

0000803c <_fast_interrupt_ptr>:
_fast_interrupt_ptr():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:21
    803c:	00008dec 	andeq	r8, r0, ip, ror #27

00008040 <_reset>:
_reset():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:50
.equ    CPSR_FIQ_INHIBIT,       0x40


;@ kernel reset vektor - tento kod se vykona pri kazdem resetu zarizeni (i prvnim spusteni)
_reset:
	mov sp, #0x8000			;@ nastavime stack pointer na spodek zasobniku
    8040:	e3a0d902 	mov	sp, #32768	; 0x8000
/home/markovda/Skola/repos/os-5/kernel/src/start.s:53

	;@ nacteni tabulky vektoru preruseni do pameti
	mov r0, #0x8000			;@ adresa 0x8000 (_start) do r0
    8044:	e3a00902 	mov	r0, #32768	; 0x8000
/home/markovda/Skola/repos/os-5/kernel/src/start.s:54
    mov r1, #0x0000			;@ adresa 0x0000 (pocatek RAM) do r1 - tam budeme vkladat tabulku vektoru preruseni
    8048:	e3a01000 	mov	r1, #0
/home/markovda/Skola/repos/os-5/kernel/src/start.s:58

	;@ Thumb instrukce - nacteni 4B slov z pameti ulozene v r0 (0x8000) do registru r2, 3, ... 9
	;@                 - ulozeni obsahu registru r2, 3, ... 9 do pameti ulozene v registru r1 (0x0000)
    ldmia r0!,{r2, r3, r4, r5, r6, r7, r8, r9}
    804c:	e8b003fc 	ldm	r0!, {r2, r3, r4, r5, r6, r7, r8, r9}
/home/markovda/Skola/repos/os-5/kernel/src/start.s:59
    stmia r1!,{r2, r3, r4, r5, r6, r7, r8, r9}
    8050:	e8a103fc 	stmia	r1!, {r2, r3, r4, r5, r6, r7, r8, r9}
/home/markovda/Skola/repos/os-5/kernel/src/start.s:60
    ldmia r0!,{r2, r3, r4, r5, r6, r7, r8, r9}
    8054:	e8b003fc 	ldm	r0!, {r2, r3, r4, r5, r6, r7, r8, r9}
/home/markovda/Skola/repos/os-5/kernel/src/start.s:61
    stmia r1!,{r2, r3, r4, r5, r6, r7, r8, r9}
    8058:	e8a103fc 	stmia	r1!, {r2, r3, r4, r5, r6, r7, r8, r9}
/home/markovda/Skola/repos/os-5/kernel/src/start.s:64

	;@ na moment se prepneme do IRQ rezimu, nastavime mu stack pointer
	mov r0, #(CPSR_MODE_IRQ | CPSR_IRQ_INHIBIT | CPSR_FIQ_INHIBIT)
    805c:	e3a000d2 	mov	r0, #210	; 0xd2
/home/markovda/Skola/repos/os-5/kernel/src/start.s:65
    msr cpsr_c, r0
    8060:	e121f000 	msr	CPSR_c, r0
/home/markovda/Skola/repos/os-5/kernel/src/start.s:66
    mov sp, #0x7000
    8064:	e3a0da07 	mov	sp, #28672	; 0x7000
/home/markovda/Skola/repos/os-5/kernel/src/start.s:69

	;@ na moment se prepneme do FIQ rezimu, nastavime mu stack pointer
	mov r0, #(CPSR_MODE_FIQ | CPSR_IRQ_INHIBIT | CPSR_FIQ_INHIBIT)
    8068:	e3a000d1 	mov	r0, #209	; 0xd1
/home/markovda/Skola/repos/os-5/kernel/src/start.s:70
    msr cpsr_c, r0
    806c:	e121f000 	msr	CPSR_c, r0
/home/markovda/Skola/repos/os-5/kernel/src/start.s:71
    mov sp, #0x6000
    8070:	e3a0da06 	mov	sp, #24576	; 0x6000
/home/markovda/Skola/repos/os-5/kernel/src/start.s:74

	;@ a vracime se zpet do supervisor modu, SP si nastavime zpet na nasi hodnotu
    mov r0, #(CPSR_MODE_SVR | CPSR_IRQ_INHIBIT | CPSR_FIQ_INHIBIT)
    8074:	e3a000d3 	mov	r0, #211	; 0xd3
/home/markovda/Skola/repos/os-5/kernel/src/start.s:75
    msr cpsr_c, r0
    8078:	e121f000 	msr	CPSR_c, r0
/home/markovda/Skola/repos/os-5/kernel/src/start.s:76
    mov sp, #0x8000
    807c:	e3a0d902 	mov	sp, #32768	; 0x8000
/home/markovda/Skola/repos/os-5/kernel/src/start.s:78

	bl _c_startup			;@ C startup kod (inicializace prostredi)
    8080:	eb000426 	bl	9120 <_c_startup>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:79
	bl _cpp_startup			;@ C++ startup kod (volani globalnich konstruktoru, ...)
    8084:	eb00043f 	bl	9188 <_cpp_startup>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:80
	bl _kernel_main			;@ skocime do hlavniho kodu jadra (v C)
    8088:	eb0003f9 	bl	9074 <_kernel_main>
/home/markovda/Skola/repos/os-5/kernel/src/start.s:81
	bl _cpp_shutdown		;@ C++ shutdown kod (volani globalnich destruktoru, ...)
    808c:	eb000453 	bl	91e0 <_cpp_shutdown>

00008090 <hang>:
hang():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:83
hang:
	b hang
    8090:	eafffffe 	b	8090 <hang>

00008094 <__cxa_guard_acquire>:
__cxa_guard_acquire():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:11
	extern "C" int __cxa_guard_acquire (__guard *);
	extern "C" void __cxa_guard_release (__guard *);
	extern "C" void __cxa_guard_abort (__guard *);

	extern "C" int __cxa_guard_acquire (__guard *g)
	{
    8094:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8098:	e28db000 	add	fp, sp, #0
    809c:	e24dd00c 	sub	sp, sp, #12
    80a0:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:12
		return !*(char *)(g);
    80a4:	e51b3008 	ldr	r3, [fp, #-8]
    80a8:	e5d33000 	ldrb	r3, [r3]
    80ac:	e3530000 	cmp	r3, #0
    80b0:	03a03001 	moveq	r3, #1
    80b4:	13a03000 	movne	r3, #0
    80b8:	e6ef3073 	uxtb	r3, r3
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:13
	}
    80bc:	e1a00003 	mov	r0, r3
    80c0:	e28bd000 	add	sp, fp, #0
    80c4:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    80c8:	e12fff1e 	bx	lr

000080cc <__cxa_guard_release>:
__cxa_guard_release():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:16

	extern "C" void __cxa_guard_release (__guard *g)
	{
    80cc:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    80d0:	e28db000 	add	fp, sp, #0
    80d4:	e24dd00c 	sub	sp, sp, #12
    80d8:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:17
		*(char *)g = 1;
    80dc:	e51b3008 	ldr	r3, [fp, #-8]
    80e0:	e3a02001 	mov	r2, #1
    80e4:	e5c32000 	strb	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:18
	}
    80e8:	e320f000 	nop	{0}
    80ec:	e28bd000 	add	sp, fp, #0
    80f0:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    80f4:	e12fff1e 	bx	lr

000080f8 <__cxa_guard_abort>:
__cxa_guard_abort():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:21

	extern "C" void __cxa_guard_abort (__guard *)
	{
    80f8:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    80fc:	e28db000 	add	fp, sp, #0
    8100:	e24dd00c 	sub	sp, sp, #12
    8104:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:23

	}
    8108:	e320f000 	nop	{0}
    810c:	e28bd000 	add	sp, fp, #0
    8110:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8114:	e12fff1e 	bx	lr

00008118 <__dso_handle>:
__dso_handle():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:27
}

extern "C" void __dso_handle()
{
    8118:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    811c:	e28db000 	add	fp, sp, #0
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:29
    // ignore dtors for now
}
    8120:	e320f000 	nop	{0}
    8124:	e28bd000 	add	sp, fp, #0
    8128:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    812c:	e12fff1e 	bx	lr

00008130 <__cxa_atexit>:
__cxa_atexit():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:32

extern "C" void __cxa_atexit()
{
    8130:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8134:	e28db000 	add	fp, sp, #0
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:34
    // ignore dtors for now
}
    8138:	e320f000 	nop	{0}
    813c:	e28bd000 	add	sp, fp, #0
    8140:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8144:	e12fff1e 	bx	lr

00008148 <__cxa_pure_virtual>:
__cxa_pure_virtual():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:37

extern "C" void __cxa_pure_virtual()
{
    8148:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    814c:	e28db000 	add	fp, sp, #0
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:39
    // pure virtual method called
}
    8150:	e320f000 	nop	{0}
    8154:	e28bd000 	add	sp, fp, #0
    8158:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    815c:	e12fff1e 	bx	lr

00008160 <__aeabi_unwind_cpp_pr1>:
__aeabi_unwind_cpp_pr1():
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:42

extern "C" void __aeabi_unwind_cpp_pr1()
{
    8160:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8164:	e28db000 	add	fp, sp, #0
/home/markovda/Skola/repos/os-5/kernel/src/cxx.cpp:43 (discriminator 1)
	while (true)
    8168:	eafffffe 	b	8168 <__aeabi_unwind_cpp_pr1+0x8>

0000816c <_ZN4CAUXC1Ej>:
_ZN4CAUXC2Ej():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:5
#include <drivers/bcm_aux.h>

CAUX sAUX(hal::AUX_Base);

CAUX::CAUX(unsigned int aux_base)
    816c:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8170:	e28db000 	add	fp, sp, #0
    8174:	e24dd00c 	sub	sp, sp, #12
    8178:	e50b0008 	str	r0, [fp, #-8]
    817c:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:6
    : mAUX_Reg(reinterpret_cast<unsigned int*>(aux_base))
    8180:	e51b200c 	ldr	r2, [fp, #-12]
    8184:	e51b3008 	ldr	r3, [fp, #-8]
    8188:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:9
{
    //
}
    818c:	e51b3008 	ldr	r3, [fp, #-8]
    8190:	e1a00003 	mov	r0, r3
    8194:	e28bd000 	add	sp, fp, #0
    8198:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    819c:	e12fff1e 	bx	lr

000081a0 <_ZN4CAUX6EnableEN3hal15AUX_PeripheralsE>:
_ZN4CAUX6EnableEN3hal15AUX_PeripheralsE():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:12

void CAUX::Enable(hal::AUX_Peripherals aux_peripheral)
{
    81a0:	e92d4800 	push	{fp, lr}
    81a4:	e28db004 	add	fp, sp, #4
    81a8:	e24dd008 	sub	sp, sp, #8
    81ac:	e50b0008 	str	r0, [fp, #-8]
    81b0:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:13
    Set_Register(hal::AUX_Reg::ENABLES, Get_Register(hal::AUX_Reg::ENABLES) | (1 << static_cast<uint32_t>(aux_peripheral)));
    81b4:	e3a01001 	mov	r1, #1
    81b8:	e51b0008 	ldr	r0, [fp, #-8]
    81bc:	eb000031 	bl	8288 <_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE>
    81c0:	e1a02000 	mov	r2, r0
    81c4:	e51b300c 	ldr	r3, [fp, #-12]
    81c8:	e3a01001 	mov	r1, #1
    81cc:	e1a03311 	lsl	r3, r1, r3
    81d0:	e1823003 	orr	r3, r2, r3
    81d4:	e1a02003 	mov	r2, r3
    81d8:	e3a01001 	mov	r1, #1
    81dc:	e51b0008 	ldr	r0, [fp, #-8]
    81e0:	eb000017 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:14
}
    81e4:	e320f000 	nop	{0}
    81e8:	e24bd004 	sub	sp, fp, #4
    81ec:	e8bd8800 	pop	{fp, pc}

000081f0 <_ZN4CAUX7DisableEN3hal15AUX_PeripheralsE>:
_ZN4CAUX7DisableEN3hal15AUX_PeripheralsE():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:17

void CAUX::Disable(hal::AUX_Peripherals aux_peripheral)
{
    81f0:	e92d4800 	push	{fp, lr}
    81f4:	e28db004 	add	fp, sp, #4
    81f8:	e24dd008 	sub	sp, sp, #8
    81fc:	e50b0008 	str	r0, [fp, #-8]
    8200:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:18
    Set_Register(hal::AUX_Reg::ENABLES, Get_Register(hal::AUX_Reg::ENABLES) & ~(1 << static_cast<uint32_t>(aux_peripheral)));
    8204:	e3a01001 	mov	r1, #1
    8208:	e51b0008 	ldr	r0, [fp, #-8]
    820c:	eb00001d 	bl	8288 <_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE>
    8210:	e1a02000 	mov	r2, r0
    8214:	e51b300c 	ldr	r3, [fp, #-12]
    8218:	e3a01001 	mov	r1, #1
    821c:	e1a03311 	lsl	r3, r1, r3
    8220:	e1e03003 	mvn	r3, r3
    8224:	e0033002 	and	r3, r3, r2
    8228:	e1a02003 	mov	r2, r3
    822c:	e3a01001 	mov	r1, #1
    8230:	e51b0008 	ldr	r0, [fp, #-8]
    8234:	eb000002 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:19
}
    8238:	e320f000 	nop	{0}
    823c:	e24bd004 	sub	sp, fp, #4
    8240:	e8bd8800 	pop	{fp, pc}

00008244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>:
_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:22

void CAUX::Set_Register(hal::AUX_Reg reg_idx, uint32_t value)
{
    8244:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8248:	e28db000 	add	fp, sp, #0
    824c:	e24dd014 	sub	sp, sp, #20
    8250:	e50b0008 	str	r0, [fp, #-8]
    8254:	e50b100c 	str	r1, [fp, #-12]
    8258:	e50b2010 	str	r2, [fp, #-16]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:23
    mAUX_Reg[static_cast<unsigned int>(reg_idx)] = value;
    825c:	e51b3008 	ldr	r3, [fp, #-8]
    8260:	e5932000 	ldr	r2, [r3]
    8264:	e51b300c 	ldr	r3, [fp, #-12]
    8268:	e1a03103 	lsl	r3, r3, #2
    826c:	e0823003 	add	r3, r2, r3
    8270:	e51b2010 	ldr	r2, [fp, #-16]
    8274:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:24
}
    8278:	e320f000 	nop	{0}
    827c:	e28bd000 	add	sp, fp, #0
    8280:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8284:	e12fff1e 	bx	lr

00008288 <_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE>:
_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:27

uint32_t CAUX::Get_Register(hal::AUX_Reg reg_idx)
{
    8288:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    828c:	e28db000 	add	fp, sp, #0
    8290:	e24dd00c 	sub	sp, sp, #12
    8294:	e50b0008 	str	r0, [fp, #-8]
    8298:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:28
    return mAUX_Reg[static_cast<unsigned int>(reg_idx)];
    829c:	e51b3008 	ldr	r3, [fp, #-8]
    82a0:	e5932000 	ldr	r2, [r3]
    82a4:	e51b300c 	ldr	r3, [fp, #-12]
    82a8:	e1a03103 	lsl	r3, r3, #2
    82ac:	e0823003 	add	r3, r2, r3
    82b0:	e5933000 	ldr	r3, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:29
}
    82b4:	e1a00003 	mov	r0, r3
    82b8:	e28bd000 	add	sp, fp, #0
    82bc:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    82c0:	e12fff1e 	bx	lr

000082c4 <_Z41__static_initialization_and_destruction_0ii>:
_Z41__static_initialization_and_destruction_0ii():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:29
    82c4:	e92d4800 	push	{fp, lr}
    82c8:	e28db004 	add	fp, sp, #4
    82cc:	e24dd008 	sub	sp, sp, #8
    82d0:	e50b0008 	str	r0, [fp, #-8]
    82d4:	e50b100c 	str	r1, [fp, #-12]
    82d8:	e51b3008 	ldr	r3, [fp, #-8]
    82dc:	e3530001 	cmp	r3, #1
    82e0:	1a000006 	bne	8300 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:29 (discriminator 1)
    82e4:	e51b300c 	ldr	r3, [fp, #-12]
    82e8:	e59f201c 	ldr	r2, [pc, #28]	; 830c <_Z41__static_initialization_and_destruction_0ii+0x48>
    82ec:	e1530002 	cmp	r3, r2
    82f0:	1a000002 	bne	8300 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:3
CAUX sAUX(hal::AUX_Base);
    82f4:	e59f1014 	ldr	r1, [pc, #20]	; 8310 <_Z41__static_initialization_and_destruction_0ii+0x4c>
    82f8:	e59f0014 	ldr	r0, [pc, #20]	; 8314 <_Z41__static_initialization_and_destruction_0ii+0x50>
    82fc:	ebffff9a 	bl	816c <_ZN4CAUXC1Ej>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:29
}
    8300:	e320f000 	nop	{0}
    8304:	e24bd004 	sub	sp, fp, #4
    8308:	e8bd8800 	pop	{fp, pc}
    830c:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
    8310:	20215000 	eorcs	r5, r1, r0
    8314:	00009638 	andeq	r9, r0, r8, lsr r6

00008318 <_GLOBAL__sub_I_sAUX>:
_GLOBAL__sub_I_sAUX():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/bcm_aux.cpp:29
    8318:	e92d4800 	push	{fp, lr}
    831c:	e28db004 	add	fp, sp, #4
    8320:	e59f1008 	ldr	r1, [pc, #8]	; 8330 <_GLOBAL__sub_I_sAUX+0x18>
    8324:	e3a00001 	mov	r0, #1
    8328:	ebffffe5 	bl	82c4 <_Z41__static_initialization_and_destruction_0ii>
    832c:	e8bd8800 	pop	{fp, pc}
    8330:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>

00008334 <_ZN13CGPIO_HandlerC1Ej>:
_ZN13CGPIO_HandlerC2Ej():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:6
#include <hal/peripherals.h>
#include <drivers/gpio.h>

CGPIO_Handler sGPIO(hal::GPIO_Base);

CGPIO_Handler::CGPIO_Handler(unsigned int gpio_base_addr)
    8334:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8338:	e28db000 	add	fp, sp, #0
    833c:	e24dd00c 	sub	sp, sp, #12
    8340:	e50b0008 	str	r0, [fp, #-8]
    8344:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:7
	: mGPIO(reinterpret_cast<unsigned int*>(gpio_base_addr))
    8348:	e51b200c 	ldr	r2, [fp, #-12]
    834c:	e51b3008 	ldr	r3, [fp, #-8]
    8350:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:10
{
	//
}
    8354:	e51b3008 	ldr	r3, [fp, #-8]
    8358:	e1a00003 	mov	r0, r3
    835c:	e28bd000 	add	sp, fp, #0
    8360:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8364:	e12fff1e 	bx	lr

00008368 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_>:
_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:13

bool CGPIO_Handler::Get_GPFSEL_Location(uint32_t pin, uint32_t& reg, uint32_t& bit_idx) const
{
    8368:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    836c:	e28db000 	add	fp, sp, #0
    8370:	e24dd014 	sub	sp, sp, #20
    8374:	e50b0008 	str	r0, [fp, #-8]
    8378:	e50b100c 	str	r1, [fp, #-12]
    837c:	e50b2010 	str	r2, [fp, #-16]
    8380:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:14
	if (pin > hal::GPIO_Pin_Count)
    8384:	e51b300c 	ldr	r3, [fp, #-12]
    8388:	e3530036 	cmp	r3, #54	; 0x36
    838c:	9a000001 	bls	8398 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0x30>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:15
		return false;
    8390:	e3a03000 	mov	r3, #0
    8394:	ea000033 	b	8468 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0x100>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:17
	
	switch (pin / 10)
    8398:	e51b300c 	ldr	r3, [fp, #-12]
    839c:	e59f20d4 	ldr	r2, [pc, #212]	; 8478 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0x110>
    83a0:	e0832392 	umull	r2, r3, r2, r3
    83a4:	e1a031a3 	lsr	r3, r3, #3
    83a8:	e3530005 	cmp	r3, #5
    83ac:	979ff103 	ldrls	pc, [pc, r3, lsl #2]
    83b0:	ea00001d 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
    83b4:	000083cc 	andeq	r8, r0, ip, asr #7
    83b8:	000083dc 	ldrdeq	r8, [r0], -ip
    83bc:	000083ec 	andeq	r8, r0, ip, ror #7
    83c0:	000083fc 	strdeq	r8, [r0], -ip
    83c4:	0000840c 	andeq	r8, r0, ip, lsl #8
    83c8:	0000841c 	andeq	r8, r0, ip, lsl r4
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:19
	{
		case 0: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL0); break;
    83cc:	e51b3010 	ldr	r3, [fp, #-16]
    83d0:	e3a02000 	mov	r2, #0
    83d4:	e5832000 	str	r2, [r3]
    83d8:	ea000013 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:20
		case 1: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL1); break;
    83dc:	e51b3010 	ldr	r3, [fp, #-16]
    83e0:	e3a02001 	mov	r2, #1
    83e4:	e5832000 	str	r2, [r3]
    83e8:	ea00000f 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:21
		case 2: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL2); break;
    83ec:	e51b3010 	ldr	r3, [fp, #-16]
    83f0:	e3a02002 	mov	r2, #2
    83f4:	e5832000 	str	r2, [r3]
    83f8:	ea00000b 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:22
		case 3: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL3); break;
    83fc:	e51b3010 	ldr	r3, [fp, #-16]
    8400:	e3a02003 	mov	r2, #3
    8404:	e5832000 	str	r2, [r3]
    8408:	ea000007 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:23
		case 4: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL4); break;
    840c:	e51b3010 	ldr	r3, [fp, #-16]
    8410:	e3a02004 	mov	r2, #4
    8414:	e5832000 	str	r2, [r3]
    8418:	ea000003 	b	842c <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0xc4>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:24
		case 5: reg = static_cast<uint32_t>(hal::GPIO_Reg::GPFSEL5); break;
    841c:	e51b3010 	ldr	r3, [fp, #-16]
    8420:	e3a02005 	mov	r2, #5
    8424:	e5832000 	str	r2, [r3]
    8428:	e320f000 	nop	{0}
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:27
	}
	
	bit_idx = (pin % 10) * 3;
    842c:	e51b100c 	ldr	r1, [fp, #-12]
    8430:	e59f3040 	ldr	r3, [pc, #64]	; 8478 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_+0x110>
    8434:	e0832193 	umull	r2, r3, r3, r1
    8438:	e1a021a3 	lsr	r2, r3, #3
    843c:	e1a03002 	mov	r3, r2
    8440:	e1a03103 	lsl	r3, r3, #2
    8444:	e0833002 	add	r3, r3, r2
    8448:	e1a03083 	lsl	r3, r3, #1
    844c:	e0412003 	sub	r2, r1, r3
    8450:	e1a03002 	mov	r3, r2
    8454:	e1a03083 	lsl	r3, r3, #1
    8458:	e0832002 	add	r2, r3, r2
    845c:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8460:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:29
	
	return true;
    8464:	e3a03001 	mov	r3, #1
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:30
}
    8468:	e1a00003 	mov	r0, r3
    846c:	e28bd000 	add	sp, fp, #0
    8470:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8474:	e12fff1e 	bx	lr
    8478:	cccccccd 	stclgt	12, cr12, [ip], {205}	; 0xcd

0000847c <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_>:
_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:33

bool CGPIO_Handler::Get_GPCLR_Location(uint32_t pin, uint32_t& reg, uint32_t& bit_idx) const
{
    847c:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8480:	e28db000 	add	fp, sp, #0
    8484:	e24dd014 	sub	sp, sp, #20
    8488:	e50b0008 	str	r0, [fp, #-8]
    848c:	e50b100c 	str	r1, [fp, #-12]
    8490:	e50b2010 	str	r2, [fp, #-16]
    8494:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:34
	if (pin > hal::GPIO_Pin_Count)
    8498:	e51b300c 	ldr	r3, [fp, #-12]
    849c:	e3530036 	cmp	r3, #54	; 0x36
    84a0:	9a000001 	bls	84ac <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_+0x30>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:35
		return false;
    84a4:	e3a03000 	mov	r3, #0
    84a8:	ea00000c 	b	84e0 <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_+0x64>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:37
	
	reg = static_cast<uint32_t>((pin < 32) ? hal::GPIO_Reg::GPCLR0 : hal::GPIO_Reg::GPCLR1);
    84ac:	e51b300c 	ldr	r3, [fp, #-12]
    84b0:	e353001f 	cmp	r3, #31
    84b4:	8a000001 	bhi	84c0 <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_+0x44>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:37 (discriminator 1)
    84b8:	e3a0200a 	mov	r2, #10
    84bc:	ea000000 	b	84c4 <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_+0x48>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:37 (discriminator 2)
    84c0:	e3a0200b 	mov	r2, #11
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:37 (discriminator 4)
    84c4:	e51b3010 	ldr	r3, [fp, #-16]
    84c8:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:38 (discriminator 4)
	bit_idx = pin % 32;
    84cc:	e51b300c 	ldr	r3, [fp, #-12]
    84d0:	e203201f 	and	r2, r3, #31
    84d4:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    84d8:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:40 (discriminator 4)
	
	return true;
    84dc:	e3a03001 	mov	r3, #1
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:41
}
    84e0:	e1a00003 	mov	r0, r3
    84e4:	e28bd000 	add	sp, fp, #0
    84e8:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    84ec:	e12fff1e 	bx	lr

000084f0 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_>:
_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:44

bool CGPIO_Handler::Get_GPSET_Location(uint32_t pin, uint32_t& reg, uint32_t& bit_idx) const
{
    84f0:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    84f4:	e28db000 	add	fp, sp, #0
    84f8:	e24dd014 	sub	sp, sp, #20
    84fc:	e50b0008 	str	r0, [fp, #-8]
    8500:	e50b100c 	str	r1, [fp, #-12]
    8504:	e50b2010 	str	r2, [fp, #-16]
    8508:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:45
	if (pin > hal::GPIO_Pin_Count)
    850c:	e51b300c 	ldr	r3, [fp, #-12]
    8510:	e3530036 	cmp	r3, #54	; 0x36
    8514:	9a000001 	bls	8520 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_+0x30>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:46
		return false;
    8518:	e3a03000 	mov	r3, #0
    851c:	ea00000c 	b	8554 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_+0x64>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:48
	
	reg = static_cast<uint32_t>((pin < 32) ? hal::GPIO_Reg::GPSET0 : hal::GPIO_Reg::GPSET1);
    8520:	e51b300c 	ldr	r3, [fp, #-12]
    8524:	e353001f 	cmp	r3, #31
    8528:	8a000001 	bhi	8534 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_+0x44>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:48 (discriminator 1)
    852c:	e3a02007 	mov	r2, #7
    8530:	ea000000 	b	8538 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_+0x48>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:48 (discriminator 2)
    8534:	e3a02008 	mov	r2, #8
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:48 (discriminator 4)
    8538:	e51b3010 	ldr	r3, [fp, #-16]
    853c:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:49 (discriminator 4)
	bit_idx = pin % 32;
    8540:	e51b300c 	ldr	r3, [fp, #-12]
    8544:	e203201f 	and	r2, r3, #31
    8548:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    854c:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:51 (discriminator 4)
	
	return true;
    8550:	e3a03001 	mov	r3, #1
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:52
}
    8554:	e1a00003 	mov	r0, r3
    8558:	e28bd000 	add	sp, fp, #0
    855c:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8560:	e12fff1e 	bx	lr

00008564 <_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_>:
_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:55

bool CGPIO_Handler::Get_GPLEV_Location(uint32_t pin, uint32_t& reg, uint32_t& bit_idx) const
{
    8564:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8568:	e28db000 	add	fp, sp, #0
    856c:	e24dd014 	sub	sp, sp, #20
    8570:	e50b0008 	str	r0, [fp, #-8]
    8574:	e50b100c 	str	r1, [fp, #-12]
    8578:	e50b2010 	str	r2, [fp, #-16]
    857c:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:56
	if (pin > hal::GPIO_Pin_Count)
    8580:	e51b300c 	ldr	r3, [fp, #-12]
    8584:	e3530036 	cmp	r3, #54	; 0x36
    8588:	9a000001 	bls	8594 <_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_+0x30>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:57
		return false;
    858c:	e3a03000 	mov	r3, #0
    8590:	ea00000c 	b	85c8 <_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_+0x64>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:59
	
	reg = static_cast<uint32_t>((pin < 32) ? hal::GPIO_Reg::GPLEV0 : hal::GPIO_Reg::GPLEV1);
    8594:	e51b300c 	ldr	r3, [fp, #-12]
    8598:	e353001f 	cmp	r3, #31
    859c:	8a000001 	bhi	85a8 <_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_+0x44>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:59 (discriminator 1)
    85a0:	e3a0200d 	mov	r2, #13
    85a4:	ea000000 	b	85ac <_ZNK13CGPIO_Handler18Get_GPLEV_LocationEjRjS0_+0x48>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:59 (discriminator 2)
    85a8:	e3a0200e 	mov	r2, #14
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:59 (discriminator 4)
    85ac:	e51b3010 	ldr	r3, [fp, #-16]
    85b0:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:60 (discriminator 4)
	bit_idx = pin % 32;
    85b4:	e51b300c 	ldr	r3, [fp, #-12]
    85b8:	e203201f 	and	r2, r3, #31
    85bc:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    85c0:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:62 (discriminator 4)
	
	return true;
    85c4:	e3a03001 	mov	r3, #1
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:63
}
    85c8:	e1a00003 	mov	r0, r3
    85cc:	e28bd000 	add	sp, fp, #0
    85d0:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    85d4:	e12fff1e 	bx	lr

000085d8 <_ZN13CGPIO_Handler17Set_GPIO_FunctionEj14NGPIO_Function>:
_ZN13CGPIO_Handler17Set_GPIO_FunctionEj14NGPIO_Function():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:66
		
void CGPIO_Handler::Set_GPIO_Function(uint32_t pin, NGPIO_Function func)
{
    85d8:	e92d4800 	push	{fp, lr}
    85dc:	e28db004 	add	fp, sp, #4
    85e0:	e24dd018 	sub	sp, sp, #24
    85e4:	e50b0010 	str	r0, [fp, #-16]
    85e8:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
    85ec:	e1a03002 	mov	r3, r2
    85f0:	e54b3015 	strb	r3, [fp, #-21]	; 0xffffffeb
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:68
	uint32_t reg, bit;
	if (Get_GPFSEL_Location(pin, reg, bit))
    85f4:	e24b300c 	sub	r3, fp, #12
    85f8:	e24b2008 	sub	r2, fp, #8
    85fc:	e51b1014 	ldr	r1, [fp, #-20]	; 0xffffffec
    8600:	e51b0010 	ldr	r0, [fp, #-16]
    8604:	ebffff57 	bl	8368 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_>
    8608:	e1a03000 	mov	r3, r0
    860c:	e3530000 	cmp	r3, #0
    8610:	1a000015 	bne	866c <_ZN13CGPIO_Handler17Set_GPIO_FunctionEj14NGPIO_Function+0x94>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:71
		return;
	
	mGPIO[reg] = (mGPIO[reg] & (~static_cast<unsigned int>(7 << bit)) )
    8614:	e51b3010 	ldr	r3, [fp, #-16]
    8618:	e5932000 	ldr	r2, [r3]
    861c:	e51b3008 	ldr	r3, [fp, #-8]
    8620:	e1a03103 	lsl	r3, r3, #2
    8624:	e0823003 	add	r3, r2, r3
    8628:	e5932000 	ldr	r2, [r3]
    862c:	e51b300c 	ldr	r3, [fp, #-12]
    8630:	e3a01007 	mov	r1, #7
    8634:	e1a03311 	lsl	r3, r1, r3
    8638:	e1e03003 	mvn	r3, r3
    863c:	e0021003 	and	r1, r2, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:72
				| (static_cast<unsigned int>(func) << bit);
    8640:	e55b2015 	ldrb	r2, [fp, #-21]	; 0xffffffeb
    8644:	e51b300c 	ldr	r3, [fp, #-12]
    8648:	e1a02312 	lsl	r2, r2, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:71
	mGPIO[reg] = (mGPIO[reg] & (~static_cast<unsigned int>(7 << bit)) )
    864c:	e51b3010 	ldr	r3, [fp, #-16]
    8650:	e5930000 	ldr	r0, [r3]
    8654:	e51b3008 	ldr	r3, [fp, #-8]
    8658:	e1a03103 	lsl	r3, r3, #2
    865c:	e0803003 	add	r3, r0, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:72
				| (static_cast<unsigned int>(func) << bit);
    8660:	e1812002 	orr	r2, r1, r2
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:71
	mGPIO[reg] = (mGPIO[reg] & (~static_cast<unsigned int>(7 << bit)) )
    8664:	e5832000 	str	r2, [r3]
    8668:	ea000000 	b	8670 <_ZN13CGPIO_Handler17Set_GPIO_FunctionEj14NGPIO_Function+0x98>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:69
		return;
    866c:	e320f000 	nop	{0}
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:73
}
    8670:	e24bd004 	sub	sp, fp, #4
    8674:	e8bd8800 	pop	{fp, pc}

00008678 <_ZNK13CGPIO_Handler17Get_GPIO_FunctionEj>:
_ZNK13CGPIO_Handler17Get_GPIO_FunctionEj():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:76

NGPIO_Function CGPIO_Handler::Get_GPIO_Function(uint32_t pin) const
{
    8678:	e92d4800 	push	{fp, lr}
    867c:	e28db004 	add	fp, sp, #4
    8680:	e24dd010 	sub	sp, sp, #16
    8684:	e50b0010 	str	r0, [fp, #-16]
    8688:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:78
	uint32_t reg, bit;
	if (Get_GPFSEL_Location(pin, reg, bit))
    868c:	e24b300c 	sub	r3, fp, #12
    8690:	e24b2008 	sub	r2, fp, #8
    8694:	e51b1014 	ldr	r1, [fp, #-20]	; 0xffffffec
    8698:	e51b0010 	ldr	r0, [fp, #-16]
    869c:	ebffff31 	bl	8368 <_ZNK13CGPIO_Handler19Get_GPFSEL_LocationEjRjS0_>
    86a0:	e1a03000 	mov	r3, r0
    86a4:	e3530000 	cmp	r3, #0
    86a8:	0a000001 	beq	86b4 <_ZNK13CGPIO_Handler17Get_GPIO_FunctionEj+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:79
		return NGPIO_Function::Unspecified;
    86ac:	e3a03008 	mov	r3, #8
    86b0:	ea00000a 	b	86e0 <_ZNK13CGPIO_Handler17Get_GPIO_FunctionEj+0x68>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:81
	
	return static_cast<NGPIO_Function>((mGPIO[reg] >> bit) & 7);
    86b4:	e51b3010 	ldr	r3, [fp, #-16]
    86b8:	e5932000 	ldr	r2, [r3]
    86bc:	e51b3008 	ldr	r3, [fp, #-8]
    86c0:	e1a03103 	lsl	r3, r3, #2
    86c4:	e0823003 	add	r3, r2, r3
    86c8:	e5932000 	ldr	r2, [r3]
    86cc:	e51b300c 	ldr	r3, [fp, #-12]
    86d0:	e1a03332 	lsr	r3, r2, r3
    86d4:	e6ef3073 	uxtb	r3, r3
    86d8:	e2033007 	and	r3, r3, #7
    86dc:	e6ef3073 	uxtb	r3, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:82 (discriminator 1)
}
    86e0:	e1a00003 	mov	r0, r3
    86e4:	e24bd004 	sub	sp, fp, #4
    86e8:	e8bd8800 	pop	{fp, pc}

000086ec <_ZN13CGPIO_Handler10Set_OutputEjb>:
_ZN13CGPIO_Handler10Set_OutputEjb():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:85

void CGPIO_Handler::Set_Output(uint32_t pin, bool set)
{
    86ec:	e92d4800 	push	{fp, lr}
    86f0:	e28db004 	add	fp, sp, #4
    86f4:	e24dd018 	sub	sp, sp, #24
    86f8:	e50b0010 	str	r0, [fp, #-16]
    86fc:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
    8700:	e1a03002 	mov	r3, r2
    8704:	e54b3015 	strb	r3, [fp, #-21]	; 0xffffffeb
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87
	uint32_t reg, bit;
	if (!(set && Get_GPSET_Location(pin, reg, bit)) && !(!set && Get_GPCLR_Location(pin, reg, bit)))
    8708:	e55b3015 	ldrb	r3, [fp, #-21]	; 0xffffffeb
    870c:	e2233001 	eor	r3, r3, #1
    8710:	e6ef3073 	uxtb	r3, r3
    8714:	e3530000 	cmp	r3, #0
    8718:	1a000009 	bne	8744 <_ZN13CGPIO_Handler10Set_OutputEjb+0x58>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 2)
    871c:	e24b300c 	sub	r3, fp, #12
    8720:	e24b2008 	sub	r2, fp, #8
    8724:	e51b1014 	ldr	r1, [fp, #-20]	; 0xffffffec
    8728:	e51b0010 	ldr	r0, [fp, #-16]
    872c:	ebffff6f 	bl	84f0 <_ZNK13CGPIO_Handler18Get_GPSET_LocationEjRjS0_>
    8730:	e1a03000 	mov	r3, r0
    8734:	e2233001 	eor	r3, r3, #1
    8738:	e6ef3073 	uxtb	r3, r3
    873c:	e3530000 	cmp	r3, #0
    8740:	0a00000e 	beq	8780 <_ZN13CGPIO_Handler10Set_OutputEjb+0x94>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 3)
    8744:	e55b3015 	ldrb	r3, [fp, #-21]	; 0xffffffeb
    8748:	e3530000 	cmp	r3, #0
    874c:	1a000009 	bne	8778 <_ZN13CGPIO_Handler10Set_OutputEjb+0x8c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 6)
    8750:	e24b300c 	sub	r3, fp, #12
    8754:	e24b2008 	sub	r2, fp, #8
    8758:	e51b1014 	ldr	r1, [fp, #-20]	; 0xffffffec
    875c:	e51b0010 	ldr	r0, [fp, #-16]
    8760:	ebffff45 	bl	847c <_ZNK13CGPIO_Handler18Get_GPCLR_LocationEjRjS0_>
    8764:	e1a03000 	mov	r3, r0
    8768:	e2233001 	eor	r3, r3, #1
    876c:	e6ef3073 	uxtb	r3, r3
    8770:	e3530000 	cmp	r3, #0
    8774:	0a000001 	beq	8780 <_ZN13CGPIO_Handler10Set_OutputEjb+0x94>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 7)
    8778:	e3a03001 	mov	r3, #1
    877c:	ea000000 	b	8784 <_ZN13CGPIO_Handler10Set_OutputEjb+0x98>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 8)
    8780:	e3a03000 	mov	r3, #0
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:87 (discriminator 10)
    8784:	e3530000 	cmp	r3, #0
    8788:	1a00000a 	bne	87b8 <_ZN13CGPIO_Handler10Set_OutputEjb+0xcc>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:90
		return;
	
	mGPIO[reg] = (1 << bit);
    878c:	e51b300c 	ldr	r3, [fp, #-12]
    8790:	e3a02001 	mov	r2, #1
    8794:	e1a01312 	lsl	r1, r2, r3
    8798:	e51b3010 	ldr	r3, [fp, #-16]
    879c:	e5932000 	ldr	r2, [r3]
    87a0:	e51b3008 	ldr	r3, [fp, #-8]
    87a4:	e1a03103 	lsl	r3, r3, #2
    87a8:	e0823003 	add	r3, r2, r3
    87ac:	e1a02001 	mov	r2, r1
    87b0:	e5832000 	str	r2, [r3]
    87b4:	ea000000 	b	87bc <_ZN13CGPIO_Handler10Set_OutputEjb+0xd0>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:88
		return;
    87b8:	e320f000 	nop	{0}
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:91
}
    87bc:	e24bd004 	sub	sp, fp, #4
    87c0:	e8bd8800 	pop	{fp, pc}

000087c4 <_Z41__static_initialization_and_destruction_0ii>:
_Z41__static_initialization_and_destruction_0ii():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:91
    87c4:	e92d4800 	push	{fp, lr}
    87c8:	e28db004 	add	fp, sp, #4
    87cc:	e24dd008 	sub	sp, sp, #8
    87d0:	e50b0008 	str	r0, [fp, #-8]
    87d4:	e50b100c 	str	r1, [fp, #-12]
    87d8:	e51b3008 	ldr	r3, [fp, #-8]
    87dc:	e3530001 	cmp	r3, #1
    87e0:	1a000006 	bne	8800 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:91 (discriminator 1)
    87e4:	e51b300c 	ldr	r3, [fp, #-12]
    87e8:	e59f201c 	ldr	r2, [pc, #28]	; 880c <_Z41__static_initialization_and_destruction_0ii+0x48>
    87ec:	e1530002 	cmp	r3, r2
    87f0:	1a000002 	bne	8800 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:4
CGPIO_Handler sGPIO(hal::GPIO_Base);
    87f4:	e59f1014 	ldr	r1, [pc, #20]	; 8810 <_Z41__static_initialization_and_destruction_0ii+0x4c>
    87f8:	e59f0014 	ldr	r0, [pc, #20]	; 8814 <_Z41__static_initialization_and_destruction_0ii+0x50>
    87fc:	ebfffecc 	bl	8334 <_ZN13CGPIO_HandlerC1Ej>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:91
}
    8800:	e320f000 	nop	{0}
    8804:	e24bd004 	sub	sp, fp, #4
    8808:	e8bd8800 	pop	{fp, pc}
    880c:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
    8810:	20200000 	eorcs	r0, r0, r0
    8814:	0000963c 	andeq	r9, r0, ip, lsr r6

00008818 <_GLOBAL__sub_I_sGPIO>:
_GLOBAL__sub_I_sGPIO():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/gpio.cpp:91
    8818:	e92d4800 	push	{fp, lr}
    881c:	e28db004 	add	fp, sp, #4
    8820:	e59f1008 	ldr	r1, [pc, #8]	; 8830 <_GLOBAL__sub_I_sGPIO+0x18>
    8824:	e3a00001 	mov	r0, #1
    8828:	ebffffe5 	bl	87c4 <_Z41__static_initialization_and_destruction_0ii>
    882c:	e8bd8800 	pop	{fp, pc}
    8830:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>

00008834 <_ZN6CTimerC1Em>:
_ZN6CTimerC2Em():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:28
    uint16_t unused_4 : 10;
};

#pragma pack(pop)

CTimer::CTimer(unsigned long timer_reg_base)
    8834:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8838:	e28db000 	add	fp, sp, #0
    883c:	e24dd00c 	sub	sp, sp, #12
    8840:	e50b0008 	str	r0, [fp, #-8]
    8844:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:29
    : mTimer_Regs(reinterpret_cast<unsigned int*>(timer_reg_base)), mCallback(nullptr)
    8848:	e51b200c 	ldr	r2, [fp, #-12]
    884c:	e51b3008 	ldr	r3, [fp, #-8]
    8850:	e5832000 	str	r2, [r3]
    8854:	e51b3008 	ldr	r3, [fp, #-8]
    8858:	e3a02000 	mov	r2, #0
    885c:	e5832004 	str	r2, [r3, #4]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:32
{
    //
}
    8860:	e51b3008 	ldr	r3, [fp, #-8]
    8864:	e1a00003 	mov	r0, r3
    8868:	e28bd000 	add	sp, fp, #0
    886c:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8870:	e12fff1e 	bx	lr

00008874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>:
_ZN6CTimer4RegsEN3hal9Timer_RegE():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:35

volatile unsigned int& CTimer::Regs(hal::Timer_Reg reg)
{
    8874:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8878:	e28db000 	add	fp, sp, #0
    887c:	e24dd00c 	sub	sp, sp, #12
    8880:	e50b0008 	str	r0, [fp, #-8]
    8884:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:36
    return mTimer_Regs[static_cast<unsigned int>(reg)];
    8888:	e51b3008 	ldr	r3, [fp, #-8]
    888c:	e5932000 	ldr	r2, [r3]
    8890:	e51b300c 	ldr	r3, [fp, #-12]
    8894:	e1a03103 	lsl	r3, r3, #2
    8898:	e0823003 	add	r3, r2, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:37
}
    889c:	e1a00003 	mov	r0, r3
    88a0:	e28bd000 	add	sp, fp, #0
    88a4:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    88a8:	e12fff1e 	bx	lr

000088ac <_ZN6CTimer6EnableEPFvvEj16NTimer_Prescaler>:
_ZN6CTimer6EnableEPFvvEj16NTimer_Prescaler():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:40

void CTimer::Enable(TTimer_Callback callback, unsigned int delay, NTimer_Prescaler prescaler)
{
    88ac:	e92d4810 	push	{r4, fp, lr}
    88b0:	e28db008 	add	fp, sp, #8
    88b4:	e24dd01c 	sub	sp, sp, #28
    88b8:	e50b0018 	str	r0, [fp, #-24]	; 0xffffffe8
    88bc:	e50b101c 	str	r1, [fp, #-28]	; 0xffffffe4
    88c0:	e50b2020 	str	r2, [fp, #-32]	; 0xffffffe0
    88c4:	e54b3021 	strb	r3, [fp, #-33]	; 0xffffffdf
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:41
    Regs(hal::Timer_Reg::Load) = delay;
    88c8:	e3a01000 	mov	r1, #0
    88cc:	e51b0018 	ldr	r0, [fp, #-24]	; 0xffffffe8
    88d0:	ebffffe7 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    88d4:	e1a02000 	mov	r2, r0
    88d8:	e51b3020 	ldr	r3, [fp, #-32]	; 0xffffffe0
    88dc:	e5823000 	str	r3, [r2]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:44

    TTimer_Ctl_Flags reg;
    reg.counter_32b = 1;
    88e0:	e55b3014 	ldrb	r3, [fp, #-20]	; 0xffffffec
    88e4:	e3833002 	orr	r3, r3, #2
    88e8:	e54b3014 	strb	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:45
    reg.interrupt_enabled = 1;
    88ec:	e55b3014 	ldrb	r3, [fp, #-20]	; 0xffffffec
    88f0:	e3833020 	orr	r3, r3, #32
    88f4:	e54b3014 	strb	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:46
    reg.timer_enabled = 1;
    88f8:	e55b3014 	ldrb	r3, [fp, #-20]	; 0xffffffec
    88fc:	e3833080 	orr	r3, r3, #128	; 0x80
    8900:	e54b3014 	strb	r3, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:47
    reg.prescaler = static_cast<uint8_t>(prescaler);
    8904:	e55b3021 	ldrb	r3, [fp, #-33]	; 0xffffffdf
    8908:	e2033003 	and	r3, r3, #3
    890c:	e6ef3073 	uxtb	r3, r3
    8910:	e55b2014 	ldrb	r2, [fp, #-20]	; 0xffffffec
    8914:	e2033003 	and	r3, r3, #3
    8918:	e3c2200c 	bic	r2, r2, #12
    891c:	e1a03103 	lsl	r3, r3, #2
    8920:	e1833002 	orr	r3, r3, r2
    8924:	e1a02003 	mov	r2, r3
    8928:	e54b2014 	strb	r2, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:49

    Regs(hal::Timer_Reg::Control) = *reinterpret_cast<unsigned int*>(&reg);
    892c:	e24b4014 	sub	r4, fp, #20
    8930:	e3a01002 	mov	r1, #2
    8934:	e51b0018 	ldr	r0, [fp, #-24]	; 0xffffffe8
    8938:	ebffffcd 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    893c:	e1a02000 	mov	r2, r0
    8940:	e5943000 	ldr	r3, [r4]
    8944:	e5823000 	str	r3, [r2]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:51
   
    Regs(hal::Timer_Reg::IRQ_Clear) = 1;
    8948:	e3a01003 	mov	r1, #3
    894c:	e51b0018 	ldr	r0, [fp, #-24]	; 0xffffffe8
    8950:	ebffffc7 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    8954:	e1a03000 	mov	r3, r0
    8958:	e3a02001 	mov	r2, #1
    895c:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:53

    mCallback = callback;
    8960:	e51b3018 	ldr	r3, [fp, #-24]	; 0xffffffe8
    8964:	e51b201c 	ldr	r2, [fp, #-28]	; 0xffffffe4
    8968:	e5832004 	str	r2, [r3, #4]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:54
}
    896c:	e320f000 	nop	{0}
    8970:	e24bd008 	sub	sp, fp, #8
    8974:	e8bd8810 	pop	{r4, fp, pc}

00008978 <_ZN6CTimer7DisableEv>:
_ZN6CTimer7DisableEv():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:57

void CTimer::Disable()
{
    8978:	e92d4800 	push	{fp, lr}
    897c:	e28db004 	add	fp, sp, #4
    8980:	e24dd010 	sub	sp, sp, #16
    8984:	e50b0010 	str	r0, [fp, #-16]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:58
    volatile TTimer_Ctl_Flags& reg = reinterpret_cast<volatile TTimer_Ctl_Flags&>(Regs(hal::Timer_Reg::Control));
    8988:	e3a01002 	mov	r1, #2
    898c:	e51b0010 	ldr	r0, [fp, #-16]
    8990:	ebffffb7 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    8994:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:60

    reg.interrupt_enabled = 0;
    8998:	e51b2008 	ldr	r2, [fp, #-8]
    899c:	e5d23000 	ldrb	r3, [r2]
    89a0:	e3c33020 	bic	r3, r3, #32
    89a4:	e5c23000 	strb	r3, [r2]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:61
    reg.timer_enabled = 0;
    89a8:	e51b2008 	ldr	r2, [fp, #-8]
    89ac:	e5d23000 	ldrb	r3, [r2]
    89b0:	e3c33080 	bic	r3, r3, #128	; 0x80
    89b4:	e5c23000 	strb	r3, [r2]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:62
}
    89b8:	e320f000 	nop	{0}
    89bc:	e24bd004 	sub	sp, fp, #4
    89c0:	e8bd8800 	pop	{fp, pc}

000089c4 <_ZN6CTimer12IRQ_CallbackEv>:
_ZN6CTimer12IRQ_CallbackEv():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:65

void CTimer::IRQ_Callback()
{
    89c4:	e92d4800 	push	{fp, lr}
    89c8:	e28db004 	add	fp, sp, #4
    89cc:	e24dd008 	sub	sp, sp, #8
    89d0:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:66
    Regs(hal::Timer_Reg::IRQ_Clear) = 1;
    89d4:	e3a01003 	mov	r1, #3
    89d8:	e51b0008 	ldr	r0, [fp, #-8]
    89dc:	ebffffa4 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    89e0:	e1a03000 	mov	r3, r0
    89e4:	e3a02001 	mov	r2, #1
    89e8:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:68

    if (mCallback)
    89ec:	e51b3008 	ldr	r3, [fp, #-8]
    89f0:	e5933004 	ldr	r3, [r3, #4]
    89f4:	e3530000 	cmp	r3, #0
    89f8:	0a000002 	beq	8a08 <_ZN6CTimer12IRQ_CallbackEv+0x44>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:69
        mCallback();
    89fc:	e51b3008 	ldr	r3, [fp, #-8]
    8a00:	e5933004 	ldr	r3, [r3, #4]
    8a04:	e12fff33 	blx	r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:70
}
    8a08:	e320f000 	nop	{0}
    8a0c:	e24bd004 	sub	sp, fp, #4
    8a10:	e8bd8800 	pop	{fp, pc}

00008a14 <_ZN6CTimer20Is_Timer_IRQ_PendingEv>:
_ZN6CTimer20Is_Timer_IRQ_PendingEv():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:73

bool CTimer::Is_Timer_IRQ_Pending()
{
    8a14:	e92d4800 	push	{fp, lr}
    8a18:	e28db004 	add	fp, sp, #4
    8a1c:	e24dd008 	sub	sp, sp, #8
    8a20:	e50b0008 	str	r0, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:74
    return Regs(hal::Timer_Reg::IRQ_Masked);
    8a24:	e3a01005 	mov	r1, #5
    8a28:	e51b0008 	ldr	r0, [fp, #-8]
    8a2c:	ebffff90 	bl	8874 <_ZN6CTimer4RegsEN3hal9Timer_RegE>
    8a30:	e1a03000 	mov	r3, r0
    8a34:	e5933000 	ldr	r3, [r3]
    8a38:	e3530000 	cmp	r3, #0
    8a3c:	13a03001 	movne	r3, #1
    8a40:	03a03000 	moveq	r3, #0
    8a44:	e6ef3073 	uxtb	r3, r3
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:75
}
    8a48:	e1a00003 	mov	r0, r3
    8a4c:	e24bd004 	sub	sp, fp, #4
    8a50:	e8bd8800 	pop	{fp, pc}

00008a54 <_Z41__static_initialization_and_destruction_0ii>:
_Z41__static_initialization_and_destruction_0ii():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:75
    8a54:	e92d4800 	push	{fp, lr}
    8a58:	e28db004 	add	fp, sp, #4
    8a5c:	e24dd008 	sub	sp, sp, #8
    8a60:	e50b0008 	str	r0, [fp, #-8]
    8a64:	e50b100c 	str	r1, [fp, #-12]
    8a68:	e51b3008 	ldr	r3, [fp, #-8]
    8a6c:	e3530001 	cmp	r3, #1
    8a70:	1a000006 	bne	8a90 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:75 (discriminator 1)
    8a74:	e51b300c 	ldr	r3, [fp, #-12]
    8a78:	e59f201c 	ldr	r2, [pc, #28]	; 8a9c <_Z41__static_initialization_and_destruction_0ii+0x48>
    8a7c:	e1530002 	cmp	r3, r2
    8a80:	1a000002 	bne	8a90 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:6
CTimer sTimer(hal::Timer_Base);
    8a84:	e59f1014 	ldr	r1, [pc, #20]	; 8aa0 <_Z41__static_initialization_and_destruction_0ii+0x4c>
    8a88:	e59f0014 	ldr	r0, [pc, #20]	; 8aa4 <_Z41__static_initialization_and_destruction_0ii+0x50>
    8a8c:	ebffff68 	bl	8834 <_ZN6CTimerC1Em>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:75
}
    8a90:	e320f000 	nop	{0}
    8a94:	e24bd004 	sub	sp, fp, #4
    8a98:	e8bd8800 	pop	{fp, pc}
    8a9c:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
    8aa0:	2000b400 	andcs	fp, r0, r0, lsl #8
    8aa4:	00009640 	andeq	r9, r0, r0, asr #12

00008aa8 <_GLOBAL__sub_I_sTimer>:
_GLOBAL__sub_I_sTimer():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/timer.cpp:75
    8aa8:	e92d4800 	push	{fp, lr}
    8aac:	e28db004 	add	fp, sp, #4
    8ab0:	e59f1008 	ldr	r1, [pc, #8]	; 8ac0 <_GLOBAL__sub_I_sTimer+0x18>
    8ab4:	e3a00001 	mov	r0, #1
    8ab8:	ebffffe5 	bl	8a54 <_Z41__static_initialization_and_destruction_0ii>
    8abc:	e8bd8800 	pop	{fp, pc}
    8ac0:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>

00008ac4 <_ZN5CUARTC1ER4CAUX>:
_ZN5CUARTC2ER4CAUX():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:6
#include <drivers/uart.h>
#include <drivers/bcm_aux.h>

CUART sUART0(sAUX);

CUART::CUART(CAUX& aux)
    8ac4:	e92d4800 	push	{fp, lr}
    8ac8:	e28db004 	add	fp, sp, #4
    8acc:	e24dd008 	sub	sp, sp, #8
    8ad0:	e50b0008 	str	r0, [fp, #-8]
    8ad4:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:7
    : mAUX(aux)
    8ad8:	e51b3008 	ldr	r3, [fp, #-8]
    8adc:	e51b200c 	ldr	r2, [fp, #-12]
    8ae0:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:9
{
    mAUX.Enable(hal::AUX_Peripherals::MiniUART);
    8ae4:	e51b3008 	ldr	r3, [fp, #-8]
    8ae8:	e5933000 	ldr	r3, [r3]
    8aec:	e3a01000 	mov	r1, #0
    8af0:	e1a00003 	mov	r0, r3
    8af4:	ebfffda9 	bl	81a0 <_ZN4CAUX6EnableEN3hal15AUX_PeripheralsE>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:10
    mAUX.Set_Register(hal::AUX_Reg::MU_IIR, 0);
    8af8:	e51b3008 	ldr	r3, [fp, #-8]
    8afc:	e5933000 	ldr	r3, [r3]
    8b00:	e3a02000 	mov	r2, #0
    8b04:	e3a01012 	mov	r1, #18
    8b08:	e1a00003 	mov	r0, r3
    8b0c:	ebfffdcc 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:11
    mAUX.Set_Register(hal::AUX_Reg::MU_IER, 0);
    8b10:	e51b3008 	ldr	r3, [fp, #-8]
    8b14:	e5933000 	ldr	r3, [r3]
    8b18:	e3a02000 	mov	r2, #0
    8b1c:	e3a01011 	mov	r1, #17
    8b20:	e1a00003 	mov	r0, r3
    8b24:	ebfffdc6 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:12
    mAUX.Set_Register(hal::AUX_Reg::MU_MCR, 0);
    8b28:	e51b3008 	ldr	r3, [fp, #-8]
    8b2c:	e5933000 	ldr	r3, [r3]
    8b30:	e3a02000 	mov	r2, #0
    8b34:	e3a01014 	mov	r1, #20
    8b38:	e1a00003 	mov	r0, r3
    8b3c:	ebfffdc0 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:13
    mAUX.Set_Register(hal::AUX_Reg::MU_CNTL, 3); // RX and TX enabled
    8b40:	e51b3008 	ldr	r3, [fp, #-8]
    8b44:	e5933000 	ldr	r3, [r3]
    8b48:	e3a02003 	mov	r2, #3
    8b4c:	e3a01018 	mov	r1, #24
    8b50:	e1a00003 	mov	r0, r3
    8b54:	ebfffdba 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:14
}
    8b58:	e51b3008 	ldr	r3, [fp, #-8]
    8b5c:	e1a00003 	mov	r0, r3
    8b60:	e24bd004 	sub	sp, fp, #4
    8b64:	e8bd8800 	pop	{fp, pc}

00008b68 <_ZN5CUART15Set_Char_LengthE17NUART_Char_Length>:
_ZN5CUART15Set_Char_LengthE17NUART_Char_Length():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:17

void CUART::Set_Char_Length(NUART_Char_Length len)
{
    8b68:	e92d4810 	push	{r4, fp, lr}
    8b6c:	e28db008 	add	fp, sp, #8
    8b70:	e24dd00c 	sub	sp, sp, #12
    8b74:	e50b0010 	str	r0, [fp, #-16]
    8b78:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:18
    mAUX.Set_Register(hal::AUX_Reg::MU_LCR, (mAUX.Get_Register(hal::AUX_Reg::MU_LCR) & 0xFFFFFFFE) | static_cast<unsigned int>(len));
    8b7c:	e51b3010 	ldr	r3, [fp, #-16]
    8b80:	e5934000 	ldr	r4, [r3]
    8b84:	e51b3010 	ldr	r3, [fp, #-16]
    8b88:	e5933000 	ldr	r3, [r3]
    8b8c:	e3a01013 	mov	r1, #19
    8b90:	e1a00003 	mov	r0, r3
    8b94:	ebfffdbb 	bl	8288 <_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE>
    8b98:	e1a03000 	mov	r3, r0
    8b9c:	e3c32001 	bic	r2, r3, #1
    8ba0:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8ba4:	e1823003 	orr	r3, r2, r3
    8ba8:	e1a02003 	mov	r2, r3
    8bac:	e3a01013 	mov	r1, #19
    8bb0:	e1a00004 	mov	r0, r4
    8bb4:	ebfffda2 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:19
}
    8bb8:	e320f000 	nop	{0}
    8bbc:	e24bd008 	sub	sp, fp, #8
    8bc0:	e8bd8810 	pop	{r4, fp, pc}

00008bc4 <_ZN5CUART13Set_Baud_RateE15NUART_Baud_Rate>:
_ZN5CUART13Set_Baud_RateE15NUART_Baud_Rate():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:22

void CUART::Set_Baud_Rate(NUART_Baud_Rate rate)
{
    8bc4:	e92d4800 	push	{fp, lr}
    8bc8:	e28db004 	add	fp, sp, #4
    8bcc:	e24dd010 	sub	sp, sp, #16
    8bd0:	e50b0010 	str	r0, [fp, #-16]
    8bd4:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:23
    const unsigned int val = ((hal::Default_Clock_Rate / static_cast<unsigned int>(rate)) / 8) - 1;
    8bd8:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8bdc:	e1a01003 	mov	r1, r3
    8be0:	e59f0060 	ldr	r0, [pc, #96]	; 8c48 <_ZN5CUART13Set_Baud_RateE15NUART_Baud_Rate+0x84>
    8be4:	eb000193 	bl	9238 <__udivsi3>
    8be8:	e1a03000 	mov	r3, r0
    8bec:	e2433001 	sub	r3, r3, #1
    8bf0:	e50b3008 	str	r3, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:25

    mAUX.Set_Register(hal::AUX_Reg::MU_CNTL, 0);
    8bf4:	e51b3010 	ldr	r3, [fp, #-16]
    8bf8:	e5933000 	ldr	r3, [r3]
    8bfc:	e3a02000 	mov	r2, #0
    8c00:	e3a01018 	mov	r1, #24
    8c04:	e1a00003 	mov	r0, r3
    8c08:	ebfffd8d 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:27

    mAUX.Set_Register(hal::AUX_Reg::MU_BAUD, val);
    8c0c:	e51b3010 	ldr	r3, [fp, #-16]
    8c10:	e5933000 	ldr	r3, [r3]
    8c14:	e51b2008 	ldr	r2, [fp, #-8]
    8c18:	e3a0101a 	mov	r1, #26
    8c1c:	e1a00003 	mov	r0, r3
    8c20:	ebfffd87 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:29

    mAUX.Set_Register(hal::AUX_Reg::MU_CNTL, 3);
    8c24:	e51b3010 	ldr	r3, [fp, #-16]
    8c28:	e5933000 	ldr	r3, [r3]
    8c2c:	e3a02003 	mov	r2, #3
    8c30:	e3a01018 	mov	r1, #24
    8c34:	e1a00003 	mov	r0, r3
    8c38:	ebfffd81 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:30
}
    8c3c:	e320f000 	nop	{0}
    8c40:	e24bd004 	sub	sp, fp, #4
    8c44:	e8bd8800 	pop	{fp, pc}
    8c48:	01dcd650 	bicseq	sp, ip, r0, asr r6

00008c4c <_ZN5CUART5WriteEc>:
_ZN5CUART5WriteEc():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:33

void CUART::Write(char c)
{
    8c4c:	e92d4800 	push	{fp, lr}
    8c50:	e28db004 	add	fp, sp, #4
    8c54:	e24dd008 	sub	sp, sp, #8
    8c58:	e50b0008 	str	r0, [fp, #-8]
    8c5c:	e1a03001 	mov	r3, r1
    8c60:	e54b3009 	strb	r3, [fp, #-9]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:35
    // dokud ma status registr priznak "vystupni fronta plna", nelze prenaset dalsi bit
    while (!(mAUX.Get_Register(hal::AUX_Reg::MU_LSR) & (1 << 5)))
    8c64:	e51b3008 	ldr	r3, [fp, #-8]
    8c68:	e5933000 	ldr	r3, [r3]
    8c6c:	e3a01015 	mov	r1, #21
    8c70:	e1a00003 	mov	r0, r3
    8c74:	ebfffd83 	bl	8288 <_ZN4CAUX12Get_RegisterEN3hal7AUX_RegE>
    8c78:	e1a03000 	mov	r3, r0
    8c7c:	e2033020 	and	r3, r3, #32
    8c80:	e3530000 	cmp	r3, #0
    8c84:	03a03001 	moveq	r3, #1
    8c88:	13a03000 	movne	r3, #0
    8c8c:	e6ef3073 	uxtb	r3, r3
    8c90:	e3530000 	cmp	r3, #0
    8c94:	0a000000 	beq	8c9c <_ZN5CUART5WriteEc+0x50>
    8c98:	eafffff1 	b	8c64 <_ZN5CUART5WriteEc+0x18>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:38
        ;

    mAUX.Set_Register(hal::AUX_Reg::MU_IO, c);
    8c9c:	e51b3008 	ldr	r3, [fp, #-8]
    8ca0:	e5933000 	ldr	r3, [r3]
    8ca4:	e55b2009 	ldrb	r2, [fp, #-9]
    8ca8:	e3a01010 	mov	r1, #16
    8cac:	e1a00003 	mov	r0, r3
    8cb0:	ebfffd63 	bl	8244 <_ZN4CAUX12Set_RegisterEN3hal7AUX_RegEj>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:39
}
    8cb4:	e320f000 	nop	{0}
    8cb8:	e24bd004 	sub	sp, fp, #4
    8cbc:	e8bd8800 	pop	{fp, pc}

00008cc0 <_ZN5CUART5WriteEPKc>:
_ZN5CUART5WriteEPKc():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:42

void CUART::Write(const char* str)
{
    8cc0:	e92d4800 	push	{fp, lr}
    8cc4:	e28db004 	add	fp, sp, #4
    8cc8:	e24dd010 	sub	sp, sp, #16
    8ccc:	e50b0010 	str	r0, [fp, #-16]
    8cd0:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:45
    int i;

    for (i = 0; str[i] != '\0'; i++)
    8cd4:	e3a03000 	mov	r3, #0
    8cd8:	e50b3008 	str	r3, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:45 (discriminator 3)
    8cdc:	e51b3008 	ldr	r3, [fp, #-8]
    8ce0:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    8ce4:	e0823003 	add	r3, r2, r3
    8ce8:	e5d33000 	ldrb	r3, [r3]
    8cec:	e3530000 	cmp	r3, #0
    8cf0:	0a00000a 	beq	8d20 <_ZN5CUART5WriteEPKc+0x60>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:46 (discriminator 2)
        Write(str[i]);
    8cf4:	e51b3008 	ldr	r3, [fp, #-8]
    8cf8:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    8cfc:	e0823003 	add	r3, r2, r3
    8d00:	e5d33000 	ldrb	r3, [r3]
    8d04:	e1a01003 	mov	r1, r3
    8d08:	e51b0010 	ldr	r0, [fp, #-16]
    8d0c:	ebffffce 	bl	8c4c <_ZN5CUART5WriteEc>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:45 (discriminator 2)
    for (i = 0; str[i] != '\0'; i++)
    8d10:	e51b3008 	ldr	r3, [fp, #-8]
    8d14:	e2833001 	add	r3, r3, #1
    8d18:	e50b3008 	str	r3, [fp, #-8]
    8d1c:	eaffffee 	b	8cdc <_ZN5CUART5WriteEPKc+0x1c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:47
    8d20:	e320f000 	nop	{0}
    8d24:	e24bd004 	sub	sp, fp, #4
    8d28:	e8bd8800 	pop	{fp, pc}

00008d2c <_Z41__static_initialization_and_destruction_0ii>:
_Z41__static_initialization_and_destruction_0ii():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:47
    8d2c:	e92d4800 	push	{fp, lr}
    8d30:	e28db004 	add	fp, sp, #4
    8d34:	e24dd008 	sub	sp, sp, #8
    8d38:	e50b0008 	str	r0, [fp, #-8]
    8d3c:	e50b100c 	str	r1, [fp, #-12]
    8d40:	e51b3008 	ldr	r3, [fp, #-8]
    8d44:	e3530001 	cmp	r3, #1
    8d48:	1a000006 	bne	8d68 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:47 (discriminator 1)
    8d4c:	e51b300c 	ldr	r3, [fp, #-12]
    8d50:	e59f201c 	ldr	r2, [pc, #28]	; 8d74 <_Z41__static_initialization_and_destruction_0ii+0x48>
    8d54:	e1530002 	cmp	r3, r2
    8d58:	1a000002 	bne	8d68 <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:4
CUART sUART0(sAUX);
    8d5c:	e59f1014 	ldr	r1, [pc, #20]	; 8d78 <_Z41__static_initialization_and_destruction_0ii+0x4c>
    8d60:	e59f0014 	ldr	r0, [pc, #20]	; 8d7c <_Z41__static_initialization_and_destruction_0ii+0x50>
    8d64:	ebffff56 	bl	8ac4 <_ZN5CUARTC1ER4CAUX>
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:47
    8d68:	e320f000 	nop	{0}
    8d6c:	e24bd004 	sub	sp, fp, #4
    8d70:	e8bd8800 	pop	{fp, pc}
    8d74:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
    8d78:	00009638 	andeq	r9, r0, r8, lsr r6
    8d7c:	00009648 	andeq	r9, r0, r8, asr #12

00008d80 <_GLOBAL__sub_I_sUART0>:
_GLOBAL__sub_I_sUART0():
/home/markovda/Skola/repos/os-5/kernel/src/drivers/uart.cpp:47
    8d80:	e92d4800 	push	{fp, lr}
    8d84:	e28db004 	add	fp, sp, #4
    8d88:	e59f1008 	ldr	r1, [pc, #8]	; 8d98 <_GLOBAL__sub_I_sUART0+0x18>
    8d8c:	e3a00001 	mov	r0, #1
    8d90:	ebffffe5 	bl	8d2c <_Z41__static_initialization_and_destruction_0ii>
    8d94:	e8bd8800 	pop	{fp, pc}
    8d98:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>

00008d9c <software_interrupt_handler>:
software_interrupt_handler():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:16
CInterrupt_Controller sInterruptCtl(hal::Interrupt_Controller_Base);

// handlery jednotlivych zdroju preruseni

extern "C" void __attribute__((interrupt("SWI"))) software_interrupt_handler()
{
    8d9c:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8da0:	e28db000 	add	fp, sp, #0
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:18
    // tady nekdy v budoucnu definujeme obsluhu volani sluzeb jadra z uzivatelskeho procesu
}
    8da4:	e320f000 	nop	{0}
    8da8:	e28bd000 	add	sp, fp, #0
    8dac:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8db0:	e1b0f00e 	movs	pc, lr

00008db4 <irq_handler>:
irq_handler():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:21

extern "C" void __attribute__((interrupt("IRQ"))) irq_handler()
{
    8db4:	e24ee004 	sub	lr, lr, #4
    8db8:	e92d581f 	push	{r0, r1, r2, r3, r4, fp, ip, lr}
    8dbc:	e28db01c 	add	fp, sp, #28
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:26
    // jelikoz ARM nerozlisuje zdroje IRQ implicitne, ani nezarucuje, ze se navzajen nemaskuji, musime
    // projit vsechny mozne zdroje a podivat se (poll), zda nebylo vyvolano preruseni

    // casovac
    if (sTimer.Is_Timer_IRQ_Pending())
    8dc0:	e59f0020 	ldr	r0, [pc, #32]	; 8de8 <irq_handler+0x34>
    8dc4:	ebffff12 	bl	8a14 <_ZN6CTimer20Is_Timer_IRQ_PendingEv>
    8dc8:	e1a03000 	mov	r3, r0
    8dcc:	e3530000 	cmp	r3, #0
    8dd0:	0a000001 	beq	8ddc <irq_handler+0x28>
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:27
        sTimer.IRQ_Callback();
    8dd4:	e59f000c 	ldr	r0, [pc, #12]	; 8de8 <irq_handler+0x34>
    8dd8:	ebfffef9 	bl	89c4 <_ZN6CTimer12IRQ_CallbackEv>
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:28
}
    8ddc:	e320f000 	nop	{0}
    8de0:	e24bd01c 	sub	sp, fp, #28
    8de4:	e8fd981f 	ldm	sp!, {r0, r1, r2, r3, r4, fp, ip, pc}^
    8de8:	00009640 	andeq	r9, r0, r0, asr #12

00008dec <fast_interrupt_handler>:
fast_interrupt_handler():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:31

extern "C" void __attribute__((interrupt("FIQ"))) fast_interrupt_handler()
{
    8dec:	e24db004 	sub	fp, sp, #4
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:33
    // zatim nepouzivame
}
    8df0:	e320f000 	nop	{0}
    8df4:	e28bd004 	add	sp, fp, #4
    8df8:	e25ef004 	subs	pc, lr, #4

00008dfc <_ZN21CInterrupt_ControllerC1Em>:
_ZN21CInterrupt_ControllerC2Em():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:37

// implementace controlleru

CInterrupt_Controller::CInterrupt_Controller(unsigned long base)
    8dfc:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8e00:	e28db000 	add	fp, sp, #0
    8e04:	e24dd00c 	sub	sp, sp, #12
    8e08:	e50b0008 	str	r0, [fp, #-8]
    8e0c:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:38
    : mInterrupt_Regs(reinterpret_cast<unsigned int*>(base))
    8e10:	e51b200c 	ldr	r2, [fp, #-12]
    8e14:	e51b3008 	ldr	r3, [fp, #-8]
    8e18:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:41
{
    //
}
    8e1c:	e51b3008 	ldr	r3, [fp, #-8]
    8e20:	e1a00003 	mov	r0, r3
    8e24:	e28bd000 	add	sp, fp, #0
    8e28:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8e2c:	e12fff1e 	bx	lr

00008e30 <_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE>:
_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:44

volatile unsigned int& CInterrupt_Controller::Regs(hal::Interrupt_Controller_Reg reg)
{
    8e30:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8e34:	e28db000 	add	fp, sp, #0
    8e38:	e24dd00c 	sub	sp, sp, #12
    8e3c:	e50b0008 	str	r0, [fp, #-8]
    8e40:	e50b100c 	str	r1, [fp, #-12]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:45
    return mInterrupt_Regs[static_cast<unsigned int>(reg)];
    8e44:	e51b3008 	ldr	r3, [fp, #-8]
    8e48:	e5932000 	ldr	r2, [r3]
    8e4c:	e51b300c 	ldr	r3, [fp, #-12]
    8e50:	e1a03103 	lsl	r3, r3, #2
    8e54:	e0823003 	add	r3, r2, r3
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:46
}
    8e58:	e1a00003 	mov	r0, r3
    8e5c:	e28bd000 	add	sp, fp, #0
    8e60:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8e64:	e12fff1e 	bx	lr

00008e68 <_ZN21CInterrupt_Controller16Enable_Basic_IRQEN3hal16IRQ_Basic_SourceE>:
_ZN21CInterrupt_Controller16Enable_Basic_IRQEN3hal16IRQ_Basic_SourceE():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:49

void CInterrupt_Controller::Enable_Basic_IRQ(hal::IRQ_Basic_Source source_idx)
{
    8e68:	e92d4810 	push	{r4, fp, lr}
    8e6c:	e28db008 	add	fp, sp, #8
    8e70:	e24dd00c 	sub	sp, sp, #12
    8e74:	e50b0010 	str	r0, [fp, #-16]
    8e78:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:50
    Regs(hal::Interrupt_Controller_Reg::IRQ_Basic_Enable) = (1 << static_cast<unsigned int>(source_idx));
    8e7c:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8e80:	e3a02001 	mov	r2, #1
    8e84:	e1a04312 	lsl	r4, r2, r3
    8e88:	e3a01006 	mov	r1, #6
    8e8c:	e51b0010 	ldr	r0, [fp, #-16]
    8e90:	ebffffe6 	bl	8e30 <_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE>
    8e94:	e1a03000 	mov	r3, r0
    8e98:	e1a02004 	mov	r2, r4
    8e9c:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:51
}
    8ea0:	e320f000 	nop	{0}
    8ea4:	e24bd008 	sub	sp, fp, #8
    8ea8:	e8bd8810 	pop	{r4, fp, pc}

00008eac <_ZN21CInterrupt_Controller17Disable_Basic_IRQEN3hal16IRQ_Basic_SourceE>:
_ZN21CInterrupt_Controller17Disable_Basic_IRQEN3hal16IRQ_Basic_SourceE():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:54

void CInterrupt_Controller::Disable_Basic_IRQ(hal::IRQ_Basic_Source source_idx)
{
    8eac:	e92d4810 	push	{r4, fp, lr}
    8eb0:	e28db008 	add	fp, sp, #8
    8eb4:	e24dd00c 	sub	sp, sp, #12
    8eb8:	e50b0010 	str	r0, [fp, #-16]
    8ebc:	e50b1014 	str	r1, [fp, #-20]	; 0xffffffec
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:55
    Regs(hal::Interrupt_Controller_Reg::IRQ_Basic_Disable) = (1 << static_cast<unsigned int>(source_idx));
    8ec0:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8ec4:	e3a02001 	mov	r2, #1
    8ec8:	e1a04312 	lsl	r4, r2, r3
    8ecc:	e3a01009 	mov	r1, #9
    8ed0:	e51b0010 	ldr	r0, [fp, #-16]
    8ed4:	ebffffd5 	bl	8e30 <_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE>
    8ed8:	e1a03000 	mov	r3, r0
    8edc:	e1a02004 	mov	r2, r4
    8ee0:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:56
}
    8ee4:	e320f000 	nop	{0}
    8ee8:	e24bd008 	sub	sp, fp, #8
    8eec:	e8bd8810 	pop	{r4, fp, pc}

00008ef0 <_ZN21CInterrupt_Controller10Enable_IRQEN3hal10IRQ_SourceE>:
_ZN21CInterrupt_Controller10Enable_IRQEN3hal10IRQ_SourceE():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:59

void CInterrupt_Controller::Enable_IRQ(hal::IRQ_Source source_idx)
{
    8ef0:	e92d4810 	push	{r4, fp, lr}
    8ef4:	e28db008 	add	fp, sp, #8
    8ef8:	e24dd014 	sub	sp, sp, #20
    8efc:	e50b0018 	str	r0, [fp, #-24]	; 0xffffffe8
    8f00:	e50b101c 	str	r1, [fp, #-28]	; 0xffffffe4
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:60
    const unsigned int idx_base = static_cast<unsigned int>(source_idx);
    8f04:	e51b301c 	ldr	r3, [fp, #-28]	; 0xffffffe4
    8f08:	e50b3010 	str	r3, [fp, #-16]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:62

    Regs(idx_base < 32 ? hal::Interrupt_Controller_Reg::IRQ_Enable_1 : hal::Interrupt_Controller_Reg::IRQ_Enable_1) = (1 << (idx_base % 32));
    8f0c:	e51b3010 	ldr	r3, [fp, #-16]
    8f10:	e203301f 	and	r3, r3, #31
    8f14:	e3a02001 	mov	r2, #1
    8f18:	e1a04312 	lsl	r4, r2, r3
    8f1c:	e3a01004 	mov	r1, #4
    8f20:	e51b0018 	ldr	r0, [fp, #-24]	; 0xffffffe8
    8f24:	ebffffc1 	bl	8e30 <_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE>
    8f28:	e1a03000 	mov	r3, r0
    8f2c:	e1a02004 	mov	r2, r4
    8f30:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:63
}
    8f34:	e320f000 	nop	{0}
    8f38:	e24bd008 	sub	sp, fp, #8
    8f3c:	e8bd8810 	pop	{r4, fp, pc}

00008f40 <_ZN21CInterrupt_Controller11Disable_IRQEN3hal10IRQ_SourceE>:
_ZN21CInterrupt_Controller11Disable_IRQEN3hal10IRQ_SourceE():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:66

void CInterrupt_Controller::Disable_IRQ(hal::IRQ_Source source_idx)
{
    8f40:	e92d4810 	push	{r4, fp, lr}
    8f44:	e28db008 	add	fp, sp, #8
    8f48:	e24dd014 	sub	sp, sp, #20
    8f4c:	e50b0018 	str	r0, [fp, #-24]	; 0xffffffe8
    8f50:	e50b101c 	str	r1, [fp, #-28]	; 0xffffffe4
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:67
    const unsigned int idx_base = static_cast<unsigned int>(source_idx);
    8f54:	e51b301c 	ldr	r3, [fp, #-28]	; 0xffffffe4
    8f58:	e50b3010 	str	r3, [fp, #-16]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:69

    Regs(idx_base < 32 ? hal::Interrupt_Controller_Reg::IRQ_Disable_1 : hal::Interrupt_Controller_Reg::IRQ_Disable_1) = (1 << (idx_base % 32));
    8f5c:	e51b3010 	ldr	r3, [fp, #-16]
    8f60:	e203301f 	and	r3, r3, #31
    8f64:	e3a02001 	mov	r2, #1
    8f68:	e1a04312 	lsl	r4, r2, r3
    8f6c:	e3a01007 	mov	r1, #7
    8f70:	e51b0018 	ldr	r0, [fp, #-24]	; 0xffffffe8
    8f74:	ebffffad 	bl	8e30 <_ZN21CInterrupt_Controller4RegsEN3hal24Interrupt_Controller_RegE>
    8f78:	e1a03000 	mov	r3, r0
    8f7c:	e1a02004 	mov	r2, r4
    8f80:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:70
}
    8f84:	e320f000 	nop	{0}
    8f88:	e24bd008 	sub	sp, fp, #8
    8f8c:	e8bd8810 	pop	{r4, fp, pc}

00008f90 <_Z41__static_initialization_and_destruction_0ii>:
_Z41__static_initialization_and_destruction_0ii():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:70
    8f90:	e92d4800 	push	{fp, lr}
    8f94:	e28db004 	add	fp, sp, #4
    8f98:	e24dd008 	sub	sp, sp, #8
    8f9c:	e50b0008 	str	r0, [fp, #-8]
    8fa0:	e50b100c 	str	r1, [fp, #-12]
    8fa4:	e51b3008 	ldr	r3, [fp, #-8]
    8fa8:	e3530001 	cmp	r3, #1
    8fac:	1a000006 	bne	8fcc <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:70 (discriminator 1)
    8fb0:	e51b300c 	ldr	r3, [fp, #-12]
    8fb4:	e59f201c 	ldr	r2, [pc, #28]	; 8fd8 <_Z41__static_initialization_and_destruction_0ii+0x48>
    8fb8:	e1530002 	cmp	r3, r2
    8fbc:	1a000002 	bne	8fcc <_Z41__static_initialization_and_destruction_0ii+0x3c>
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:11
CInterrupt_Controller sInterruptCtl(hal::Interrupt_Controller_Base);
    8fc0:	e59f1014 	ldr	r1, [pc, #20]	; 8fdc <_Z41__static_initialization_and_destruction_0ii+0x4c>
    8fc4:	e59f0014 	ldr	r0, [pc, #20]	; 8fe0 <_Z41__static_initialization_and_destruction_0ii+0x50>
    8fc8:	ebffff8b 	bl	8dfc <_ZN21CInterrupt_ControllerC1Em>
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:70
}
    8fcc:	e320f000 	nop	{0}
    8fd0:	e24bd004 	sub	sp, fp, #4
    8fd4:	e8bd8800 	pop	{fp, pc}
    8fd8:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
    8fdc:	2000b200 	andcs	fp, r0, r0, lsl #4
    8fe0:	0000964c 	andeq	r9, r0, ip, asr #12

00008fe4 <_GLOBAL__sub_I_sInterruptCtl>:
_GLOBAL__sub_I_sInterruptCtl():
/home/markovda/Skola/repos/os-5/kernel/src/interrupt_controller.cpp:70
    8fe4:	e92d4800 	push	{fp, lr}
    8fe8:	e28db004 	add	fp, sp, #4
    8fec:	e59f1008 	ldr	r1, [pc, #8]	; 8ffc <_GLOBAL__sub_I_sInterruptCtl+0x18>
    8ff0:	e3a00001 	mov	r0, #1
    8ff4:	ebffffe5 	bl	8f90 <_Z41__static_initialization_and_destruction_0ii>
    8ff8:	e8bd8800 	pop	{fp, pc}
    8ffc:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>

00009000 <ACT_LED_Blinker>:
ACT_LED_Blinker():
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:16

// je LEDka zapnuta?
volatile int LED_State = 0;

extern "C" void ACT_LED_Blinker()
{
    9000:	e92d4800 	push	{fp, lr}
    9004:	e28db004 	add	fp, sp, #4
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:19
	// prepinani LED pri kazdem vytikani casovace

	if (LED_State)
    9008:	e59f305c 	ldr	r3, [pc, #92]	; 906c <ACT_LED_Blinker+0x6c>
    900c:	e5933000 	ldr	r3, [r3]
    9010:	e3530000 	cmp	r3, #0
    9014:	13a03001 	movne	r3, #1
    9018:	03a03000 	moveq	r3, #0
    901c:	e6ef3073 	uxtb	r3, r3
    9020:	e3530000 	cmp	r3, #0
    9024:	0a000007 	beq	9048 <ACT_LED_Blinker+0x48>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:21
	{
		LED_State = 0;
    9028:	e59f303c 	ldr	r3, [pc, #60]	; 906c <ACT_LED_Blinker+0x6c>
    902c:	e3a02000 	mov	r2, #0
    9030:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:22
		sGPIO.Set_Output(ACT_Pin, true);
    9034:	e3a02001 	mov	r2, #1
    9038:	e3a0102f 	mov	r1, #47	; 0x2f
    903c:	e59f002c 	ldr	r0, [pc, #44]	; 9070 <ACT_LED_Blinker+0x70>
    9040:	ebfffda9 	bl	86ec <_ZN13CGPIO_Handler10Set_OutputEjb>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:29
	else
	{
		LED_State = 1;
		sGPIO.Set_Output(ACT_Pin, false);
	}
}
    9044:	ea000006 	b	9064 <ACT_LED_Blinker+0x64>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:26
		LED_State = 1;
    9048:	e59f301c 	ldr	r3, [pc, #28]	; 906c <ACT_LED_Blinker+0x6c>
    904c:	e3a02001 	mov	r2, #1
    9050:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:27
		sGPIO.Set_Output(ACT_Pin, false);
    9054:	e3a02000 	mov	r2, #0
    9058:	e3a0102f 	mov	r1, #47	; 0x2f
    905c:	e59f000c 	ldr	r0, [pc, #12]	; 9070 <ACT_LED_Blinker+0x70>
    9060:	ebfffda1 	bl	86ec <_ZN13CGPIO_Handler10Set_OutputEjb>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:29
}
    9064:	e320f000 	nop	{0}
    9068:	e8bd8800 	pop	{fp, pc}
    906c:	00009650 	andeq	r9, r0, r0, asr r6
    9070:	0000963c 	andeq	r9, r0, ip, lsr r6

00009074 <_kernel_main>:
_kernel_main():
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:32

extern "C" int _kernel_main(void)
{
    9074:	e92d4800 	push	{fp, lr}
    9078:	e28db004 	add	fp, sp, #4
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:34
	// nastavime ACT LED pin na vystupni
	sGPIO.Set_GPIO_Function(ACT_Pin, NGPIO_Function::Output);
    907c:	e3a02001 	mov	r2, #1
    9080:	e3a0102f 	mov	r1, #47	; 0x2f
    9084:	e59f0058 	ldr	r0, [pc, #88]	; 90e4 <_kernel_main+0x70>
    9088:	ebfffd52 	bl	85d8 <_ZN13CGPIO_Handler17Set_GPIO_FunctionEj14NGPIO_Function>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:37

	// inicializujeme UART kanal 0
	sUART0.Set_Baud_Rate(NUART_Baud_Rate::BR_115200);
    908c:	e59f1054 	ldr	r1, [pc, #84]	; 90e8 <_kernel_main+0x74>
    9090:	e59f0054 	ldr	r0, [pc, #84]	; 90ec <_kernel_main+0x78>
    9094:	ebfffeca 	bl	8bc4 <_ZN5CUART13Set_Baud_RateE15NUART_Baud_Rate>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:38
	sUART0.Set_Char_Length(NUART_Char_Length::Char_8);
    9098:	e3a01001 	mov	r1, #1
    909c:	e59f0048 	ldr	r0, [pc, #72]	; 90ec <_kernel_main+0x78>
    90a0:	ebfffeb0 	bl	8b68 <_ZN5CUART15Set_Char_LengthE17NUART_Char_Length>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:41

	// vypiseme ladici hlasku
	sUART0.Write("Welcome to KIV/OS RPiOS kernel\r\n");
    90a4:	e59f1044 	ldr	r1, [pc, #68]	; 90f0 <_kernel_main+0x7c>
    90a8:	e59f003c 	ldr	r0, [pc, #60]	; 90ec <_kernel_main+0x78>
    90ac:	ebffff03 	bl	8cc0 <_ZN5CUART5WriteEPKc>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:44

	// zatim zakazeme IRQ casovacé
	sInterruptCtl.Disable_Basic_IRQ(hal::IRQ_Basic_Source::Timer);
    90b0:	e3a01000 	mov	r1, #0
    90b4:	e59f0038 	ldr	r0, [pc, #56]	; 90f4 <_kernel_main+0x80>
    90b8:	ebffff7b 	bl	8eac <_ZN21CInterrupt_Controller17Disable_Basic_IRQEN3hal16IRQ_Basic_SourceE>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:47

	// nastavime casovac - budeme pro ted blikat LEDkou, v budoucnu muzeme treba spoustet planovac procesu
	sTimer.Enable(ACT_LED_Blinker, 0x200, NTimer_Prescaler::Prescaler_256);
    90bc:	e3a03002 	mov	r3, #2
    90c0:	e3a02c02 	mov	r2, #512	; 0x200
    90c4:	e59f102c 	ldr	r1, [pc, #44]	; 90f8 <_kernel_main+0x84>
    90c8:	e59f002c 	ldr	r0, [pc, #44]	; 90fc <_kernel_main+0x88>
    90cc:	ebfffdf6 	bl	88ac <_ZN6CTimer6EnableEPFvvEj16NTimer_Prescaler>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:50

	// povolime IRQ casovace
	sInterruptCtl.Enable_Basic_IRQ(hal::IRQ_Basic_Source::Timer);
    90d0:	e3a01000 	mov	r1, #0
    90d4:	e59f0018 	ldr	r0, [pc, #24]	; 90f4 <_kernel_main+0x80>
    90d8:	ebffff62 	bl	8e68 <_ZN21CInterrupt_Controller16Enable_Basic_IRQEN3hal16IRQ_Basic_SourceE>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:52

	enable_irq();
    90dc:	eb000007 	bl	9100 <enable_irq>
/home/markovda/Skola/repos/os-5/kernel/src/main.cpp:55 (discriminator 1)
	
	// nekonecna smycka - tadyodsud se CPU uz nedostane jinak, nez treba prerusenim
    while (1)
    90e0:	eafffffe 	b	90e0 <_kernel_main+0x6c>
    90e4:	0000963c 	andeq	r9, r0, ip, lsr r6
    90e8:	0001c200 	andeq	ip, r1, r0, lsl #4
    90ec:	00009648 	andeq	r9, r0, r8, asr #12
    90f0:	00009600 	andeq	r9, r0, r0, lsl #12
    90f4:	0000964c 	andeq	r9, r0, ip, asr #12
    90f8:	00009000 	andeq	r9, r0, r0
    90fc:	00009640 	andeq	r9, r0, r0, asr #12

00009100 <enable_irq>:
enable_irq():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:90
;@ tady budou ostatni symboly, ktere nevyzaduji zadne specialni misto
.section .text

.global enable_irq
enable_irq:
    mrs r0, cpsr		;@ presun ridiciho registru (CPSR) do general purpose registru (R0)
    9100:	e10f0000 	mrs	r0, CPSR
/home/markovda/Skola/repos/os-5/kernel/src/start.s:91
    bic r0, r0, #0x80	;@ vypne bit 7 v registru r0 ("IRQ mask bit")
    9104:	e3c00080 	bic	r0, r0, #128	; 0x80
/home/markovda/Skola/repos/os-5/kernel/src/start.s:92
    msr cpsr_c, r0		;@ nacteme upraveny general purpose (R0) registr do ridiciho (CPSR)
    9108:	e121f000 	msr	CPSR_c, r0
/home/markovda/Skola/repos/os-5/kernel/src/start.s:93
    cpsie i				;@ povoli preruseni
    910c:	f1080080 	cpsie	i
/home/markovda/Skola/repos/os-5/kernel/src/start.s:94
    bx lr
    9110:	e12fff1e 	bx	lr

00009114 <undefined_instruction_handler>:
undefined_instruction_handler():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:97

undefined_instruction_handler:
	b hang
    9114:	eafffbdd 	b	8090 <hang>

00009118 <prefetch_abort_handler>:
prefetch_abort_handler():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:102

prefetch_abort_handler:
	;@ tady pak muzeme osetrit, kdyz program zasahne do mista, ktere nema mapovane ve svem virtualnim adr. prostoru
	;@ a treba vyvolat nasi obdobu segfaultu
	b hang
    9118:	eafffbdc 	b	8090 <hang>

0000911c <data_abort_handler>:
data_abort_handler():
/home/markovda/Skola/repos/os-5/kernel/src/start.s:107

data_abort_handler:
	;@ tady pak muzeme osetrit, kdyz program zasahne do mista, ktere nema mapovane ve svem virtualnim adr. prostoru
	;@ a treba vyvolat nasi obdobu segfaultu
	b hang
    911c:	eafffbdb 	b	8090 <hang>

00009120 <_c_startup>:
_c_startup():
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:21
extern "C" dtor_ptr __DTOR_LIST__[0];
// konec pole destruktoru
extern "C" dtor_ptr __DTOR_END__[0];

extern "C" int _c_startup(void)
{
    9120:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    9124:	e28db000 	add	fp, sp, #0
    9128:	e24dd00c 	sub	sp, sp, #12
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:25
	int* i;
	
	// vynulujeme .bss sekci
	for (i = (int*)_bss_start; i < (int*)_bss_end; i++)
    912c:	e59f304c 	ldr	r3, [pc, #76]	; 9180 <_c_startup+0x60>
    9130:	e5933000 	ldr	r3, [r3]
    9134:	e50b3008 	str	r3, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:25 (discriminator 3)
    9138:	e59f3044 	ldr	r3, [pc, #68]	; 9184 <_c_startup+0x64>
    913c:	e5933000 	ldr	r3, [r3]
    9140:	e1a02003 	mov	r2, r3
    9144:	e51b3008 	ldr	r3, [fp, #-8]
    9148:	e1530002 	cmp	r3, r2
    914c:	2a000006 	bcs	916c <_c_startup+0x4c>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:26 (discriminator 2)
		*i = 0;
    9150:	e51b3008 	ldr	r3, [fp, #-8]
    9154:	e3a02000 	mov	r2, #0
    9158:	e5832000 	str	r2, [r3]
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:25 (discriminator 2)
	for (i = (int*)_bss_start; i < (int*)_bss_end; i++)
    915c:	e51b3008 	ldr	r3, [fp, #-8]
    9160:	e2833004 	add	r3, r3, #4
    9164:	e50b3008 	str	r3, [fp, #-8]
    9168:	eafffff2 	b	9138 <_c_startup+0x18>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:28
	
	return 0;
    916c:	e3a03000 	mov	r3, #0
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:29
}
    9170:	e1a00003 	mov	r0, r3
    9174:	e28bd000 	add	sp, fp, #0
    9178:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    917c:	e12fff1e 	bx	lr
    9180:	00009638 	andeq	r9, r0, r8, lsr r6
    9184:	00009664 	andeq	r9, r0, r4, ror #12

00009188 <_cpp_startup>:
_cpp_startup():
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:32

extern "C" int _cpp_startup(void)
{
    9188:	e92d4800 	push	{fp, lr}
    918c:	e28db004 	add	fp, sp, #4
    9190:	e24dd008 	sub	sp, sp, #8
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:37
	ctor_ptr* fnptr;
	
	// zavolame konstruktory globalnich C++ trid
	// v poli __CTOR_LIST__ jsou ukazatele na vygenerovane stuby volani konstruktoru
	for (fnptr = __CTOR_LIST__; fnptr < __CTOR_END__; fnptr++)
    9194:	e59f303c 	ldr	r3, [pc, #60]	; 91d8 <_cpp_startup+0x50>
    9198:	e50b3008 	str	r3, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:37 (discriminator 3)
    919c:	e51b3008 	ldr	r3, [fp, #-8]
    91a0:	e59f2034 	ldr	r2, [pc, #52]	; 91dc <_cpp_startup+0x54>
    91a4:	e1530002 	cmp	r3, r2
    91a8:	2a000006 	bcs	91c8 <_cpp_startup+0x40>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:38 (discriminator 2)
		(*fnptr)();
    91ac:	e51b3008 	ldr	r3, [fp, #-8]
    91b0:	e5933000 	ldr	r3, [r3]
    91b4:	e12fff33 	blx	r3
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:37 (discriminator 2)
	for (fnptr = __CTOR_LIST__; fnptr < __CTOR_END__; fnptr++)
    91b8:	e51b3008 	ldr	r3, [fp, #-8]
    91bc:	e2833004 	add	r3, r3, #4
    91c0:	e50b3008 	str	r3, [fp, #-8]
    91c4:	eafffff4 	b	919c <_cpp_startup+0x14>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:40
	
	return 0;
    91c8:	e3a03000 	mov	r3, #0
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:41
}
    91cc:	e1a00003 	mov	r0, r3
    91d0:	e24bd004 	sub	sp, fp, #4
    91d4:	e8bd8800 	pop	{fp, pc}
    91d8:	00009624 	andeq	r9, r0, r4, lsr #12
    91dc:	00009638 	andeq	r9, r0, r8, lsr r6

000091e0 <_cpp_shutdown>:
_cpp_shutdown():
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:44

extern "C" int _cpp_shutdown(void)
{
    91e0:	e92d4800 	push	{fp, lr}
    91e4:	e28db004 	add	fp, sp, #4
    91e8:	e24dd008 	sub	sp, sp, #8
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:48
	dtor_ptr* fnptr;
	
	// zavolame destruktory globalnich C++ trid
	for (fnptr = __DTOR_LIST__; fnptr < __DTOR_END__; fnptr++)
    91ec:	e59f303c 	ldr	r3, [pc, #60]	; 9230 <_cpp_shutdown+0x50>
    91f0:	e50b3008 	str	r3, [fp, #-8]
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:48 (discriminator 3)
    91f4:	e51b3008 	ldr	r3, [fp, #-8]
    91f8:	e59f2034 	ldr	r2, [pc, #52]	; 9234 <_cpp_shutdown+0x54>
    91fc:	e1530002 	cmp	r3, r2
    9200:	2a000006 	bcs	9220 <_cpp_shutdown+0x40>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:49 (discriminator 2)
		(*fnptr)();
    9204:	e51b3008 	ldr	r3, [fp, #-8]
    9208:	e5933000 	ldr	r3, [r3]
    920c:	e12fff33 	blx	r3
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:48 (discriminator 2)
	for (fnptr = __DTOR_LIST__; fnptr < __DTOR_END__; fnptr++)
    9210:	e51b3008 	ldr	r3, [fp, #-8]
    9214:	e2833004 	add	r3, r3, #4
    9218:	e50b3008 	str	r3, [fp, #-8]
    921c:	eafffff4 	b	91f4 <_cpp_shutdown+0x14>
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:51
	
	return 0;
    9220:	e3a03000 	mov	r3, #0
/home/markovda/Skola/repos/os-5/kernel/src/startup.cpp:52
}
    9224:	e1a00003 	mov	r0, r3
    9228:	e24bd004 	sub	sp, fp, #4
    922c:	e8bd8800 	pop	{fp, pc}
    9230:	00009638 	andeq	r9, r0, r8, lsr r6
    9234:	00009638 	andeq	r9, r0, r8, lsr r6

00009238 <__udivsi3>:
__udivsi3():
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1099
    9238:	e2512001 	subs	r2, r1, #1
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1101
    923c:	012fff1e 	bxeq	lr
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1102
    9240:	3a000074 	bcc	9418 <__udivsi3+0x1e0>
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1103
    9244:	e1500001 	cmp	r0, r1
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1104
    9248:	9a00006b 	bls	93fc <__udivsi3+0x1c4>
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1105
    924c:	e1110002 	tst	r1, r2
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1106
    9250:	0a00006c 	beq	9408 <__udivsi3+0x1d0>
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1108
    9254:	e16f3f10 	clz	r3, r0
    9258:	e16f2f11 	clz	r2, r1
    925c:	e0423003 	sub	r3, r2, r3
    9260:	e273301f 	rsbs	r3, r3, #31
    9264:	10833083 	addne	r3, r3, r3, lsl #1
    9268:	e3a02000 	mov	r2, #0
    926c:	108ff103 	addne	pc, pc, r3, lsl #2
    9270:	e1a00000 	nop			; (mov r0, r0)
    9274:	e1500f81 	cmp	r0, r1, lsl #31
    9278:	e0a22002 	adc	r2, r2, r2
    927c:	20400f81 	subcs	r0, r0, r1, lsl #31
    9280:	e1500f01 	cmp	r0, r1, lsl #30
    9284:	e0a22002 	adc	r2, r2, r2
    9288:	20400f01 	subcs	r0, r0, r1, lsl #30
    928c:	e1500e81 	cmp	r0, r1, lsl #29
    9290:	e0a22002 	adc	r2, r2, r2
    9294:	20400e81 	subcs	r0, r0, r1, lsl #29
    9298:	e1500e01 	cmp	r0, r1, lsl #28
    929c:	e0a22002 	adc	r2, r2, r2
    92a0:	20400e01 	subcs	r0, r0, r1, lsl #28
    92a4:	e1500d81 	cmp	r0, r1, lsl #27
    92a8:	e0a22002 	adc	r2, r2, r2
    92ac:	20400d81 	subcs	r0, r0, r1, lsl #27
    92b0:	e1500d01 	cmp	r0, r1, lsl #26
    92b4:	e0a22002 	adc	r2, r2, r2
    92b8:	20400d01 	subcs	r0, r0, r1, lsl #26
    92bc:	e1500c81 	cmp	r0, r1, lsl #25
    92c0:	e0a22002 	adc	r2, r2, r2
    92c4:	20400c81 	subcs	r0, r0, r1, lsl #25
    92c8:	e1500c01 	cmp	r0, r1, lsl #24
    92cc:	e0a22002 	adc	r2, r2, r2
    92d0:	20400c01 	subcs	r0, r0, r1, lsl #24
    92d4:	e1500b81 	cmp	r0, r1, lsl #23
    92d8:	e0a22002 	adc	r2, r2, r2
    92dc:	20400b81 	subcs	r0, r0, r1, lsl #23
    92e0:	e1500b01 	cmp	r0, r1, lsl #22
    92e4:	e0a22002 	adc	r2, r2, r2
    92e8:	20400b01 	subcs	r0, r0, r1, lsl #22
    92ec:	e1500a81 	cmp	r0, r1, lsl #21
    92f0:	e0a22002 	adc	r2, r2, r2
    92f4:	20400a81 	subcs	r0, r0, r1, lsl #21
    92f8:	e1500a01 	cmp	r0, r1, lsl #20
    92fc:	e0a22002 	adc	r2, r2, r2
    9300:	20400a01 	subcs	r0, r0, r1, lsl #20
    9304:	e1500981 	cmp	r0, r1, lsl #19
    9308:	e0a22002 	adc	r2, r2, r2
    930c:	20400981 	subcs	r0, r0, r1, lsl #19
    9310:	e1500901 	cmp	r0, r1, lsl #18
    9314:	e0a22002 	adc	r2, r2, r2
    9318:	20400901 	subcs	r0, r0, r1, lsl #18
    931c:	e1500881 	cmp	r0, r1, lsl #17
    9320:	e0a22002 	adc	r2, r2, r2
    9324:	20400881 	subcs	r0, r0, r1, lsl #17
    9328:	e1500801 	cmp	r0, r1, lsl #16
    932c:	e0a22002 	adc	r2, r2, r2
    9330:	20400801 	subcs	r0, r0, r1, lsl #16
    9334:	e1500781 	cmp	r0, r1, lsl #15
    9338:	e0a22002 	adc	r2, r2, r2
    933c:	20400781 	subcs	r0, r0, r1, lsl #15
    9340:	e1500701 	cmp	r0, r1, lsl #14
    9344:	e0a22002 	adc	r2, r2, r2
    9348:	20400701 	subcs	r0, r0, r1, lsl #14
    934c:	e1500681 	cmp	r0, r1, lsl #13
    9350:	e0a22002 	adc	r2, r2, r2
    9354:	20400681 	subcs	r0, r0, r1, lsl #13
    9358:	e1500601 	cmp	r0, r1, lsl #12
    935c:	e0a22002 	adc	r2, r2, r2
    9360:	20400601 	subcs	r0, r0, r1, lsl #12
    9364:	e1500581 	cmp	r0, r1, lsl #11
    9368:	e0a22002 	adc	r2, r2, r2
    936c:	20400581 	subcs	r0, r0, r1, lsl #11
    9370:	e1500501 	cmp	r0, r1, lsl #10
    9374:	e0a22002 	adc	r2, r2, r2
    9378:	20400501 	subcs	r0, r0, r1, lsl #10
    937c:	e1500481 	cmp	r0, r1, lsl #9
    9380:	e0a22002 	adc	r2, r2, r2
    9384:	20400481 	subcs	r0, r0, r1, lsl #9
    9388:	e1500401 	cmp	r0, r1, lsl #8
    938c:	e0a22002 	adc	r2, r2, r2
    9390:	20400401 	subcs	r0, r0, r1, lsl #8
    9394:	e1500381 	cmp	r0, r1, lsl #7
    9398:	e0a22002 	adc	r2, r2, r2
    939c:	20400381 	subcs	r0, r0, r1, lsl #7
    93a0:	e1500301 	cmp	r0, r1, lsl #6
    93a4:	e0a22002 	adc	r2, r2, r2
    93a8:	20400301 	subcs	r0, r0, r1, lsl #6
    93ac:	e1500281 	cmp	r0, r1, lsl #5
    93b0:	e0a22002 	adc	r2, r2, r2
    93b4:	20400281 	subcs	r0, r0, r1, lsl #5
    93b8:	e1500201 	cmp	r0, r1, lsl #4
    93bc:	e0a22002 	adc	r2, r2, r2
    93c0:	20400201 	subcs	r0, r0, r1, lsl #4
    93c4:	e1500181 	cmp	r0, r1, lsl #3
    93c8:	e0a22002 	adc	r2, r2, r2
    93cc:	20400181 	subcs	r0, r0, r1, lsl #3
    93d0:	e1500101 	cmp	r0, r1, lsl #2
    93d4:	e0a22002 	adc	r2, r2, r2
    93d8:	20400101 	subcs	r0, r0, r1, lsl #2
    93dc:	e1500081 	cmp	r0, r1, lsl #1
    93e0:	e0a22002 	adc	r2, r2, r2
    93e4:	20400081 	subcs	r0, r0, r1, lsl #1
    93e8:	e1500001 	cmp	r0, r1
    93ec:	e0a22002 	adc	r2, r2, r2
    93f0:	20400001 	subcs	r0, r0, r1
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1110
    93f4:	e1a00002 	mov	r0, r2
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1111
    93f8:	e12fff1e 	bx	lr
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1114
    93fc:	03a00001 	moveq	r0, #1
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1115
    9400:	13a00000 	movne	r0, #0
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1116
    9404:	e12fff1e 	bx	lr
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1118
    9408:	e16f2f11 	clz	r2, r1
    940c:	e262201f 	rsb	r2, r2, #31
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1120
    9410:	e1a00230 	lsr	r0, r0, r2
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1121
    9414:	e12fff1e 	bx	lr
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1125
    9418:	e3500000 	cmp	r0, #0
    941c:	13e00000 	mvnne	r0, #0
    9420:	ea000007 	b	9444 <__aeabi_idiv0>

00009424 <__aeabi_uidivmod>:
__aeabi_uidivmod():
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1156
    9424:	e3510000 	cmp	r1, #0
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1157
    9428:	0afffffa 	beq	9418 <__udivsi3+0x1e0>
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1158
    942c:	e92d4003 	push	{r0, r1, lr}
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1159
    9430:	ebffff80 	bl	9238 <__udivsi3>
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1160
    9434:	e8bd4006 	pop	{r1, r2, lr}
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1161
    9438:	e0030092 	mul	r3, r2, r0
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1162
    943c:	e0411003 	sub	r1, r1, r3
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1163
    9440:	e12fff1e 	bx	lr

00009444 <__aeabi_idiv0>:
__aeabi_ldiv0():
/build/gcc-arm-none-eabi-Gl9kT9/gcc-arm-none-eabi-9-2019-q4/build/arm-none-eabi/arm/v5te/hard/libgcc/../../../../../../libgcc/config/arm/lib1funcs.S:1461
    9444:	e12fff1e 	bx	lr

Disassembly of section .ARM.extab:

00009448 <.ARM.extab>:
    9448:	81019b40 	tsthi	r1, r0, asr #22
    944c:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    9450:	00000000 	andeq	r0, r0, r0
    9454:	81019b40 	tsthi	r1, r0, asr #22
    9458:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    945c:	00000000 	andeq	r0, r0, r0
    9460:	81019b41 	tsthi	r1, r1, asr #22
    9464:	8481b0b0 	strhi	fp, [r1], #176	; 0xb0
    9468:	00000000 	andeq	r0, r0, r0
    946c:	81019b40 	tsthi	r1, r0, asr #22
    9470:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    9474:	00000000 	andeq	r0, r0, r0
    9478:	81019b40 	tsthi	r1, r0, asr #22
    947c:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    9480:	00000000 	andeq	r0, r0, r0
    9484:	81019b40 	tsthi	r1, r0, asr #22
    9488:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    948c:	00000000 	andeq	r0, r0, r0
    9490:	81019b46 	tsthi	r1, r6, asr #22
    9494:	b10f8581 	smlabblt	pc, r1, r5, r8	; <UNPREDICTABLE>
    9498:	00000000 	andeq	r0, r0, r0
    949c:	81019b40 	tsthi	r1, r0, asr #22
    94a0:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    94a4:	00000000 	andeq	r0, r0, r0
    94a8:	81019b40 	tsthi	r1, r0, asr #22
    94ac:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    94b0:	00000000 	andeq	r0, r0, r0
    94b4:	81019b40 	tsthi	r1, r0, asr #22
    94b8:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    94bc:	00000000 	andeq	r0, r0, r0
    94c0:	81019b40 	tsthi	r1, r0, asr #22
    94c4:	8480b0b0 	strhi	fp, [r0], #176	; 0xb0
    94c8:	00000000 	andeq	r0, r0, r0

Disassembly of section .ARM.exidx:

000094cc <.ARM.exidx>:
    94cc:	7fffebc8 	svcvc	0x00ffebc8
    94d0:	00000001 	andeq	r0, r0, r1
    94d4:	7ffff4f0 	svcvc	0x00fff4f0
    94d8:	7fffff70 	svcvc	0x00ffff70
    94dc:	7ffff538 	svcvc	0x00fff538
    94e0:	00000001 	andeq	r0, r0, r1
    94e4:	7ffff5e0 	svcvc	0x00fff5e0
    94e8:	7fffff6c 	svcvc	0x00ffff6c
    94ec:	7ffff67c 	svcvc	0x00fff67c
    94f0:	7fffff70 	svcvc	0x00ffff70
    94f4:	7ffff6d0 	svcvc	0x00fff6d0
    94f8:	7fffff74 	svcvc	0x00ffff74
    94fc:	7ffff750 	svcvc	0x00fff750
    9500:	7fffff78 	svcvc	0x00ffff78
    9504:	7ffff7bc 	svcvc	0x00fff7bc
    9508:	7fffff7c 	svcvc	0x00ffff7c
    950c:	7ffff820 	svcvc	0x00fff820
    9510:	00000001 	andeq	r0, r0, r1
    9514:	7ffff8a0 	svcvc	0x00fff8a0
    9518:	7fffff78 	svcvc	0x00ffff78
    951c:	7ffff8d0 	svcvc	0x00fff8d0
    9520:	00000001 	andeq	r0, r0, r1
    9524:	7ffffadc 	svcvc	0x00fffadc
    9528:	7fffff74 	svcvc	0x00ffff74
    952c:	7ffffb48 	svcvc	0x00fffb48
    9530:	7fffff78 	svcvc	0x00ffff78
    9534:	7ffffbcc 	svcvc	0x00fffbcc
    9538:	00000001 	andeq	r0, r0, r1
    953c:	7ffffc4c 	svcvc	0x00fffc4c
    9540:	7fffff74 	svcvc	0x00ffff74
    9544:	7ffffc9c 	svcvc	0x00fffc9c
    9548:	7fffff78 	svcvc	0x00ffff78
    954c:	7ffffcec 	svcvc	0x00fffcec
    9550:	00000001 	andeq	r0, r0, r1

Disassembly of section .rodata:

00009554 <_ZN3halL18Default_Clock_RateE>:
    9554:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

00009558 <_ZN3halL15Peripheral_BaseE>:
    9558:	20000000 	andcs	r0, r0, r0

0000955c <_ZN3halL9GPIO_BaseE>:
    955c:	20200000 	eorcs	r0, r0, r0

00009560 <_ZN3halL14GPIO_Pin_CountE>:
    9560:	00000036 	andeq	r0, r0, r6, lsr r0

00009564 <_ZN3halL8AUX_BaseE>:
    9564:	20215000 	eorcs	r5, r1, r0

00009568 <_ZN3halL25Interrupt_Controller_BaseE>:
    9568:	2000b200 	andcs	fp, r0, r0, lsl #4

0000956c <_ZN3halL10Timer_BaseE>:
    956c:	2000b400 	andcs	fp, r0, r0, lsl #8

00009570 <_ZN3halL18Default_Clock_RateE>:
    9570:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

00009574 <_ZN3halL15Peripheral_BaseE>:
    9574:	20000000 	andcs	r0, r0, r0

00009578 <_ZN3halL9GPIO_BaseE>:
    9578:	20200000 	eorcs	r0, r0, r0

0000957c <_ZN3halL14GPIO_Pin_CountE>:
    957c:	00000036 	andeq	r0, r0, r6, lsr r0

00009580 <_ZN3halL8AUX_BaseE>:
    9580:	20215000 	eorcs	r5, r1, r0

00009584 <_ZN3halL25Interrupt_Controller_BaseE>:
    9584:	2000b200 	andcs	fp, r0, r0, lsl #4

00009588 <_ZN3halL10Timer_BaseE>:
    9588:	2000b400 	andcs	fp, r0, r0, lsl #8

0000958c <_ZN3halL18Default_Clock_RateE>:
    958c:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

00009590 <_ZN3halL15Peripheral_BaseE>:
    9590:	20000000 	andcs	r0, r0, r0

00009594 <_ZN3halL9GPIO_BaseE>:
    9594:	20200000 	eorcs	r0, r0, r0

00009598 <_ZN3halL14GPIO_Pin_CountE>:
    9598:	00000036 	andeq	r0, r0, r6, lsr r0

0000959c <_ZN3halL8AUX_BaseE>:
    959c:	20215000 	eorcs	r5, r1, r0

000095a0 <_ZN3halL25Interrupt_Controller_BaseE>:
    95a0:	2000b200 	andcs	fp, r0, r0, lsl #4

000095a4 <_ZN3halL10Timer_BaseE>:
    95a4:	2000b400 	andcs	fp, r0, r0, lsl #8

000095a8 <_ZN3halL18Default_Clock_RateE>:
    95a8:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

000095ac <_ZN3halL15Peripheral_BaseE>:
    95ac:	20000000 	andcs	r0, r0, r0

000095b0 <_ZN3halL9GPIO_BaseE>:
    95b0:	20200000 	eorcs	r0, r0, r0

000095b4 <_ZN3halL14GPIO_Pin_CountE>:
    95b4:	00000036 	andeq	r0, r0, r6, lsr r0

000095b8 <_ZN3halL8AUX_BaseE>:
    95b8:	20215000 	eorcs	r5, r1, r0

000095bc <_ZN3halL25Interrupt_Controller_BaseE>:
    95bc:	2000b200 	andcs	fp, r0, r0, lsl #4

000095c0 <_ZN3halL10Timer_BaseE>:
    95c0:	2000b400 	andcs	fp, r0, r0, lsl #8

000095c4 <_ZN3halL18Default_Clock_RateE>:
    95c4:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

000095c8 <_ZN3halL15Peripheral_BaseE>:
    95c8:	20000000 	andcs	r0, r0, r0

000095cc <_ZN3halL9GPIO_BaseE>:
    95cc:	20200000 	eorcs	r0, r0, r0

000095d0 <_ZN3halL14GPIO_Pin_CountE>:
    95d0:	00000036 	andeq	r0, r0, r6, lsr r0

000095d4 <_ZN3halL8AUX_BaseE>:
    95d4:	20215000 	eorcs	r5, r1, r0

000095d8 <_ZN3halL25Interrupt_Controller_BaseE>:
    95d8:	2000b200 	andcs	fp, r0, r0, lsl #4

000095dc <_ZN3halL10Timer_BaseE>:
    95dc:	2000b400 	andcs	fp, r0, r0, lsl #8

000095e0 <_ZN3halL18Default_Clock_RateE>:
    95e0:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}

000095e4 <_ZN3halL15Peripheral_BaseE>:
    95e4:	20000000 	andcs	r0, r0, r0

000095e8 <_ZN3halL9GPIO_BaseE>:
    95e8:	20200000 	eorcs	r0, r0, r0

000095ec <_ZN3halL14GPIO_Pin_CountE>:
    95ec:	00000036 	andeq	r0, r0, r6, lsr r0

000095f0 <_ZN3halL8AUX_BaseE>:
    95f0:	20215000 	eorcs	r5, r1, r0

000095f4 <_ZN3halL25Interrupt_Controller_BaseE>:
    95f4:	2000b200 	andcs	fp, r0, r0, lsl #4

000095f8 <_ZN3halL10Timer_BaseE>:
    95f8:	2000b400 	andcs	fp, r0, r0, lsl #8

000095fc <_ZL7ACT_Pin>:
    95fc:	0000002f 	andeq	r0, r0, pc, lsr #32
    9600:	636c6557 	cmnvs	ip, #364904448	; 0x15c00000
    9604:	20656d6f 	rsbcs	r6, r5, pc, ror #26
    9608:	4b206f74 	blmi	8253e0 <_bss_end+0x81bd7c>
    960c:	4f2f5649 	svcmi	0x002f5649
    9610:	50522053 	subspl	r2, r2, r3, asr r0
    9614:	20534f69 	subscs	r4, r3, r9, ror #30
    9618:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
    961c:	0a0d6c65 	beq	3647b8 <_bss_end+0x35b154>
	...

Disassembly of section .data:

00009624 <__CTOR_LIST__>:
    9624:	00008318 	andeq	r8, r0, r8, lsl r3
    9628:	00008818 	andeq	r8, r0, r8, lsl r8
    962c:	00008aa8 	andeq	r8, r0, r8, lsr #21
    9630:	00008d80 	andeq	r8, r0, r0, lsl #27
    9634:	00008fe4 	andeq	r8, r0, r4, ror #31

Disassembly of section .bss:

00009638 <sAUX>:
    9638:	00000000 	andeq	r0, r0, r0

0000963c <sGPIO>:
    963c:	00000000 	andeq	r0, r0, r0

00009640 <sTimer>:
	...

00009648 <sUART0>:
    9648:	00000000 	andeq	r0, r0, r0

0000964c <sInterruptCtl>:
    964c:	00000000 	andeq	r0, r0, r0

00009650 <LED_State>:
	...

Disassembly of section .debug_info:

00000000 <.debug_info>:
       0:	00000126 	andeq	r0, r0, r6, lsr #2
       4:	00000004 	andeq	r0, r0, r4
       8:	01040000 	mrseq	r0, (UNDEF: 4)
       c:	00000014 	andeq	r0, r0, r4, lsl r0
      10:	0000ca04 	andeq	ip, r0, r4, lsl #20
      14:	00013d00 	andeq	r3, r1, r0, lsl #26
      18:	00809400 	addeq	r9, r0, r0, lsl #8
      1c:	0000d800 	andeq	sp, r0, r0, lsl #16
      20:	00000000 	andeq	r0, r0, r0
      24:	01810200 	orreq	r0, r1, r0, lsl #4
      28:	29010000 	stmdbcs	r1, {}	; <UNPREDICTABLE>
      2c:	00816011 	addeq	r6, r1, r1, lsl r0
      30:	00000c00 	andeq	r0, r0, r0, lsl #24
      34:	029c0100 	addseq	r0, ip, #0, 2
      38:	0000016e 	andeq	r0, r0, lr, ror #2
      3c:	48112401 	ldmdami	r1, {r0, sl, sp}
      40:	18000081 	stmdane	r0, {r0, r7}
      44:	01000000 	mrseq	r0, (UNDEF: 0)
      48:	0130029c 	teqeq	r0, ip	; <illegal shifter operand>
      4c:	1f010000 	svcne	0x00010000
      50:	00813011 	addeq	r3, r1, r1, lsl r0
      54:	00001800 	andeq	r1, r0, r0, lsl #16
      58:	029c0100 	addseq	r0, ip, #0, 2
      5c:	00000123 	andeq	r0, r0, r3, lsr #2
      60:	18111a01 	ldmdane	r1, {r0, r9, fp, ip}
      64:	18000081 	stmdane	r0, {r0, r7}
      68:	01000000 	mrseq	r0, (UNDEF: 0)
      6c:	0163039c 			; <UNDEFINED> instruction: 0x0163039c
      70:	00020000 	andeq	r0, r2, r0
      74:	000000bb 	strheq	r0, [r0], -fp
      78:	00011104 	andeq	r1, r1, r4, lsl #2
      7c:	12140100 	andsne	r0, r4, #0, 2
      80:	0000008a 	andeq	r0, r0, sl, lsl #1
      84:	0000bb05 	andeq	fp, r0, r5, lsl #22
      88:	98060000 	stmdals	r6, {}	; <UNPREDICTABLE>
      8c:	01000001 	tsteq	r0, r1
      90:	00c11c04 	sbceq	r1, r1, r4, lsl #24
      94:	fd040000 	stc2	0, cr0, [r4, #-0]
      98:	01000000 	mrseq	r0, (UNDEF: 0)
      9c:	00a8120f 	adceq	r1, r8, pc, lsl #4
      a0:	bb050000 	bllt	1400a8 <_bss_end+0x136a44>
      a4:	00000000 	andeq	r0, r0, r0
      a8:	00000007 	andeq	r0, r0, r7
      ac:	110a0100 	mrsne	r0, (UNDEF: 26)
      b0:	00000105 	andeq	r0, r0, r5, lsl #2
      b4:	0000bb05 	andeq	fp, r0, r5, lsl #22
      b8:	08000000 	stmdaeq	r0, {}	; <UNPREDICTABLE>
      bc:	00008a04 	andeq	r8, r0, r4, lsl #20
      c0:	05080900 	streq	r0, [r8, #-2304]	; 0xfffff700
      c4:	000001a0 	andeq	r0, r0, r0, lsr #3
      c8:	0000780a 	andeq	r7, r0, sl, lsl #16
      cc:	0080f800 	addeq	pc, r0, r0, lsl #16
      d0:	00002000 	andeq	r2, r0, r0
      d4:	e49c0100 	ldr	r0, [ip], #256	; 0x100
      d8:	0b000000 	bleq	e0 <CPSR_IRQ_INHIBIT+0x60>
      dc:	000000bb 	strheq	r0, [r0], -fp
      e0:	00749102 	rsbseq	r9, r4, r2, lsl #2
      e4:	0000960a 	andeq	r9, r0, sl, lsl #12
      e8:	0080cc00 	addeq	ip, r0, r0, lsl #24
      ec:	00002c00 	andeq	r2, r0, r0, lsl #24
      f0:	059c0100 	ldreq	r0, [ip, #256]	; 0x100
      f4:	0c000001 	stceq	0, cr0, [r0], {1}
      f8:	0f010067 	svceq	0x00010067
      fc:	0000bb30 	andeq	fp, r0, r0, lsr fp
     100:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     104:	05040d00 	streq	r0, [r4, #-3328]	; 0xfffff300
     108:	00746e69 	rsbseq	r6, r4, r9, ror #28
     10c:	0000a80e 	andeq	sl, r0, lr, lsl #16
     110:	00809400 	addeq	r9, r0, r0, lsl #8
     114:	00003800 	andeq	r3, r0, r0, lsl #16
     118:	0c9c0100 	ldfeqs	f0, [ip], {0}
     11c:	0a010067 	beq	402c0 <_bss_end+0x36c5c>
     120:	0000bb2f 	andeq	fp, r0, pc, lsr #22
     124:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     128:	04370000 	ldrteq	r0, [r7], #-0
     12c:	00040000 	andeq	r0, r4, r0
     130:	000000d0 	ldrdeq	r0, [r0], -r0	; <UNPREDICTABLE>
     134:	00140104 	andseq	r0, r4, r4, lsl #2
     138:	07040000 	streq	r0, [r4, -r0]
     13c:	3d000003 	stccc	0, cr0, [r0, #-12]
     140:	6c000001 	stcvs	0, cr0, [r0], {1}
     144:	c8000081 	stmdagt	r0, {r0, r7}
     148:	94000001 	strls	r0, [r0], #-1
     14c:	02000000 	andeq	r0, r0, #0
     150:	03020801 	movweq	r0, #10241	; 0x2801
     154:	02020000 	andeq	r0, r2, #0
     158:	0001b905 	andeq	fp, r1, r5, lsl #18
     15c:	05040300 	streq	r0, [r4, #-768]	; 0xfffffd00
     160:	00746e69 	rsbseq	r6, r4, r9, ror #28
     164:	f9080102 			; <UNDEFINED> instruction: 0xf9080102
     168:	02000002 	andeq	r0, r0, #2
     16c:	03910702 	orrseq	r0, r1, #524288	; 0x80000
     170:	15040000 	strne	r0, [r4, #-0]
     174:	04000002 	streq	r0, [r0], #-2
     178:	0059070b 	subseq	r0, r9, fp, lsl #14
     17c:	48050000 	stmdami	r5, {}	; <UNPREDICTABLE>
     180:	02000000 	andeq	r0, r0, #0
     184:	16fa0704 	ldrbtne	r0, [sl], r4, lsl #14
     188:	59050000 	stmdbpl	r5, {}	; <UNPREDICTABLE>
     18c:	06000000 	streq	r0, [r0], -r0
     190:	006c6168 	rsbeq	r6, ip, r8, ror #2
     194:	a10b0702 	tstge	fp, r2, lsl #14
     198:	07000001 	streq	r0, [r0, -r1]
     19c:	00000484 	andeq	r0, r0, r4, lsl #9
     1a0:	60190902 	andsvs	r0, r9, r2, lsl #18
     1a4:	80000000 	andhi	r0, r0, r0
     1a8:	070ee6b2 			; <UNDEFINED> instruction: 0x070ee6b2
     1ac:	000002c4 	andeq	r0, r0, r4, asr #5
     1b0:	ad1a0c02 	ldcge	12, cr0, [sl, #-8]
     1b4:	00000001 	andeq	r0, r0, r1
     1b8:	07200000 	streq	r0, [r0, -r0]!
     1bc:	00000354 	andeq	r0, r0, r4, asr r3
     1c0:	ad1a0f02 	ldcge	15, cr0, [sl, #-8]
     1c4:	00000001 	andeq	r0, r0, r1
     1c8:	08202000 	stmdaeq	r0!, {sp}
     1cc:	000003cf 	andeq	r0, r0, pc, asr #7
     1d0:	54151202 	ldrpl	r1, [r5], #-514	; 0xfffffdfe
     1d4:	36000000 	strcc	r0, [r0], -r0
     1d8:	00044607 	andeq	r4, r4, r7, lsl #12
     1dc:	1a440200 	bne	11009e4 <_bss_end+0x10f7380>
     1e0:	000001ad 	andeq	r0, r0, sp, lsr #3
     1e4:	20215000 	eorcs	r5, r1, r0
     1e8:	0001d809 	andeq	sp, r1, r9, lsl #16
     1ec:	33040500 	movwcc	r0, #17664	; 0x4500
     1f0:	02000000 	andeq	r0, r0, #0
     1f4:	015b0d46 	cmpeq	fp, r6, asr #26
     1f8:	490a0000 	stmdbmi	sl, {}	; <UNPREDICTABLE>
     1fc:	00005152 	andeq	r5, r0, r2, asr r1
     200:	00021e0b 	andeq	r1, r2, fp, lsl #28
     204:	660b0100 	strvs	r0, [fp], -r0, lsl #2
     208:	10000004 	andne	r0, r0, r4
     20c:	0003460b 	andeq	r4, r3, fp, lsl #12
     210:	780b1100 	stmdavc	fp, {r8, ip}
     214:	12000003 	andne	r0, r0, #3
     218:	0003ac0b 	andeq	sl, r3, fp, lsl #24
     21c:	4d0b1300 	stcmi	3, cr1, [fp, #-0]
     220:	14000003 	strne	r0, [r0], #-3
     224:	0004750b 	andeq	r7, r4, fp, lsl #10
     228:	3f0b1500 	svccc	0x000b1500
     22c:	16000004 	strne	r0, [r0], -r4
     230:	0004c00b 	andeq	ip, r4, fp
     234:	7f0b1700 	svcvc	0x000b1700
     238:	18000003 	stmdane	r0, {r0, r1}
     23c:	00044f0b 	andeq	r4, r4, fp, lsl #30
     240:	bd0b1900 	vstrlt.16	s2, [fp, #-0]	; <UNPREDICTABLE>
     244:	1a000003 	bne	258 <CPSR_IRQ_INHIBIT+0x1d8>
     248:	0002ae0b 	andeq	sl, r2, fp, lsl #28
     24c:	b90b2000 	stmdblt	fp, {sp}
     250:	21000002 	tstcs	r0, r2
     254:	0003c50b 	andeq	ip, r3, fp, lsl #10
     258:	920b2200 	andls	r2, fp, #0, 4
     25c:	24000002 	strcs	r0, [r0], #-2
     260:	0003b30b 	andeq	fp, r3, fp, lsl #6
     264:	e30b2500 	movw	r2, #46336	; 0xb500
     268:	30000002 	andcc	r0, r0, r2
     26c:	0002ee0b 	andeq	lr, r2, fp, lsl #28
     270:	cd0b3100 	stfgts	f3, [fp, #-0]
     274:	32000001 	andcc	r0, r0, #1
     278:	0003a40b 	andeq	sl, r3, fp, lsl #8
     27c:	c30b3400 	movwgt	r3, #46080	; 0xb400
     280:	35000001 	strcc	r0, [r0, #-1]
     284:	024e0900 	subeq	r0, lr, #0, 18
     288:	04050000 	streq	r0, [r5], #-0
     28c:	00000033 	andeq	r0, r0, r3, lsr r0
     290:	800d6c02 	andhi	r6, sp, r2, lsl #24
     294:	0b000001 	bleq	2a0 <CPSR_IRQ_INHIBIT+0x220>
     298:	0000046c 	andeq	r0, r0, ip, ror #8
     29c:	03870b00 	orreq	r0, r7, #0, 22
     2a0:	0b010000 	bleq	402a8 <_bss_end+0x36c44>
     2a4:	0000038c 	andeq	r0, r0, ip, lsl #7
     2a8:	18070002 	stmdane	r7, {r1}
     2ac:	02000004 	andeq	r0, r0, #4
     2b0:	01ad1a73 			; <UNDEFINED> instruction: 0x01ad1a73
     2b4:	b2000000 	andlt	r0, r0, #0
     2b8:	ae072000 	cdpge	0, 0, cr2, cr7, cr0, {0}
     2bc:	02000001 	andeq	r0, r0, #1
     2c0:	01ad1aa6 			; <UNDEFINED> instruction: 0x01ad1aa6
     2c4:	b4000000 	strlt	r0, [r0], #-0
     2c8:	0c002000 	stceq	0, cr2, [r0], {-0}
     2cc:	00000071 	andeq	r0, r0, r1, ror r0
     2d0:	f5070402 			; <UNDEFINED> instruction: 0xf5070402
     2d4:	05000016 	streq	r0, [r0, #-22]	; 0xffffffea
     2d8:	000001a6 	andeq	r0, r0, r6, lsr #3
     2dc:	0000810c 	andeq	r8, r0, ip, lsl #2
     2e0:	00910c00 	addseq	r0, r1, r0, lsl #24
     2e4:	a10c0000 	mrsge	r0, (UNDEF: 12)
     2e8:	0c000000 	stceq	0, cr0, [r0], {-0}
     2ec:	000000ae 	andeq	r0, r0, lr, lsr #1
     2f0:	0001800c 	andeq	r8, r1, ip
     2f4:	01900c00 	orrseq	r0, r0, r0, lsl #24
     2f8:	270d0000 	strcs	r0, [sp, -r0]
     2fc:	0400000c 	streq	r0, [r0], #-12
     300:	94070603 	strls	r0, [r7], #-1539	; 0xfffff9fd
     304:	0e000002 	cdpeq	0, 0, cr0, cr0, cr2, {0}
     308:	000001d7 	ldrdeq	r0, [r0], -r7
     30c:	9a1d0a03 	bls	742b20 <_bss_end+0x7394bc>
     310:	00000002 	andeq	r0, r0, r2
     314:	000c270f 	andeq	r2, ip, pc, lsl #14
     318:	090d0300 	stmdbeq	sp, {r8, r9}
     31c:	0000025e 	andeq	r0, r0, lr, asr r2
     320:	0000029f 	muleq	r0, pc, r2	; <UNPREDICTABLE>
     324:	00020301 	andeq	r0, r2, r1, lsl #6
     328:	00020e00 	andeq	r0, r2, r0, lsl #28
     32c:	029f1000 	addseq	r1, pc, #0
     330:	59110000 	ldmdbpl	r1, {}	; <UNPREDICTABLE>
     334:	00000000 	andeq	r0, r0, r0
     338:	000f8012 	andeq	r8, pc, r2, lsl r0	; <UNPREDICTABLE>
     33c:	0e100300 	cdpeq	3, 1, cr0, cr0, cr0, {0}
     340:	00000226 	andeq	r0, r0, r6, lsr #4
     344:	00022301 	andeq	r2, r2, r1, lsl #6
     348:	00022e00 	andeq	r2, r2, r0, lsl #28
     34c:	029f1000 	addseq	r1, pc, #0
     350:	5b110000 	blpl	440358 <_bss_end+0x436cf4>
     354:	00000001 	andeq	r0, r0, r1
     358:	000e8012 	andeq	r8, lr, r2, lsl r0
     35c:	0e120300 	cdpeq	3, 1, cr0, cr2, cr0, {0}
     360:	00000497 	muleq	r0, r7, r4
     364:	00024301 	andeq	r4, r2, r1, lsl #6
     368:	00024e00 	andeq	r4, r2, r0, lsl #28
     36c:	029f1000 	addseq	r1, pc, #0
     370:	5b110000 	blpl	440378 <_bss_end+0x436d14>
     374:	00000001 	andeq	r0, r0, r1
     378:	00043212 	andeq	r3, r4, r2, lsl r2
     37c:	0e150300 	cdpeq	3, 1, cr0, cr5, cr0, {0}
     380:	0000026b 	andeq	r0, r0, fp, ror #4
     384:	00026301 	andeq	r6, r2, r1, lsl #6
     388:	00027300 	andeq	r7, r2, r0, lsl #6
     38c:	029f1000 	addseq	r1, pc, #0
     390:	be110000 	cdplt	0, 1, cr0, cr1, cr0, {0}
     394:	11000000 	mrsne	r0, (UNDEF: 0)
     398:	00000048 	andeq	r0, r0, r8, asr #32
     39c:	036b1300 	cmneq	fp, #0, 6
     3a0:	17030000 	strne	r0, [r3, -r0]
     3a4:	0001e012 	andeq	lr, r1, r2, lsl r0
     3a8:	00004800 	andeq	r4, r0, r0, lsl #16
     3ac:	02880100 	addeq	r0, r8, #0, 2
     3b0:	9f100000 	svcls	0x00100000
     3b4:	11000002 	tstne	r0, r2
     3b8:	000000be 	strheq	r0, [r0], -lr
     3bc:	04140000 	ldreq	r0, [r4], #-0
     3c0:	00000059 	andeq	r0, r0, r9, asr r0
     3c4:	00029405 	andeq	r9, r2, r5, lsl #8
     3c8:	d0041400 	andle	r1, r4, r0, lsl #8
     3cc:	05000001 	streq	r0, [r0, #-1]
     3d0:	0000029f 	muleq	r0, pc, r2	; <UNPREDICTABLE>
     3d4:	0002a915 	andeq	sl, r2, r5, lsl r9
     3d8:	0d1a0300 	ldceq	3, cr0, [sl, #-0]
     3dc:	000001d0 	ldrdeq	r0, [r0], -r0	; <UNPREDICTABLE>
     3e0:	0002aa16 	andeq	sl, r2, r6, lsl sl
     3e4:	06030100 	streq	r0, [r3], -r0, lsl #2
     3e8:	96380305 	ldrtls	r0, [r8], -r5, lsl #6
     3ec:	9a170000 	bls	5c03f4 <_bss_end+0x5b6d90>
     3f0:	18000002 	stmdane	r0, {r1}
     3f4:	1c000083 	stcne	0, cr0, [r0], {131}	; 0x83
     3f8:	01000000 	mrseq	r0, (UNDEF: 0)
     3fc:	03de189c 	bicseq	r1, lr, #156, 16	; 0x9c0000
     400:	82c40000 	sbchi	r0, r4, #0
     404:	00540000 	subseq	r0, r4, r0
     408:	9c010000 	stcls	0, cr0, [r1], {-0}
     40c:	00000305 	andeq	r0, r0, r5, lsl #6
     410:	0002d419 	andeq	sp, r2, r9, lsl r4
     414:	011d0100 	tsteq	sp, r0, lsl #2
     418:	00000033 	andeq	r0, r0, r3, lsr r0
     41c:	19749102 	ldmdbne	r4!, {r1, r8, ip, pc}^
     420:	00000408 	andeq	r0, r0, r8, lsl #8
     424:	33011d01 	movwcc	r1, #7425	; 0x1d01
     428:	02000000 	andeq	r0, r0, #0
     42c:	1a007091 	bne	1c678 <_bss_end+0x13014>
     430:	00000273 	andeq	r0, r0, r3, ror r2
     434:	1f0a1a01 	svcne	0x000a1a01
     438:	88000003 	stmdahi	r0, {r0, r1}
     43c:	3c000082 	stccc	0, cr0, [r0], {130}	; 0x82
     440:	01000000 	mrseq	r0, (UNDEF: 0)
     444:	00033b9c 	muleq	r3, ip, fp
     448:	04131b00 	ldreq	r1, [r3], #-2816	; 0xfffff500
     44c:	02a50000 	adceq	r0, r5, #0
     450:	91020000 	mrsls	r0, (UNDEF: 2)
     454:	047c1974 	ldrbteq	r1, [ip], #-2420	; 0xfffff68c
     458:	1a010000 	bne	40460 <_bss_end+0x36dfc>
     45c:	0000be2a 	andeq	fp, r0, sl, lsr #28
     460:	70910200 	addsvc	r0, r1, r0, lsl #4
     464:	024e1c00 	subeq	r1, lr, #0, 24
     468:	06010000 	streq	r0, [r1], -r0
     46c:	00000354 	andeq	r0, r0, r4, asr r3
     470:	00008244 	andeq	r8, r0, r4, asr #4
     474:	00000044 	andeq	r0, r0, r4, asr #32
     478:	037f9c01 	cmneq	pc, #256	; 0x100
     47c:	131b0000 	tstne	fp, #0
     480:	a5000004 	strge	r0, [r0, #-4]
     484:	02000002 	andeq	r0, r0, #2
     488:	7c197491 	cfldrsvc	mvf7, [r9], {145}	; 0x91
     48c:	01000004 	tsteq	r0, r4
     490:	00be2615 	adcseq	r2, lr, r5, lsl r6
     494:	91020000 	mrsls	r0, (UNDEF: 2)
     498:	02061970 	andeq	r1, r6, #112, 18	; 0x1c0000
     49c:	15010000 	strne	r0, [r1, #-0]
     4a0:	00004838 	andeq	r4, r0, r8, lsr r8
     4a4:	6c910200 	lfmvs	f0, 4, [r1], {0}
     4a8:	022e1d00 	eoreq	r1, lr, #0, 26
     4ac:	10010000 	andne	r0, r1, r0
     4b0:	00039906 	andeq	r9, r3, r6, lsl #18
     4b4:	0081f000 	addeq	pc, r1, r0
     4b8:	00005400 	andeq	r5, r0, r0, lsl #8
     4bc:	b59c0100 	ldrlt	r0, [ip, #256]	; 0x100
     4c0:	1b000003 	blne	4d4 <CPSR_IRQ_INHIBIT+0x454>
     4c4:	00000413 	andeq	r0, r0, r3, lsl r4
     4c8:	000002a5 	andeq	r0, r0, r5, lsr #5
     4cc:	19749102 	ldmdbne	r4!, {r1, r8, ip, pc}^
     4d0:	00000457 	andeq	r0, r0, r7, asr r4
     4d4:	5b291001 	blpl	a444e0 <_bss_end+0xa3ae7c>
     4d8:	02000001 	andeq	r0, r0, #1
     4dc:	1d007091 	stcne	0, cr7, [r0, #-580]	; 0xfffffdbc
     4e0:	0000020e 	andeq	r0, r0, lr, lsl #4
     4e4:	cf060b01 	svcgt	0x00060b01
     4e8:	a0000003 	andge	r0, r0, r3
     4ec:	50000081 	andpl	r0, r0, r1, lsl #1
     4f0:	01000000 	mrseq	r0, (UNDEF: 0)
     4f4:	0003eb9c 	muleq	r3, ip, fp
     4f8:	04131b00 	ldreq	r1, [r3], #-2816	; 0xfffff500
     4fc:	02a50000 	adceq	r0, r5, #0
     500:	91020000 	mrsls	r0, (UNDEF: 2)
     504:	04571974 	ldrbeq	r1, [r7], #-2420	; 0xfffff68c
     508:	0b010000 	bleq	40510 <_bss_end+0x36eac>
     50c:	00015b28 	andeq	r5, r1, r8, lsr #22
     510:	70910200 	addsvc	r0, r1, r0, lsl #4
     514:	01ea1e00 	mvneq	r1, r0, lsl #28
     518:	05010000 	streq	r0, [r1, #-0]
     51c:	0003fc01 	andeq	pc, r3, r1, lsl #24
     520:	04120000 	ldreq	r0, [r2], #-0
     524:	131f0000 	tstne	pc, #0
     528:	a5000004 	strge	r0, [r0, #-4]
     52c:	20000002 	andcs	r0, r0, r2
     530:	0000020c 	andeq	r0, r0, ip, lsl #4
     534:	59190501 	ldmdbpl	r9, {r0, r8, sl}
     538:	00000000 	andeq	r0, r0, r0
     53c:	0003eb21 	andeq	lr, r3, r1, lsr #22
     540:	00035e00 	andeq	r5, r3, r0, lsl #28
     544:	00042900 	andeq	r2, r4, r0, lsl #18
     548:	00816c00 	addeq	r6, r1, r0, lsl #24
     54c:	00003400 	andeq	r3, r0, r0, lsl #8
     550:	229c0100 	addscs	r0, ip, #0, 2
     554:	000003fc 	strdeq	r0, [r0], -ip
     558:	22749102 	rsbscs	r9, r4, #-2147483648	; 0x80000000
     55c:	00000405 	andeq	r0, r0, r5, lsl #8
     560:	00709102 	rsbseq	r9, r0, r2, lsl #2
     564:	0006c900 	andeq	ip, r6, r0, lsl #18
     568:	fd000400 	stc2	4, cr0, [r0, #-0]
     56c:	04000002 	streq	r0, [r0], #-2
     570:	00001401 	andeq	r1, r0, r1, lsl #8
     574:	06360400 	ldrteq	r0, [r6], -r0, lsl #8
     578:	013d0000 	teqeq	sp, r0
     57c:	83340000 	teqhi	r4, #0
     580:	05000000 	streq	r0, [r0, #-0]
     584:	01fc0000 	mvnseq	r0, r0
     588:	01020000 	mrseq	r0, (UNDEF: 2)
     58c:	00030208 	andeq	r0, r3, r8, lsl #4
     590:	05020200 	streq	r0, [r2, #-512]	; 0xfffffe00
     594:	000001b9 			; <UNDEFINED> instruction: 0x000001b9
     598:	69050403 	stmdbvs	r5, {r0, r1, sl}
     59c:	0400746e 	streq	r7, [r0], #-1134	; 0xfffffb92
     5a0:	000005d0 	ldrdeq	r0, [r0], -r0	; <UNPREDICTABLE>
     5a4:	46070902 	strmi	r0, [r7], -r2, lsl #18
     5a8:	02000000 	andeq	r0, r0, #0
     5ac:	02f90801 	rscseq	r0, r9, #65536	; 0x10000
     5b0:	02020000 	andeq	r0, r2, #0
     5b4:	00039107 	andeq	r9, r3, r7, lsl #2
     5b8:	02150400 	andseq	r0, r5, #0, 8
     5bc:	0b020000 	bleq	805c4 <_bss_end+0x76f60>
     5c0:	00006507 	andeq	r6, r0, r7, lsl #10
     5c4:	00540500 	subseq	r0, r4, r0, lsl #10
     5c8:	04020000 	streq	r0, [r2], #-0
     5cc:	0016fa07 	andseq	pc, r6, r7, lsl #20
     5d0:	00650500 	rsbeq	r0, r5, r0, lsl #10
     5d4:	68060000 	stmdavs	r6, {}	; <UNPREDICTABLE>
     5d8:	03006c61 	movweq	r6, #3169	; 0xc61
     5dc:	01ac0b07 			; <UNDEFINED> instruction: 0x01ac0b07
     5e0:	84070000 	strhi	r0, [r7], #-0
     5e4:	03000004 	movweq	r0, #4
     5e8:	006c1909 	rsbeq	r1, ip, r9, lsl #18
     5ec:	b2800000 	addlt	r0, r0, #0
     5f0:	c4070ee6 	strgt	r0, [r7], #-3814	; 0xfffff11a
     5f4:	03000002 	movweq	r0, #2
     5f8:	01b81a0c 			; <UNDEFINED> instruction: 0x01b81a0c
     5fc:	00000000 	andeq	r0, r0, r0
     600:	54072000 	strpl	r2, [r7], #-0
     604:	03000003 	movweq	r0, #3
     608:	01b81a0f 			; <UNDEFINED> instruction: 0x01b81a0f
     60c:	00000000 	andeq	r0, r0, r0
     610:	cf082020 	svcgt	0x00082020
     614:	03000003 	movweq	r0, #3
     618:	00601512 	rsbeq	r1, r0, r2, lsl r5
     61c:	09360000 	ldmdbeq	r6!, {}	; <UNPREDICTABLE>
     620:	0000076e 	andeq	r0, r0, lr, ror #14
     624:	00330405 	eorseq	r0, r3, r5, lsl #8
     628:	15030000 	strne	r0, [r3, #-0]
     62c:	00017b0d 	andeq	r7, r1, sp, lsl #22
     630:	05080a00 	streq	r0, [r8, #-2560]	; 0xfffff600
     634:	0a000000 	beq	63c <CPSR_IRQ_INHIBIT+0x5bc>
     638:	00000510 	andeq	r0, r0, r0, lsl r5
     63c:	05180a01 	ldreq	r0, [r8, #-2561]	; 0xfffff5ff
     640:	0a020000 	beq	80648 <_bss_end+0x76fe4>
     644:	00000520 	andeq	r0, r0, r0, lsr #10
     648:	05280a03 	streq	r0, [r8, #-2563]!	; 0xfffff5fd
     64c:	0a040000 	beq	100654 <_bss_end+0xf6ff0>
     650:	00000530 	andeq	r0, r0, r0, lsr r5
     654:	04fa0a05 	ldrbteq	r0, [sl], #2565	; 0xa05
     658:	0a070000 	beq	1c0660 <_bss_end+0x1b6ffc>
     65c:	00000501 	andeq	r0, r0, r1, lsl #10
     660:	08550a08 	ldmdaeq	r5, {r3, r9, fp}^
     664:	0a0a0000 	beq	28066c <_bss_end+0x277008>
     668:	0000071f 	andeq	r0, r0, pc, lsl r7
     66c:	07b50a0b 	ldreq	r0, [r5, fp, lsl #20]!
     670:	0a0d0000 	beq	340678 <_bss_end+0x337014>
     674:	000007bc 			; <UNDEFINED> instruction: 0x000007bc
     678:	05d80a0e 	ldrbeq	r0, [r8, #2574]	; 0xa0e
     67c:	0a100000 	beq	400684 <_bss_end+0x3f7020>
     680:	000005df 	ldrdeq	r0, [r0], -pc	; <UNPREDICTABLE>
     684:	05530a11 	ldrbeq	r0, [r3, #-2577]	; 0xfffff5ef
     688:	0a130000 	beq	4c0690 <_bss_end+0x4b702c>
     68c:	0000055a 	andeq	r0, r0, sl, asr r5
     690:	08250a14 	stmdaeq	r5!, {r2, r4, r9, fp}
     694:	0a160000 	beq	58069c <_bss_end+0x577038>
     698:	00000538 	andeq	r0, r0, r8, lsr r5
     69c:	07360a17 			; <UNDEFINED> instruction: 0x07360a17
     6a0:	0a190000 	beq	6406a8 <_bss_end+0x637044>
     6a4:	0000073d 	andeq	r0, r0, sp, lsr r7
     6a8:	05f70a1a 	ldrbeq	r0, [r7, #2586]!	; 0xa1a
     6ac:	0a1c0000 	beq	7006b4 <_bss_end+0x6f7050>
     6b0:	00000753 	andeq	r0, r0, r3, asr r7
     6b4:	07260a1d 			; <UNDEFINED> instruction: 0x07260a1d
     6b8:	0a1f0000 	beq	7c06c0 <_bss_end+0x7b705c>
     6bc:	0000072e 	andeq	r0, r0, lr, lsr #14
     6c0:	06cb0a20 	strbeq	r0, [fp], r0, lsr #20
     6c4:	0a220000 	beq	8806cc <_bss_end+0x877068>
     6c8:	000006d3 	ldrdeq	r0, [r0], -r3
     6cc:	06720a23 	ldrbteq	r0, [r2], -r3, lsr #20
     6d0:	0a250000 	beq	9406d8 <_bss_end+0x937074>
     6d4:	0000053f 	andeq	r0, r0, pc, lsr r5
     6d8:	05490a26 	strbeq	r0, [r9, #-2598]	; 0xfffff5da
     6dc:	00270000 	eoreq	r0, r7, r0
     6e0:	00044607 	andeq	r4, r4, r7, lsl #12
     6e4:	1a440300 	bne	11012ec <_bss_end+0x10f7c88>
     6e8:	000001b8 			; <UNDEFINED> instruction: 0x000001b8
     6ec:	20215000 	eorcs	r5, r1, r0
     6f0:	00041807 	andeq	r1, r4, r7, lsl #16
     6f4:	1a730300 	bne	1cc12fc <_bss_end+0x1cb7c98>
     6f8:	000001b8 			; <UNDEFINED> instruction: 0x000001b8
     6fc:	2000b200 	andcs	fp, r0, r0, lsl #4
     700:	0001ae07 	andeq	sl, r1, r7, lsl #28
     704:	1aa60300 	bne	fe98130c <_bss_end+0xfe977ca8>
     708:	000001b8 			; <UNDEFINED> instruction: 0x000001b8
     70c:	2000b400 	andcs	fp, r0, r0, lsl #8
     710:	007d0b00 	rsbseq	r0, sp, r0, lsl #22
     714:	04020000 	streq	r0, [r2], #-0
     718:	0016f507 	andseq	pc, r6, r7, lsl #10
     71c:	01b10500 			; <UNDEFINED> instruction: 0x01b10500
     720:	8d0b0000 	stchi	0, cr0, [fp, #-0]
     724:	0b000000 	bleq	72c <CPSR_IRQ_INHIBIT+0x6ac>
     728:	0000009d 	muleq	r0, sp, r0
     72c:	0000ad0b 	andeq	sl, r0, fp, lsl #26
     730:	017b0b00 	cmneq	fp, r0, lsl #22
     734:	8b0b0000 	blhi	2c073c <_bss_end+0x2b70d8>
     738:	0b000001 	bleq	744 <CPSR_IRQ_INHIBIT+0x6c4>
     73c:	0000019b 	muleq	r0, fp, r1
     740:	0007a009 	andeq	sl, r7, r9
     744:	3a010700 	bcc	4234c <_bss_end+0x38ce8>
     748:	04000000 	streq	r0, [r0], #-0
     74c:	02240c06 	eoreq	r0, r4, #1536	; 0x600
     750:	1f0a0000 	svcne	0x000a0000
     754:	00000008 	andeq	r0, r0, r8
     758:	0008300a 	andeq	r3, r8, sl
     75c:	4f0a0100 	svcmi	0x000a0100
     760:	02000008 	andeq	r0, r0, #8
     764:	0008490a 	andeq	r4, r8, sl, lsl #18
     768:	370a0300 	strcc	r0, [sl, -r0, lsl #6]
     76c:	04000008 	streq	r0, [r0], #-8
     770:	00083d0a 	andeq	r3, r8, sl, lsl #26
     774:	af0a0500 	svcge	0x000a0500
     778:	06000007 	streq	r0, [r0], -r7
     77c:	0008430a 	andeq	r4, r8, sl, lsl #6
     780:	eb0a0700 	bl	282388 <_bss_end+0x278d24>
     784:	08000005 	stmdaeq	r0, {r0, r2}
     788:	06150c00 	ldreq	r0, [r5], -r0, lsl #24
     78c:	04040000 	streq	r0, [r4], #-0
     790:	0385071a 	orreq	r0, r5, #6815744	; 0x680000
     794:	b30d0000 	movwlt	r0, #53248	; 0xd000
     798:	04000005 	streq	r0, [r0], #-5
     79c:	0390171e 	orrseq	r1, r0, #7864320	; 0x780000
     7a0:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
     7a4:	0000075a 	andeq	r0, r0, sl, asr r7
     7a8:	83082204 	movwhi	r2, #33284	; 0x8204
     7ac:	95000005 	strls	r0, [r0, #-5]
     7b0:	02000003 	andeq	r0, r0, #3
     7b4:	00000257 	andeq	r0, r0, r7, asr r2
     7b8:	0000026c 	andeq	r0, r0, ip, ror #4
     7bc:	00039c0f 	andeq	r9, r3, pc, lsl #24
     7c0:	00541000 	subseq	r1, r4, r0
     7c4:	a7100000 	ldrge	r0, [r0, -r0]
     7c8:	10000003 	andne	r0, r0, r3
     7cc:	000003a7 	andeq	r0, r0, r7, lsr #7
     7d0:	080c0e00 	stmdaeq	ip, {r9, sl, fp}
     7d4:	24040000 	strcs	r0, [r4], #-0
     7d8:	0006db08 	andeq	sp, r6, r8, lsl #22
     7dc:	00039500 	andeq	r9, r3, r0, lsl #10
     7e0:	02850200 	addeq	r0, r5, #0, 4
     7e4:	029a0000 	addseq	r0, sl, #0
     7e8:	9c0f0000 	stcls	0, cr0, [pc], {-0}
     7ec:	10000003 	andne	r0, r0, r3
     7f0:	00000054 	andeq	r0, r0, r4, asr r0
     7f4:	0003a710 	andeq	sl, r3, r0, lsl r7
     7f8:	03a71000 			; <UNDEFINED> instruction: 0x03a71000
     7fc:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
     800:	00000623 	andeq	r0, r0, r3, lsr #12
     804:	dd082604 	stcle	6, cr2, [r8, #-16]
     808:	95000007 	strls	r0, [r0, #-7]
     80c:	02000003 	andeq	r0, r0, #3
     810:	000002b3 			; <UNDEFINED> instruction: 0x000002b3
     814:	000002c8 	andeq	r0, r0, r8, asr #5
     818:	00039c0f 	andeq	r9, r3, pc, lsl #24
     81c:	00541000 	subseq	r1, r4, r0
     820:	a7100000 	ldrge	r0, [r0, -r0]
     824:	10000003 	andne	r0, r0, r3
     828:	000003a7 	andeq	r0, r0, r7, lsr #7
     82c:	06780e00 	ldrbteq	r0, [r8], -r0, lsl #28
     830:	28040000 	stmdacs	r4, {}	; <UNPREDICTABLE>
     834:	0004cb08 	andeq	ip, r4, r8, lsl #22
     838:	00039500 	andeq	r9, r3, r0, lsl #10
     83c:	02e10200 	rsceq	r0, r1, #0, 4
     840:	02f60000 	rscseq	r0, r6, #0
     844:	9c0f0000 	stcls	0, cr0, [pc], {-0}
     848:	10000003 	andne	r0, r0, r3
     84c:	00000054 	andeq	r0, r0, r4, asr r0
     850:	0003a710 	andeq	sl, r3, r0, lsl r7
     854:	03a71000 			; <UNDEFINED> instruction: 0x03a71000
     858:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
     85c:	00000615 	andeq	r0, r0, r5, lsl r6
     860:	b9032b04 	stmdblt	r3, {r2, r8, r9, fp, sp}
     864:	ad000005 	stcge	0, cr0, [r0, #-20]	; 0xffffffec
     868:	01000003 	tsteq	r0, r3
     86c:	0000030f 	andeq	r0, r0, pc, lsl #6
     870:	0000031a 	andeq	r0, r0, sl, lsl r3
     874:	0003ad0f 	andeq	sl, r3, pc, lsl #26
     878:	00651000 	rsbeq	r1, r5, r0
     87c:	11000000 	mrsne	r0, (UNDEF: 0)
     880:	000007c3 	andeq	r0, r0, r3, asr #15
     884:	77082e04 	strvc	r2, [r8, -r4, lsl #28]
     888:	01000007 	tsteq	r0, r7
     88c:	0000032f 	andeq	r0, r0, pc, lsr #6
     890:	0000033f 	andeq	r0, r0, pc, lsr r3
     894:	0003ad0f 	andeq	sl, r3, pc, lsl #26
     898:	00541000 	subseq	r1, r4, r0
     89c:	db100000 	blle	4008a4 <_bss_end+0x3f7240>
     8a0:	00000001 	andeq	r0, r0, r1
     8a4:	0006030e 	andeq	r0, r6, lr, lsl #6
     8a8:	12300400 	eorsne	r0, r0, #0, 8
     8ac:	0000068b 	andeq	r0, r0, fp, lsl #13
     8b0:	000001db 	ldrdeq	r0, [r0], -fp
     8b4:	00035801 	andeq	r5, r3, r1, lsl #16
     8b8:	00036300 	andeq	r6, r3, r0, lsl #6
     8bc:	039c0f00 	orrseq	r0, ip, #0, 30
     8c0:	54100000 	ldrpl	r0, [r0], #-0
     8c4:	00000000 	andeq	r0, r0, r0
     8c8:	00082c12 	andeq	r2, r8, r2, lsl ip
     8cc:	08330400 	ldmdaeq	r3!, {sl}
     8d0:	00000561 	andeq	r0, r0, r1, ror #10
     8d4:	00037401 	andeq	r7, r3, r1, lsl #8
     8d8:	03ad0f00 			; <UNDEFINED> instruction: 0x03ad0f00
     8dc:	54100000 	ldrpl	r0, [r0], #-0
     8e0:	10000000 	andne	r0, r0, r0
     8e4:	00000395 	muleq	r0, r5, r3
     8e8:	24050000 	strcs	r0, [r5], #-0
     8ec:	13000002 	movwne	r0, #2
     8f0:	00006504 	andeq	r6, r0, r4, lsl #10
     8f4:	038a0500 	orreq	r0, sl, #0, 10
     8f8:	01020000 	mrseq	r0, (UNDEF: 2)
     8fc:	0005e602 	andeq	lr, r5, r2, lsl #12
     900:	85041300 	strhi	r1, [r4, #-768]	; 0xfffffd00
     904:	05000003 	streq	r0, [r0, #-3]
     908:	0000039c 	muleq	r0, ip, r3
     90c:	00540414 	subseq	r0, r4, r4, lsl r4
     910:	04130000 	ldreq	r0, [r3], #-0
     914:	00000224 	andeq	r0, r0, r4, lsr #4
     918:	0003ad05 	andeq	sl, r3, r5, lsl #26
     91c:	07191500 	ldreq	r1, [r9, -r0, lsl #10]
     920:	37040000 	strcc	r0, [r4, -r0]
     924:	00022416 	andeq	r2, r2, r6, lsl r4
     928:	03b81600 			; <UNDEFINED> instruction: 0x03b81600
     92c:	04010000 	streq	r0, [r1], #-0
     930:	3c03050f 	cfstr32cc	mvfx0, [r3], {15}
     934:	17000096 			; <UNDEFINED> instruction: 0x17000096
     938:	0000070a 	andeq	r0, r0, sl, lsl #14
     93c:	00008818 	andeq	r8, r0, r8, lsl r8
     940:	0000001c 	andeq	r0, r0, ip, lsl r0
     944:	de189c01 	cdple	12, 1, cr9, cr8, cr1, {0}
     948:	c4000003 	strgt	r0, [r0], #-3
     94c:	54000087 	strpl	r0, [r0], #-135	; 0xffffff79
     950:	01000000 	mrseq	r0, (UNDEF: 0)
     954:	0004139c 	muleq	r4, ip, r3
     958:	02d41900 	sbcseq	r1, r4, #0, 18
     95c:	5b010000 	blpl	40964 <_bss_end+0x37300>
     960:	00003301 	andeq	r3, r0, r1, lsl #6
     964:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     968:	00040819 	andeq	r0, r4, r9, lsl r8
     96c:	015b0100 	cmpeq	fp, r0, lsl #2
     970:	00000033 	andeq	r0, r0, r3, lsr r0
     974:	00709102 	rsbseq	r9, r0, r2, lsl #2
     978:	0003631a 	andeq	r6, r3, sl, lsl r3
     97c:	06540100 	ldrbeq	r0, [r4], -r0, lsl #2
     980:	0000042d 	andeq	r0, r0, sp, lsr #8
     984:	000086ec 	andeq	r8, r0, ip, ror #13
     988:	000000d8 	ldrdeq	r0, [r0], -r8
     98c:	04769c01 	ldrbteq	r9, [r6], #-3073	; 0xfffff3ff
     990:	131b0000 	tstne	fp, #0
     994:	b3000004 	movwlt	r0, #4
     998:	02000003 	andeq	r0, r0, #3
     99c:	701c6c91 	mulsvc	ip, r1, ip
     9a0:	01006e69 	tsteq	r0, r9, ror #28
     9a4:	00542954 	subseq	r2, r4, r4, asr r9
     9a8:	91020000 	mrsls	r0, (UNDEF: 2)
     9ac:	65731c68 	ldrbvs	r1, [r3, #-3176]!	; 0xfffff398
     9b0:	54010074 	strpl	r0, [r1], #-116	; 0xffffff8c
     9b4:	00039533 	andeq	r9, r3, r3, lsr r5
     9b8:	67910200 	ldrvs	r0, [r1, r0, lsl #4]
     9bc:	6765721d 			; <UNDEFINED> instruction: 0x6765721d
     9c0:	0b560100 	bleq	1580dc8 <_bss_end+0x1577764>
     9c4:	00000054 	andeq	r0, r0, r4, asr r0
     9c8:	1d749102 	ldfnep	f1, [r4, #-8]!
     9cc:	00746962 	rsbseq	r6, r4, r2, ror #18
     9d0:	54105601 	ldrpl	r5, [r0], #-1537	; 0xfffff9ff
     9d4:	02000000 	andeq	r0, r0, #0
     9d8:	1a007091 	bne	1cc24 <_bss_end+0x135c0>
     9dc:	0000033f 	andeq	r0, r0, pc, lsr r3
     9e0:	90104b01 	andsls	r4, r0, r1, lsl #22
     9e4:	78000004 	stmdavc	r0, {r2}
     9e8:	74000086 	strvc	r0, [r0], #-134	; 0xffffff7a
     9ec:	01000000 	mrseq	r0, (UNDEF: 0)
     9f0:	0004ca9c 	muleq	r4, ip, sl
     9f4:	04131b00 	ldreq	r1, [r3], #-2816	; 0xfffff500
     9f8:	03a20000 			; <UNDEFINED> instruction: 0x03a20000
     9fc:	91020000 	mrsls	r0, (UNDEF: 2)
     a00:	69701c6c 	ldmdbvs	r0!, {r2, r3, r5, r6, sl, fp, ip}^
     a04:	4b01006e 	blmi	40bc4 <_bss_end+0x37560>
     a08:	0000543a 	andeq	r5, r0, sl, lsr r4
     a0c:	68910200 	ldmvs	r1, {r9}
     a10:	6765721d 			; <UNDEFINED> instruction: 0x6765721d
     a14:	0b4d0100 	bleq	1340e1c <_bss_end+0x13377b8>
     a18:	00000054 	andeq	r0, r0, r4, asr r0
     a1c:	1d749102 	ldfnep	f1, [r4, #-8]!
     a20:	00746962 	rsbseq	r6, r4, r2, ror #18
     a24:	54104d01 	ldrpl	r4, [r0], #-3329	; 0xfffff2ff
     a28:	02000000 	andeq	r0, r0, #0
     a2c:	1a007091 	bne	1cc78 <_bss_end+0x13614>
     a30:	0000031a 	andeq	r0, r0, sl, lsl r3
     a34:	e4064101 	str	r4, [r6], #-257	; 0xfffffeff
     a38:	d8000004 	stmdale	r0, {r2}
     a3c:	a0000085 	andge	r0, r0, r5, lsl #1
     a40:	01000000 	mrseq	r0, (UNDEF: 0)
     a44:	00052d9c 	muleq	r5, ip, sp
     a48:	04131b00 	ldreq	r1, [r3], #-2816	; 0xfffff500
     a4c:	03b30000 			; <UNDEFINED> instruction: 0x03b30000
     a50:	91020000 	mrsls	r0, (UNDEF: 2)
     a54:	69701c6c 	ldmdbvs	r0!, {r2, r3, r5, r6, sl, fp, ip}^
     a58:	4101006e 	tstmi	r1, lr, rrx
     a5c:	00005430 	andeq	r5, r0, r0, lsr r4
     a60:	68910200 	ldmvs	r1, {r9}
     a64:	0005fe19 	andeq	pc, r5, r9, lsl lr	; <UNPREDICTABLE>
     a68:	44410100 	strbmi	r0, [r1], #-256	; 0xffffff00
     a6c:	000001db 	ldrdeq	r0, [r0], -fp
     a70:	1d679102 	stfnep	f1, [r7, #-8]!
     a74:	00676572 	rsbeq	r6, r7, r2, ror r5
     a78:	540b4301 	strpl	r4, [fp], #-769	; 0xfffffcff
     a7c:	02000000 	andeq	r0, r0, #0
     a80:	621d7491 	andsvs	r7, sp, #-1862270976	; 0x91000000
     a84:	01007469 	tsteq	r0, r9, ror #8
     a88:	00541043 	subseq	r1, r4, r3, asr #32
     a8c:	91020000 	mrsls	r0, (UNDEF: 2)
     a90:	c81e0070 	ldmdagt	lr, {r4, r5, r6}
     a94:	01000002 	tsteq	r0, r2
     a98:	05470636 	strbeq	r0, [r7, #-1590]	; 0xfffff9ca
     a9c:	85640000 	strbhi	r0, [r4, #-0]!
     aa0:	00740000 	rsbseq	r0, r4, r0
     aa4:	9c010000 	stcls	0, cr0, [r1], {-0}
     aa8:	00000581 	andeq	r0, r0, r1, lsl #11
     aac:	0004131b 	andeq	r1, r4, fp, lsl r3
     ab0:	0003a200 	andeq	sl, r3, r0, lsl #4
     ab4:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     ab8:	6e69701c 	mcrvs	0, 3, r7, cr9, cr12, {0}
     abc:	31360100 	teqcc	r6, r0, lsl #2
     ac0:	00000054 	andeq	r0, r0, r4, asr r0
     ac4:	1c709102 	ldfnep	f1, [r0], #-8
     ac8:	00676572 	rsbeq	r6, r7, r2, ror r5
     acc:	a7403601 	strbge	r3, [r0, -r1, lsl #12]
     ad0:	02000003 	andeq	r0, r0, #3
     ad4:	d5196c91 	ldrle	r6, [r9, #-3217]	; 0xfffff36f
     ad8:	01000007 	tsteq	r0, r7
     adc:	03a74f36 			; <UNDEFINED> instruction: 0x03a74f36
     ae0:	91020000 	mrsls	r0, (UNDEF: 2)
     ae4:	9a1e0068 	bls	780c8c <_bss_end+0x777628>
     ae8:	01000002 	tsteq	r0, r2
     aec:	059b062b 	ldreq	r0, [fp, #1579]	; 0x62b
     af0:	84f00000 	ldrbthi	r0, [r0], #0
     af4:	00740000 	rsbseq	r0, r4, r0
     af8:	9c010000 	stcls	0, cr0, [r1], {-0}
     afc:	000005d5 	ldrdeq	r0, [r0], -r5
     b00:	0004131b 	andeq	r1, r4, fp, lsl r3
     b04:	0003a200 	andeq	sl, r3, r0, lsl #4
     b08:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     b0c:	6e69701c 	mcrvs	0, 3, r7, cr9, cr12, {0}
     b10:	312b0100 			; <UNDEFINED> instruction: 0x312b0100
     b14:	00000054 	andeq	r0, r0, r4, asr r0
     b18:	1c709102 	ldfnep	f1, [r0], #-8
     b1c:	00676572 	rsbeq	r6, r7, r2, ror r5
     b20:	a7402b01 	strbge	r2, [r0, -r1, lsl #22]
     b24:	02000003 	andeq	r0, r0, #3
     b28:	d5196c91 	ldrle	r6, [r9, #-3217]	; 0xfffff36f
     b2c:	01000007 	tsteq	r0, r7
     b30:	03a74f2b 			; <UNDEFINED> instruction: 0x03a74f2b
     b34:	91020000 	mrsls	r0, (UNDEF: 2)
     b38:	6c1e0068 	ldcvs	0, cr0, [lr], {104}	; 0x68
     b3c:	01000002 	tsteq	r0, r2
     b40:	05ef0620 	strbeq	r0, [pc, #1568]!	; 1168 <CPSR_IRQ_INHIBIT+0x10e8>
     b44:	847c0000 	ldrbthi	r0, [ip], #-0
     b48:	00740000 	rsbseq	r0, r4, r0
     b4c:	9c010000 	stcls	0, cr0, [r1], {-0}
     b50:	00000629 	andeq	r0, r0, r9, lsr #12
     b54:	0004131b 	andeq	r1, r4, fp, lsl r3
     b58:	0003a200 	andeq	sl, r3, r0, lsl #4
     b5c:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     b60:	6e69701c 	mcrvs	0, 3, r7, cr9, cr12, {0}
     b64:	31200100 			; <UNDEFINED> instruction: 0x31200100
     b68:	00000054 	andeq	r0, r0, r4, asr r0
     b6c:	1c709102 	ldfnep	f1, [r0], #-8
     b70:	00676572 	rsbeq	r6, r7, r2, ror r5
     b74:	a7402001 	strbge	r2, [r0, -r1]
     b78:	02000003 	andeq	r0, r0, #3
     b7c:	d5196c91 	ldrle	r6, [r9, #-3217]	; 0xfffff36f
     b80:	01000007 	tsteq	r0, r7
     b84:	03a74f20 			; <UNDEFINED> instruction: 0x03a74f20
     b88:	91020000 	mrsls	r0, (UNDEF: 2)
     b8c:	3e1e0068 	cdpcc	0, 1, cr0, cr14, cr8, {3}
     b90:	01000002 	tsteq	r0, r2
     b94:	0643060c 	strbeq	r0, [r3], -ip, lsl #12
     b98:	83680000 	cmnhi	r8, #0
     b9c:	01140000 	tsteq	r4, r0
     ba0:	9c010000 	stcls	0, cr0, [r1], {-0}
     ba4:	0000067d 	andeq	r0, r0, sp, ror r6
     ba8:	0004131b 	andeq	r1, r4, fp, lsl r3
     bac:	0003a200 	andeq	sl, r3, r0, lsl #4
     bb0:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     bb4:	6e69701c 	mcrvs	0, 3, r7, cr9, cr12, {0}
     bb8:	320c0100 	andcc	r0, ip, #0, 2
     bbc:	00000054 	andeq	r0, r0, r4, asr r0
     bc0:	1c709102 	ldfnep	f1, [r0], #-8
     bc4:	00676572 	rsbeq	r6, r7, r2, ror r5
     bc8:	a7410c01 	strbge	r0, [r1, -r1, lsl #24]
     bcc:	02000003 	andeq	r0, r0, #3
     bd0:	d5196c91 	ldrle	r6, [r9, #-3217]	; 0xfffff36f
     bd4:	01000007 	tsteq	r0, r7
     bd8:	03a7500c 			; <UNDEFINED> instruction: 0x03a7500c
     bdc:	91020000 	mrsls	r0, (UNDEF: 2)
     be0:	f61f0068 			; <UNDEFINED> instruction: 0xf61f0068
     be4:	01000002 	tsteq	r0, r2
     be8:	068e0106 	streq	r0, [lr], r6, lsl #2
     bec:	a4000000 	strge	r0, [r0], #-0
     bf0:	20000006 	andcs	r0, r0, r6
     bf4:	00000413 	andeq	r0, r0, r3, lsl r4
     bf8:	000003b3 			; <UNDEFINED> instruction: 0x000003b3
     bfc:	00074421 	andeq	r4, r7, r1, lsr #8
     c00:	2b060100 	blcs	181008 <_bss_end+0x1779a4>
     c04:	00000065 	andeq	r0, r0, r5, rrx
     c08:	067d2200 	ldrbteq	r2, [sp], -r0, lsl #4
     c0c:	06b40000 	ldrteq	r0, [r4], r0
     c10:	06bb0000 	ldrteq	r0, [fp], r0
     c14:	83340000 	teqhi	r4, #0
     c18:	00340000 	eorseq	r0, r4, r0
     c1c:	9c010000 	stcls	0, cr0, [r1], {-0}
     c20:	00068e23 	andeq	r8, r6, r3, lsr #28
     c24:	74910200 	ldrvc	r0, [r1], #512	; 0x200
     c28:	00069723 	andeq	r9, r6, r3, lsr #14
     c2c:	70910200 	addsvc	r0, r1, r0, lsl #4
     c30:	083c0000 	ldmdaeq	ip!, {}	; <UNPREDICTABLE>
     c34:	00040000 	andeq	r0, r4, r0
     c38:	00000532 	andeq	r0, r0, r2, lsr r5
     c3c:	00140104 	andseq	r0, r4, r4, lsl #2
     c40:	95040000 	strls	r0, [r4, #-0]
     c44:	3d00000a 	stccc	0, cr0, [r0, #-40]	; 0xffffffd8
     c48:	34000001 	strcc	r0, [r0], #-1
     c4c:	90000088 	andls	r0, r0, r8, lsl #1
     c50:	a0000002 	andge	r0, r0, r2
     c54:	02000004 	andeq	r0, r0, #4
     c58:	03020801 	movweq	r0, #10241	; 0x2801
     c5c:	25030000 	strcs	r0, [r3, #-0]
     c60:	02000000 	andeq	r0, r0, #0
     c64:	01b90502 			; <UNDEFINED> instruction: 0x01b90502
     c68:	04040000 	streq	r0, [r4], #-0
     c6c:	746e6905 	strbtvc	r6, [lr], #-2309	; 0xfffff6fb
     c70:	05d00500 	ldrbeq	r0, [r0, #1280]	; 0x500
     c74:	09020000 	stmdbeq	r2, {}	; <UNPREDICTABLE>
     c78:	00004b07 	andeq	r4, r0, r7, lsl #22
     c7c:	08010200 	stmdaeq	r1, {r9}
     c80:	000002f9 	strdeq	r0, [r0], -r9
     c84:	00090b05 	andeq	r0, r9, r5, lsl #22
     c88:	070a0200 	streq	r0, [sl, -r0, lsl #4]
     c8c:	0000005e 	andeq	r0, r0, lr, asr r0
     c90:	91070202 	tstls	r7, r2, lsl #4
     c94:	05000003 	streq	r0, [r0, #-3]
     c98:	00000215 	andeq	r0, r0, r5, lsl r2
     c9c:	76070b02 	strvc	r0, [r7], -r2, lsl #22
     ca0:	03000000 	movweq	r0, #0
     ca4:	00000065 	andeq	r0, r0, r5, rrx
     ca8:	fa070402 	blx	1c1cb8 <_bss_end+0x1b8654>
     cac:	03000016 	movweq	r0, #22
     cb0:	00000076 	andeq	r0, r0, r6, ror r0
     cb4:	00007606 	andeq	r7, r0, r6, lsl #12
     cb8:	61680700 	cmnvs	r8, r0, lsl #14
     cbc:	0703006c 	streq	r0, [r3, -ip, rrx]
     cc0:	0002080b 	andeq	r0, r2, fp, lsl #16
     cc4:	04840800 	streq	r0, [r4], #2048	; 0x800
     cc8:	09030000 	stmdbeq	r3, {}	; <UNPREDICTABLE>
     ccc:	00007d19 	andeq	r7, r0, r9, lsl sp
     cd0:	e6b28000 	ldrt	r8, [r2], r0
     cd4:	02c4080e 	sbceq	r0, r4, #917504	; 0xe0000
     cd8:	0c030000 	stceq	0, cr0, [r3], {-0}
     cdc:	0002141a 	andeq	r1, r2, sl, lsl r4
     ce0:	00000000 	andeq	r0, r0, r0
     ce4:	03540820 	cmpeq	r4, #32, 16	; 0x200000
     ce8:	0f030000 	svceq	0x00030000
     cec:	0002141a 	andeq	r1, r2, sl, lsl r4
     cf0:	20000000 	andcs	r0, r0, r0
     cf4:	03cf0920 	biceq	r0, pc, #32, 18	; 0x80000
     cf8:	12030000 	andne	r0, r3, #0
     cfc:	00007115 	andeq	r7, r0, r5, lsl r1
     d00:	46083600 	strmi	r3, [r8], -r0, lsl #12
     d04:	03000004 	movweq	r0, #4
     d08:	02141a44 	andseq	r1, r4, #68, 20	; 0x44000
     d0c:	50000000 	andpl	r0, r0, r0
     d10:	d80a2021 	stmdale	sl, {r0, r5, sp}
     d14:	05000001 	streq	r0, [r0, #-1]
     d18:	00003804 	andeq	r3, r0, r4, lsl #16
     d1c:	0d460300 	stcleq	3, cr0, [r6, #-0]
     d20:	0000017d 	andeq	r0, r0, sp, ror r1
     d24:	5152490b 	cmppl	r2, fp, lsl #18
     d28:	1e0c0000 	cdpne	0, 0, cr0, cr12, cr0, {0}
     d2c:	01000002 	tsteq	r0, r2
     d30:	0004660c 	andeq	r6, r4, ip, lsl #12
     d34:	460c1000 	strmi	r1, [ip], -r0
     d38:	11000003 	tstne	r0, r3
     d3c:	0003780c 	andeq	r7, r3, ip, lsl #16
     d40:	ac0c1200 	sfmge	f1, 4, [ip], {-0}
     d44:	13000003 	movwne	r0, #3
     d48:	00034d0c 	andeq	r4, r3, ip, lsl #26
     d4c:	750c1400 	strvc	r1, [ip, #-1024]	; 0xfffffc00
     d50:	15000004 	strne	r0, [r0, #-4]
     d54:	00043f0c 	andeq	r3, r4, ip, lsl #30
     d58:	c00c1600 	andgt	r1, ip, r0, lsl #12
     d5c:	17000004 	strne	r0, [r0, -r4]
     d60:	00037f0c 	andeq	r7, r3, ip, lsl #30
     d64:	4f0c1800 	svcmi	0x000c1800
     d68:	19000004 	stmdbne	r0, {r2}
     d6c:	0003bd0c 	andeq	fp, r3, ip, lsl #26
     d70:	ae0c1a00 	vmlage.f32	s2, s24, s0
     d74:	20000002 	andcs	r0, r0, r2
     d78:	0002b90c 	andeq	fp, r2, ip, lsl #18
     d7c:	c50c2100 	strgt	r2, [ip, #-256]	; 0xffffff00
     d80:	22000003 	andcs	r0, r0, #3
     d84:	0002920c 	andeq	r9, r2, ip, lsl #4
     d88:	b30c2400 	movwlt	r2, #50176	; 0xc400
     d8c:	25000003 	strcs	r0, [r0, #-3]
     d90:	0002e30c 	andeq	lr, r2, ip, lsl #6
     d94:	ee0c3000 	cdp	0, 0, cr3, cr12, cr0, {0}
     d98:	31000002 	tstcc	r0, r2
     d9c:	0001cd0c 	andeq	ip, r1, ip, lsl #26
     da0:	a40c3200 	strge	r3, [ip], #-512	; 0xfffffe00
     da4:	34000003 	strcc	r0, [r0], #-3
     da8:	0001c30c 	andeq	ip, r1, ip, lsl #6
     dac:	0a003500 	beq	e1b4 <_bss_end+0x4b50>
     db0:	0000024e 	andeq	r0, r0, lr, asr #4
     db4:	00380405 	eorseq	r0, r8, r5, lsl #8
     db8:	6c030000 	stcvs	0, cr0, [r3], {-0}
     dbc:	0001a20d 	andeq	sl, r1, sp, lsl #4
     dc0:	046c0c00 	strbteq	r0, [ip], #-3072	; 0xfffff400
     dc4:	0c000000 	stceq	0, cr0, [r0], {-0}
     dc8:	00000387 	andeq	r0, r0, r7, lsl #7
     dcc:	038c0c01 	orreq	r0, ip, #256	; 0x100
     dd0:	00020000 	andeq	r0, r2, r0
     dd4:	00041808 	andeq	r1, r4, r8, lsl #16
     dd8:	1a730300 	bne	1cc19e0 <_bss_end+0x1cb837c>
     ddc:	00000214 	andeq	r0, r0, r4, lsl r2
     de0:	2000b200 	andcs	fp, r0, r0, lsl #4
     de4:	0001ae08 	andeq	sl, r1, r8, lsl #28
     de8:	1aa60300 	bne	fe9819f0 <_bss_end+0xfe97838c>
     dec:	00000214 	andeq	r0, r0, r4, lsl r2
     df0:	2000b400 	andcs	fp, r0, r0, lsl #8
     df4:	000af30d 	andeq	pc, sl, sp, lsl #6
     df8:	38040500 	stmdacc	r4, {r8, sl}
     dfc:	03000000 	movweq	r0, #0
     e00:	140c0da8 	strne	r0, [ip], #-3496	; 0xfffff258
     e04:	0000000c 	andeq	r0, r0, ip
     e08:	000bbe0c 	andeq	fp, fp, ip, lsl #28
     e0c:	620c0100 	andvs	r0, ip, #0, 2
     e10:	0200000f 	andeq	r0, r0, #15
     e14:	000b470c 	andeq	r4, fp, ip, lsl #14
     e18:	d30c0300 	movwle	r0, #49920	; 0xc300
     e1c:	04000008 	streq	r0, [r0], #-8
     e20:	000a320c 	andeq	r3, sl, ip, lsl #4
     e24:	bc0c0500 	cfstr32lt	mvfx0, [ip], {-0}
     e28:	06000009 	streq	r0, [r0], -r9
     e2c:	0008e00c 	andeq	lr, r8, ip
     e30:	e60c0700 	str	r0, [ip], -r0, lsl #14
     e34:	0800000b 	stmdaeq	r0, {r0, r1, r3}
     e38:	930e0000 	movwls	r0, #57344	; 0xe000
     e3c:	02000000 	andeq	r0, r0, #0
     e40:	16f50704 	ldrbtne	r0, [r5], r4, lsl #14
     e44:	0d030000 	stceq	0, cr0, [r3, #-0]
     e48:	0e000002 	cdpeq	0, 0, cr0, cr0, cr2, {0}
     e4c:	000000a3 	andeq	r0, r0, r3, lsr #1
     e50:	0000b30e 	andeq	fp, r0, lr, lsl #6
     e54:	00c30e00 	sbceq	r0, r3, r0, lsl #28
     e58:	d00e0000 	andle	r0, lr, r0
     e5c:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
     e60:	000001a2 	andeq	r0, r0, r2, lsr #3
     e64:	0001b20e 	andeq	fp, r1, lr, lsl #4
     e68:	08c20a00 	stmiaeq	r2, {r9, fp}^
     e6c:	01070000 	mrseq	r0, (UNDEF: 7)
     e70:	0000003f 	andeq	r0, r0, pc, lsr r0
     e74:	5c0c0604 	stcpl	6, cr0, [ip], {4}
     e78:	0c000002 	stceq	0, cr0, [r0], {2}
     e7c:	00000950 	andeq	r0, r0, r0, asr r9
     e80:	0bc40c00 	bleq	ff103e88 <_bss_end+0xff0fa824>
     e84:	0c010000 	stceq	0, cr0, [r1], {-0}
     e88:	00000ba4 	andeq	r0, r0, r4, lsr #23
     e8c:	e60f0002 	str	r0, [pc], -r2
     e90:	0800000a 	stmdaeq	r0, {r1, r3}
     e94:	54070d04 	strpl	r0, [r7], #-3332	; 0xfffff2fc
     e98:	10000003 	andne	r0, r0, r3
     e9c:	0000085c 	andeq	r0, r0, ip, asr r8
     ea0:	54201504 	strtpl	r1, [r0], #-1284	; 0xfffffafc
     ea4:	00000003 	andeq	r0, r0, r3
     ea8:	0008ec11 	andeq	lr, r8, r1, lsl ip
     eac:	0f110400 	svceq	0x00110400
     eb0:	0000035a 	andeq	r0, r0, sl, asr r3
     eb4:	0a8b1001 	beq	fe2c4ec0 <_bss_end+0xfe2bb85c>
     eb8:	18040000 	stmdane	r4, {}	; <UNPREDICTABLE>
     ebc:	00027619 	andeq	r7, r2, r9, lsl r6
     ec0:	63120400 	tstvs	r2, #0, 8
     ec4:	04000008 	streq	r0, [r0], #-8
     ec8:	0bf3201b 	bleq	ffcc8f3c <_bss_end+0xffcbf8d8>
     ecc:	03610000 	cmneq	r1, #0
     ed0:	a9020000 	stmdbge	r2, {}	; <UNPREDICTABLE>
     ed4:	b4000002 	strlt	r0, [r0], #-2
     ed8:	13000002 	movwne	r0, #2
     edc:	00000367 	andeq	r0, r0, r7, ror #6
     ee0:	0001c214 	andeq	ip, r1, r4, lsl r2
     ee4:	e6120000 	ldr	r0, [r2], -r0
     ee8:	0400000a 	streq	r0, [r0], #-10
     eec:	0868091e 	stmdaeq	r8!, {r1, r2, r3, r4, r8, fp}^
     ef0:	03670000 	cmneq	r7, #0
     ef4:	cd010000 	stcgt	0, cr0, [r1, #-0]
     ef8:	d8000002 	stmdale	r0, {r1}
     efc:	13000002 	movwne	r0, #2
     f00:	00000367 	andeq	r0, r0, r7, ror #6
     f04:	00020d14 	andeq	r0, r2, r4, lsl sp
     f08:	80150000 	andshi	r0, r5, r0
     f0c:	0400000f 	streq	r0, [r0], #-15
     f10:	08a80e21 	stmiaeq	r8!, {r0, r5, r9, sl, fp}
     f14:	ed010000 	stc	0, cr0, [r1, #-0]
     f18:	02000002 	andeq	r0, r0, #2
     f1c:	13000003 	movwne	r0, #3
     f20:	00000367 	andeq	r0, r0, r7, ror #6
     f24:	00027614 	andeq	r7, r2, r4, lsl r6
     f28:	00761400 	rsbseq	r1, r6, r0, lsl #8
     f2c:	37140000 	ldrcc	r0, [r4, -r0]
     f30:	00000002 	andeq	r0, r0, r2
     f34:	000e8015 	andeq	r8, lr, r5, lsl r0
     f38:	0e230400 	cdpeq	4, 2, cr0, cr3, cr0, {0}
     f3c:	00000b1e 	andeq	r0, r0, lr, lsl fp
     f40:	00031701 	andeq	r1, r3, r1, lsl #14
     f44:	00031d00 	andeq	r1, r3, r0, lsl #26
     f48:	03671300 	cmneq	r7, #0, 6
     f4c:	15000000 	strne	r0, [r0, #-0]
     f50:	00000bd9 	ldrdeq	r0, [r0], -r9
     f54:	3d0e2604 	stccc	6, cr2, [lr, #-16]
     f58:	0100000a 	tsteq	r0, sl
     f5c:	00000332 	andeq	r0, r0, r2, lsr r3
     f60:	00000338 	andeq	r0, r0, r8, lsr r3
     f64:	00036713 	andeq	r6, r3, r3, lsl r7
     f68:	8a160000 	bhi	580f70 <_bss_end+0x57790c>
     f6c:	04000009 	streq	r0, [r0], #-9
     f70:	08770e28 	ldmdaeq	r7!, {r3, r5, r9, sl, fp}^
     f74:	03720000 	cmneq	r2, #0
     f78:	4d010000 	stcmi	0, cr0, [r1, #-0]
     f7c:	13000003 	movwne	r0, #3
     f80:	00000367 	andeq	r0, r0, r7, ror #6
     f84:	04170000 	ldreq	r0, [r7], #-0
     f88:	00000082 	andeq	r0, r0, r2, lsl #1
     f8c:	03600417 	cmneq	r0, #385875968	; 0x17000000
     f90:	19180000 	ldmdbne	r8, {}	; <UNPREDICTABLE>
     f94:	00008204 	andeq	r8, r0, r4, lsl #4
     f98:	5c041700 	stcpl	7, cr1, [r4], {-0}
     f9c:	03000002 	movweq	r0, #2
     fa0:	00000367 	andeq	r0, r0, r7, ror #6
     fa4:	e6020102 	str	r0, [r2], -r2, lsl #2
     fa8:	1a000005 	bne	fc4 <CPSR_IRQ_INHIBIT+0xf44>
     fac:	0000096b 	andeq	r0, r0, fp, ror #18
     fb0:	5c0f2b04 			; <UNDEFINED> instruction: 0x5c0f2b04
     fb4:	0f000002 	svceq	0x00000002
     fb8:	00000c27 	andeq	r0, r0, r7, lsr #24
     fbc:	07060504 	streq	r0, [r6, -r4, lsl #10]
     fc0:	00000449 	andeq	r0, r0, r9, asr #8
     fc4:	0001d710 	andeq	sp, r1, r0, lsl r7
     fc8:	1d0a0500 	cfstr32ne	mvfx0, [sl, #-0]
     fcc:	0000044f 	andeq	r0, r0, pc, asr #8
     fd0:	0c271200 	sfmeq	f1, 4, [r7], #-0
     fd4:	0d050000 	stceq	0, cr0, [r5, #-0]
     fd8:	00025e09 	andeq	r5, r2, r9, lsl #28
     fdc:	00045400 	andeq	r5, r4, r0, lsl #8
     fe0:	03b80100 			; <UNDEFINED> instruction: 0x03b80100
     fe4:	03c30000 	biceq	r0, r3, #0
     fe8:	54130000 	ldrpl	r0, [r3], #-0
     fec:	14000004 	strne	r0, [r0], #-4
     ff0:	00000076 	andeq	r0, r0, r6, ror r0
     ff4:	0f801500 	svceq	0x00801500
     ff8:	10050000 	andne	r0, r5, r0
     ffc:	0002260e 	andeq	r2, r2, lr, lsl #12
    1000:	03d80100 	bicseq	r0, r8, #0, 2
    1004:	03e30000 	mvneq	r0, #0
    1008:	54130000 	ldrpl	r0, [r3], #-0
    100c:	14000004 	strne	r0, [r0], #-4
    1010:	0000017d 	andeq	r0, r0, sp, ror r1
    1014:	0e801500 	cdpeq	5, 8, cr1, cr0, cr0, {0}
    1018:	12050000 	andne	r0, r5, #0
    101c:	0004970e 	andeq	r9, r4, lr, lsl #14
    1020:	03f80100 	mvnseq	r0, #0, 2
    1024:	04030000 	streq	r0, [r3], #-0
    1028:	54130000 	ldrpl	r0, [r3], #-0
    102c:	14000004 	strne	r0, [r0], #-4
    1030:	0000017d 	andeq	r0, r0, sp, ror r1
    1034:	04321500 	ldrteq	r1, [r2], #-1280	; 0xfffffb00
    1038:	15050000 	strne	r0, [r5, #-0]
    103c:	00026b0e 	andeq	r6, r2, lr, lsl #22
    1040:	04180100 	ldreq	r0, [r8], #-256	; 0xffffff00
    1044:	04280000 	strteq	r0, [r8], #-0
    1048:	54130000 	ldrpl	r0, [r3], #-0
    104c:	14000004 	strne	r0, [r0], #-4
    1050:	000000e0 	andeq	r0, r0, r0, ror #1
    1054:	00006514 	andeq	r6, r0, r4, lsl r5
    1058:	6b160000 	blvs	581060 <_bss_end+0x5779fc>
    105c:	05000003 	streq	r0, [r0, #-3]
    1060:	01e01217 	mvneq	r1, r7, lsl r2
    1064:	00650000 	rsbeq	r0, r5, r0
    1068:	3d010000 	stccc	0, cr0, [r1, #-0]
    106c:	13000004 	movwne	r0, #4
    1070:	00000454 	andeq	r0, r0, r4, asr r4
    1074:	0000e014 	andeq	lr, r0, r4, lsl r0
    1078:	17000000 	strne	r0, [r0, -r0]
    107c:	00007604 	andeq	r7, r0, r4, lsl #12
    1080:	04490300 	strbeq	r0, [r9], #-768	; 0xfffffd00
    1084:	04170000 	ldreq	r0, [r7], #-0
    1088:	00000385 	andeq	r0, r0, r5, lsl #7
    108c:	0002a91a 	andeq	sl, r2, sl, lsl r9
    1090:	0d1a0500 	cfldr32eq	mvfx0, [sl, #-0]
    1094:	00000385 	andeq	r0, r0, r5, lsl #7
    1098:	000b6e0a 	andeq	r6, fp, sl, lsl #28
    109c:	38040500 	stmdacc	r4, {r8, sl}
    10a0:	06000000 	streq	r0, [r0], -r0
    10a4:	04850c06 	streq	r0, [r5], #3078	; 0xc06
    10a8:	040c0000 	streq	r0, [ip], #-0
    10ac:	00000009 	andeq	r0, r0, r9
    10b0:	00093b0c 	andeq	r3, r9, ip, lsl #22
    10b4:	0a000100 	beq	14bc <CPSR_IRQ_INHIBIT+0x143c>
    10b8:	00000a7b 	andeq	r0, r0, fp, ror sl
    10bc:	00380405 	eorseq	r0, r8, r5, lsl #8
    10c0:	0c060000 	stceq	0, cr0, [r6], {-0}
    10c4:	0004d20c 	andeq	sp, r4, ip, lsl #4
    10c8:	0a581b00 	beq	1607cd0 <_bss_end+0x15fe66c>
    10cc:	04b00000 	ldrteq	r0, [r0], #0
    10d0:	00089a1b 	andeq	r9, r8, fp, lsl sl
    10d4:	1b096000 	blne	2590dc <_bss_end+0x24fa78>
    10d8:	000008fc 	strdeq	r0, [r0], -ip
    10dc:	d11b12c0 	tstle	fp, r0, asr #5
    10e0:	8000000b 	andhi	r0, r0, fp
    10e4:	09c31b25 	stmibeq	r3, {r0, r2, r5, r8, r9, fp, ip}^
    10e8:	4b000000 	blmi	10f0 <CPSR_IRQ_INHIBIT+0x1070>
    10ec:	0009cc1b 	andeq	ip, r9, fp, lsl ip
    10f0:	1b960000 	blne	fe5810f8 <_bss_end+0xfe577a94>
    10f4:	000009e7 	andeq	r0, r0, r7, ror #19
    10f8:	161ce100 	ldrne	lr, [ip], -r0, lsl #2
    10fc:	0000000a 	andeq	r0, r0, sl
    1100:	000001c2 	andeq	r0, r0, r2, asr #3
    1104:	0009720f 	andeq	r7, r9, pc, lsl #4
    1108:	18060400 	stmdane	r6, {sl}
    110c:	00058d07 	andeq	r8, r5, r7, lsl #26
    1110:	08db1000 	ldmeq	fp, {ip}^
    1114:	1b060000 	blne	18111c <_bss_end+0x177ab8>
    1118:	00058d0f 	andeq	r8, r5, pc, lsl #26
    111c:	72120000 	andsvc	r0, r2, #0
    1120:	06000009 	streq	r0, [r0], -r9
    1124:	0b80091e 	bleq	fe0035a4 <_bss_end+0xfdff9f40>
    1128:	05930000 	ldreq	r0, [r3]
    112c:	05010000 	streq	r0, [r1, #-0]
    1130:	10000005 	andne	r0, r0, r5
    1134:	13000005 	movwne	r0, #5
    1138:	00000593 	muleq	r0, r3, r5
    113c:	00058d14 	andeq	r8, r5, r4, lsl sp
    1140:	2b150000 	blcs	541148 <_bss_end+0x537ae4>
    1144:	06000009 	streq	r0, [r0], -r9
    1148:	0b510e20 	bleq	14449d0 <_bss_end+0x143b36c>
    114c:	25010000 	strcs	r0, [r1, #-0]
    1150:	30000005 	andcc	r0, r0, r5
    1154:	13000005 	movwne	r0, #5
    1158:	00000593 	muleq	r0, r3, r5
    115c:	00046614 	andeq	r6, r4, r4, lsl r6
    1160:	ff150000 			; <UNDEFINED> instruction: 0xff150000
    1164:	06000009 	streq	r0, [r0], -r9
    1168:	0a600e21 	beq	18049f4 <_bss_end+0x17fb390>
    116c:	45010000 	strmi	r0, [r1, #-0]
    1170:	50000005 	andpl	r0, r0, r5
    1174:	13000005 	movwne	r0, #5
    1178:	00000593 	muleq	r0, r3, r5
    117c:	00048514 	andeq	r8, r4, r4, lsl r5
    1180:	a2150000 	andsge	r0, r5, #0
    1184:	06000008 	streq	r0, [r0], -r8
    1188:	0b0c0e25 	bleq	304a24 <_bss_end+0x2fb3c0>
    118c:	65010000 	strvs	r0, [r1, #-0]
    1190:	70000005 	andvc	r0, r0, r5
    1194:	13000005 	movwne	r0, #5
    1198:	00000593 	muleq	r0, r3, r5
    119c:	00002514 	andeq	r2, r0, r4, lsl r5
    11a0:	a21d0000 	andsge	r0, sp, #0
    11a4:	06000008 	streq	r0, [r0], -r8
    11a8:	0b330e26 	bleq	cc4a48 <_bss_end+0xcbb3e4>
    11ac:	81010000 	mrshi	r0, (UNDEF: 1)
    11b0:	13000005 	movwne	r0, #5
    11b4:	00000593 	muleq	r0, r3, r5
    11b8:	00059914 	andeq	r9, r5, r4, lsl r9
    11bc:	19000000 	stmdbne	r0, {}	; <UNPREDICTABLE>
    11c0:	00038504 	andeq	r8, r3, r4, lsl #10
    11c4:	d2041700 	andle	r1, r4, #0, 14
    11c8:	17000004 	strne	r0, [r0, -r4]
    11cc:	00002c04 	andeq	r2, r0, r4, lsl #24
    11d0:	0c3b1a00 			; <UNDEFINED> instruction: 0x0c3b1a00
    11d4:	29060000 	stmdbcs	r6, {}	; <UNPREDICTABLE>
    11d8:	0004d20e 	andeq	sp, r4, lr, lsl #4
    11dc:	03791e00 	cmneq	r9, #0, 28
    11e0:	06010000 	streq	r0, [r1], -r0
    11e4:	40030508 	andmi	r0, r3, r8, lsl #10
    11e8:	1f000096 	svcne	0x00000096
    11ec:	00000b93 	muleq	r0, r3, fp
    11f0:	080a0106 	stmdaeq	sl, {r1, r2, r8}
    11f4:	00000687 	andeq	r0, r0, r7, lsl #13
    11f8:	000a0d20 	andeq	r0, sl, r0, lsr #26
    11fc:	0d0c0100 	stfeqs	f0, [ip, #-0]
    1200:	0000003f 	andeq	r0, r0, pc, lsr r0
    1204:	00070101 	andeq	r0, r7, r1, lsl #2
    1208:	000bb220 	andeq	fp, fp, r0, lsr #4
    120c:	0d0d0100 	stfeqs	f0, [sp, #-0]
    1210:	0000003f 	andeq	r0, r0, pc, lsr r0
    1214:	00060101 	andeq	r0, r6, r1, lsl #2
    1218:	00092120 	andeq	r2, r9, r0, lsr #2
    121c:	0d0e0100 	stfeqs	f0, [lr, #-0]
    1220:	0000003f 	andeq	r0, r0, pc, lsr r0
    1224:	00040201 	andeq	r0, r4, r1, lsl #4
    1228:	000a2020 	andeq	r2, sl, r0, lsr #32
    122c:	0d0f0100 	stfeqs	f0, [pc, #-0]	; 1234 <CPSR_IRQ_INHIBIT+0x11b4>
    1230:	0000003f 	andeq	r0, r0, pc, lsr r0
    1234:	00030101 	andeq	r0, r3, r1, lsl #2
    1238:	00097820 	andeq	r7, r9, r0, lsr #16
    123c:	0d100100 	ldfeqs	f0, [r0, #-0]
    1240:	0000003f 	andeq	r0, r0, pc, lsr r0
    1244:	00020101 	andeq	r0, r2, r1, lsl #2
    1248:	000a2920 	andeq	r2, sl, r0, lsr #18
    124c:	0d110100 	ldfeqs	f0, [r1, #-0]
    1250:	0000003f 	andeq	r0, r0, pc, lsr r0
    1254:	00010101 	andeq	r0, r1, r1, lsl #2
    1258:	00094220 	andeq	r4, r9, r0, lsr #4
    125c:	0d120100 	ldfeqs	f0, [r2, #-0]
    1260:	0000003f 	andeq	r0, r0, pc, lsr r0
    1264:	00000101 	andeq	r0, r0, r1, lsl #2
    1268:	00099f20 	andeq	r9, r9, r0, lsr #30
    126c:	0d130100 	ldfeqs	f0, [r3, #-0]
    1270:	0000003f 	andeq	r0, r0, pc, lsr r0
    1274:	01070101 	tsteq	r7, r1, lsl #2
    1278:	000ad220 	andeq	sp, sl, r0, lsr #4
    127c:	0d140100 	ldfeqs	f0, [r4, #-0]
    1280:	0000003f 	andeq	r0, r0, pc, lsr r0
    1284:	01060101 	tsteq	r6, r1, lsl #2
    1288:	0009d521 	andeq	sp, r9, r1, lsr #10
    128c:	0e150100 	mufeqs	f0, f5, f0
    1290:	00000052 	andeq	r0, r0, r2, asr r0
    1294:	007c0a02 	rsbseq	r0, ip, r2, lsl #20
    1298:	00091421 	andeq	r1, r9, r1, lsr #8
    129c:	0e160100 	mufeqs	f0, f6, f0
    12a0:	00000052 	andeq	r0, r0, r2, asr r0
    12a4:	027c1002 	rsbseq	r1, ip, #2
    12a8:	0009de20 	andeq	sp, r9, r0, lsr #28
    12ac:	0e170100 	mufeqs	f0, f7, f0
    12b0:	00000052 	andeq	r0, r0, r2, asr r0
    12b4:	04020a02 	streq	r0, [r2], #-2562	; 0xfffff5fe
    12b8:	05b90600 	ldreq	r0, [r9, #1536]!	; 0x600
    12bc:	5c220000 	stcpl	0, cr0, [r2], #-0
    12c0:	a8000009 	stmdage	r0, {r0, r3}
    12c4:	1c00008a 	stcne	0, cr0, [r0], {138}	; 0x8a
    12c8:	01000000 	mrseq	r0, (UNDEF: 0)
    12cc:	03de239c 	bicseq	r2, lr, #156, 6	; 0x70000002
    12d0:	8a540000 	bhi	15012d8 <_bss_end+0x14f7c74>
    12d4:	00540000 	subseq	r0, r4, r0
    12d8:	9c010000 	stcls	0, cr0, [r1], {-0}
    12dc:	000006cd 	andeq	r0, r0, sp, asr #13
    12e0:	0002d424 	andeq	sp, r2, r4, lsr #8
    12e4:	014b0100 	mrseq	r0, (UNDEF: 91)
    12e8:	00000038 	andeq	r0, r0, r8, lsr r0
    12ec:	24749102 	ldrbtcs	r9, [r4], #-258	; 0xfffffefe
    12f0:	00000408 	andeq	r0, r0, r8, lsl #8
    12f4:	38014b01 	stmdacc	r1, {r0, r8, r9, fp, lr}
    12f8:	02000000 	andeq	r0, r0, #0
    12fc:	25007091 	strcs	r7, [r0, #-145]	; 0xffffff6f
    1300:	00000338 	andeq	r0, r0, r8, lsr r3
    1304:	e7064801 	str	r4, [r6, -r1, lsl #16]
    1308:	14000006 	strne	r0, [r0], #-6
    130c:	4000008a 	andmi	r0, r0, sl, lsl #1
    1310:	01000000 	mrseq	r0, (UNDEF: 0)
    1314:	0006f49c 	muleq	r6, ip, r4
    1318:	04132600 	ldreq	r2, [r3], #-1536	; 0xfffffa00
    131c:	036d0000 	cmneq	sp, #0
    1320:	91020000 	mrsls	r0, (UNDEF: 2)
    1324:	1d250074 	stcne	0, cr0, [r5, #-464]!	; 0xfffffe30
    1328:	01000003 	tsteq	r0, r3
    132c:	070e0640 	streq	r0, [lr, -r0, asr #12]
    1330:	89c40000 	stmibhi	r4, {}^	; <UNPREDICTABLE>
    1334:	00500000 	subseq	r0, r0, r0
    1338:	9c010000 	stcls	0, cr0, [r1], {-0}
    133c:	0000071b 	andeq	r0, r0, fp, lsl r7
    1340:	00041326 	andeq	r1, r4, r6, lsr #6
    1344:	00036d00 	andeq	r6, r3, r0, lsl #26
    1348:	74910200 	ldrvc	r0, [r1], #512	; 0x200
    134c:	03022500 	movweq	r2, #9472	; 0x2500
    1350:	38010000 	stmdacc	r1, {}	; <UNPREDICTABLE>
    1354:	00073506 	andeq	r3, r7, r6, lsl #10
    1358:	00897800 	addeq	r7, r9, r0, lsl #16
    135c:	00004c00 	andeq	r4, r0, r0, lsl #24
    1360:	519c0100 	orrspl	r0, ip, r0, lsl #2
    1364:	26000007 	strcs	r0, [r0], -r7
    1368:	00000413 	andeq	r0, r0, r3, lsl r4
    136c:	0000036d 	andeq	r0, r0, sp, ror #6
    1370:	276c9102 	strbcs	r9, [ip, -r2, lsl #2]!
    1374:	00676572 	rsbeq	r6, r7, r2, ror r5
    1378:	51203a01 			; <UNDEFINED> instruction: 0x51203a01
    137c:	02000007 	andeq	r0, r0, #7
    1380:	19007491 	stmdbne	r0, {r0, r4, r7, sl, ip, sp, lr}
    1384:	00068704 	andeq	r8, r6, r4, lsl #14
    1388:	02d82500 	sbcseq	r2, r8, #0, 10
    138c:	27010000 	strcs	r0, [r1, -r0]
    1390:	00077106 	andeq	r7, r7, r6, lsl #2
    1394:	0088ac00 	addeq	sl, r8, r0, lsl #24
    1398:	0000cc00 	andeq	ip, r0, r0, lsl #24
    139c:	ba9c0100 	blt	fe7017a4 <_bss_end+0xfe6f8140>
    13a0:	26000007 	strcs	r0, [r0], -r7
    13a4:	00000413 	andeq	r0, r0, r3, lsl r4
    13a8:	0000036d 	andeq	r0, r0, sp, ror #6
    13ac:	24649102 	strbtcs	r9, [r4], #-258	; 0xfffffefe
    13b0:	000009b3 			; <UNDEFINED> instruction: 0x000009b3
    13b4:	76252701 	strtvc	r2, [r5], -r1, lsl #14
    13b8:	02000002 	andeq	r0, r0, #2
    13bc:	ed246091 	stc	0, cr6, [r4, #-580]!	; 0xfffffdbc
    13c0:	0100000a 	tsteq	r0, sl
    13c4:	00763c27 	rsbseq	r3, r6, r7, lsr #24
    13c8:	91020000 	mrsls	r0, (UNDEF: 2)
    13cc:	0921245c 	stmdbeq	r1!, {r2, r3, r4, r6, sl, sp}
    13d0:	27010000 	strcs	r0, [r1, -r0]
    13d4:	00023754 	andeq	r3, r2, r4, asr r7
    13d8:	5b910200 	blpl	fe441be0 <_bss_end+0xfe43857c>
    13dc:	67657227 	strbvs	r7, [r5, -r7, lsr #4]!
    13e0:	162b0100 	strtne	r0, [fp], -r0, lsl #2
    13e4:	000005b9 			; <UNDEFINED> instruction: 0x000005b9
    13e8:	00689102 	rsbeq	r9, r8, r2, lsl #2
    13ec:	00029028 	andeq	r9, r2, r8, lsr #32
    13f0:	18220100 	stmdane	r2!, {r8}
    13f4:	000007d4 	ldrdeq	r0, [r0], -r4
    13f8:	00008874 	andeq	r8, r0, r4, ror r8
    13fc:	00000038 	andeq	r0, r0, r8, lsr r0
    1400:	07f09c01 	ldrbeq	r9, [r0, r1, lsl #24]!
    1404:	13260000 			; <UNDEFINED> instruction: 0x13260000
    1408:	6d000004 	stcvs	0, cr0, [r0, #-16]
    140c:	02000003 	andeq	r0, r0, #3
    1410:	72297491 	eorvc	r7, r9, #-1862270976	; 0x91000000
    1414:	01006765 	tsteq	r0, r5, ror #14
    1418:	01c23422 	biceq	r3, r2, r2, lsr #8
    141c:	91020000 	mrsls	r0, (UNDEF: 2)
    1420:	b42a0070 	strtlt	r0, [sl], #-112	; 0xffffff90
    1424:	01000002 	tsteq	r0, r2
    1428:	0801011c 	stmdaeq	r1, {r2, r3, r4, r8}
    142c:	17000000 	strne	r0, [r0, -r0]
    1430:	2b000008 	blcs	1458 <CPSR_IRQ_INHIBIT+0x13d8>
    1434:	00000413 	andeq	r0, r0, r3, lsl r4
    1438:	0000036d 	andeq	r0, r0, sp, ror #6
    143c:	0009f02c 	andeq	pc, r9, ip, lsr #32
    1440:	1e1c0100 	mufnee	f0, f4, f0
    1444:	0000020d 	andeq	r0, r0, sp, lsl #4
    1448:	07f02d00 	ldrbeq	r2, [r0, r0, lsl #26]!
    144c:	0afd0000 	beq	fff41454 <_bss_end+0xfff37df0>
    1450:	082e0000 	stmdaeq	lr!, {}	; <UNPREDICTABLE>
    1454:	88340000 	ldmdahi	r4!, {}	; <UNPREDICTABLE>
    1458:	00400000 	subeq	r0, r0, r0
    145c:	9c010000 	stcls	0, cr0, [r1], {-0}
    1460:	0008012e 	andeq	r0, r8, lr, lsr #2
    1464:	74910200 	ldrvc	r0, [r1], #512	; 0x200
    1468:	00080a2e 	andeq	r0, r8, lr, lsr #20
    146c:	70910200 	addsvc	r0, r1, r0, lsl #4
    1470:	058d0000 	streq	r0, [sp]
    1474:	00040000 	andeq	r0, r4, r0
    1478:	0000080c 	andeq	r0, r0, ip, lsl #16
    147c:	00140104 	andseq	r0, r4, r4, lsl #2
    1480:	42040000 	andmi	r0, r4, #0
    1484:	3d00000c 	stccc	0, cr0, [r0, #-48]	; 0xffffffd0
    1488:	c4000001 	strgt	r0, [r0], #-1
    148c:	d800008a 	stmdale	r0, {r1, r3, r7}
    1490:	4b000002 	blmi	14a0 <CPSR_IRQ_INHIBIT+0x1420>
    1494:	02000006 	andeq	r0, r0, #6
    1498:	03020801 	movweq	r0, #10241	; 0x2801
    149c:	25030000 	strcs	r0, [r3, #-0]
    14a0:	02000000 	andeq	r0, r0, #0
    14a4:	01b90502 			; <UNDEFINED> instruction: 0x01b90502
    14a8:	04040000 	streq	r0, [r4], #-0
    14ac:	746e6905 	strbtvc	r6, [lr], #-2309	; 0xfffff6fb
    14b0:	08010200 	stmdaeq	r1, {r9}
    14b4:	000002f9 	strdeq	r0, [r0], -r9
    14b8:	91070202 	tstls	r7, r2, lsl #4
    14bc:	05000003 	streq	r0, [r0, #-3]
    14c0:	00000215 	andeq	r0, r0, r5, lsl r2
    14c4:	5e070b05 	vmlapl.f64	d0, d7, d5
    14c8:	03000000 	movweq	r0, #0
    14cc:	0000004d 	andeq	r0, r0, sp, asr #32
    14d0:	fa070402 	blx	1c24e0 <_bss_end+0x1b8e7c>
    14d4:	03000016 	movweq	r0, #22
    14d8:	0000005e 	andeq	r0, r0, lr, asr r0
    14dc:	6c616806 	stclvs	8, cr6, [r1], #-24	; 0xffffffe8
    14e0:	0b070200 	bleq	1c1ce8 <_bss_end+0x1b8684>
    14e4:	000001a6 	andeq	r0, r0, r6, lsr #3
    14e8:	00048407 	andeq	r8, r4, r7, lsl #8
    14ec:	19090200 	stmdbne	r9, {r9}
    14f0:	00000065 	andeq	r0, r0, r5, rrx
    14f4:	0ee6b280 	cdpeq	2, 14, cr11, cr6, cr0, {4}
    14f8:	0002c407 	andeq	ip, r2, r7, lsl #8
    14fc:	1a0c0200 	bne	301d04 <_bss_end+0x2f86a0>
    1500:	000001b2 			; <UNDEFINED> instruction: 0x000001b2
    1504:	20000000 	andcs	r0, r0, r0
    1508:	00035407 	andeq	r5, r3, r7, lsl #8
    150c:	1a0f0200 	bne	3c1d14 <_bss_end+0x3b86b0>
    1510:	000001b2 			; <UNDEFINED> instruction: 0x000001b2
    1514:	20200000 	eorcs	r0, r0, r0
    1518:	0003cf08 	andeq	ip, r3, r8, lsl #30
    151c:	15120200 	ldrne	r0, [r2, #-512]	; 0xfffffe00
    1520:	00000059 	andeq	r0, r0, r9, asr r0
    1524:	04460736 	strbeq	r0, [r6], #-1846	; 0xfffff8ca
    1528:	44020000 	strmi	r0, [r2], #-0
    152c:	0001b21a 	andeq	fp, r1, sl, lsl r2
    1530:	21500000 	cmpcs	r0, r0
    1534:	01d80920 	bicseq	r0, r8, r0, lsr #18
    1538:	04050000 	streq	r0, [r5], #-0
    153c:	00000038 	andeq	r0, r0, r8, lsr r0
    1540:	600d4602 	andvs	r4, sp, r2, lsl #12
    1544:	0a000001 	beq	1550 <CPSR_IRQ_INHIBIT+0x14d0>
    1548:	00515249 	subseq	r5, r1, r9, asr #4
    154c:	021e0b00 	andseq	r0, lr, #0, 22
    1550:	0b010000 	bleq	41558 <_bss_end+0x37ef4>
    1554:	00000466 	andeq	r0, r0, r6, ror #8
    1558:	03460b10 	movteq	r0, #27408	; 0x6b10
    155c:	0b110000 	bleq	441564 <_bss_end+0x437f00>
    1560:	00000378 	andeq	r0, r0, r8, ror r3
    1564:	03ac0b12 			; <UNDEFINED> instruction: 0x03ac0b12
    1568:	0b130000 	bleq	4c1570 <_bss_end+0x4b7f0c>
    156c:	0000034d 	andeq	r0, r0, sp, asr #6
    1570:	04750b14 	ldrbteq	r0, [r5], #-2836	; 0xfffff4ec
    1574:	0b150000 	bleq	54157c <_bss_end+0x537f18>
    1578:	0000043f 	andeq	r0, r0, pc, lsr r4
    157c:	04c00b16 	strbeq	r0, [r0], #2838	; 0xb16
    1580:	0b170000 	bleq	5c1588 <_bss_end+0x5b7f24>
    1584:	0000037f 	andeq	r0, r0, pc, ror r3
    1588:	044f0b18 	strbeq	r0, [pc], #-2840	; 1590 <CPSR_IRQ_INHIBIT+0x1510>
    158c:	0b190000 	bleq	641594 <_bss_end+0x637f30>
    1590:	000003bd 			; <UNDEFINED> instruction: 0x000003bd
    1594:	02ae0b1a 	adceq	r0, lr, #26624	; 0x6800
    1598:	0b200000 	bleq	8015a0 <_bss_end+0x7f7f3c>
    159c:	000002b9 			; <UNDEFINED> instruction: 0x000002b9
    15a0:	03c50b21 	biceq	r0, r5, #33792	; 0x8400
    15a4:	0b220000 	bleq	8815ac <_bss_end+0x877f48>
    15a8:	00000292 	muleq	r0, r2, r2
    15ac:	03b30b24 			; <UNDEFINED> instruction: 0x03b30b24
    15b0:	0b250000 	bleq	9415b8 <_bss_end+0x937f54>
    15b4:	000002e3 	andeq	r0, r0, r3, ror #5
    15b8:	02ee0b30 	rsceq	r0, lr, #48, 22	; 0xc000
    15bc:	0b310000 	bleq	c415c4 <_bss_end+0xc37f60>
    15c0:	000001cd 	andeq	r0, r0, sp, asr #3
    15c4:	03a40b32 			; <UNDEFINED> instruction: 0x03a40b32
    15c8:	0b340000 	bleq	d015d0 <_bss_end+0xcf7f6c>
    15cc:	000001c3 	andeq	r0, r0, r3, asr #3
    15d0:	4e090035 	mcrmi	0, 0, r0, cr9, cr5, {1}
    15d4:	05000002 	streq	r0, [r0, #-2]
    15d8:	00003804 	andeq	r3, r0, r4, lsl #16
    15dc:	0d6c0200 	sfmeq	f0, 2, [ip, #-0]
    15e0:	00000185 	andeq	r0, r0, r5, lsl #3
    15e4:	00046c0b 	andeq	r6, r4, fp, lsl #24
    15e8:	870b0000 	strhi	r0, [fp, -r0]
    15ec:	01000003 	tsteq	r0, r3
    15f0:	00038c0b 	andeq	r8, r3, fp, lsl #24
    15f4:	07000200 	streq	r0, [r0, -r0, lsl #4]
    15f8:	00000418 	andeq	r0, r0, r8, lsl r4
    15fc:	b21a7302 	andslt	r7, sl, #134217728	; 0x8000000
    1600:	00000001 	andeq	r0, r0, r1
    1604:	072000b2 			; <UNDEFINED> instruction: 0x072000b2
    1608:	000001ae 	andeq	r0, r0, lr, lsr #3
    160c:	b21aa602 	andslt	sl, sl, #2097152	; 0x200000
    1610:	00000001 	andeq	r0, r0, r1
    1614:	002000b4 	strhteq	r0, [r0], -r4
    1618:	0000760c 	andeq	r7, r0, ip, lsl #12
    161c:	07040200 	streq	r0, [r4, -r0, lsl #4]
    1620:	000016f5 	strdeq	r1, [r0], -r5
    1624:	0001ab03 	andeq	sl, r1, r3, lsl #22
    1628:	00860c00 	addeq	r0, r6, r0, lsl #24
    162c:	960c0000 	strls	r0, [ip], -r0
    1630:	0c000000 	stceq	0, cr0, [r0], {-0}
    1634:	000000a6 	andeq	r0, r0, r6, lsr #1
    1638:	0000b30c 	andeq	fp, r0, ip, lsl #6
    163c:	01850c00 	orreq	r0, r5, r0, lsl #24
    1640:	950c0000 	strls	r0, [ip, #-0]
    1644:	0d000001 	stceq	0, cr0, [r0, #-4]
    1648:	00000c27 	andeq	r0, r0, r7, lsr #24
    164c:	07060304 	streq	r0, [r6, -r4, lsl #6]
    1650:	00000299 	muleq	r0, r9, r2
    1654:	0001d70e 	andeq	sp, r1, lr, lsl #14
    1658:	1d0a0300 	stcne	3, cr0, [sl, #-0]
    165c:	0000029f 	muleq	r0, pc, r2	; <UNPREDICTABLE>
    1660:	0c270f00 	stceq	15, cr0, [r7], #-0
    1664:	0d030000 	stceq	0, cr0, [r3, #-0]
    1668:	00025e09 	andeq	r5, r2, r9, lsl #28
    166c:	0002a400 	andeq	sl, r2, r0, lsl #8
    1670:	02080100 	andeq	r0, r8, #0, 2
    1674:	02130000 	andseq	r0, r3, #0
    1678:	a4100000 	ldrge	r0, [r0], #-0
    167c:	11000002 	tstne	r0, r2
    1680:	0000005e 	andeq	r0, r0, lr, asr r0
    1684:	0f801200 	svceq	0x00801200
    1688:	10030000 	andne	r0, r3, r0
    168c:	0002260e 	andeq	r2, r2, lr, lsl #12
    1690:	02280100 	eoreq	r0, r8, #0, 2
    1694:	02330000 	eorseq	r0, r3, #0
    1698:	a4100000 	ldrge	r0, [r0], #-0
    169c:	11000002 	tstne	r0, r2
    16a0:	00000160 	andeq	r0, r0, r0, ror #2
    16a4:	0e801200 	cdpeq	2, 8, cr1, cr0, cr0, {0}
    16a8:	12030000 	andne	r0, r3, #0
    16ac:	0004970e 	andeq	r9, r4, lr, lsl #14
    16b0:	02480100 	subeq	r0, r8, #0, 2
    16b4:	02530000 	subseq	r0, r3, #0
    16b8:	a4100000 	ldrge	r0, [r0], #-0
    16bc:	11000002 	tstne	r0, r2
    16c0:	00000160 	andeq	r0, r0, r0, ror #2
    16c4:	04321200 	ldrteq	r1, [r2], #-512	; 0xfffffe00
    16c8:	15030000 	strne	r0, [r3, #-0]
    16cc:	00026b0e 	andeq	r6, r2, lr, lsl #22
    16d0:	02680100 	rsbeq	r0, r8, #0, 2
    16d4:	02780000 	rsbseq	r0, r8, #0
    16d8:	a4100000 	ldrge	r0, [r0], #-0
    16dc:	11000002 	tstne	r0, r2
    16e0:	000000c3 	andeq	r0, r0, r3, asr #1
    16e4:	00004d11 	andeq	r4, r0, r1, lsl sp
    16e8:	6b130000 	blvs	4c16f0 <_bss_end+0x4b808c>
    16ec:	03000003 	movweq	r0, #3
    16f0:	01e01217 	mvneq	r1, r7, lsl r2
    16f4:	004d0000 	subeq	r0, sp, r0
    16f8:	8d010000 	stchi	0, cr0, [r1, #-0]
    16fc:	10000002 	andne	r0, r0, r2
    1700:	000002a4 	andeq	r0, r0, r4, lsr #5
    1704:	0000c311 	andeq	ip, r0, r1, lsl r3
    1708:	14000000 	strne	r0, [r0], #-0
    170c:	00005e04 	andeq	r5, r0, r4, lsl #28
    1710:	02990300 	addseq	r0, r9, #0, 6
    1714:	04140000 	ldreq	r0, [r4], #-0
    1718:	000001d5 	ldrdeq	r0, [r0], -r5
    171c:	0002a915 	andeq	sl, r2, r5, lsl r9
    1720:	0d1a0300 	ldceq	3, cr0, [sl, #-0]
    1724:	000001d5 	ldrdeq	r0, [r0], -r5
    1728:	000b6e09 	andeq	r6, fp, r9, lsl #28
    172c:	38040500 	stmdacc	r4, {r8, sl}
    1730:	04000000 	streq	r0, [r0], #-0
    1734:	02d50c06 	sbcseq	r0, r5, #1536	; 0x600
    1738:	040b0000 	streq	r0, [fp], #-0
    173c:	00000009 	andeq	r0, r0, r9
    1740:	00093b0b 	andeq	r3, r9, fp, lsl #22
    1744:	09000100 	stmdbeq	r0, {r8}
    1748:	00000a7b 	andeq	r0, r0, fp, ror sl
    174c:	00380405 	eorseq	r0, r8, r5, lsl #8
    1750:	0c040000 	stceq	0, cr0, [r4], {-0}
    1754:	0003220c 	andeq	r2, r3, ip, lsl #4
    1758:	0a581600 	beq	1606f60 <_bss_end+0x15fd8fc>
    175c:	04b00000 	ldrteq	r0, [r0], #0
    1760:	00089a16 	andeq	r9, r8, r6, lsl sl
    1764:	16096000 	strne	r6, [r9], -r0
    1768:	000008fc 	strdeq	r0, [r0], -ip
    176c:	d11612c0 	tstle	r6, r0, asr #5
    1770:	8000000b 	andhi	r0, r0, fp
    1774:	09c31625 	stmibeq	r3, {r0, r2, r5, r9, sl, ip}^
    1778:	4b000000 	blmi	1780 <CPSR_IRQ_INHIBIT+0x1700>
    177c:	0009cc16 	andeq	ip, r9, r6, lsl ip
    1780:	16960000 	ldrne	r0, [r6], r0
    1784:	000009e7 	andeq	r0, r0, r7, ror #19
    1788:	1617e100 	ldrne	lr, [r7], -r0, lsl #2
    178c:	0000000a 	andeq	r0, r0, sl
    1790:	000001c2 	andeq	r0, r0, r2, asr #3
    1794:	0009720d 	andeq	r7, r9, sp, lsl #4
    1798:	18040400 	stmdane	r4, {sl}
    179c:	0003dd07 	andeq	sp, r3, r7, lsl #26
    17a0:	08db0e00 	ldmeq	fp, {r9, sl, fp}^
    17a4:	1b040000 	blne	1017ac <_bss_end+0xf8148>
    17a8:	0003dd0f 	andeq	sp, r3, pc, lsl #26
    17ac:	720f0000 	andvc	r0, pc, #0
    17b0:	04000009 	streq	r0, [r0], #-9
    17b4:	0b80091e 	bleq	fe003c34 <_bss_end+0xfdffa5d0>
    17b8:	03e30000 	mvneq	r0, #0
    17bc:	55010000 	strpl	r0, [r1, #-0]
    17c0:	60000003 	andvs	r0, r0, r3
    17c4:	10000003 	andne	r0, r0, r3
    17c8:	000003e3 	andeq	r0, r0, r3, ror #7
    17cc:	0003dd11 	andeq	sp, r3, r1, lsl sp
    17d0:	2b120000 	blcs	4817d8 <_bss_end+0x478174>
    17d4:	04000009 	streq	r0, [r0], #-9
    17d8:	0b510e20 	bleq	1445060 <_bss_end+0x143b9fc>
    17dc:	75010000 	strvc	r0, [r1, #-0]
    17e0:	80000003 	andhi	r0, r0, r3
    17e4:	10000003 	andne	r0, r0, r3
    17e8:	000003e3 	andeq	r0, r0, r3, ror #7
    17ec:	0002b611 	andeq	fp, r2, r1, lsl r6
    17f0:	ff120000 			; <UNDEFINED> instruction: 0xff120000
    17f4:	04000009 	streq	r0, [r0], #-9
    17f8:	0a600e21 	beq	1805084 <_bss_end+0x17fba20>
    17fc:	95010000 	strls	r0, [r1, #-0]
    1800:	a0000003 	andge	r0, r0, r3
    1804:	10000003 	andne	r0, r0, r3
    1808:	000003e3 	andeq	r0, r0, r3, ror #7
    180c:	0002d511 	andeq	sp, r2, r1, lsl r5
    1810:	a2120000 	andsge	r0, r2, #0
    1814:	04000008 	streq	r0, [r0], #-8
    1818:	0b0c0e25 	bleq	3050b4 <_bss_end+0x2fba50>
    181c:	b5010000 	strlt	r0, [r1, #-0]
    1820:	c0000003 	andgt	r0, r0, r3
    1824:	10000003 	andne	r0, r0, r3
    1828:	000003e3 	andeq	r0, r0, r3, ror #7
    182c:	00002511 	andeq	r2, r0, r1, lsl r5
    1830:	a2180000 	andsge	r0, r8, #0
    1834:	04000008 	streq	r0, [r0], #-8
    1838:	0b330e26 	bleq	cc50d8 <_bss_end+0xcbba74>
    183c:	d1010000 	mrsle	r0, (UNDEF: 1)
    1840:	10000003 	andne	r0, r0, r3
    1844:	000003e3 	andeq	r0, r0, r3, ror #7
    1848:	0003ee11 	andeq	lr, r3, r1, lsl lr
    184c:	19000000 	stmdbne	r0, {}	; <UNPREDICTABLE>
    1850:	0001d504 	andeq	sp, r1, r4, lsl #10
    1854:	22041400 	andcs	r1, r4, #0, 8
    1858:	03000003 	movweq	r0, #3
    185c:	000003e3 	andeq	r0, r0, r3, ror #7
    1860:	002c0414 	eoreq	r0, ip, r4, lsl r4
    1864:	3b150000 	blcc	54186c <_bss_end+0x538208>
    1868:	0400000c 	streq	r0, [r0], #-12
    186c:	03220e29 			; <UNDEFINED> instruction: 0x03220e29
    1870:	f41a0000 			; <UNDEFINED> instruction: 0xf41a0000
    1874:	01000003 	tsteq	r0, r3
    1878:	03050704 	movweq	r0, #22276	; 0x5704
    187c:	00009648 	andeq	r9, r0, r8, asr #12
    1880:	000c2c1b 	andeq	r2, ip, fp, lsl ip
    1884:	008d8000 	addeq	r8, sp, r0
    1888:	00001c00 	andeq	r1, r0, r0, lsl #24
    188c:	1c9c0100 	ldfnes	f0, [ip], {0}
    1890:	000003de 	ldrdeq	r0, [r0], -lr
    1894:	00008d2c 	andeq	r8, r0, ip, lsr #26
    1898:	00000054 	andeq	r0, r0, r4, asr r0
    189c:	044f9c01 	strbeq	r9, [pc], #-3073	; 18a4 <CPSR_IRQ_INHIBIT+0x1824>
    18a0:	d41d0000 	ldrle	r0, [sp], #-0
    18a4:	01000002 	tsteq	r0, r2
    18a8:	0038012f 	eorseq	r0, r8, pc, lsr #2
    18ac:	91020000 	mrsls	r0, (UNDEF: 2)
    18b0:	04081d74 	streq	r1, [r8], #-3444	; 0xfffff28c
    18b4:	2f010000 	svccs	0x00010000
    18b8:	00003801 	andeq	r3, r0, r1, lsl #16
    18bc:	70910200 	addsvc	r0, r1, r0, lsl #4
    18c0:	03c01e00 	biceq	r1, r0, #0, 28
    18c4:	29010000 	stmdbcs	r1, {}	; <UNPREDICTABLE>
    18c8:	00046906 	andeq	r6, r4, r6, lsl #18
    18cc:	008cc000 	addeq	ip, ip, r0
    18d0:	00006c00 	andeq	r6, r0, r0, lsl #24
    18d4:	929c0100 	addsls	r0, ip, #0, 2
    18d8:	1f000004 	svcne	0x00000004
    18dc:	00000413 	andeq	r0, r0, r3, lsl r4
    18e0:	000003e9 	andeq	r0, r0, r9, ror #7
    18e4:	206c9102 	rsbcs	r9, ip, r2, lsl #2
    18e8:	00727473 	rsbseq	r7, r2, r3, ror r4
    18ec:	ee1f2901 	vnmls.f16	s4, s30, s2
    18f0:	02000003 	andeq	r0, r0, #3
    18f4:	69216891 	stmdbvs	r1!, {r0, r4, r7, fp, sp, lr}
    18f8:	092b0100 	stmdbeq	fp!, {r8}
    18fc:	00000038 	andeq	r0, r0, r8, lsr r0
    1900:	00749102 	rsbseq	r9, r4, r2, lsl #2
    1904:	0003a01e 	andeq	sl, r3, lr, lsl r0
    1908:	06200100 	strteq	r0, [r0], -r0, lsl #2
    190c:	000004ac 	andeq	r0, r0, ip, lsr #9
    1910:	00008c4c 	andeq	r8, r0, ip, asr #24
    1914:	00000074 	andeq	r0, r0, r4, ror r0
    1918:	04c69c01 	strbeq	r9, [r6], #3073	; 0xc01
    191c:	131f0000 	tstne	pc, #0
    1920:	e9000004 	stmdb	r0, {r2}
    1924:	02000003 	andeq	r0, r0, #3
    1928:	63207491 			; <UNDEFINED> instruction: 0x63207491
    192c:	18200100 	stmdane	r0!, {r8}
    1930:	00000025 	andeq	r0, r0, r5, lsr #32
    1934:	00739102 	rsbseq	r9, r3, r2, lsl #2
    1938:	0003801e 	andeq	r8, r3, lr, lsl r0
    193c:	06150100 	ldreq	r0, [r5], -r0, lsl #2
    1940:	000004e0 	andeq	r0, r0, r0, ror #9
    1944:	00008bc4 	andeq	r8, r0, r4, asr #23
    1948:	00000088 	andeq	r0, r0, r8, lsl #1
    194c:	050b9c01 	streq	r9, [fp, #-3073]	; 0xfffff3ff
    1950:	131f0000 	tstne	pc, #0
    1954:	e9000004 	stmdb	r0, {r2}
    1958:	02000003 	andeq	r0, r0, #3
    195c:	7e1d6c91 	mrcvc	12, 0, r6, cr13, cr1, {4}
    1960:	0100000c 	tsteq	r0, ip
    1964:	02d52b15 	sbcseq	r2, r5, #21504	; 0x5400
    1968:	91020000 	mrsls	r0, (UNDEF: 2)
    196c:	61762168 	cmnvs	r6, r8, ror #2
    1970:	1701006c 	strne	r0, [r1, -ip, rrx]
    1974:	00006518 	andeq	r6, r0, r8, lsl r5
    1978:	74910200 	ldrvc	r0, [r1], #512	; 0x200
    197c:	03601e00 	cmneq	r0, #0, 28
    1980:	10010000 	andne	r0, r1, r0
    1984:	00052506 	andeq	r2, r5, r6, lsl #10
    1988:	008b6800 	addeq	r6, fp, r0, lsl #16
    198c:	00005c00 	andeq	r5, r0, r0, lsl #24
    1990:	419c0100 	orrsmi	r0, ip, r0, lsl #2
    1994:	1f000005 	svcne	0x00000005
    1998:	00000413 	andeq	r0, r0, r3, lsl r4
    199c:	000003e9 	andeq	r0, r0, r9, ror #7
    19a0:	206c9102 	rsbcs	r9, ip, r2, lsl #2
    19a4:	006e656c 	rsbeq	r6, lr, ip, ror #10
    19a8:	b62f1001 	strtlt	r1, [pc], -r1
    19ac:	02000002 	andeq	r0, r0, #2
    19b0:	22006891 	andcs	r6, r0, #9502720	; 0x910000
    19b4:	0000033c 	andeq	r0, r0, ip, lsr r3
    19b8:	52010601 	andpl	r0, r1, #1048576	; 0x100000
    19bc:	00000005 	andeq	r0, r0, r5
    19c0:	00000568 	andeq	r0, r0, r8, ror #10
    19c4:	00041323 	andeq	r1, r4, r3, lsr #6
    19c8:	0003e900 	andeq	lr, r3, r0, lsl #18
    19cc:	75612400 	strbvc	r2, [r1, #-1024]!	; 0xfffffc00
    19d0:	06010078 			; <UNDEFINED> instruction: 0x06010078
    19d4:	0003dd14 	andeq	sp, r3, r4, lsl sp
    19d8:	41250000 			; <UNDEFINED> instruction: 0x41250000
    19dc:	19000005 	stmdbne	r0, {r0, r2}
    19e0:	7f00000c 	svcvc	0x0000000c
    19e4:	c4000005 	strgt	r0, [r0], #-5
    19e8:	a400008a 	strge	r0, [r0], #-138	; 0xffffff76
    19ec:	01000000 	mrseq	r0, (UNDEF: 0)
    19f0:	0552269c 	ldrbeq	r2, [r2, #-1692]	; 0xfffff964
    19f4:	91020000 	mrsls	r0, (UNDEF: 2)
    19f8:	055b2674 	ldrbeq	r2, [fp, #-1652]	; 0xfffff98c
    19fc:	91020000 	mrsls	r0, (UNDEF: 2)
    1a00:	84000070 	strhi	r0, [r0], #-112	; 0xffffff90
    1a04:	04000009 	streq	r0, [r0], #-9
    1a08:	000a5b00 	andeq	r5, sl, r0, lsl #22
    1a0c:	14010400 	strne	r0, [r1], #-1024	; 0xfffffc00
    1a10:	04000000 	streq	r0, [r0], #-0
    1a14:	00000fe5 	andeq	r0, r0, r5, ror #31
    1a18:	0000013d 	andeq	r0, r0, sp, lsr r1
    1a1c:	00008d9c 	muleq	r0, ip, sp
    1a20:	00000264 	andeq	r0, r0, r4, ror #4
    1a24:	0000081c 	andeq	r0, r0, ip, lsl r8
    1a28:	02080102 	andeq	r0, r8, #-2147483648	; 0x80000000
    1a2c:	03000003 	movweq	r0, #3
    1a30:	00000025 	andeq	r0, r0, r5, lsr #32
    1a34:	b9050202 	stmdblt	r5, {r1, r9}
    1a38:	04000001 	streq	r0, [r0], #-1
    1a3c:	6e690504 	cdpvs	5, 6, cr0, cr9, cr4, {0}
    1a40:	d0050074 	andle	r0, r5, r4, ror r0
    1a44:	02000005 	andeq	r0, r0, #5
    1a48:	004b0709 	subeq	r0, fp, r9, lsl #14
    1a4c:	01020000 	mrseq	r0, (UNDEF: 2)
    1a50:	0002f908 	andeq	pc, r2, r8, lsl #18
    1a54:	07020200 	streq	r0, [r2, -r0, lsl #4]
    1a58:	00000391 	muleq	r0, r1, r3
    1a5c:	00021505 	andeq	r1, r2, r5, lsl #10
    1a60:	070b0200 	streq	r0, [fp, -r0, lsl #4]
    1a64:	0000006a 	andeq	r0, r0, sl, rrx
    1a68:	00005903 	andeq	r5, r0, r3, lsl #18
    1a6c:	07040200 	streq	r0, [r4, -r0, lsl #4]
    1a70:	000016fa 	strdeq	r1, [r0], -sl
    1a74:	00006a03 	andeq	r6, r0, r3, lsl #20
    1a78:	006a0600 	rsbeq	r0, sl, r0, lsl #12
    1a7c:	68070000 	stmdavs	r7, {}	; <UNPREDICTABLE>
    1a80:	03006c61 	movweq	r6, #3169	; 0xc61
    1a84:	02ef0b07 	rsceq	r0, pc, #7168	; 0x1c00
    1a88:	84080000 	strhi	r0, [r8], #-0
    1a8c:	03000004 	movweq	r0, #4
    1a90:	00711909 	rsbseq	r1, r1, r9, lsl #18
    1a94:	b2800000 	addlt	r0, r0, #0
    1a98:	c4080ee6 	strgt	r0, [r8], #-3814	; 0xfffff11a
    1a9c:	03000002 	movweq	r0, #2
    1aa0:	02fb1a0c 	rscseq	r1, fp, #12, 20	; 0xc000
    1aa4:	00000000 	andeq	r0, r0, r0
    1aa8:	54082000 	strpl	r2, [r8], #-0
    1aac:	03000003 	movweq	r0, #3
    1ab0:	02fb1a0f 	rscseq	r1, fp, #61440	; 0xf000
    1ab4:	00000000 	andeq	r0, r0, r0
    1ab8:	cf092020 	svcgt	0x00092020
    1abc:	03000003 	movweq	r0, #3
    1ac0:	00651512 	rsbeq	r1, r5, r2, lsl r5
    1ac4:	08360000 	ldmdaeq	r6!, {}	; <UNPREDICTABLE>
    1ac8:	00000446 	andeq	r0, r0, r6, asr #8
    1acc:	fb1a4403 	blx	692ae2 <_bss_end+0x68947e>
    1ad0:	00000002 	andeq	r0, r0, r2
    1ad4:	0a202150 	beq	80a01c <_bss_end+0x8009b8>
    1ad8:	000001d8 	ldrdeq	r0, [r0], -r8
    1adc:	00380405 	eorseq	r0, r8, r5, lsl #8
    1ae0:	46030000 	strmi	r0, [r3], -r0
    1ae4:	0001710d 	andeq	r7, r1, sp, lsl #2
    1ae8:	52490b00 	subpl	r0, r9, #0, 22
    1aec:	0c000051 	stceq	0, cr0, [r0], {81}	; 0x51
    1af0:	0000021e 	andeq	r0, r0, lr, lsl r2
    1af4:	04660c01 	strbteq	r0, [r6], #-3073	; 0xfffff3ff
    1af8:	0c100000 	ldceq	0, cr0, [r0], {-0}
    1afc:	00000346 	andeq	r0, r0, r6, asr #6
    1b00:	03780c11 	cmneq	r8, #4352	; 0x1100
    1b04:	0c120000 	ldceq	0, cr0, [r2], {-0}
    1b08:	000003ac 	andeq	r0, r0, ip, lsr #7
    1b0c:	034d0c13 	movteq	r0, #56339	; 0xdc13
    1b10:	0c140000 	ldceq	0, cr0, [r4], {-0}
    1b14:	00000475 	andeq	r0, r0, r5, ror r4
    1b18:	043f0c15 	ldrteq	r0, [pc], #-3093	; 1b20 <CPSR_IRQ_INHIBIT+0x1aa0>
    1b1c:	0c160000 	ldceq	0, cr0, [r6], {-0}
    1b20:	000004c0 	andeq	r0, r0, r0, asr #9
    1b24:	037f0c17 	cmneq	pc, #5888	; 0x1700
    1b28:	0c180000 	ldceq	0, cr0, [r8], {-0}
    1b2c:	0000044f 	andeq	r0, r0, pc, asr #8
    1b30:	03bd0c19 			; <UNDEFINED> instruction: 0x03bd0c19
    1b34:	0c1a0000 	ldceq	0, cr0, [sl], {-0}
    1b38:	000002ae 	andeq	r0, r0, lr, lsr #5
    1b3c:	02b90c20 	adcseq	r0, r9, #32, 24	; 0x2000
    1b40:	0c210000 	stceq	0, cr0, [r1], #-0
    1b44:	000003c5 	andeq	r0, r0, r5, asr #7
    1b48:	02920c22 	addseq	r0, r2, #8704	; 0x2200
    1b4c:	0c240000 	stceq	0, cr0, [r4], #-0
    1b50:	000003b3 			; <UNDEFINED> instruction: 0x000003b3
    1b54:	02e30c25 	rsceq	r0, r3, #9472	; 0x2500
    1b58:	0c300000 	ldceq	0, cr0, [r0], #-0
    1b5c:	000002ee 	andeq	r0, r0, lr, ror #5
    1b60:	01cd0c31 	biceq	r0, sp, r1, lsr ip
    1b64:	0c320000 	ldceq	0, cr0, [r2], #-0
    1b68:	000003a4 	andeq	r0, r0, r4, lsr #7
    1b6c:	01c30c34 	biceq	r0, r3, r4, lsr ip
    1b70:	00350000 	eorseq	r0, r5, r0
    1b74:	00024e0a 	andeq	r4, r2, sl, lsl #28
    1b78:	38040500 	stmdacc	r4, {r8, sl}
    1b7c:	03000000 	movweq	r0, #0
    1b80:	01960d6c 	orrseq	r0, r6, ip, ror #26
    1b84:	6c0c0000 	stcvs	0, cr0, [ip], {-0}
    1b88:	00000004 	andeq	r0, r0, r4
    1b8c:	0003870c 	andeq	r8, r3, ip, lsl #14
    1b90:	8c0c0100 	stfhis	f0, [ip], {-0}
    1b94:	02000003 	andeq	r0, r0, #3
    1b98:	04180800 	ldreq	r0, [r8], #-2048	; 0xfffff800
    1b9c:	73030000 	movwvc	r0, #12288	; 0x3000
    1ba0:	0002fb1a 	andeq	pc, r2, sl, lsl fp	; <UNPREDICTABLE>
    1ba4:	00b20000 	adcseq	r0, r2, r0
    1ba8:	0da40a20 			; <UNDEFINED> instruction: 0x0da40a20
    1bac:	04050000 	streq	r0, [r5], #-0
    1bb0:	00000038 	andeq	r0, r0, r8, lsr r0
    1bb4:	f50d7503 			; <UNDEFINED> instruction: 0xf50d7503
    1bb8:	0c000001 	stceq	0, cr0, [r0], {1}
    1bbc:	00000e4e 	andeq	r0, r0, lr, asr #28
    1bc0:	0fa20c00 	svceq	0x00a20c00
    1bc4:	0c010000 	stceq	0, cr0, [r1], {-0}
    1bc8:	00000fbb 			; <UNDEFINED> instruction: 0x00000fbb
    1bcc:	0f5e0c02 	svceq	0x005e0c02
    1bd0:	0c030000 	stceq	0, cr0, [r3], {-0}
    1bd4:	00000d43 	andeq	r0, r0, r3, asr #26
    1bd8:	0d500c04 	ldcleq	12, cr0, [r0, #-16]
    1bdc:	0c050000 	stceq	0, cr0, [r5], {-0}
    1be0:	00000f76 	andeq	r0, r0, r6, ror pc
    1be4:	10460c06 	subne	r0, r6, r6, lsl #24
    1be8:	0c070000 	stceq	0, cr0, [r7], {-0}
    1bec:	00001054 	andeq	r1, r0, r4, asr r0
    1bf0:	0e760c08 	cdpeq	12, 7, cr0, cr6, cr8, {0}
    1bf4:	00090000 	andeq	r0, r9, r0
    1bf8:	000deb0a 	andeq	lr, sp, sl, lsl #22
    1bfc:	38040500 	stmdacc	r4, {r8, sl}
    1c00:	03000000 	movweq	r0, #0
    1c04:	02380d83 	eorseq	r0, r8, #8384	; 0x20c0
    1c08:	e70c0000 	str	r0, [ip, -r0]
    1c0c:	0000000a 	andeq	r0, r0, sl
    1c10:	000d110c 	andeq	r1, sp, ip, lsl #2
    1c14:	a30c0100 	movwge	r0, #49408	; 0xc100
    1c18:	0200000e 	andeq	r0, r0, #14
    1c1c:	000eae0c 	andeq	sl, lr, ip, lsl #28
    1c20:	990c0300 	stmdbls	ip, {r8, r9}
    1c24:	0400000e 	streq	r0, [r0], #-14
    1c28:	000d070c 	andeq	r0, sp, ip, lsl #14
    1c2c:	c90c0500 	stmdbgt	ip, {r8, sl}
    1c30:	0600000d 	streq	r0, [r0], -sp
    1c34:	000dda0c 	andeq	sp, sp, ip, lsl #20
    1c38:	0a000700 	beq	3840 <CPSR_IRQ_INHIBIT+0x37c0>
    1c3c:	00000fb0 			; <UNDEFINED> instruction: 0x00000fb0
    1c40:	00380405 	eorseq	r0, r8, r5, lsl #8
    1c44:	8f030000 	svchi	0x00030000
    1c48:	0002990d 	andeq	r9, r2, sp, lsl #18
    1c4c:	55410b00 	strbpl	r0, [r1, #-2816]	; 0xfffff500
    1c50:	0c1d0058 	ldceq	0, cr0, [sp], {88}	; 0x58
    1c54:	00000f4b 	andeq	r0, r0, fp, asr #30
    1c58:	10620c2b 	rsbne	r0, r2, fp, lsr #24
    1c5c:	0c2d0000 	stceq	0, cr0, [sp], #-0
    1c60:	00001068 	andeq	r1, r0, r8, rrx
    1c64:	4d530b2e 	vldrmi	d16, [r3, #-184]	; 0xffffff48
    1c68:	0c300049 	ldceq	0, cr0, [r0], #-292	; 0xfffffedc
    1c6c:	00000fc9 	andeq	r0, r0, r9, asr #31
    1c70:	0fd00c31 	svceq	0x00d00c31
    1c74:	0c320000 	ldceq	0, cr0, [r2], #-0
    1c78:	00000fd7 	ldrdeq	r0, [r0], -r7
    1c7c:	0fde0c33 	svceq	0x00de0c33
    1c80:	0b340000 	bleq	d01c88 <_bss_end+0xcf8624>
    1c84:	00433249 	subeq	r3, r3, r9, asr #4
    1c88:	50530b35 	subspl	r0, r3, r5, lsr fp
    1c8c:	0b360049 	bleq	d81db8 <_bss_end+0xd78754>
    1c90:	004d4350 	subeq	r4, sp, r0, asr r3
    1c94:	09730c37 	ldmdbeq	r3!, {r0, r1, r2, r4, r5, sl, fp}^
    1c98:	00390000 	eorseq	r0, r9, r0
    1c9c:	0001ae08 	andeq	sl, r1, r8, lsl #28
    1ca0:	1aa60300 	bne	fe9828a8 <_bss_end+0xfe979244>
    1ca4:	000002fb 	strdeq	r0, [r0], -fp
    1ca8:	2000b400 	andcs	fp, r0, r0, lsl #8
    1cac:	000af30d 	andeq	pc, sl, sp, lsl #6
    1cb0:	38040500 	stmdacc	r4, {r8, sl}
    1cb4:	03000000 	movweq	r0, #0
    1cb8:	140c0da8 	strne	r0, [ip], #-3496	; 0xfffff258
    1cbc:	0000000c 	andeq	r0, r0, ip
    1cc0:	000bbe0c 	andeq	fp, fp, ip, lsl #28
    1cc4:	620c0100 	andvs	r0, ip, #0, 2
    1cc8:	0200000f 	andeq	r0, r0, #15
    1ccc:	000b470c 	andeq	r4, fp, ip, lsl #14
    1cd0:	d30c0300 	movwle	r0, #49920	; 0xc300
    1cd4:	04000008 	streq	r0, [r0], #-8
    1cd8:	000a320c 	andeq	r3, sl, ip, lsl #4
    1cdc:	bc0c0500 	cfstr32lt	mvfx0, [ip], {-0}
    1ce0:	06000009 	streq	r0, [r0], -r9
    1ce4:	0008e00c 	andeq	lr, r8, ip
    1ce8:	e60c0700 	str	r0, [ip], -r0, lsl #14
    1cec:	0800000b 	stmdaeq	r0, {r0, r1, r3}
    1cf0:	870e0000 	strhi	r0, [lr, -r0]
    1cf4:	02000000 	andeq	r0, r0, #0
    1cf8:	16f50704 	ldrbtne	r0, [r5], r4, lsl #14
    1cfc:	f4030000 	vst4.8	{d0-d3}, [r3], r0
    1d00:	0e000002 	cdpeq	0, 0, cr0, cr0, cr2, {0}
    1d04:	00000097 	muleq	r0, r7, r0
    1d08:	0000a70e 	andeq	sl, r0, lr, lsl #14
    1d0c:	00b70e00 	adcseq	r0, r7, r0, lsl #28
    1d10:	c40e0000 	strgt	r0, [lr], #-0
    1d14:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    1d18:	00000196 	muleq	r0, r6, r1
    1d1c:	0002990e 	andeq	r9, r2, lr, lsl #18
    1d20:	0c270f00 	stceq	15, cr0, [r7], #-0
    1d24:	04040000 	streq	r0, [r4], #-0
    1d28:	03e20706 	mvneq	r0, #1572864	; 0x180000
    1d2c:	d7100000 	ldrle	r0, [r0, -r0]
    1d30:	04000001 	streq	r0, [r0], #-1
    1d34:	03e81d0a 	mvneq	r1, #640	; 0x280
    1d38:	11000000 	mrsne	r0, (UNDEF: 0)
    1d3c:	00000c27 	andeq	r0, r0, r7, lsr #24
    1d40:	5e090d04 	cdppl	13, 0, cr0, cr9, cr4, {0}
    1d44:	ed000002 	stc	0, cr0, [r0, #-8]
    1d48:	01000003 	tsteq	r0, r3
    1d4c:	00000351 	andeq	r0, r0, r1, asr r3
    1d50:	0000035c 	andeq	r0, r0, ip, asr r3
    1d54:	0003ed12 	andeq	lr, r3, r2, lsl sp
    1d58:	006a1300 	rsbeq	r1, sl, r0, lsl #6
    1d5c:	14000000 	strne	r0, [r0], #-0
    1d60:	00000f80 	andeq	r0, r0, r0, lsl #31
    1d64:	260e1004 	strcs	r1, [lr], -r4
    1d68:	01000002 	tsteq	r0, r2
    1d6c:	00000371 	andeq	r0, r0, r1, ror r3
    1d70:	0000037c 	andeq	r0, r0, ip, ror r3
    1d74:	0003ed12 	andeq	lr, r3, r2, lsl sp
    1d78:	01711300 	cmneq	r1, r0, lsl #6
    1d7c:	14000000 	strne	r0, [r0], #-0
    1d80:	00000e80 	andeq	r0, r0, r0, lsl #29
    1d84:	970e1204 	strls	r1, [lr, -r4, lsl #4]
    1d88:	01000004 	tsteq	r0, r4
    1d8c:	00000391 	muleq	r0, r1, r3
    1d90:	0000039c 	muleq	r0, ip, r3
    1d94:	0003ed12 	andeq	lr, r3, r2, lsl sp
    1d98:	01711300 	cmneq	r1, r0, lsl #6
    1d9c:	14000000 	strne	r0, [r0], #-0
    1da0:	00000432 	andeq	r0, r0, r2, lsr r4
    1da4:	6b0e1504 	blvs	3871bc <_bss_end+0x37db58>
    1da8:	01000002 	tsteq	r0, r2
    1dac:	000003b1 			; <UNDEFINED> instruction: 0x000003b1
    1db0:	000003c1 	andeq	r0, r0, r1, asr #7
    1db4:	0003ed12 	andeq	lr, r3, r2, lsl sp
    1db8:	00d41300 	sbcseq	r1, r4, r0, lsl #6
    1dbc:	59130000 	ldmdbpl	r3, {}	; <UNPREDICTABLE>
    1dc0:	00000000 	andeq	r0, r0, r0
    1dc4:	00036b15 	andeq	r6, r3, r5, lsl fp
    1dc8:	12170400 	andsne	r0, r7, #0, 8
    1dcc:	000001e0 	andeq	r0, r0, r0, ror #3
    1dd0:	00000059 	andeq	r0, r0, r9, asr r0
    1dd4:	0003d601 	andeq	sp, r3, r1, lsl #12
    1dd8:	03ed1200 	mvneq	r1, #0, 4
    1ddc:	d4130000 	ldrle	r0, [r3], #-0
    1de0:	00000000 	andeq	r0, r0, r0
    1de4:	6a041600 	bvs	1075ec <_bss_end+0xfdf88>
    1de8:	03000000 	movweq	r0, #0
    1dec:	000003e2 	andeq	r0, r0, r2, ror #7
    1df0:	031e0416 	tsteq	lr, #369098752	; 0x16000000
    1df4:	a9170000 	ldmdbge	r7, {}	; <UNPREDICTABLE>
    1df8:	04000002 	streq	r0, [r0], #-2
    1dfc:	031e0d1a 	tsteq	lr, #1664	; 0x680
    1e00:	6e0a0000 	cdpvs	0, 0, cr0, cr10, cr0, {0}
    1e04:	0500000b 	streq	r0, [r0, #-11]
    1e08:	00003804 	andeq	r3, r0, r4, lsl #16
    1e0c:	0c060500 	cfstr32eq	mvfx0, [r6], {-0}
    1e10:	0000041e 	andeq	r0, r0, lr, lsl r4
    1e14:	0009040c 	andeq	r0, r9, ip, lsl #8
    1e18:	3b0c0000 	blcc	301e20 <_bss_end+0x2f87bc>
    1e1c:	01000009 	tsteq	r0, r9
    1e20:	0a7b0a00 	beq	1ec4628 <_bss_end+0x1ebafc4>
    1e24:	04050000 	streq	r0, [r5], #-0
    1e28:	00000038 	andeq	r0, r0, r8, lsr r0
    1e2c:	6b0c0c05 	blvs	304e48 <_bss_end+0x2fb7e4>
    1e30:	18000004 	stmdane	r0, {r2}
    1e34:	00000a58 	andeq	r0, r0, r8, asr sl
    1e38:	9a1804b0 	bls	603100 <_bss_end+0x5f9a9c>
    1e3c:	60000008 	andvs	r0, r0, r8
    1e40:	08fc1809 	ldmeq	ip!, {r0, r3, fp, ip}^
    1e44:	12c00000 	sbcne	r0, r0, #0
    1e48:	000bd118 	andeq	sp, fp, r8, lsl r1
    1e4c:	18258000 	stmdane	r5!, {pc}
    1e50:	000009c3 	andeq	r0, r0, r3, asr #19
    1e54:	cc184b00 			; <UNDEFINED> instruction: 0xcc184b00
    1e58:	00000009 	andeq	r0, r0, r9
    1e5c:	09e71896 	stmibeq	r7!, {r1, r2, r4, r7, fp, ip}^
    1e60:	e1000000 	mrs	r0, (UNDEF: 0)
    1e64:	000a1619 	andeq	r1, sl, r9, lsl r6
    1e68:	01c20000 	biceq	r0, r2, r0
    1e6c:	720f0000 	andvc	r0, pc, #0
    1e70:	04000009 	streq	r0, [r0], #-9
    1e74:	26071805 	strcs	r1, [r7], -r5, lsl #16
    1e78:	10000005 	andne	r0, r0, r5
    1e7c:	000008db 	ldrdeq	r0, [r0], -fp
    1e80:	260f1b05 	strcs	r1, [pc], -r5, lsl #22
    1e84:	00000005 	andeq	r0, r0, r5
    1e88:	00097211 	andeq	r7, r9, r1, lsl r2
    1e8c:	091e0500 	ldmdbeq	lr, {r8, sl}
    1e90:	00000b80 	andeq	r0, r0, r0, lsl #23
    1e94:	0000052c 	andeq	r0, r0, ip, lsr #10
    1e98:	00049e01 	andeq	r9, r4, r1, lsl #28
    1e9c:	0004a900 	andeq	sl, r4, r0, lsl #18
    1ea0:	052c1200 	streq	r1, [ip, #-512]!	; 0xfffffe00
    1ea4:	26130000 	ldrcs	r0, [r3], -r0
    1ea8:	00000005 	andeq	r0, r0, r5
    1eac:	00092b14 	andeq	r2, r9, r4, lsl fp
    1eb0:	0e200500 	cfsh64eq	mvdx0, mvdx0, #0
    1eb4:	00000b51 	andeq	r0, r0, r1, asr fp
    1eb8:	0004be01 	andeq	fp, r4, r1, lsl #28
    1ebc:	0004c900 	andeq	ip, r4, r0, lsl #18
    1ec0:	052c1200 	streq	r1, [ip, #-512]!	; 0xfffffe00
    1ec4:	ff130000 			; <UNDEFINED> instruction: 0xff130000
    1ec8:	00000003 	andeq	r0, r0, r3
    1ecc:	0009ff14 	andeq	pc, r9, r4, lsl pc	; <UNPREDICTABLE>
    1ed0:	0e210500 	cfsh64eq	mvdx0, mvdx1, #0
    1ed4:	00000a60 	andeq	r0, r0, r0, ror #20
    1ed8:	0004de01 	andeq	sp, r4, r1, lsl #28
    1edc:	0004e900 	andeq	lr, r4, r0, lsl #18
    1ee0:	052c1200 	streq	r1, [ip, #-512]!	; 0xfffffe00
    1ee4:	1e130000 	cdpne	0, 1, cr0, cr3, cr0, {0}
    1ee8:	00000004 	andeq	r0, r0, r4
    1eec:	0008a214 	andeq	sl, r8, r4, lsl r2
    1ef0:	0e250500 	cfsh64eq	mvdx0, mvdx5, #0
    1ef4:	00000b0c 	andeq	r0, r0, ip, lsl #22
    1ef8:	0004fe01 	andeq	pc, r4, r1, lsl #28
    1efc:	00050900 	andeq	r0, r5, r0, lsl #18
    1f00:	052c1200 	streq	r1, [ip, #-512]!	; 0xfffffe00
    1f04:	25130000 	ldrcs	r0, [r3, #-0]
    1f08:	00000000 	andeq	r0, r0, r0
    1f0c:	0008a21a 	andeq	sl, r8, sl, lsl r2
    1f10:	0e260500 	cfsh64eq	mvdx0, mvdx6, #0
    1f14:	00000b33 	andeq	r0, r0, r3, lsr fp
    1f18:	00051a01 	andeq	r1, r5, r1, lsl #20
    1f1c:	052c1200 	streq	r1, [ip, #-512]!	; 0xfffffe00
    1f20:	32130000 	andscc	r0, r3, #0
    1f24:	00000005 	andeq	r0, r0, r5
    1f28:	1e041b00 	vmlane.f64	d1, d4, d0
    1f2c:	16000003 	strne	r0, [r0], -r3
    1f30:	00046b04 	andeq	r6, r4, r4, lsl #22
    1f34:	2c041600 	stccs	6, cr1, [r4], {-0}
    1f38:	17000000 	strne	r0, [r0, -r0]
    1f3c:	00000c3b 	andeq	r0, r0, fp, lsr ip
    1f40:	6b0e2905 	blvs	38c35c <_bss_end+0x382cf8>
    1f44:	0f000004 	svceq	0x00000004
    1f48:	00000e60 	andeq	r0, r0, r0, ror #28
    1f4c:	07050604 	streq	r0, [r5, -r4, lsl #12]
    1f50:	00000623 	andeq	r0, r0, r3, lsr #12
    1f54:	000eb910 	andeq	fp, lr, r0, lsl r9
    1f58:	20090600 	andcs	r0, r9, r0, lsl #12
    1f5c:	00000623 	andeq	r0, r0, r3, lsr #12
    1f60:	08631100 	stmdaeq	r3!, {r8, ip}^
    1f64:	0c060000 	stceq	0, cr0, [r6], {-0}
    1f68:	000cc620 	andeq	ip, ip, r0, lsr #12
    1f6c:	00062900 	andeq	r2, r6, r0, lsl #18
    1f70:	05770200 	ldrbeq	r0, [r7, #-512]!	; 0xfffffe00
    1f74:	05820000 	streq	r0, [r2]
    1f78:	2f120000 	svccs	0x00120000
    1f7c:	13000006 	movwne	r0, #6
    1f80:	000001a6 	andeq	r0, r0, r6, lsr #3
    1f84:	0e601100 	poweqs	f1, f0, f0
    1f88:	0f060000 	svceq	0x00060000
    1f8c:	000f2c09 	andeq	r2, pc, r9, lsl #24
    1f90:	00062f00 	andeq	r2, r6, r0, lsl #30
    1f94:	059b0100 	ldreq	r0, [fp, #256]	; 0x100
    1f98:	05a60000 	streq	r0, [r6, #0]!
    1f9c:	2f120000 	svccs	0x00120000
    1fa0:	13000006 	movwne	r0, #6
    1fa4:	000002f4 	strdeq	r0, [r0], -r4
    1fa8:	0e881400 	cdpeq	4, 8, cr1, cr8, cr0, {0}
    1fac:	12060000 	andne	r0, r6, #0
    1fb0:	000ee60e 	andeq	lr, lr, lr, lsl #12
    1fb4:	05bb0100 	ldreq	r0, [fp, #256]!	; 0x100
    1fb8:	05c60000 	strbeq	r0, [r6]
    1fbc:	2f120000 	svccs	0x00120000
    1fc0:	13000006 	movwne	r0, #6
    1fc4:	000001f5 	strdeq	r0, [r0], -r5
    1fc8:	10291400 	eorne	r1, r9, r0, lsl #8
    1fcc:	14060000 	strne	r0, [r6], #-0
    1fd0:	000d5d0e 	andeq	r5, sp, lr, lsl #26
    1fd4:	05db0100 	ldrbeq	r0, [fp, #256]	; 0x100
    1fd8:	05e60000 	strbeq	r0, [r6, #0]!
    1fdc:	2f120000 	svccs	0x00120000
    1fe0:	13000006 	movwne	r0, #6
    1fe4:	000001f5 	strdeq	r0, [r0], -r5
    1fe8:	0d191400 	cfldrseq	mvf1, [r9, #-0]
    1fec:	17060000 	strne	r0, [r6, -r0]
    1ff0:	000c830e 	andeq	r8, ip, lr, lsl #6
    1ff4:	05fb0100 	ldrbeq	r0, [fp, #256]!	; 0x100
    1ff8:	06060000 	streq	r0, [r6], -r0
    1ffc:	2f120000 	svccs	0x00120000
    2000:	13000006 	movwne	r0, #6
    2004:	00000238 	andeq	r0, r0, r8, lsr r2
    2008:	0dbd1a00 			; <UNDEFINED> instruction: 0x0dbd1a00
    200c:	19060000 	stmdbne	r6, {}	; <UNPREDICTABLE>
    2010:	000dfc0e 	andeq	pc, sp, lr, lsl #24
    2014:	06170100 	ldreq	r0, [r7], -r0, lsl #2
    2018:	2f120000 	svccs	0x00120000
    201c:	13000006 	movwne	r0, #6
    2020:	00000238 	andeq	r0, r0, r8, lsr r2
    2024:	04160000 	ldreq	r0, [r6], #-0
    2028:	00000076 	andeq	r0, r0, r6, ror r0
    202c:	0076041b 	rsbseq	r0, r6, fp, lsl r4
    2030:	04160000 	ldreq	r0, [r6], #-0
    2034:	00000544 	andeq	r0, r0, r4, asr #10
    2038:	00062f03 	andeq	r2, r6, r3, lsl #30
    203c:	0ed81700 	cdpeq	7, 13, cr1, cr8, cr0, {0}
    2040:	1c060000 	stcne	0, cr0, [r6], {-0}
    2044:	0005441e 	andeq	r4, r5, lr, lsl r4
    2048:	08c20a00 	stmiaeq	r2, {r9, fp}^
    204c:	01070000 	mrseq	r0, (UNDEF: 7)
    2050:	0000003f 	andeq	r0, r0, pc, lsr r0
    2054:	6b0c0607 	blvs	303878 <_bss_end+0x2fa214>
    2058:	0c000006 	stceq	0, cr0, [r0], {6}
    205c:	00000950 	andeq	r0, r0, r0, asr r9
    2060:	0bc40c00 	bleq	ff105068 <_bss_end+0xff0fba04>
    2064:	0c010000 	stceq	0, cr0, [r1], {-0}
    2068:	00000ba4 	andeq	r0, r0, r4, lsr #23
    206c:	e60f0002 	str	r0, [pc], -r2
    2070:	0800000a 	stmdaeq	r0, {r1, r3}
    2074:	63070d07 	movwvs	r0, #32007	; 0x7d07
    2078:	10000007 	andne	r0, r0, r7
    207c:	0000085c 	andeq	r0, r0, ip, asr r8
    2080:	23201507 			; <UNDEFINED> instruction: 0x23201507
    2084:	00000006 	andeq	r0, r0, r6
    2088:	0008ec1c 	andeq	lr, r8, ip, lsl ip
    208c:	0f110700 	svceq	0x00110700
    2090:	00000763 	andeq	r0, r0, r3, ror #14
    2094:	0a8b1001 	beq	fe2c60a0 <_bss_end+0xfe2bca3c>
    2098:	18070000 	stmdane	r7, {}	; <UNPREDICTABLE>
    209c:	00068519 	andeq	r8, r6, r9, lsl r5
    20a0:	63110400 	tstvs	r1, #0, 8
    20a4:	07000008 	streq	r0, [r0, -r8]
    20a8:	0bf3201b 	bleq	ffcca11c <_bss_end+0xffcc0ab8>
    20ac:	06290000 	strteq	r0, [r9], -r0
    20b0:	b8020000 	stmdalt	r2, {}	; <UNPREDICTABLE>
    20b4:	c3000006 	movwgt	r0, #6
    20b8:	12000006 	andne	r0, r0, #6
    20bc:	0000076a 	andeq	r0, r0, sl, ror #14
    20c0:	0002a913 	andeq	sl, r2, r3, lsl r9
    20c4:	e6110000 	ldr	r0, [r1], -r0
    20c8:	0700000a 	streq	r0, [r0, -sl]
    20cc:	0868091e 	stmdaeq	r8!, {r1, r2, r3, r4, r8, fp}^
    20d0:	076a0000 	strbeq	r0, [sl, -r0]!
    20d4:	dc010000 	stcle	0, cr0, [r1], {-0}
    20d8:	e7000006 	str	r0, [r0, -r6]
    20dc:	12000006 	andne	r0, r0, #6
    20e0:	0000076a 	andeq	r0, r0, sl, ror #14
    20e4:	0002f413 	andeq	pc, r2, r3, lsl r4	; <UNPREDICTABLE>
    20e8:	80140000 	andshi	r0, r4, r0
    20ec:	0700000f 	streq	r0, [r0, -pc]
    20f0:	08a80e21 	stmiaeq	r8!, {r0, r5, r9, sl, fp}
    20f4:	fc010000 	stc2	0, cr0, [r1], {-0}
    20f8:	11000006 	tstne	r0, r6
    20fc:	12000007 	andne	r0, r0, #7
    2100:	0000076a 	andeq	r0, r0, sl, ror #14
    2104:	00068513 	andeq	r8, r6, r3, lsl r5
    2108:	006a1300 	rsbeq	r1, sl, r0, lsl #6
    210c:	46130000 	ldrmi	r0, [r3], -r0
    2110:	00000006 	andeq	r0, r0, r6
    2114:	000e8014 	andeq	r8, lr, r4, lsl r0
    2118:	0e230700 	cdpeq	7, 2, cr0, cr3, cr0, {0}
    211c:	00000b1e 	andeq	r0, r0, lr, lsl fp
    2120:	00072601 	andeq	r2, r7, r1, lsl #12
    2124:	00072c00 	andeq	r2, r7, r0, lsl #24
    2128:	076a1200 	strbeq	r1, [sl, -r0, lsl #4]!
    212c:	14000000 	strne	r0, [r0], #-0
    2130:	00000bd9 	ldrdeq	r0, [r0], -r9
    2134:	3d0e2607 	stccc	6, cr2, [lr, #-28]	; 0xffffffe4
    2138:	0100000a 	tsteq	r0, sl
    213c:	00000741 	andeq	r0, r0, r1, asr #14
    2140:	00000747 	andeq	r0, r0, r7, asr #14
    2144:	00076a12 	andeq	r6, r7, r2, lsl sl
    2148:	8a150000 	bhi	542150 <_bss_end+0x538aec>
    214c:	07000009 	streq	r0, [r0, -r9]
    2150:	08770e28 	ldmdaeq	r7!, {r3, r5, r9, sl, fp}^
    2154:	07700000 	ldrbeq	r0, [r0, -r0]!
    2158:	5c010000 	stcpl	0, cr0, [r1], {-0}
    215c:	12000007 	andne	r0, r0, #7
    2160:	0000076a 	andeq	r0, r0, sl, ror #14
    2164:	04160000 	ldreq	r0, [r6], #-0
    2168:	00000769 	andeq	r0, r0, r9, ror #14
    216c:	6b04161d 	blvs	1079e8 <_bss_end+0xfe384>
    2170:	02000006 	andeq	r0, r0, #6
    2174:	05e60201 	strbeq	r0, [r6, #513]!	; 0x201
    2178:	6b170000 	blvs	5c2180 <_bss_end+0x5b8b1c>
    217c:	07000009 	streq	r0, [r0, -r9]
    2180:	066b0f2b 	strbteq	r0, [fp], -fp, lsr #30
    2184:	3a1e0000 	bcc	78218c <_bss_end+0x778b28>
    2188:	01000006 	tsteq	r0, r6
    218c:	0305170b 	movweq	r1, #22283	; 0x570b
    2190:	0000964c 	andeq	r9, r0, ip, asr #12
    2194:	000ec91f 	andeq	ip, lr, pc, lsl r9
    2198:	008fe400 	addeq	lr, pc, r0, lsl #8
    219c:	00001c00 	andeq	r1, r0, r0, lsl #24
    21a0:	209c0100 	addscs	r0, ip, r0, lsl #2
    21a4:	000003de 	ldrdeq	r0, [r0], -lr
    21a8:	00008f90 	muleq	r0, r0, pc	; <UNPREDICTABLE>
    21ac:	00000054 	andeq	r0, r0, r4, asr r0
    21b0:	07d29c01 	ldrbeq	r9, [r2, r1, lsl #24]
    21b4:	d4210000 	strtle	r0, [r1], #-0
    21b8:	01000002 	tsteq	r0, r2
    21bc:	00380146 	eorseq	r0, r8, r6, asr #2
    21c0:	91020000 	mrsls	r0, (UNDEF: 2)
    21c4:	04082174 	streq	r2, [r8], #-372	; 0xfffffe8c
    21c8:	46010000 	strmi	r0, [r1], -r0
    21cc:	00003801 	andeq	r3, r0, r1, lsl #16
    21d0:	70910200 	addsvc	r0, r1, r0, lsl #4
    21d4:	06062200 	streq	r2, [r6], -r0, lsl #4
    21d8:	41010000 	mrsmi	r0, (UNDEF: 1)
    21dc:	0007ec06 	andeq	lr, r7, r6, lsl #24
    21e0:	008f4000 	addeq	r4, pc, r0
    21e4:	00005000 	andeq	r5, r0, r0
    21e8:	179c0100 	ldrne	r0, [ip, r0, lsl #2]
    21ec:	23000008 	movwcs	r0, #8
    21f0:	00000413 	andeq	r0, r0, r3, lsl r4
    21f4:	00000635 	andeq	r0, r0, r5, lsr r6
    21f8:	21649102 	cmncs	r4, r2, lsl #2
    21fc:	0000103b 	andeq	r1, r0, fp, lsr r0
    2200:	38394101 	ldmdacc	r9!, {r0, r8, lr}
    2204:	02000002 	andeq	r0, r0, #2
    2208:	bd246091 	stclt	0, cr6, [r4, #-580]!	; 0xfffffdbc
    220c:	0100000c 	tsteq	r0, ip
    2210:	00711843 	rsbseq	r1, r1, r3, asr #16
    2214:	91020000 	mrsls	r0, (UNDEF: 2)
    2218:	e622006c 	strt	r0, [r2], -ip, rrx
    221c:	01000005 	tsteq	r0, r5
    2220:	0831063a 	ldmdaeq	r1!, {r1, r3, r4, r5, r9, sl}
    2224:	8ef00000 	cdphi	0, 15, cr0, cr0, cr0, {0}
    2228:	00500000 	subseq	r0, r0, r0
    222c:	9c010000 	stcls	0, cr0, [r1], {-0}
    2230:	0000085c 	andeq	r0, r0, ip, asr r8
    2234:	00041323 	andeq	r1, r4, r3, lsr #6
    2238:	00063500 	andeq	r3, r6, r0, lsl #10
    223c:	64910200 	ldrvs	r0, [r1], #512	; 0x200
    2240:	00103b21 	andseq	r3, r0, r1, lsr #22
    2244:	383a0100 	ldmdacc	sl!, {r8}
    2248:	00000238 	andeq	r0, r0, r8, lsr r2
    224c:	24609102 	strbtcs	r9, [r0], #-258	; 0xfffffefe
    2250:	00000cbd 			; <UNDEFINED> instruction: 0x00000cbd
    2254:	71183c01 	tstvc	r8, r1, lsl #24
    2258:	02000000 	andeq	r0, r0, #0
    225c:	22006c91 	andcs	r6, r0, #37120	; 0x9100
    2260:	000005c6 	andeq	r0, r0, r6, asr #11
    2264:	76063501 	strvc	r3, [r6], -r1, lsl #10
    2268:	ac000008 	stcge	0, cr0, [r0], {8}
    226c:	4400008e 	strmi	r0, [r0], #-142	; 0xffffff72
    2270:	01000000 	mrseq	r0, (UNDEF: 0)
    2274:	0008929c 	muleq	r8, ip, r2
    2278:	04132300 	ldreq	r2, [r3], #-768	; 0xfffffd00
    227c:	06350000 	ldrteq	r0, [r5], -r0
    2280:	91020000 	mrsls	r0, (UNDEF: 2)
    2284:	103b216c 	eorsne	r2, fp, ip, ror #2
    2288:	35010000 	strcc	r0, [r1, #-0]
    228c:	0001f545 	andeq	pc, r1, r5, asr #10
    2290:	68910200 	ldmvs	r1, {r9}
    2294:	05a62200 	streq	r2, [r6, #512]!	; 0x200
    2298:	30010000 	andcc	r0, r1, r0
    229c:	0008ac06 	andeq	sl, r8, r6, lsl #24
    22a0:	008e6800 	addeq	r6, lr, r0, lsl #16
    22a4:	00004400 	andeq	r4, r0, r0, lsl #8
    22a8:	c89c0100 	ldmgt	ip, {r8}
    22ac:	23000008 	movwcs	r0, #8
    22b0:	00000413 	andeq	r0, r0, r3, lsl r4
    22b4:	00000635 	andeq	r0, r0, r5, lsr r6
    22b8:	216c9102 	cmncs	ip, r2, lsl #2
    22bc:	0000103b 	andeq	r1, r0, fp, lsr r0
    22c0:	f5443001 			; <UNDEFINED> instruction: 0xf5443001
    22c4:	02000001 	andeq	r0, r0, #1
    22c8:	25006891 	strcs	r6, [r0, #-2193]	; 0xfffff76f
    22cc:	0000055e 	andeq	r0, r0, lr, asr r5
    22d0:	e2182b01 	ands	r2, r8, #1024	; 0x400
    22d4:	30000008 	andcc	r0, r0, r8
    22d8:	3800008e 	stmdacc	r0, {r1, r2, r3, r7}
    22dc:	01000000 	mrseq	r0, (UNDEF: 0)
    22e0:	0008fe9c 	muleq	r8, ip, lr
    22e4:	04132300 	ldreq	r2, [r3], #-768	; 0xfffffd00
    22e8:	06350000 	ldrteq	r0, [r5], -r0
    22ec:	91020000 	mrsls	r0, (UNDEF: 2)
    22f0:	65722674 	ldrbvs	r2, [r2, #-1652]!	; 0xfffff98c
    22f4:	2b010067 	blcs	42498 <_bss_end+0x38e34>
    22f8:	0001a652 	andeq	sl, r1, r2, asr r6
    22fc:	70910200 	addsvc	r0, r1, r0, lsl #4
    2300:	05822700 	streq	r2, [r2, #1792]	; 0x700
    2304:	25010000 	strcs	r0, [r1, #-0]
    2308:	00090f01 	andeq	r0, r9, r1, lsl #30
    230c:	09250000 	stmdbeq	r5!, {}	; <UNPREDICTABLE>
    2310:	13280000 			; <UNDEFINED> instruction: 0x13280000
    2314:	35000004 	strcc	r0, [r0, #-4]
    2318:	29000006 	stmdbcs	r0, {r1, r2}
    231c:	000009fa 	strdeq	r0, [r0], -sl
    2320:	f43c2501 			; <UNDEFINED> instruction: 0xf43c2501
    2324:	00000002 	andeq	r0, r0, r2
    2328:	0008fe2a 	andeq	pc, r8, sl, lsr #28
    232c:	000d2400 	andeq	r2, sp, r0, lsl #8
    2330:	00094000 	andeq	r4, r9, r0
    2334:	008dfc00 	addeq	pc, sp, r0, lsl #24
    2338:	00003400 	andeq	r3, r0, r0, lsl #8
    233c:	519c0100 	orrspl	r0, ip, r0, lsl #2
    2340:	2b000009 	blcs	236c <CPSR_IRQ_INHIBIT+0x22ec>
    2344:	0000090f 	andeq	r0, r0, pc, lsl #18
    2348:	2b749102 	blcs	1d26758 <_bss_end+0x1d1d0f4>
    234c:	00000918 	andeq	r0, r0, r8, lsl r9
    2350:	00709102 	rsbseq	r9, r0, r2, lsl #2
    2354:	000e372c 	andeq	r3, lr, ip, lsr #14
    2358:	331e0100 	tstcc	lr, #0, 2
    235c:	00008dec 	andeq	r8, r0, ip, ror #27
    2360:	00000010 	andeq	r0, r0, r0, lsl r0
    2364:	6a2d9c01 	bvs	b69370 <_bss_end+0xb5fd0c>
    2368:	0100000f 	tsteq	r0, pc
    236c:	8db43314 	ldchi	3, cr3, [r4, #80]!	; 0x50
    2370:	00380000 	eorseq	r0, r8, r0
    2374:	9c010000 	stcls	0, cr0, [r1], {-0}
    2378:	000f872c 	andeq	r8, pc, ip, lsr #14
    237c:	330f0100 	movwcc	r0, #61696	; 0xf100
    2380:	00008d9c 	muleq	r0, ip, sp
    2384:	00000018 	andeq	r0, r0, r8, lsl r0
    2388:	99009c01 	stmdbls	r0, {r0, sl, fp, ip, pc}
    238c:	04000009 	streq	r0, [r0], #-9
    2390:	000d2800 	andeq	r2, sp, r0, lsl #16
    2394:	14010400 	strne	r0, [r1], #-1024	; 0xfffffc00
    2398:	04000000 	streq	r0, [r0], #-0
    239c:	00001078 	andeq	r1, r0, r8, ror r0
    23a0:	0000013d 	andeq	r0, r0, sp, lsr r1
    23a4:	00009000 	andeq	r9, r0, r0
    23a8:	00000100 	andeq	r0, r0, r0, lsl #2
    23ac:	00000a1e 	andeq	r0, r0, lr, lsl sl
    23b0:	02080102 	andeq	r0, r8, #-2147483648	; 0x80000000
    23b4:	03000003 	movweq	r0, #3
    23b8:	00000025 	andeq	r0, r0, r5, lsr #32
    23bc:	b9050202 	stmdblt	r5, {r1, r9}
    23c0:	04000001 	streq	r0, [r0], #-1
    23c4:	6e690504 	cdpvs	5, 6, cr0, cr9, cr4, {0}
    23c8:	38050074 	stmdacc	r5, {r2, r4, r5, r6}
    23cc:	06000000 	streq	r0, [r0], -r0
    23d0:	000005d0 	ldrdeq	r0, [r0], -r0	; <UNPREDICTABLE>
    23d4:	50070902 	andpl	r0, r7, r2, lsl #18
    23d8:	02000000 	andeq	r0, r0, #0
    23dc:	02f90801 	rscseq	r0, r9, #65536	; 0x10000
    23e0:	02020000 	andeq	r0, r2, #0
    23e4:	00039107 	andeq	r9, r3, r7, lsl #2
    23e8:	02150600 	andseq	r0, r5, #0, 12
    23ec:	0b020000 	bleq	823f4 <_bss_end+0x78d90>
    23f0:	00006f07 	andeq	r6, r0, r7, lsl #30
    23f4:	005e0300 	subseq	r0, lr, r0, lsl #6
    23f8:	04020000 	streq	r0, [r2], #-0
    23fc:	0016fa07 	andseq	pc, r6, r7, lsl #20
    2400:	006f0300 	rsbeq	r0, pc, r0, lsl #6
    2404:	6f050000 	svcvs	0x00050000
    2408:	07000000 	streq	r0, [r0, -r0]
    240c:	000007a0 	andeq	r0, r0, r0, lsr #15
    2410:	00440107 	subeq	r0, r4, r7, lsl #2
    2414:	06030000 	streq	r0, [r3], -r0
    2418:	0000c90c 	andeq	ip, r0, ip, lsl #18
    241c:	081f0800 	ldmdaeq	pc, {fp}	; <UNPREDICTABLE>
    2420:	08000000 	stmdaeq	r0, {}	; <UNPREDICTABLE>
    2424:	00000830 	andeq	r0, r0, r0, lsr r8
    2428:	084f0801 	stmdaeq	pc, {r0, fp}^	; <UNPREDICTABLE>
    242c:	08020000 	stmdaeq	r2, {}	; <UNPREDICTABLE>
    2430:	00000849 	andeq	r0, r0, r9, asr #16
    2434:	08370803 	ldmdaeq	r7!, {r0, r1, fp}
    2438:	08040000 	stmdaeq	r4, {}	; <UNPREDICTABLE>
    243c:	0000083d 	andeq	r0, r0, sp, lsr r8
    2440:	07af0805 	streq	r0, [pc, r5, lsl #16]!
    2444:	08060000 	stmdaeq	r6, {}	; <UNPREDICTABLE>
    2448:	00000843 	andeq	r0, r0, r3, asr #16
    244c:	05eb0807 	strbeq	r0, [fp, #2055]!	; 0x807
    2450:	00080000 	andeq	r0, r8, r0
    2454:	00061509 	andeq	r1, r6, r9, lsl #10
    2458:	1a030400 	bne	c3460 <_bss_end+0xb9dfc>
    245c:	00022a07 	andeq	r2, r2, r7, lsl #20
    2460:	05b30a00 	ldreq	r0, [r3, #2560]!	; 0xa00
    2464:	1e030000 	cdpne	0, 0, cr0, cr3, cr0, {0}
    2468:	00023517 	andeq	r3, r2, r7, lsl r5
    246c:	5a0b0000 	bpl	2c2474 <_bss_end+0x2b8e10>
    2470:	03000007 	movweq	r0, #7
    2474:	05830822 	streq	r0, [r3, #2082]	; 0x822
    2478:	023a0000 	eorseq	r0, sl, #0
    247c:	fc020000 	stc2	0, cr0, [r2], {-0}
    2480:	11000000 	mrsne	r0, (UNDEF: 0)
    2484:	0c000001 	stceq	0, cr0, [r0], {1}
    2488:	00000241 	andeq	r0, r0, r1, asr #4
    248c:	00005e0d 	andeq	r5, r0, sp, lsl #28
    2490:	02470d00 	subeq	r0, r7, #0, 26
    2494:	470d0000 	strmi	r0, [sp, -r0]
    2498:	00000002 	andeq	r0, r0, r2
    249c:	00080c0b 	andeq	r0, r8, fp, lsl #24
    24a0:	08240300 	stmdaeq	r4!, {r8, r9}
    24a4:	000006db 	ldrdeq	r0, [r0], -fp
    24a8:	0000023a 	andeq	r0, r0, sl, lsr r2
    24ac:	00012a02 	andeq	r2, r1, r2, lsl #20
    24b0:	00013f00 	andeq	r3, r1, r0, lsl #30
    24b4:	02410c00 	subeq	r0, r1, #0, 24
    24b8:	5e0d0000 	cdppl	0, 0, cr0, cr13, cr0, {0}
    24bc:	0d000000 	stceq	0, cr0, [r0, #-0]
    24c0:	00000247 	andeq	r0, r0, r7, asr #4
    24c4:	0002470d 	andeq	r4, r2, sp, lsl #14
    24c8:	230b0000 	movwcs	r0, #45056	; 0xb000
    24cc:	03000006 	movweq	r0, #6
    24d0:	07dd0826 	ldrbeq	r0, [sp, r6, lsr #16]
    24d4:	023a0000 	eorseq	r0, sl, #0
    24d8:	58020000 	stmdapl	r2, {}	; <UNPREDICTABLE>
    24dc:	6d000001 	stcvs	0, cr0, [r0, #-4]
    24e0:	0c000001 	stceq	0, cr0, [r0], {1}
    24e4:	00000241 	andeq	r0, r0, r1, asr #4
    24e8:	00005e0d 	andeq	r5, r0, sp, lsl #28
    24ec:	02470d00 	subeq	r0, r7, #0, 26
    24f0:	470d0000 	strmi	r0, [sp, -r0]
    24f4:	00000002 	andeq	r0, r0, r2
    24f8:	0006780b 	andeq	r7, r6, fp, lsl #16
    24fc:	08280300 	stmdaeq	r8!, {r8, r9}
    2500:	000004cb 	andeq	r0, r0, fp, asr #9
    2504:	0000023a 	andeq	r0, r0, sl, lsr r2
    2508:	00018602 	andeq	r8, r1, r2, lsl #12
    250c:	00019b00 	andeq	r9, r1, r0, lsl #22
    2510:	02410c00 	subeq	r0, r1, #0, 24
    2514:	5e0d0000 	cdppl	0, 0, cr0, cr13, cr0, {0}
    2518:	0d000000 	stceq	0, cr0, [r0, #-0]
    251c:	00000247 	andeq	r0, r0, r7, asr #4
    2520:	0002470d 	andeq	r4, r2, sp, lsl #14
    2524:	150b0000 	strne	r0, [fp, #-0]
    2528:	03000006 	movweq	r0, #6
    252c:	05b9032b 	ldreq	r0, [r9, #811]!	; 0x32b
    2530:	024d0000 	subeq	r0, sp, #0
    2534:	b4010000 	strlt	r0, [r1], #-0
    2538:	bf000001 	svclt	0x00000001
    253c:	0c000001 	stceq	0, cr0, [r0], {1}
    2540:	0000024d 	andeq	r0, r0, sp, asr #4
    2544:	00006f0d 	andeq	r6, r0, sp, lsl #30
    2548:	c30e0000 	movwgt	r0, #57344	; 0xe000
    254c:	03000007 	movweq	r0, #7
    2550:	0777082e 	ldrbeq	r0, [r7, -lr, lsr #16]!
    2554:	d4010000 	strle	r0, [r1], #-0
    2558:	e4000001 	str	r0, [r0], #-1
    255c:	0c000001 	stceq	0, cr0, [r0], {1}
    2560:	0000024d 	andeq	r0, r0, sp, asr #4
    2564:	00005e0d 	andeq	r5, r0, sp, lsl #28
    2568:	00800d00 	addeq	r0, r0, r0, lsl #26
    256c:	0b000000 	bleq	2574 <CPSR_IRQ_INHIBIT+0x24f4>
    2570:	00000603 	andeq	r0, r0, r3, lsl #12
    2574:	8b123003 	blhi	48e588 <_bss_end+0x484f24>
    2578:	80000006 	andhi	r0, r0, r6
    257c:	01000000 	mrseq	r0, (UNDEF: 0)
    2580:	000001fd 	strdeq	r0, [r0], -sp
    2584:	00000208 	andeq	r0, r0, r8, lsl #4
    2588:	0002410c 	andeq	r4, r2, ip, lsl #2
    258c:	005e0d00 	subseq	r0, lr, r0, lsl #26
    2590:	0f000000 	svceq	0x00000000
    2594:	0000082c 	andeq	r0, r0, ip, lsr #16
    2598:	61083303 	tstvs	r8, r3, lsl #6
    259c:	01000005 	tsteq	r0, r5
    25a0:	00000219 	andeq	r0, r0, r9, lsl r2
    25a4:	00024d0c 	andeq	r4, r2, ip, lsl #26
    25a8:	005e0d00 	subseq	r0, lr, r0, lsl #26
    25ac:	3a0d0000 	bcc	3425b4 <_bss_end+0x338f50>
    25b0:	00000002 	andeq	r0, r0, r2
    25b4:	00c90300 	sbceq	r0, r9, r0, lsl #6
    25b8:	04100000 	ldreq	r0, [r0], #-0
    25bc:	0000006f 	andeq	r0, r0, pc, rrx
    25c0:	00022f03 	andeq	r2, r2, r3, lsl #30
    25c4:	02010200 	andeq	r0, r1, #0, 4
    25c8:	000005e6 	andeq	r0, r0, r6, ror #11
    25cc:	022a0410 	eoreq	r0, sl, #16, 8	; 0x10000000
    25d0:	04110000 	ldreq	r0, [r1], #-0
    25d4:	0000005e 	andeq	r0, r0, lr, asr r0
    25d8:	00c90410 	sbceq	r0, r9, r0, lsl r4
    25dc:	19120000 	ldmdbne	r2, {}	; <UNPREDICTABLE>
    25e0:	03000007 	movweq	r0, #7
    25e4:	00c91637 	sbceq	r1, r9, r7, lsr r6
    25e8:	68130000 	ldmdavs	r3, {}	; <UNPREDICTABLE>
    25ec:	04006c61 	streq	r6, [r0], #-3169	; 0xfffff39f
    25f0:	04d30b07 	ldrbeq	r0, [r3], #2823	; 0xb07
    25f4:	84140000 	ldrhi	r0, [r4], #-0
    25f8:	04000004 	streq	r0, [r0], #-4
    25fc:	00761909 	rsbseq	r1, r6, r9, lsl #18
    2600:	b2800000 	addlt	r0, r0, #0
    2604:	c4140ee6 	ldrgt	r0, [r4], #-3814	; 0xfffff11a
    2608:	04000002 	streq	r0, [r0], #-2
    260c:	04df1a0c 	ldrbeq	r1, [pc], #2572	; 2614 <CPSR_IRQ_INHIBIT+0x2594>
    2610:	00000000 	andeq	r0, r0, r0
    2614:	54142000 	ldrpl	r2, [r4], #-0
    2618:	04000003 	streq	r0, [r0], #-3
    261c:	04df1a0f 	ldrbeq	r1, [pc], #2575	; 2624 <CPSR_IRQ_INHIBIT+0x25a4>
    2620:	00000000 	andeq	r0, r0, r0
    2624:	cf152020 	svcgt	0x00152020
    2628:	04000003 	streq	r0, [r0], #-3
    262c:	006a1512 	rsbeq	r1, sl, r2, lsl r5
    2630:	14360000 	ldrtne	r0, [r6], #-0
    2634:	00000446 	andeq	r0, r0, r6, asr #8
    2638:	df1a4404 	svcle	0x001a4404
    263c:	00000004 	andeq	r0, r0, r4
    2640:	07202150 			; <UNDEFINED> instruction: 0x07202150
    2644:	000001d8 	ldrdeq	r0, [r0], -r8
    2648:	00380405 	eorseq	r0, r8, r5, lsl #8
    264c:	46040000 	strmi	r0, [r4], -r0
    2650:	0003550d 	andeq	r5, r3, sp, lsl #10
    2654:	52491600 	subpl	r1, r9, #0, 12
    2658:	08000051 	stmdaeq	r0, {r0, r4, r6}
    265c:	0000021e 	andeq	r0, r0, lr, lsl r2
    2660:	04660801 	strbteq	r0, [r6], #-2049	; 0xfffff7ff
    2664:	08100000 	ldmdaeq	r0, {}	; <UNPREDICTABLE>
    2668:	00000346 	andeq	r0, r0, r6, asr #6
    266c:	03780811 	cmneq	r8, #1114112	; 0x110000
    2670:	08120000 	ldmdaeq	r2, {}	; <UNPREDICTABLE>
    2674:	000003ac 	andeq	r0, r0, ip, lsr #7
    2678:	034d0813 	movteq	r0, #55315	; 0xd813
    267c:	08140000 	ldmdaeq	r4, {}	; <UNPREDICTABLE>
    2680:	00000475 	andeq	r0, r0, r5, ror r4
    2684:	043f0815 	ldrteq	r0, [pc], #-2069	; 268c <CPSR_IRQ_INHIBIT+0x260c>
    2688:	08160000 	ldmdaeq	r6, {}	; <UNPREDICTABLE>
    268c:	000004c0 	andeq	r0, r0, r0, asr #9
    2690:	037f0817 	cmneq	pc, #1507328	; 0x170000
    2694:	08180000 	ldmdaeq	r8, {}	; <UNPREDICTABLE>
    2698:	0000044f 	andeq	r0, r0, pc, asr #8
    269c:	03bd0819 			; <UNDEFINED> instruction: 0x03bd0819
    26a0:	081a0000 	ldmdaeq	sl, {}	; <UNPREDICTABLE>
    26a4:	000002ae 	andeq	r0, r0, lr, lsr #5
    26a8:	02b90820 	adcseq	r0, r9, #32, 16	; 0x200000
    26ac:	08210000 	stmdaeq	r1!, {}	; <UNPREDICTABLE>
    26b0:	000003c5 	andeq	r0, r0, r5, asr #7
    26b4:	02920822 	addseq	r0, r2, #2228224	; 0x220000
    26b8:	08240000 	stmdaeq	r4!, {}	; <UNPREDICTABLE>
    26bc:	000003b3 			; <UNDEFINED> instruction: 0x000003b3
    26c0:	02e30825 	rsceq	r0, r3, #2424832	; 0x250000
    26c4:	08300000 	ldmdaeq	r0!, {}	; <UNPREDICTABLE>
    26c8:	000002ee 	andeq	r0, r0, lr, ror #5
    26cc:	01cd0831 	biceq	r0, sp, r1, lsr r8
    26d0:	08320000 	ldmdaeq	r2!, {}	; <UNPREDICTABLE>
    26d4:	000003a4 	andeq	r0, r0, r4, lsr #7
    26d8:	01c30834 	biceq	r0, r3, r4, lsr r8
    26dc:	00350000 	eorseq	r0, r5, r0
    26e0:	00024e07 	andeq	r4, r2, r7, lsl #28
    26e4:	38040500 	stmdacc	r4, {r8, sl}
    26e8:	04000000 	streq	r0, [r0], #-0
    26ec:	037a0d6c 	cmneq	sl, #108, 26	; 0x1b00
    26f0:	6c080000 	stcvs	0, cr0, [r8], {-0}
    26f4:	00000004 	andeq	r0, r0, r4
    26f8:	00038708 	andeq	r8, r3, r8, lsl #14
    26fc:	8c080100 	stfhis	f0, [r8], {-0}
    2700:	02000003 	andeq	r0, r0, #3
    2704:	04181400 	ldreq	r1, [r8], #-1024	; 0xfffffc00
    2708:	73040000 	movwvc	r0, #16384	; 0x4000
    270c:	0004df1a 	andeq	sp, r4, sl, lsl pc
    2710:	00b20000 	adcseq	r0, r2, r0
    2714:	0da40720 	stceq	7, cr0, [r4, #128]!	; 0x80
    2718:	04050000 	streq	r0, [r5], #-0
    271c:	00000038 	andeq	r0, r0, r8, lsr r0
    2720:	d90d7504 	stmdble	sp, {r2, r8, sl, ip, sp, lr}
    2724:	08000003 	stmdaeq	r0, {r0, r1}
    2728:	00000e4e 	andeq	r0, r0, lr, asr #28
    272c:	0fa20800 	svceq	0x00a20800
    2730:	08010000 	stmdaeq	r1, {}	; <UNPREDICTABLE>
    2734:	00000fbb 			; <UNDEFINED> instruction: 0x00000fbb
    2738:	0f5e0802 	svceq	0x005e0802
    273c:	08030000 	stmdaeq	r3, {}	; <UNPREDICTABLE>
    2740:	00000d43 	andeq	r0, r0, r3, asr #26
    2744:	0d500804 	ldcleq	8, cr0, [r0, #-16]
    2748:	08050000 	stmdaeq	r5, {}	; <UNPREDICTABLE>
    274c:	00000f76 	andeq	r0, r0, r6, ror pc
    2750:	10460806 	subne	r0, r6, r6, lsl #16
    2754:	08070000 	stmdaeq	r7, {}	; <UNPREDICTABLE>
    2758:	00001054 	andeq	r1, r0, r4, asr r0
    275c:	0e760808 	cdpeq	8, 7, cr0, cr6, cr8, {0}
    2760:	00090000 	andeq	r0, r9, r0
    2764:	000deb07 	andeq	lr, sp, r7, lsl #22
    2768:	38040500 	stmdacc	r4, {r8, sl}
    276c:	04000000 	streq	r0, [r0], #-0
    2770:	041c0d83 	ldreq	r0, [ip], #-3459	; 0xfffff27d
    2774:	e7080000 	str	r0, [r8, -r0]
    2778:	0000000a 	andeq	r0, r0, sl
    277c:	000d1108 	andeq	r1, sp, r8, lsl #2
    2780:	a3080100 	movwge	r0, #33024	; 0x8100
    2784:	0200000e 	andeq	r0, r0, #14
    2788:	000eae08 	andeq	sl, lr, r8, lsl #28
    278c:	99080300 	stmdbls	r8, {r8, r9}
    2790:	0400000e 	streq	r0, [r0], #-14
    2794:	000d0708 	andeq	r0, sp, r8, lsl #14
    2798:	c9080500 	stmdbgt	r8, {r8, sl}
    279c:	0600000d 	streq	r0, [r0], -sp
    27a0:	000dda08 	andeq	sp, sp, r8, lsl #20
    27a4:	07000700 	streq	r0, [r0, -r0, lsl #14]
    27a8:	00000fb0 			; <UNDEFINED> instruction: 0x00000fb0
    27ac:	00380405 	eorseq	r0, r8, r5, lsl #8
    27b0:	8f040000 	svchi	0x00040000
    27b4:	00047d0d 	andeq	r7, r4, sp, lsl #26
    27b8:	55411600 	strbpl	r1, [r1, #-1536]	; 0xfffffa00
    27bc:	081d0058 	ldmdaeq	sp, {r3, r4, r6}
    27c0:	00000f4b 	andeq	r0, r0, fp, asr #30
    27c4:	1062082b 	rsbne	r0, r2, fp, lsr #16
    27c8:	082d0000 	stmdaeq	sp!, {}	; <UNPREDICTABLE>
    27cc:	00001068 	andeq	r1, r0, r8, rrx
    27d0:	4d53162e 	ldclmi	6, cr1, [r3, #-184]	; 0xffffff48
    27d4:	08300049 	ldmdaeq	r0!, {r0, r3, r6}
    27d8:	00000fc9 	andeq	r0, r0, r9, asr #31
    27dc:	0fd00831 	svceq	0x00d00831
    27e0:	08320000 	ldmdaeq	r2!, {}	; <UNPREDICTABLE>
    27e4:	00000fd7 	ldrdeq	r0, [r0], -r7
    27e8:	0fde0833 	svceq	0x00de0833
    27ec:	16340000 	ldrtne	r0, [r4], -r0
    27f0:	00433249 	subeq	r3, r3, r9, asr #4
    27f4:	50531635 	subspl	r1, r3, r5, lsr r6
    27f8:	16360049 	ldrtne	r0, [r6], -r9, asr #32
    27fc:	004d4350 	subeq	r4, sp, r0, asr r3
    2800:	09730837 	ldmdbeq	r3!, {r0, r1, r2, r4, r5, fp}^
    2804:	00390000 	eorseq	r0, r9, r0
    2808:	0001ae14 	andeq	sl, r1, r4, lsl lr
    280c:	1aa60400 	bne	fe983814 <_bss_end+0xfe97a1b0>
    2810:	000004df 	ldrdeq	r0, [r0], -pc	; <UNPREDICTABLE>
    2814:	2000b400 	andcs	fp, r0, r0, lsl #8
    2818:	000af317 	andeq	pc, sl, r7, lsl r3	; <UNPREDICTABLE>
    281c:	38040500 	stmdacc	r4, {r8, sl}
    2820:	04000000 	streq	r0, [r0], #-0
    2824:	14080da8 	strne	r0, [r8], #-3496	; 0xfffff258
    2828:	0000000c 	andeq	r0, r0, ip
    282c:	000bbe08 	andeq	fp, fp, r8, lsl #28
    2830:	62080100 	andvs	r0, r8, #0, 2
    2834:	0200000f 	andeq	r0, r0, #15
    2838:	000b4708 	andeq	r4, fp, r8, lsl #14
    283c:	d3080300 	movwle	r0, #33536	; 0x8300
    2840:	04000008 	streq	r0, [r0], #-8
    2844:	000a3208 	andeq	r3, sl, r8, lsl #4
    2848:	bc080500 	cfstr32lt	mvfx0, [r8], {-0}
    284c:	06000009 	streq	r0, [r0], -r9
    2850:	0008e008 	andeq	lr, r8, r8
    2854:	e6080700 	str	r0, [r8], -r0, lsl #14
    2858:	0800000b 	stmdaeq	r0, {r0, r1, r3}
    285c:	6b180000 	blvs	602864 <_bss_end+0x5f9200>
    2860:	02000002 	andeq	r0, r0, #2
    2864:	16f50704 	ldrbtne	r0, [r5], r4, lsl #14
    2868:	d8030000 	stmdale	r3, {}	; <UNPREDICTABLE>
    286c:	18000004 	stmdane	r0, {r2}
    2870:	0000027b 	andeq	r0, r0, fp, ror r2
    2874:	00028b18 	andeq	r8, r2, r8, lsl fp
    2878:	029b1800 	addseq	r1, fp, #0, 16
    287c:	a8180000 	ldmdage	r8, {}	; <UNPREDICTABLE>
    2880:	18000002 	stmdane	r0, {r1}
    2884:	0000037a 	andeq	r0, r0, sl, ror r3
    2888:	00047d18 	andeq	r7, r4, r8, lsl sp
    288c:	0c270900 			; <UNDEFINED> instruction: 0x0c270900
    2890:	05040000 	streq	r0, [r4, #-0]
    2894:	05c60706 	strbeq	r0, [r6, #1798]	; 0x706
    2898:	d70a0000 	strle	r0, [sl, -r0]
    289c:	05000001 	streq	r0, [r0, #-1]
    28a0:	02351d0a 	eorseq	r1, r5, #640	; 0x280
    28a4:	0b000000 	bleq	28ac <CPSR_IRQ_INHIBIT+0x282c>
    28a8:	00000c27 	andeq	r0, r0, r7, lsr #24
    28ac:	5e090d05 	cdppl	13, 0, cr0, cr9, cr5, {0}
    28b0:	c6000002 	strgt	r0, [r0], -r2
    28b4:	01000005 	tsteq	r0, r5
    28b8:	00000535 	andeq	r0, r0, r5, lsr r5
    28bc:	00000540 	andeq	r0, r0, r0, asr #10
    28c0:	0005c60c 	andeq	ip, r5, ip, lsl #12
    28c4:	006f0d00 	rsbeq	r0, pc, r0, lsl #26
    28c8:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    28cc:	00000f80 	andeq	r0, r0, r0, lsl #31
    28d0:	260e1005 	strcs	r1, [lr], -r5
    28d4:	01000002 	tsteq	r0, r2
    28d8:	00000555 	andeq	r0, r0, r5, asr r5
    28dc:	00000560 	andeq	r0, r0, r0, ror #10
    28e0:	0005c60c 	andeq	ip, r5, ip, lsl #12
    28e4:	03550d00 	cmpeq	r5, #0, 26
    28e8:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    28ec:	00000e80 	andeq	r0, r0, r0, lsl #29
    28f0:	970e1205 	strls	r1, [lr, -r5, lsl #4]
    28f4:	01000004 	tsteq	r0, r4
    28f8:	00000575 	andeq	r0, r0, r5, ror r5
    28fc:	00000580 	andeq	r0, r0, r0, lsl #11
    2900:	0005c60c 	andeq	ip, r5, ip, lsl #12
    2904:	03550d00 	cmpeq	r5, #0, 26
    2908:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    290c:	00000432 	andeq	r0, r0, r2, lsr r4
    2910:	6b0e1505 	blvs	387d2c <_bss_end+0x37e6c8>
    2914:	01000002 	tsteq	r0, r2
    2918:	00000595 	muleq	r0, r5, r5
    291c:	000005a5 	andeq	r0, r0, r5, lsr #11
    2920:	0005c60c 	andeq	ip, r5, ip, lsl #12
    2924:	02b80d00 	adcseq	r0, r8, #0, 26
    2928:	5e0d0000 	cdppl	0, 0, cr0, cr13, cr0, {0}
    292c:	00000000 	andeq	r0, r0, r0
    2930:	00036b19 	andeq	r6, r3, r9, lsl fp
    2934:	12170500 	andsne	r0, r7, #0, 10
    2938:	000001e0 	andeq	r0, r0, r0, ror #3
    293c:	0000005e 	andeq	r0, r0, lr, asr r0
    2940:	0005ba01 	andeq	fp, r5, r1, lsl #20
    2944:	05c60c00 	strbeq	r0, [r6, #3072]	; 0xc00
    2948:	b80d0000 	stmdalt	sp, {}	; <UNPREDICTABLE>
    294c:	00000002 	andeq	r0, r0, r2
    2950:	02041000 	andeq	r1, r4, #0
    2954:	12000005 	andne	r0, r0, #5
    2958:	000002a9 	andeq	r0, r0, r9, lsr #5
    295c:	020d1a05 	andeq	r1, sp, #20480	; 0x5000
    2960:	07000005 	streq	r0, [r0, -r5]
    2964:	00000b6e 	andeq	r0, r0, lr, ror #22
    2968:	00380405 	eorseq	r0, r8, r5, lsl #8
    296c:	06060000 	streq	r0, [r6], -r0
    2970:	0005f70c 	andeq	pc, r5, ip, lsl #14
    2974:	09040800 	stmdbeq	r4, {fp}
    2978:	08000000 	stmdaeq	r0, {}	; <UNPREDICTABLE>
    297c:	0000093b 	andeq	r0, r0, fp, lsr r9
    2980:	7b070001 	blvc	1c298c <_bss_end+0x1b9328>
    2984:	0500000a 	streq	r0, [r0, #-10]
    2988:	00003804 	andeq	r3, r0, r4, lsl #16
    298c:	0c0c0600 	stceq	6, cr0, [ip], {-0}
    2990:	00000644 	andeq	r0, r0, r4, asr #12
    2994:	000a581a 	andeq	r5, sl, sl, lsl r8
    2998:	1a04b000 	bne	12e9a0 <_bss_end+0x12533c>
    299c:	0000089a 	muleq	r0, sl, r8
    29a0:	fc1a0960 	ldc2	9, cr0, [sl], {96}	; 0x60	; <UNPREDICTABLE>
    29a4:	c0000008 	andgt	r0, r0, r8
    29a8:	0bd11a12 	bleq	ff4491f8 <_bss_end+0xff43fb94>
    29ac:	25800000 	strcs	r0, [r0]
    29b0:	0009c31a 	andeq	ip, r9, sl, lsl r3
    29b4:	1a4b0000 	bne	12c29bc <_bss_end+0x12b9358>
    29b8:	000009cc 	andeq	r0, r0, ip, asr #19
    29bc:	e71a9600 	ldr	r9, [sl, -r0, lsl #12]
    29c0:	00000009 	andeq	r0, r0, r9
    29c4:	0a161be1 	beq	589950 <_bss_end+0x5802ec>
    29c8:	c2000000 	andgt	r0, r0, #0
    29cc:	09000001 	stmdbeq	r0, {r0}
    29d0:	00000972 	andeq	r0, r0, r2, ror r9
    29d4:	07180604 	ldreq	r0, [r8, -r4, lsl #12]
    29d8:	000006ff 	strdeq	r0, [r0], -pc	; <UNPREDICTABLE>
    29dc:	0008db0a 	andeq	sp, r8, sl, lsl #22
    29e0:	0f1b0600 	svceq	0x001b0600
    29e4:	000006ff 	strdeq	r0, [r0], -pc	; <UNPREDICTABLE>
    29e8:	09720b00 	ldmdbeq	r2!, {r8, r9, fp}^
    29ec:	1e060000 	cdpne	0, 0, cr0, cr6, cr0, {0}
    29f0:	000b8009 	andeq	r8, fp, r9
    29f4:	00070500 	andeq	r0, r7, r0, lsl #10
    29f8:	06770100 	ldrbteq	r0, [r7], -r0, lsl #2
    29fc:	06820000 	streq	r0, [r2], r0
    2a00:	050c0000 	streq	r0, [ip, #-0]
    2a04:	0d000007 	stceq	0, cr0, [r0, #-28]	; 0xffffffe4
    2a08:	000006ff 	strdeq	r0, [r0], -pc	; <UNPREDICTABLE>
    2a0c:	092b0e00 	stmdbeq	fp!, {r9, sl, fp}
    2a10:	20060000 	andcs	r0, r6, r0
    2a14:	000b510e 	andeq	r5, fp, lr, lsl #2
    2a18:	06970100 	ldreq	r0, [r7], r0, lsl #2
    2a1c:	06a20000 	strteq	r0, [r2], r0
    2a20:	050c0000 	streq	r0, [ip, #-0]
    2a24:	0d000007 	stceq	0, cr0, [r0, #-28]	; 0xffffffe4
    2a28:	000005d8 	ldrdeq	r0, [r0], -r8
    2a2c:	09ff0e00 	ldmibeq	pc!, {r9, sl, fp}^	; <UNPREDICTABLE>
    2a30:	21060000 	mrscs	r0, (UNDEF: 6)
    2a34:	000a600e 	andeq	r6, sl, lr
    2a38:	06b70100 	ldrteq	r0, [r7], r0, lsl #2
    2a3c:	06c20000 	strbeq	r0, [r2], r0
    2a40:	050c0000 	streq	r0, [ip, #-0]
    2a44:	0d000007 	stceq	0, cr0, [r0, #-28]	; 0xffffffe4
    2a48:	000005f7 	strdeq	r0, [r0], -r7
    2a4c:	08a20e00 	stmiaeq	r2!, {r9, sl, fp}
    2a50:	25060000 	strcs	r0, [r6, #-0]
    2a54:	000b0c0e 	andeq	r0, fp, lr, lsl #24
    2a58:	06d70100 	ldrbeq	r0, [r7], r0, lsl #2
    2a5c:	06e20000 	strbteq	r0, [r2], r0
    2a60:	050c0000 	streq	r0, [ip, #-0]
    2a64:	0d000007 	stceq	0, cr0, [r0, #-28]	; 0xffffffe4
    2a68:	00000025 	andeq	r0, r0, r5, lsr #32
    2a6c:	08a20f00 	stmiaeq	r2!, {r8, r9, sl, fp}
    2a70:	26060000 	strcs	r0, [r6], -r0
    2a74:	000b330e 	andeq	r3, fp, lr, lsl #6
    2a78:	06f30100 	ldrbteq	r0, [r3], r0, lsl #2
    2a7c:	050c0000 	streq	r0, [ip, #-0]
    2a80:	0d000007 	stceq	0, cr0, [r0, #-28]	; 0xffffffe4
    2a84:	0000070b 	andeq	r0, r0, fp, lsl #14
    2a88:	04110000 	ldreq	r0, [r1], #-0
    2a8c:	00000502 	andeq	r0, r0, r2, lsl #10
    2a90:	06440410 			; <UNDEFINED> instruction: 0x06440410
    2a94:	04100000 	ldreq	r0, [r0], #-0
    2a98:	0000002c 	andeq	r0, r0, ip, lsr #32
    2a9c:	000c3b12 	andeq	r3, ip, r2, lsl fp
    2aa0:	0e290600 	cfmadda32eq	mvax0, mvax0, mvfx9, mvfx0
    2aa4:	00000644 	andeq	r0, r0, r4, asr #12
    2aa8:	0008c207 	andeq	ip, r8, r7, lsl #4
    2aac:	44010700 	strmi	r0, [r1], #-1792	; 0xfffff900
    2ab0:	07000000 	streq	r0, [r0, -r0]
    2ab4:	07420c06 	strbeq	r0, [r2, -r6, lsl #24]
    2ab8:	50080000 	andpl	r0, r8, r0
    2abc:	00000009 	andeq	r0, r0, r9
    2ac0:	000bc408 	andeq	ip, fp, r8, lsl #8
    2ac4:	a4080100 	strge	r0, [r8], #-256	; 0xffffff00
    2ac8:	0200000b 	andeq	r0, r0, #11
    2acc:	0ae60900 	beq	ff984ed4 <_bss_end+0xff97b870>
    2ad0:	07080000 	streq	r0, [r8, -r0]
    2ad4:	083a070d 	ldmdaeq	sl!, {r0, r2, r3, r8, r9, sl}
    2ad8:	5c0a0000 	stcpl	0, cr0, [sl], {-0}
    2adc:	07000008 	streq	r0, [r0, -r8]
    2ae0:	083a2015 	ldmdaeq	sl!, {r0, r2, r4, sp}
    2ae4:	1c000000 	stcne	0, cr0, [r0], {-0}
    2ae8:	000008ec 	andeq	r0, r0, ip, ror #17
    2aec:	400f1107 	andmi	r1, pc, r7, lsl #2
    2af0:	01000008 	tsteq	r0, r8
    2af4:	000a8b0a 	andeq	r8, sl, sl, lsl #22
    2af8:	19180700 	ldmdbne	r8, {r8, r9, sl}
    2afc:	0000075c 	andeq	r0, r0, ip, asr r7
    2b00:	08630b04 	stmdaeq	r3!, {r2, r8, r9, fp}^
    2b04:	1b070000 	blne	1c2b0c <_bss_end+0x1b94a8>
    2b08:	000bf320 	andeq	pc, fp, r0, lsr #6
    2b0c:	00084700 	andeq	r4, r8, r0, lsl #14
    2b10:	078f0200 	streq	r0, [pc, r0, lsl #4]
    2b14:	079a0000 	ldreq	r0, [sl, r0]
    2b18:	4d0c0000 	stcmi	0, cr0, [ip, #-0]
    2b1c:	0d000008 	stceq	0, cr0, [r0, #-32]	; 0xffffffe0
    2b20:	0000048d 	andeq	r0, r0, sp, lsl #9
    2b24:	0ae60b00 	beq	ff98572c <_bss_end+0xff97c0c8>
    2b28:	1e070000 	cdpne	0, 0, cr0, cr7, cr0, {0}
    2b2c:	00086809 	andeq	r6, r8, r9, lsl #16
    2b30:	00084d00 	andeq	r4, r8, r0, lsl #26
    2b34:	07b30100 	ldreq	r0, [r3, r0, lsl #2]!
    2b38:	07be0000 	ldreq	r0, [lr, r0]!
    2b3c:	4d0c0000 	stcmi	0, cr0, [ip, #-0]
    2b40:	0d000008 	stceq	0, cr0, [r0, #-32]	; 0xffffffe0
    2b44:	000004d8 	ldrdeq	r0, [r0], -r8
    2b48:	0f800e00 	svceq	0x00800e00
    2b4c:	21070000 	mrscs	r0, (UNDEF: 7)
    2b50:	0008a80e 	andeq	sl, r8, lr, lsl #16
    2b54:	07d30100 	ldrbeq	r0, [r3, r0, lsl #2]
    2b58:	07e80000 	strbeq	r0, [r8, r0]!
    2b5c:	4d0c0000 	stcmi	0, cr0, [ip, #-0]
    2b60:	0d000008 	stceq	0, cr0, [r0, #-32]	; 0xffffffe0
    2b64:	0000075c 	andeq	r0, r0, ip, asr r7
    2b68:	00006f0d 	andeq	r6, r0, sp, lsl #30
    2b6c:	071d0d00 	ldreq	r0, [sp, -r0, lsl #26]
    2b70:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    2b74:	00000e80 	andeq	r0, r0, r0, lsl #29
    2b78:	1e0e2307 	cdpne	3, 0, cr2, cr14, cr7, {0}
    2b7c:	0100000b 	tsteq	r0, fp
    2b80:	000007fd 	strdeq	r0, [r0], -sp
    2b84:	00000803 	andeq	r0, r0, r3, lsl #16
    2b88:	00084d0c 	andeq	r4, r8, ip, lsl #26
    2b8c:	d90e0000 	stmdble	lr, {}	; <UNPREDICTABLE>
    2b90:	0700000b 	streq	r0, [r0, -fp]
    2b94:	0a3d0e26 	beq	f46434 <_bss_end+0xf3cdd0>
    2b98:	18010000 	stmdane	r1, {}	; <UNPREDICTABLE>
    2b9c:	1e000008 	cdpne	0, 0, cr0, cr0, cr8, {0}
    2ba0:	0c000008 	stceq	0, cr0, [r0], {8}
    2ba4:	0000084d 	andeq	r0, r0, sp, asr #16
    2ba8:	098a1900 	stmibeq	sl, {r8, fp, ip}
    2bac:	28070000 	stmdacs	r7, {}	; <UNPREDICTABLE>
    2bb0:	0008770e 	andeq	r7, r8, lr, lsl #14
    2bb4:	00023a00 	andeq	r3, r2, r0, lsl #20
    2bb8:	08330100 	ldmdaeq	r3!, {r8}
    2bbc:	4d0c0000 	stcmi	0, cr0, [ip, #-0]
    2bc0:	00000008 	andeq	r0, r0, r8
    2bc4:	7b041000 	blvc	106bcc <_bss_end+0xfd568>
    2bc8:	10000000 	andne	r0, r0, r0
    2bcc:	00084604 	andeq	r4, r8, r4, lsl #12
    2bd0:	04111d00 	ldreq	r1, [r1], #-3328	; 0xfffff300
    2bd4:	0000007b 	andeq	r0, r0, fp, ror r0
    2bd8:	07420410 	smlaldeq	r0, r2, r0, r4
    2bdc:	6b120000 	blvs	482be4 <_bss_end+0x479580>
    2be0:	07000009 	streq	r0, [r0, -r9]
    2be4:	07420f2b 	strbeq	r0, [r2, -fp, lsr #30]
    2be8:	60090000 	andvs	r0, r9, r0
    2bec:	0400000e 	streq	r0, [r0], #-14
    2bf0:	3e070508 	cfsh32cc	mvfx0, mvfx7, #8
    2bf4:	0a000009 	beq	2c20 <CPSR_IRQ_INHIBIT+0x2ba0>
    2bf8:	00000eb9 			; <UNDEFINED> instruction: 0x00000eb9
    2bfc:	3a200908 	bcc	805024 <_bss_end+0x7fb9c0>
    2c00:	00000008 	andeq	r0, r0, r8
    2c04:	0008630b 	andeq	r6, r8, fp, lsl #6
    2c08:	200c0800 	andcs	r0, ip, r0, lsl #16
    2c0c:	00000cc6 	andeq	r0, r0, r6, asr #25
    2c10:	00000847 	andeq	r0, r0, r7, asr #16
    2c14:	00089202 	andeq	r9, r8, r2, lsl #4
    2c18:	00089d00 	andeq	r9, r8, r0, lsl #26
    2c1c:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2c20:	8a0d0000 	bhi	342c28 <_bss_end+0x3395c4>
    2c24:	00000003 	andeq	r0, r0, r3
    2c28:	000e600b 	andeq	r6, lr, fp
    2c2c:	090f0800 	stmdbeq	pc, {fp}	; <UNPREDICTABLE>
    2c30:	00000f2c 	andeq	r0, r0, ip, lsr #30
    2c34:	0000093e 	andeq	r0, r0, lr, lsr r9
    2c38:	0008b601 	andeq	fp, r8, r1, lsl #12
    2c3c:	0008c100 	andeq	ip, r8, r0, lsl #2
    2c40:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2c44:	d80d0000 	stmdale	sp, {}	; <UNPREDICTABLE>
    2c48:	00000004 	andeq	r0, r0, r4
    2c4c:	000e880e 	andeq	r8, lr, lr, lsl #16
    2c50:	0e120800 	cdpeq	8, 1, cr0, cr2, cr0, {0}
    2c54:	00000ee6 	andeq	r0, r0, r6, ror #29
    2c58:	0008d601 	andeq	sp, r8, r1, lsl #12
    2c5c:	0008e100 	andeq	lr, r8, r0, lsl #2
    2c60:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2c64:	d90d0000 	stmdble	sp, {}	; <UNPREDICTABLE>
    2c68:	00000003 	andeq	r0, r0, r3
    2c6c:	0010290e 	andseq	r2, r0, lr, lsl #18
    2c70:	0e140800 	cdpeq	8, 1, cr0, cr4, cr0, {0}
    2c74:	00000d5d 	andeq	r0, r0, sp, asr sp
    2c78:	0008f601 	andeq	pc, r8, r1, lsl #12
    2c7c:	00090100 	andeq	r0, r9, r0, lsl #2
    2c80:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2c84:	d90d0000 	stmdble	sp, {}	; <UNPREDICTABLE>
    2c88:	00000003 	andeq	r0, r0, r3
    2c8c:	000d190e 	andeq	r1, sp, lr, lsl #18
    2c90:	0e170800 	cdpeq	8, 1, cr0, cr7, cr0, {0}
    2c94:	00000c83 	andeq	r0, r0, r3, lsl #25
    2c98:	00091601 	andeq	r1, r9, r1, lsl #12
    2c9c:	00092100 	andeq	r2, r9, r0, lsl #2
    2ca0:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2ca4:	1c0d0000 	stcne	0, cr0, [sp], {-0}
    2ca8:	00000004 	andeq	r0, r0, r4
    2cac:	000dbd0f 	andeq	fp, sp, pc, lsl #26
    2cb0:	0e190800 	cdpeq	8, 1, cr0, cr9, cr0, {0}
    2cb4:	00000dfc 	strdeq	r0, [r0], -ip
    2cb8:	00093201 	andeq	r3, r9, r1, lsl #4
    2cbc:	093e0c00 	ldmdbeq	lr!, {sl, fp}
    2cc0:	1c0d0000 	stcne	0, cr0, [sp], {-0}
    2cc4:	00000004 	andeq	r0, r0, r4
    2cc8:	5f041000 	svcpl	0x00041000
    2ccc:	12000008 	andne	r0, r0, #8
    2cd0:	00000ed8 	ldrdeq	r0, [r0], -r8
    2cd4:	5f1e1c08 	svcpl	0x001e1c08
    2cd8:	1e000008 	cdpne	0, 0, cr0, cr0, cr8, {0}
    2cdc:	000010bc 	strheq	r1, [r0], -ip
    2ce0:	6a140701 	bvs	5048ec <_bss_end+0x4fb288>
    2ce4:	05000000 	streq	r0, [r0, #-0]
    2ce8:	0095fc03 	addseq	pc, r5, r3, lsl #24
    2cec:	106e1f00 	rsbne	r1, lr, r0, lsl #30
    2cf0:	0d010000 	stceq	0, cr0, [r1, #-0]
    2cf4:	00003f0e 	andeq	r3, r0, lr, lsl #30
    2cf8:	50030500 	andpl	r0, r3, r0, lsl #10
    2cfc:	20000096 	mulcs	r0, r6, r0
    2d00:	000010c4 	andeq	r1, r0, r4, asr #1
    2d04:	38101f01 	ldmdacc	r0, {r0, r8, r9, sl, fp, ip}
    2d08:	74000000 	strvc	r0, [r0], #-0
    2d0c:	8c000090 	stchi	0, cr0, [r0], {144}	; 0x90
    2d10:	01000000 	mrseq	r0, (UNDEF: 0)
    2d14:	10ac219c 	umlalne	r2, ip, ip, r1
    2d18:	0f010000 	svceq	0x00010000
    2d1c:	00900011 	addseq	r0, r0, r1, lsl r0
    2d20:	00007400 	andeq	r7, r0, r0, lsl #8
    2d24:	009c0100 	addseq	r0, ip, r0, lsl #2
    2d28:	0000001e 	andeq	r0, r0, lr, lsl r0
    2d2c:	0f280002 	svceq	0x00280002
    2d30:	01040000 	mrseq	r0, (UNDEF: 4)
    2d34:	00000bc2 	andeq	r0, r0, r2, asr #23
    2d38:	00000000 	andeq	r0, r0, r0
    2d3c:	000010d1 	ldrdeq	r1, [r0], -r1	; <UNPREDICTABLE>
    2d40:	0000013d 	andeq	r0, r0, sp, lsr r1
    2d44:	00001104 	andeq	r1, r0, r4, lsl #2
    2d48:	014b8001 	cmpeq	fp, r1
    2d4c:	00040000 	andeq	r0, r4, r0
    2d50:	00000f3a 	andeq	r0, r0, sl, lsr pc
    2d54:	00140104 	andseq	r0, r4, r4, lsl #2
    2d58:	85040000 	strhi	r0, [r4, #-0]
    2d5c:	3d000011 	stccc	0, cr0, [r0, #-68]	; 0xffffffbc
    2d60:	20000001 	andcs	r0, r0, r1
    2d64:	18000091 	stmdane	r0, {r0, r4, r7}
    2d68:	5a000001 	bpl	2d74 <CPSR_IRQ_INHIBIT+0x2cf4>
    2d6c:	0200000c 	andeq	r0, r0, #12
    2d70:	0000117c 	andeq	r1, r0, ip, ror r1
    2d74:	31070201 	tstcc	r7, r1, lsl #4
    2d78:	03000000 	movweq	r0, #0
    2d7c:	00003704 	andeq	r3, r0, r4, lsl #14
    2d80:	73020400 	movwvc	r0, #9216	; 0x2400
    2d84:	01000011 	tsteq	r0, r1, lsl r0
    2d88:	00310703 	eorseq	r0, r1, r3, lsl #14
    2d8c:	1b050000 	blne	142d94 <_bss_end+0x139730>
    2d90:	01000011 	tsteq	r0, r1, lsl r0
    2d94:	00501006 	subseq	r1, r0, r6
    2d98:	04060000 	streq	r0, [r6], #-0
    2d9c:	746e6905 	strbtvc	r6, [lr], #-2309	; 0xfffff6fb
    2da0:	115c0500 	cmpne	ip, r0, lsl #10
    2da4:	08010000 	stmdaeq	r1, {}	; <UNPREDICTABLE>
    2da8:	00005010 	andeq	r5, r0, r0, lsl r0
    2dac:	00250700 	eoreq	r0, r5, r0, lsl #14
    2db0:	00760000 	rsbseq	r0, r6, r0
    2db4:	76080000 	strvc	r0, [r8], -r0
    2db8:	ff000000 			; <UNDEFINED> instruction: 0xff000000
    2dbc:	00ffffff 	ldrshteq	pc, [pc], #255	; <UNPREDICTABLE>
    2dc0:	fa070409 	blx	1c3dec <_bss_end+0x1ba788>
    2dc4:	05000016 	streq	r0, [r0, #-22]	; 0xffffffea
    2dc8:	00001133 	andeq	r1, r0, r3, lsr r1
    2dcc:	63150b01 	tstvs	r5, #1024	; 0x400
    2dd0:	05000000 	streq	r0, [r0, #-0]
    2dd4:	00001126 	andeq	r1, r0, r6, lsr #2
    2dd8:	63150d01 	tstvs	r5, #1, 26	; 0x40
    2ddc:	07000000 	streq	r0, [r0, -r0]
    2de0:	00000038 	andeq	r0, r0, r8, lsr r0
    2de4:	000000a8 	andeq	r0, r0, r8, lsr #1
    2de8:	00007608 	andeq	r7, r0, r8, lsl #12
    2dec:	ffffff00 			; <UNDEFINED> instruction: 0xffffff00
    2df0:	650500ff 	strvs	r0, [r5, #-255]	; 0xffffff01
    2df4:	01000011 	tsteq	r0, r1, lsl r0
    2df8:	00951510 	addseq	r1, r5, r0, lsl r5
    2dfc:	41050000 	mrsmi	r0, (UNDEF: 5)
    2e00:	01000011 	tsteq	r0, r1, lsl r0
    2e04:	00951512 	addseq	r1, r5, r2, lsl r5
    2e08:	4e0a0000 	cdpmi	0, 0, cr0, cr10, cr0, {0}
    2e0c:	01000011 	tsteq	r0, r1, lsl r0
    2e10:	0050102b 	subseq	r1, r0, fp, lsr #32
    2e14:	91e00000 	mvnls	r0, r0
    2e18:	00580000 	subseq	r0, r8, r0
    2e1c:	9c010000 	stcls	0, cr0, [r1], {-0}
    2e20:	000000ea 	andeq	r0, r0, sl, ror #1
    2e24:	0011c90b 	andseq	ip, r1, fp, lsl #18
    2e28:	0c2d0100 	stfeqs	f0, [sp], #-0
    2e2c:	000000ea 	andeq	r0, r0, sl, ror #1
    2e30:	00749102 	rsbseq	r9, r4, r2, lsl #2
    2e34:	00380403 	eorseq	r0, r8, r3, lsl #8
    2e38:	bc0a0000 	stclt	0, cr0, [sl], {-0}
    2e3c:	01000011 	tsteq	r0, r1, lsl r0
    2e40:	0050101f 	subseq	r1, r0, pc, lsl r0
    2e44:	91880000 	orrls	r0, r8, r0
    2e48:	00580000 	subseq	r0, r8, r0
    2e4c:	9c010000 	stcls	0, cr0, [r1], {-0}
    2e50:	0000011a 	andeq	r0, r0, sl, lsl r1
    2e54:	0011c90b 	andseq	ip, r1, fp, lsl #18
    2e58:	0c210100 	stfeqs	f0, [r1], #-0
    2e5c:	0000011a 	andeq	r0, r0, sl, lsl r1
    2e60:	00749102 	rsbseq	r9, r4, r2, lsl #2
    2e64:	00250403 	eoreq	r0, r5, r3, lsl #8
    2e68:	100c0000 	andne	r0, ip, r0
    2e6c:	01000011 	tsteq	r0, r1, lsl r0
    2e70:	00501014 	subseq	r1, r0, r4, lsl r0
    2e74:	91200000 			; <UNDEFINED> instruction: 0x91200000
    2e78:	00680000 	rsbeq	r0, r8, r0
    2e7c:	9c010000 	stcls	0, cr0, [r1], {-0}
    2e80:	00000148 	andeq	r0, r0, r8, asr #2
    2e84:	0100690d 	tsteq	r0, sp, lsl #18
    2e88:	01480716 	cmpeq	r8, r6, lsl r7
    2e8c:	91020000 	mrsls	r0, (UNDEF: 2)
    2e90:	04030074 	streq	r0, [r3], #-116	; 0xffffff8c
    2e94:	00000050 	andeq	r0, r0, r0, asr r0
    2e98:	00002200 	andeq	r2, r0, r0, lsl #4
    2e9c:	00000200 	andeq	r0, r0, r0, lsl #4
    2ea0:	04000010 	streq	r0, [r0], #-16
    2ea4:	000d2c01 	andeq	r2, sp, r1, lsl #24
    2ea8:	00923800 	addseq	r3, r2, r0, lsl #16
    2eac:	00944400 	addseq	r4, r4, r0, lsl #8
    2eb0:	0011cf00 	andseq	ip, r1, r0, lsl #30
    2eb4:	0011ff00 	andseq	pc, r1, r0, lsl #30
    2eb8:	00110400 	andseq	r0, r1, r0, lsl #8
    2ebc:	22800100 	addcs	r0, r0, #0, 2
    2ec0:	02000000 	andeq	r0, r0, #0
    2ec4:	00101400 	andseq	r1, r0, r0, lsl #8
    2ec8:	a9010400 	stmdbge	r1, {sl}
    2ecc:	4400000d 	strmi	r0, [r0], #-13
    2ed0:	48000094 	stmdami	r0, {r2, r4, r7}
    2ed4:	cf000094 	svcgt	0x00000094
    2ed8:	ff000011 			; <UNDEFINED> instruction: 0xff000011
    2edc:	04000011 	streq	r0, [r0], #-17	; 0xffffffef
    2ee0:	01000011 	tsteq	r0, r1, lsl r0
    2ee4:	00093280 	andeq	r3, r9, r0, lsl #5
    2ee8:	28000400 	stmdacs	r0, {sl}
    2eec:	04000010 	streq	r0, [r0], #-16
    2ef0:	0015cd01 	andseq	ip, r5, r1, lsl #26
    2ef4:	15240c00 	strne	r0, [r4, #-3072]!	; 0xfffff400
    2ef8:	11ff0000 	mvnsne	r0, r0
    2efc:	0e090000 	cdpeq	0, 0, cr0, cr9, cr0, {0}
    2f00:	04020000 	streq	r0, [r2], #-0
    2f04:	746e6905 	strbtvc	r6, [lr], #-2309	; 0xfffff6fb
    2f08:	07040300 	streq	r0, [r4, -r0, lsl #6]
    2f0c:	000016fa 	strdeq	r1, [r0], -sl
    2f10:	a0050803 	andge	r0, r5, r3, lsl #16
    2f14:	03000001 	movweq	r0, #1
    2f18:	1dcc0408 	cfstrdne	mvd0, [ip, #32]
    2f1c:	7f040000 	svcvc	0x00040000
    2f20:	01000015 	tsteq	r0, r5, lsl r0
    2f24:	0024162a 	eoreq	r1, r4, sl, lsr #12
    2f28:	ee040000 	cdp	0, 0, cr0, cr4, cr0, {0}
    2f2c:	01000019 	tsteq	r0, r9, lsl r0
    2f30:	0051152f 	subseq	r1, r1, pc, lsr #10
    2f34:	04050000 	streq	r0, [r5], #-0
    2f38:	00000057 	andeq	r0, r0, r7, asr r0
    2f3c:	00003906 	andeq	r3, r0, r6, lsl #18
    2f40:	00006600 	andeq	r6, r0, r0, lsl #12
    2f44:	00660700 	rsbeq	r0, r6, r0, lsl #14
    2f48:	05000000 	streq	r0, [r0, #-0]
    2f4c:	00006c04 	andeq	r6, r0, r4, lsl #24
    2f50:	20040800 	andcs	r0, r4, r0, lsl #16
    2f54:	01000021 	tsteq	r0, r1, lsr #32
    2f58:	00790f36 	rsbseq	r0, r9, r6, lsr pc
    2f5c:	04050000 	streq	r0, [r5], #-0
    2f60:	0000007f 	andeq	r0, r0, pc, ror r0
    2f64:	00001d06 	andeq	r1, r0, r6, lsl #26
    2f68:	00009300 	andeq	r9, r0, r0, lsl #6
    2f6c:	00660700 	rsbeq	r0, r6, r0, lsl #14
    2f70:	66070000 	strvs	r0, [r7], -r0
    2f74:	00000000 	andeq	r0, r0, r0
    2f78:	f9080103 			; <UNDEFINED> instruction: 0xf9080103
    2f7c:	09000002 	stmdbeq	r0, {r1}
    2f80:	00001c26 	andeq	r1, r0, r6, lsr #24
    2f84:	4512bb01 	ldrmi	fp, [r2, #-2817]	; 0xfffff4ff
    2f88:	09000000 	stmdbeq	r0, {}	; <UNPREDICTABLE>
    2f8c:	0000214e 	andeq	r2, r0, lr, asr #2
    2f90:	6d10be01 	ldcvs	14, cr11, [r0, #-4]
    2f94:	03000000 	movweq	r0, #0
    2f98:	02fb0601 	rscseq	r0, fp, #1048576	; 0x100000
    2f9c:	0e0a0000 	cdpeq	0, 0, cr0, cr10, cr0, {0}
    2fa0:	07000019 	smladeq	r0, r9, r0, r0
    2fa4:	00009301 	andeq	r9, r0, r1, lsl #6
    2fa8:	06170200 	ldreq	r0, [r7], -r0, lsl #4
    2fac:	000001e6 	andeq	r0, r0, r6, ror #3
    2fb0:	0013dd0b 	andseq	sp, r3, fp, lsl #26
    2fb4:	2b0b0000 	blcs	2c2fbc <_bss_end+0x2b9958>
    2fb8:	01000018 	tsteq	r0, r8, lsl r0
    2fbc:	001cf10b 	andseq	pc, ip, fp, lsl #2
    2fc0:	620b0200 	andvs	r0, fp, #0, 4
    2fc4:	03000020 	movweq	r0, #32
    2fc8:	001c950b 	andseq	r9, ip, fp, lsl #10
    2fcc:	6b0b0400 	blvs	2c3fd4 <_bss_end+0x2ba970>
    2fd0:	0500001f 	streq	r0, [r0, #-31]	; 0xffffffe1
    2fd4:	001ecf0b 	andseq	ip, lr, fp, lsl #30
    2fd8:	fe0b0600 	cdp2	6, 0, cr0, cr11, cr0, {0}
    2fdc:	07000013 	smladeq	r0, r3, r0, r0
    2fe0:	001f800b 	andseq	r8, pc, fp
    2fe4:	8e0b0800 	cdphi	8, 0, cr0, cr11, cr0, {0}
    2fe8:	0900001f 	stmdbeq	r0, {r0, r1, r2, r3, r4}
    2fec:	0020550b 	eoreq	r5, r0, fp, lsl #10
    2ff0:	ec0b0a00 			; <UNDEFINED> instruction: 0xec0b0a00
    2ff4:	0b00001b 	bleq	3068 <CPSR_IRQ_INHIBIT+0x2fe8>
    2ff8:	0015c00b 	andseq	ip, r5, fp
    2ffc:	9d0b0c00 	stcls	12, cr0, [fp, #-0]
    3000:	0d000016 	stceq	0, cr0, [r0, #-88]	; 0xffffffa8
    3004:	0019520b 	andseq	r5, r9, fp, lsl #4
    3008:	680b0e00 	stmdavs	fp, {r9, sl, fp}
    300c:	0f000019 	svceq	0x00000019
    3010:	0018650b 	andseq	r6, r8, fp, lsl #10
    3014:	790b1000 	stmdbvc	fp, {ip}
    3018:	1100001c 	tstne	r0, ip, lsl r0
    301c:	0018d10b 	andseq	sp, r8, fp, lsl #2
    3020:	e70b1200 	str	r1, [fp, -r0, lsl #4]
    3024:	13000022 	movwne	r0, #34	; 0x22
    3028:	0014670b 	andseq	r6, r4, fp, lsl #14
    302c:	f50b1400 			; <UNDEFINED> instruction: 0xf50b1400
    3030:	15000018 	strne	r0, [r0, #-24]	; 0xffffffe8
    3034:	0013a40b 	andseq	sl, r3, fp, lsl #8
    3038:	850b1600 	strhi	r1, [fp, #-1536]	; 0xfffffa00
    303c:	17000020 	strne	r0, [r0, -r0, lsr #32]
    3040:	0021a70b 	eoreq	sl, r1, fp, lsl #14
    3044:	1a0b1800 	bne	2c904c <_bss_end+0x2bf9e8>
    3048:	19000019 	stmdbne	r0, {r0, r3, r4}
    304c:	001d630b 	andseq	r6, sp, fp, lsl #6
    3050:	930b1a00 	movwls	r1, #47616	; 0xba00
    3054:	1b000020 	blne	30dc <CPSR_IRQ_INHIBIT+0x305c>
    3058:	0012d30b 	andseq	sp, r2, fp, lsl #6
    305c:	a10b1c00 	tstge	fp, r0, lsl #24
    3060:	1d000020 	stcne	0, cr0, [r0, #-128]	; 0xffffff80
    3064:	0020af0b 	eoreq	sl, r0, fp, lsl #30
    3068:	810b1e00 	tsthi	fp, r0, lsl #28
    306c:	1f000012 	svcne	0x00000012
    3070:	0020d90b 	eoreq	sp, r0, fp, lsl #18
    3074:	100b2000 	andne	r2, fp, r0
    3078:	2100001e 	tstcs	r0, lr, lsl r0
    307c:	001c4b0b 	andseq	r4, ip, fp, lsl #22
    3080:	780b2200 	stmdavc	fp, {r9, sp}
    3084:	23000020 	movwcs	r0, #32
    3088:	001b4f0b 	andseq	r4, fp, fp, lsl #30
    308c:	510b2400 	tstpl	fp, r0, lsl #8
    3090:	2500001a 	strcs	r0, [r0, #-26]	; 0xffffffe6
    3094:	00176b0b 	andseq	r6, r7, fp, lsl #22
    3098:	6f0b2600 	svcvs	0x000b2600
    309c:	2700001a 	smladcs	r0, sl, r0, r0
    30a0:	0018070b 	andseq	r0, r8, fp, lsl #14
    30a4:	7f0b2800 	svcvc	0x000b2800
    30a8:	2900001a 	stmdbcs	r0, {r1, r3, r4}
    30ac:	001a8f0b 	andseq	r8, sl, fp, lsl #30
    30b0:	d20b2a00 	andle	r2, fp, #0, 20
    30b4:	2b00001b 	blcs	3128 <CPSR_IRQ_INHIBIT+0x30a8>
    30b8:	0019f80b 	andseq	pc, r9, fp, lsl #16
    30bc:	1d0b2c00 	stcne	12, cr2, [fp, #-0]
    30c0:	2d00001e 	stccs	0, cr0, [r0, #-120]	; 0xffffff88
    30c4:	0017ac0b 	andseq	sl, r7, fp, lsl #24
    30c8:	0a002e00 	beq	e8d0 <_bss_end+0x526c>
    30cc:	0000198a 	andeq	r1, r0, sl, lsl #19
    30d0:	00930107 	addseq	r0, r3, r7, lsl #2
    30d4:	17030000 	strne	r0, [r3, -r0]
    30d8:	0003c706 	andeq	ip, r3, r6, lsl #14
    30dc:	16bf0b00 	ldrtne	r0, [pc], r0, lsl #22
    30e0:	0b000000 	bleq	30e8 <CPSR_IRQ_INHIBIT+0x3068>
    30e4:	00001311 	andeq	r1, r0, r1, lsl r3
    30e8:	22950b01 	addscs	r0, r5, #1024	; 0x400
    30ec:	0b020000 	bleq	830f4 <_bss_end+0x79a90>
    30f0:	00002128 	andeq	r2, r0, r8, lsr #2
    30f4:	16df0b03 	ldrbne	r0, [pc], r3, lsl #22
    30f8:	0b040000 	bleq	103100 <_bss_end+0xf9a9c>
    30fc:	000013c9 	andeq	r1, r0, r9, asr #7
    3100:	17880b05 	strne	r0, [r8, r5, lsl #22]
    3104:	0b060000 	bleq	18310c <_bss_end+0x179aa8>
    3108:	000016cf 	andeq	r1, r0, pc, asr #13
    310c:	1fbc0b07 	svcne	0x00bc0b07
    3110:	0b080000 	bleq	203118 <_bss_end+0x1f9ab4>
    3114:	0000210d 	andeq	r2, r0, sp, lsl #2
    3118:	1ef30b09 	vmovne.f64	d16, #57	; 0x41c80000  25.0
    311c:	0b0a0000 	bleq	283124 <_bss_end+0x279ac0>
    3120:	0000141c 	andeq	r1, r0, ip, lsl r4
    3124:	17290b0b 	strne	r0, [r9, -fp, lsl #22]!
    3128:	0b0c0000 	bleq	303130 <_bss_end+0x2f9acc>
    312c:	00001392 	muleq	r0, r2, r3
    3130:	22ca0b0d 	sbccs	r0, sl, #13312	; 0x3400
    3134:	0b0e0000 	bleq	38313c <_bss_end+0x379ad8>
    3138:	00001bbf 			; <UNDEFINED> instruction: 0x00001bbf
    313c:	189c0b0f 	ldmne	ip, {r0, r1, r2, r3, r8, r9, fp}
    3140:	0b100000 	bleq	403148 <_bss_end+0x3f9ae4>
    3144:	00001bfc 	strdeq	r1, [r0], -ip
    3148:	21e90b11 	mvncs	r0, r1, lsl fp
    314c:	0b120000 	bleq	483154 <_bss_end+0x479af0>
    3150:	000014df 	ldrdeq	r1, [r0], -pc	; <UNPREDICTABLE>
    3154:	18af0b13 	stmiane	pc!, {r0, r1, r4, r8, r9, fp}	; <UNPREDICTABLE>
    3158:	0b140000 	bleq	503160 <_bss_end+0x4f9afc>
    315c:	00001b12 	andeq	r1, r0, r2, lsl fp
    3160:	16aa0b15 	ssatne	r0, #11, r5, lsl #22
    3164:	0b160000 	bleq	58316c <_bss_end+0x579b08>
    3168:	00001b5e 	andeq	r1, r0, lr, asr fp
    316c:	19740b17 	ldmdbne	r4!, {r0, r1, r2, r4, r8, r9, fp}^
    3170:	0b180000 	bleq	603178 <_bss_end+0x5f9b14>
    3174:	000013e7 	andeq	r1, r0, r7, ror #7
    3178:	21900b19 	orrscs	r0, r0, r9, lsl fp
    317c:	0b1a0000 	bleq	683184 <_bss_end+0x679b20>
    3180:	00001ade 	ldrdeq	r1, [r0], -lr
    3184:	18860b1b 	stmne	r6, {r0, r1, r3, r4, r8, r9, fp}
    3188:	0b1c0000 	bleq	703190 <_bss_end+0x6f9b2c>
    318c:	000012bc 			; <UNDEFINED> instruction: 0x000012bc
    3190:	1a290b1d 	bne	a45e0c <_bss_end+0xa3c7a8>
    3194:	0b1e0000 	bleq	78319c <_bss_end+0x779b38>
    3198:	00001a15 	andeq	r1, r0, r5, lsl sl
    319c:	1eb00b1f 	vmovne.32	r0, d0[1]
    31a0:	0b200000 	bleq	8031a8 <_bss_end+0x7f9b44>
    31a4:	00001f3b 	andeq	r1, r0, fp, lsr pc
    31a8:	216f0b21 	cmncs	pc, r1, lsr #22
    31ac:	0b220000 	bleq	8831b4 <_bss_end+0x879b50>
    31b0:	000017b9 			; <UNDEFINED> instruction: 0x000017b9
    31b4:	1d130b23 	vldrne	d0, [r3, #-140]	; 0xffffff74
    31b8:	0b240000 	bleq	9031c0 <_bss_end+0x8f9b5c>
    31bc:	00001f08 	andeq	r1, r0, r8, lsl #30
    31c0:	1e2c0b25 	vmulne.f64	d0, d12, d21
    31c4:	0b260000 	bleq	9831cc <_bss_end+0x979b68>
    31c8:	00001e40 	andeq	r1, r0, r0, asr #28
    31cc:	1e540b27 	vnmlsne.f64	d16, d4, d23
    31d0:	0b280000 	bleq	a031d8 <_bss_end+0x9f9b74>
    31d4:	0000156a 	andeq	r1, r0, sl, ror #10
    31d8:	14ca0b29 	strbne	r0, [sl], #2857	; 0xb29
    31dc:	0b2a0000 	bleq	a831e4 <_bss_end+0xa79b80>
    31e0:	000014f2 	strdeq	r1, [r0], -r2
    31e4:	20050b2b 	andcs	r0, r5, fp, lsr #22
    31e8:	0b2c0000 	bleq	b031f0 <_bss_end+0xaf9b8c>
    31ec:	00001547 	andeq	r1, r0, r7, asr #10
    31f0:	20190b2d 	andscs	r0, r9, sp, lsr #22
    31f4:	0b2e0000 	bleq	b831fc <_bss_end+0xb79b98>
    31f8:	0000202d 	andeq	r2, r0, sp, lsr #32
    31fc:	20410b2f 	subcs	r0, r1, pc, lsr #22
    3200:	0b300000 	bleq	c03208 <_bss_end+0xbf9ba4>
    3204:	0000173b 	andeq	r1, r0, fp, lsr r7
    3208:	17150b31 			; <UNDEFINED> instruction: 0x17150b31
    320c:	0b320000 	bleq	c83214 <_bss_end+0xc79bb0>
    3210:	00001a3d 	andeq	r1, r0, sp, lsr sl
    3214:	1c0f0b33 			; <UNDEFINED> instruction: 0x1c0f0b33
    3218:	0b340000 	bleq	d03220 <_bss_end+0xcf9bbc>
    321c:	0000221e 	andeq	r2, r0, lr, lsl r2
    3220:	12640b35 	rsbne	r0, r4, #54272	; 0xd400
    3224:	0b360000 	bleq	d8322c <_bss_end+0xd79bc8>
    3228:	0000183b 	andeq	r1, r0, fp, lsr r8
    322c:	18500b37 	ldmdane	r0, {r0, r1, r2, r4, r5, r8, r9, fp}^
    3230:	0b380000 	bleq	e03238 <_bss_end+0xdf9bd4>
    3234:	00001a9f 	muleq	r0, pc, sl	; <UNPREDICTABLE>
    3238:	1ac90b39 	bne	ff245f24 <_bss_end+0xff23c8c0>
    323c:	0b3a0000 	bleq	e83244 <_bss_end+0xe79be0>
    3240:	00002247 	andeq	r2, r0, r7, asr #4
    3244:	1cfe0b3b 	fldmiaxne	lr!, {d16-d44}	;@ Deprecated
    3248:	0b3c0000 	bleq	f03250 <_bss_end+0xef9bec>
    324c:	000017de 	ldrdeq	r1, [r0], -lr
    3250:	13230b3d 			; <UNDEFINED> instruction: 0x13230b3d
    3254:	0b3e0000 	bleq	f8325c <_bss_end+0xf79bf8>
    3258:	000012e1 	andeq	r1, r0, r1, ror #5
    325c:	1c5b0b3f 	vmovne	r0, fp, d31
    3260:	0b400000 	bleq	1003268 <_bss_end+0xff9c04>
    3264:	00001d7f 	andeq	r1, r0, pc, ror sp
    3268:	1e920b41 	vfnmane.f64	d0, d2, d1
    326c:	0b420000 	bleq	1083274 <_bss_end+0x1079c10>
    3270:	00001ab4 			; <UNDEFINED> instruction: 0x00001ab4
    3274:	22800b43 	addcs	r0, r0, #68608	; 0x10c00
    3278:	0b440000 	bleq	1103280 <_bss_end+0x10f9c1c>
    327c:	00001d29 	andeq	r1, r0, r9, lsr #26
    3280:	150e0b45 	strne	r0, [lr, #-2885]	; 0xfffff4bb
    3284:	0b460000 	bleq	118328c <_bss_end+0x1179c28>
    3288:	00001b8f 	andeq	r1, r0, pc, lsl #23
    328c:	19c20b47 	stmibne	r2, {r0, r1, r2, r6, r8, r9, fp}^
    3290:	0b480000 	bleq	1203298 <_bss_end+0x11f9c34>
    3294:	000012a0 	andeq	r1, r0, r0, lsr #5
    3298:	13b40b49 			; <UNDEFINED> instruction: 0x13b40b49
    329c:	0b4a0000 	bleq	12832a4 <_bss_end+0x1279c40>
    32a0:	000017f2 	strdeq	r1, [r0], -r2
    32a4:	1af00b4b 	bne	ffc05fd8 <_bss_end+0xffbfc974>
    32a8:	004c0000 	subeq	r0, ip, r0
    32ac:	91070203 	tstls	r7, r3, lsl #4
    32b0:	0c000003 	stceq	0, cr0, [r0], {3}
    32b4:	000003e4 	andeq	r0, r0, r4, ror #7
    32b8:	000003d9 	ldrdeq	r0, [r0], -r9
    32bc:	ce0e000d 	cdpgt	0, 0, cr0, cr14, cr13, {0}
    32c0:	05000003 	streq	r0, [r0, #-3]
    32c4:	0003f004 	andeq	pc, r3, r4
    32c8:	03de0e00 	bicseq	r0, lr, #0, 28
    32cc:	01030000 	mrseq	r0, (UNDEF: 3)
    32d0:	00030208 	andeq	r0, r3, r8, lsl #4
    32d4:	03e90e00 	mvneq	r0, #0, 28
    32d8:	580f0000 	stmdapl	pc, {}	; <UNPREDICTABLE>
    32dc:	04000014 	streq	r0, [r0], #-20	; 0xffffffec
    32e0:	d91a014c 	ldmdble	sl, {r2, r3, r6, r8}
    32e4:	0f000003 	svceq	0x00000003
    32e8:	00001876 	andeq	r1, r0, r6, ror r8
    32ec:	1a018204 	bne	63b04 <_bss_end+0x5a4a0>
    32f0:	000003d9 	ldrdeq	r0, [r0], -r9
    32f4:	0003e90c 	andeq	lr, r3, ip, lsl #18
    32f8:	00041a00 	andeq	r1, r4, r0, lsl #20
    32fc:	09000d00 	stmdbeq	r0, {r8, sl, fp}
    3300:	00001a61 	andeq	r1, r0, r1, ror #20
    3304:	0f0d2d05 	svceq	0x000d2d05
    3308:	09000004 	stmdbeq	r0, {r2}
    330c:	000020e9 	andeq	r2, r0, r9, ror #1
    3310:	e61c3805 	ldr	r3, [ip], -r5, lsl #16
    3314:	0a000001 	beq	3320 <CPSR_IRQ_INHIBIT+0x32a0>
    3318:	0000174f 	andeq	r1, r0, pc, asr #14
    331c:	00930107 	addseq	r0, r3, r7, lsl #2
    3320:	3a050000 	bcc	143328 <_bss_end+0x139cc4>
    3324:	0004a50e 	andeq	sl, r4, lr, lsl #10
    3328:	12b50b00 	adcsne	r0, r5, #0, 22
    332c:	0b000000 	bleq	3334 <CPSR_IRQ_INHIBIT+0x32b4>
    3330:	00001961 	andeq	r1, r0, r1, ror #18
    3334:	21fb0b01 	mvnscs	r0, r1, lsl #22
    3338:	0b020000 	bleq	83340 <_bss_end+0x79cdc>
    333c:	000021be 			; <UNDEFINED> instruction: 0x000021be
    3340:	1cb80b03 	fldmiaxne	r8!, {d0}	;@ Deprecated
    3344:	0b040000 	bleq	10334c <_bss_end+0xf9ce8>
    3348:	00001f79 	andeq	r1, r0, r9, ror pc
    334c:	149b0b05 	ldrne	r0, [fp], #2821	; 0xb05
    3350:	0b060000 	bleq	183358 <_bss_end+0x179cf4>
    3354:	0000147d 	andeq	r1, r0, sp, ror r4
    3358:	16960b07 	ldrne	r0, [r6], r7, lsl #22
    335c:	0b080000 	bleq	203364 <_bss_end+0x1f9d00>
    3360:	00001b74 	andeq	r1, r0, r4, ror fp
    3364:	14a20b09 	strtne	r0, [r2], #2825	; 0xb09
    3368:	0b0a0000 	bleq	283370 <_bss_end+0x279d0c>
    336c:	00001b7b 	andeq	r1, r0, fp, ror fp
    3370:	15070b0b 	strne	r0, [r7, #-2827]	; 0xfffff4f5
    3374:	0b0c0000 	bleq	30337c <_bss_end+0x2f9d18>
    3378:	00001494 	muleq	r0, r4, r4
    337c:	1fd00b0d 	svcne	0x00d00b0d
    3380:	0b0e0000 	bleq	383388 <_bss_end+0x379d24>
    3384:	00001d9d 	muleq	r0, sp, sp
    3388:	c804000f 	stmdagt	r4, {r0, r1, r2, r3}
    338c:	0500001e 	streq	r0, [r0, #-30]	; 0xffffffe2
    3390:	0432013f 	ldrteq	r0, [r2], #-319	; 0xfffffec1
    3394:	5c090000 	stcpl	0, cr0, [r9], {-0}
    3398:	0500001f 	streq	r0, [r0, #-31]	; 0xffffffe1
    339c:	04a50f41 	strteq	r0, [r5], #3905	; 0xf41
    33a0:	e4090000 	str	r0, [r9], #-0
    33a4:	0500001f 	streq	r0, [r0, #-31]	; 0xffffffe1
    33a8:	001d0c4a 	andseq	r0, sp, sl, asr #24
    33ac:	3c090000 	stccc	0, cr0, [r9], {-0}
    33b0:	05000014 	streq	r0, [r0, #-20]	; 0xffffffec
    33b4:	001d0c4b 	andseq	r0, sp, fp, asr #24
    33b8:	bd100000 	ldclt	0, cr0, [r0, #-0]
    33bc:	09000020 	stmdbeq	r0, {r5}
    33c0:	00001ff5 	strdeq	r1, [r0], -r5
    33c4:	e6144c05 	ldr	r4, [r4], -r5, lsl #24
    33c8:	05000004 	streq	r0, [r0, #-4]
    33cc:	0004d504 	andeq	sp, r4, r4, lsl #10
    33d0:	2b091100 	blcs	2477d8 <_bss_end+0x23e174>
    33d4:	05000019 	streq	r0, [r0, #-25]	; 0xffffffe7
    33d8:	04f90f4e 	ldrbteq	r0, [r9], #3918	; 0xf4e
    33dc:	04050000 	streq	r0, [r5], #-0
    33e0:	000004ec 	andeq	r0, r0, ip, ror #9
    33e4:	001ede12 	andseq	sp, lr, r2, lsl lr
    33e8:	1ca50900 			; <UNDEFINED> instruction: 0x1ca50900
    33ec:	52050000 	andpl	r0, r5, #0
    33f0:	0005100d 	andeq	r1, r5, sp
    33f4:	ff040500 			; <UNDEFINED> instruction: 0xff040500
    33f8:	13000004 	movwne	r0, #4
    33fc:	000015b3 			; <UNDEFINED> instruction: 0x000015b3
    3400:	01670534 	cmneq	r7, r4, lsr r5
    3404:	00054115 	andeq	r4, r5, r5, lsl r1
    3408:	1a6a1400 	bne	1a88410 <_bss_end+0x1a7edac>
    340c:	69050000 	stmdbvs	r5, {}	; <UNPREDICTABLE>
    3410:	03de0f01 	bicseq	r0, lr, #1, 30
    3414:	14000000 	strne	r0, [r0], #-0
    3418:	00001597 	muleq	r0, r7, r5
    341c:	14016a05 	strne	r6, [r1], #-2565	; 0xfffff5fb
    3420:	00000546 	andeq	r0, r0, r6, asr #10
    3424:	160e0004 	strne	r0, [lr], -r4
    3428:	0c000005 	stceq	0, cr0, [r0], {5}
    342c:	000000b9 	strheq	r0, [r0], -r9
    3430:	00000556 	andeq	r0, r0, r6, asr r5
    3434:	00002415 	andeq	r2, r0, r5, lsl r4
    3438:	0c002d00 	stceq	13, cr2, [r0], {-0}
    343c:	00000541 	andeq	r0, r0, r1, asr #10
    3440:	00000561 	andeq	r0, r0, r1, ror #10
    3444:	560e000d 	strpl	r0, [lr], -sp
    3448:	0f000005 	svceq	0x00000005
    344c:	00001999 	muleq	r0, r9, r9
    3450:	03016b05 	movweq	r6, #6917	; 0x1b05
    3454:	00000561 	andeq	r0, r0, r1, ror #10
    3458:	001bdf0f 	andseq	sp, fp, pc, lsl #30
    345c:	016e0500 	cmneq	lr, r0, lsl #10
    3460:	00001d0c 	andeq	r1, r0, ip, lsl #26
    3464:	1f1c1600 	svcne	0x001c1600
    3468:	01070000 	mrseq	r0, (UNDEF: 7)
    346c:	00000093 	muleq	r0, r3, r0
    3470:	06018105 	streq	r8, [r1], -r5, lsl #2
    3474:	0000062a 	andeq	r0, r0, sl, lsr #12
    3478:	00134a0b 	andseq	r4, r3, fp, lsl #20
    347c:	560b0000 	strpl	r0, [fp], -r0
    3480:	02000013 	andeq	r0, r0, #19
    3484:	0013620b 	andseq	r6, r3, fp, lsl #4
    3488:	7b0b0300 	blvc	2c4090 <_bss_end+0x2baa2c>
    348c:	03000017 	movweq	r0, #23
    3490:	00136e0b 	andseq	r6, r3, fp, lsl #28
    3494:	c40b0400 	strgt	r0, [fp], #-1024	; 0xfffffc00
    3498:	04000018 	streq	r0, [r0], #-24	; 0xffffffe8
    349c:	0019aa0b 	andseq	sl, r9, fp, lsl #20
    34a0:	000b0500 	andeq	r0, fp, r0, lsl #10
    34a4:	05000019 	streq	r0, [r0, #-25]	; 0xffffffe7
    34a8:	00142d0b 	andseq	r2, r4, fp, lsl #26
    34ac:	7a0b0500 	bvc	2c48b4 <_bss_end+0x2bb250>
    34b0:	06000013 			; <UNDEFINED> instruction: 0x06000013
    34b4:	001b280b 	andseq	r2, fp, fp, lsl #16
    34b8:	890b0600 	stmdbhi	fp, {r9, sl}
    34bc:	06000015 			; <UNDEFINED> instruction: 0x06000015
    34c0:	001b350b 	andseq	r3, fp, fp, lsl #10
    34c4:	9c0b0600 	stcls	6, cr0, [fp], {-0}
    34c8:	0600001f 			; <UNDEFINED> instruction: 0x0600001f
    34cc:	001b420b 	andseq	r4, fp, fp, lsl #4
    34d0:	820b0600 	andhi	r0, fp, #0, 12
    34d4:	0600001b 			; <UNDEFINED> instruction: 0x0600001b
    34d8:	0013860b 	andseq	r8, r3, fp, lsl #12
    34dc:	880b0700 	stmdahi	fp, {r8, r9, sl}
    34e0:	0700001c 	smladeq	r0, ip, r0, r0
    34e4:	001cd50b 	andseq	sp, ip, fp, lsl #10
    34e8:	d70b0700 	strle	r0, [fp, -r0, lsl #14]
    34ec:	0700001f 	smladeq	r0, pc, r0, r0	; <UNPREDICTABLE>
    34f0:	00155c0b 	andseq	r5, r5, fp, lsl #24
    34f4:	560b0700 	strpl	r0, [fp], -r0, lsl #14
    34f8:	0800001d 	stmdaeq	r0, {r0, r2, r3, r4}
    34fc:	0012ff0b 	andseq	pc, r2, fp, lsl #30
    3500:	aa0b0800 	bge	2c5508 <_bss_end+0x2bbea4>
    3504:	0800001f 	stmdaeq	r0, {r0, r1, r2, r3, r4}
    3508:	001d720b 	andseq	r7, sp, fp, lsl #4
    350c:	0f000800 	svceq	0x00000800
    3510:	00002210 	andeq	r2, r0, r0, lsl r2
    3514:	1f019f05 	svcne	0x00019f05
    3518:	00000580 	andeq	r0, r0, r0, lsl #11
    351c:	001da40f 	andseq	sl, sp, pc, lsl #8
    3520:	01a20500 			; <UNDEFINED> instruction: 0x01a20500
    3524:	00001d0c 	andeq	r1, r0, ip, lsl #26
    3528:	19b70f00 	ldmibne	r7!, {r8, r9, sl, fp}
    352c:	a5050000 	strge	r0, [r5, #-0]
    3530:	001d0c01 	andseq	r0, sp, r1, lsl #24
    3534:	dc0f0000 	stcle	0, cr0, [pc], {-0}
    3538:	05000022 	streq	r0, [r0, #-34]	; 0xffffffde
    353c:	1d0c01a8 	stfnes	f0, [ip, #-672]	; 0xfffffd60
    3540:	0f000000 	svceq	0x00000000
    3544:	0000144c 	andeq	r1, r0, ip, asr #8
    3548:	0c01ab05 			; <UNDEFINED> instruction: 0x0c01ab05
    354c:	0000001d 	andeq	r0, r0, sp, lsl r0
    3550:	001dae0f 	andseq	sl, sp, pc, lsl #28
    3554:	01ae0500 			; <UNDEFINED> instruction: 0x01ae0500
    3558:	00001d0c 	andeq	r1, r0, ip, lsl #26
    355c:	1cbf0f00 	ldcne	15, cr0, [pc]	; 3564 <CPSR_IRQ_INHIBIT+0x34e4>
    3560:	b1050000 	mrslt	r0, (UNDEF: 5)
    3564:	001d0c01 	andseq	r0, sp, r1, lsl #24
    3568:	ca0f0000 	bgt	3c3570 <_bss_end+0x3b9f0c>
    356c:	0500001c 	streq	r0, [r0, #-28]	; 0xffffffe4
    3570:	1d0c01b4 	stfnes	f0, [ip, #-720]	; 0xfffffd30
    3574:	0f000000 	svceq	0x00000000
    3578:	00001db8 			; <UNDEFINED> instruction: 0x00001db8
    357c:	0c01b705 	stceq	7, cr11, [r1], {5}
    3580:	0000001d 	andeq	r0, r0, sp, lsl r0
    3584:	001b040f 	andseq	r0, fp, pc, lsl #8
    3588:	01ba0500 			; <UNDEFINED> instruction: 0x01ba0500
    358c:	00001d0c 	andeq	r1, r0, ip, lsl #26
    3590:	223b0f00 	eorscs	r0, fp, #0, 30
    3594:	bd050000 	stclt	0, cr0, [r5, #-0]
    3598:	001d0c01 	andseq	r0, sp, r1, lsl #24
    359c:	c20f0000 	andgt	r0, pc, #0
    35a0:	0500001d 	streq	r0, [r0, #-29]	; 0xffffffe3
    35a4:	1d0c01c0 	stfnes	f0, [ip, #-768]	; 0xfffffd00
    35a8:	0f000000 	svceq	0x00000000
    35ac:	000022ff 	strdeq	r2, [r0], -pc	; <UNPREDICTABLE>
    35b0:	0c01c305 	stceq	3, cr12, [r1], {5}
    35b4:	0000001d 	andeq	r0, r0, sp, lsl r0
    35b8:	0021c50f 	eoreq	ip, r1, pc, lsl #10
    35bc:	01c60500 	biceq	r0, r6, r0, lsl #10
    35c0:	00001d0c 	andeq	r1, r0, ip, lsl #26
    35c4:	21d10f00 	bicscs	r0, r1, r0, lsl #30
    35c8:	c9050000 	stmdbgt	r5, {}	; <UNPREDICTABLE>
    35cc:	001d0c01 	andseq	r0, sp, r1, lsl #24
    35d0:	dd0f0000 	stcle	0, cr0, [pc, #-0]	; 35d8 <CPSR_IRQ_INHIBIT+0x3558>
    35d4:	05000021 	streq	r0, [r0, #-33]	; 0xffffffdf
    35d8:	1d0c01cc 	stfnes	f0, [ip, #-816]	; 0xfffffcd0
    35dc:	0f000000 	svceq	0x00000000
    35e0:	00002202 	andeq	r2, r0, r2, lsl #4
    35e4:	0c01d005 	stceq	0, cr13, [r1], {5}
    35e8:	0000001d 	andeq	r0, r0, sp, lsl r0
    35ec:	0022f20f 	eoreq	pc, r2, pc, lsl #4
    35f0:	01d30500 	bicseq	r0, r3, r0, lsl #10
    35f4:	00001d0c 	andeq	r1, r0, ip, lsl #26
    35f8:	14a90f00 	strtne	r0, [r9], #3840	; 0xf00
    35fc:	d6050000 	strle	r0, [r5], -r0
    3600:	001d0c01 	andseq	r0, sp, r1, lsl #24
    3604:	900f0000 	andls	r0, pc, r0
    3608:	05000012 	streq	r0, [r0, #-18]	; 0xffffffee
    360c:	1d0c01d9 	stfnes	f0, [ip, #-868]	; 0xfffffc9c
    3610:	0f000000 	svceq	0x00000000
    3614:	0000179b 	muleq	r0, fp, r7
    3618:	0c01dc05 	stceq	12, cr13, [r1], {5}
    361c:	0000001d 	andeq	r0, r0, sp, lsl r0
    3620:	0014840f 	andseq	r8, r4, pc, lsl #8
    3624:	01df0500 	bicseq	r0, pc, r0, lsl #10
    3628:	00001d0c 	andeq	r1, r0, ip, lsl #26
    362c:	1dd80f00 	ldclne	15, cr0, [r8]
    3630:	e2050000 	and	r0, r5, #0
    3634:	001d0c01 	andseq	r0, sp, r1, lsl #24
    3638:	e00f0000 	and	r0, pc, r0
    363c:	05000019 	streq	r0, [r0, #-25]	; 0xffffffe7
    3640:	1d0c01e5 	stfnes	f0, [ip, #-916]	; 0xfffffc6c
    3644:	0f000000 	svceq	0x00000000
    3648:	00001c38 	andeq	r1, r0, r8, lsr ip
    364c:	0c01e805 	stceq	8, cr14, [r1], {5}
    3650:	0000001d 	andeq	r0, r0, sp, lsl r0
    3654:	0020f20f 	eoreq	pc, r0, pc, lsl #4
    3658:	01ef0500 	mvneq	r0, r0, lsl #10
    365c:	00001d0c 	andeq	r1, r0, ip, lsl #26
    3660:	22aa0f00 	adccs	r0, sl, #0, 30
    3664:	f2050000 	vhadd.s8	d0, d5, d0
    3668:	001d0c01 	andseq	r0, sp, r1, lsl #24
    366c:	ba0f0000 	blt	3c3674 <_bss_end+0x3ba010>
    3670:	05000022 	streq	r0, [r0, #-34]	; 0xffffffde
    3674:	1d0c01f5 	stfnes	f0, [ip, #-980]	; 0xfffffc2c
    3678:	0f000000 	svceq	0x00000000
    367c:	000015a0 	andeq	r1, r0, r0, lsr #11
    3680:	0c01f805 	stceq	8, cr15, [r1], {5}
    3684:	0000001d 	andeq	r0, r0, sp, lsl r0
    3688:	0021390f 	eoreq	r3, r1, pc, lsl #18
    368c:	01fb0500 	mvnseq	r0, r0, lsl #10
    3690:	00001d0c 	andeq	r1, r0, ip, lsl #26
    3694:	1d3e0f00 	ldcne	15, cr0, [lr, #-0]
    3698:	fe050000 	cdp2	0, 0, cr0, cr5, cr0, {0}
    369c:	001d0c01 	andseq	r0, sp, r1, lsl #24
    36a0:	140f0000 	strne	r0, [pc], #-0	; 36a8 <CPSR_IRQ_INHIBIT+0x3628>
    36a4:	05000018 	streq	r0, [r0, #-24]	; 0xffffffe8
    36a8:	1d0c0202 	sfmne	f0, 4, [ip, #-8]
    36ac:	0f000000 	svceq	0x00000000
    36b0:	00001f2e 	andeq	r1, r0, lr, lsr #30
    36b4:	0c020a05 			; <UNDEFINED> instruction: 0x0c020a05
    36b8:	0000001d 	andeq	r0, r0, sp, lsl r0
    36bc:	0017070f 	andseq	r0, r7, pc, lsl #14
    36c0:	020d0500 	andeq	r0, sp, #0, 10
    36c4:	00001d0c 	andeq	r1, r0, ip, lsl #26
    36c8:	001d0c00 	andseq	r0, sp, r0, lsl #24
    36cc:	07ef0000 	strbeq	r0, [pc, r0]!
    36d0:	000d0000 	andeq	r0, sp, r0
    36d4:	0018e00f 	andseq	lr, r8, pc
    36d8:	03fb0500 	mvnseq	r0, #0, 10
    36dc:	0007e40c 	andeq	lr, r7, ip, lsl #8
    36e0:	04e60c00 	strbteq	r0, [r6], #3072	; 0xc00
    36e4:	080c0000 	stmdaeq	ip, {}	; <UNPREDICTABLE>
    36e8:	24150000 	ldrcs	r0, [r5], #-0
    36ec:	0d000000 	stceq	0, cr0, [r0, #-0]
    36f0:	1dfb0f00 	ldclne	15, cr0, [fp]
    36f4:	84050000 	strhi	r0, [r5], #-0
    36f8:	07fc1405 	ldrbeq	r1, [ip, r5, lsl #8]!
    36fc:	a2160000 	andsge	r0, r6, #0
    3700:	07000019 	smladeq	r0, r9, r0, r0
    3704:	00009301 	andeq	r9, r0, r1, lsl #6
    3708:	058b0500 	streq	r0, [fp, #1280]	; 0x500
    370c:	00085706 	andeq	r5, r8, r6, lsl #14
    3710:	175d0b00 	ldrbne	r0, [sp, -r0, lsl #22]
    3714:	0b000000 	bleq	371c <CPSR_IRQ_INHIBIT+0x369c>
    3718:	00001bad 	andeq	r1, r0, sp, lsr #23
    371c:	13350b01 	teqne	r5, #1024	; 0x400
    3720:	0b020000 	bleq	83728 <_bss_end+0x7a0c4>
    3724:	0000226c 	andeq	r2, r0, ip, ror #4
    3728:	1e750b03 	vaddne.f64	d16, d5, d3
    372c:	0b040000 	bleq	103734 <_bss_end+0xfa0d0>
    3730:	00001e68 	andeq	r1, r0, r8, ror #28
    3734:	140c0b05 	strne	r0, [ip], #-2821	; 0xfffff4fb
    3738:	00060000 	andeq	r0, r6, r0
    373c:	00225c0f 	eoreq	r5, r2, pc, lsl #24
    3740:	05980500 	ldreq	r0, [r8, #1280]	; 0x500
    3744:	00081915 	andeq	r1, r8, r5, lsl r9
    3748:	215e0f00 	cmpcs	lr, r0, lsl #30
    374c:	99050000 	stmdbls	r5, {}	; <UNPREDICTABLE>
    3750:	00241107 	eoreq	r1, r4, r7, lsl #2
    3754:	e80f0000 	stmda	pc, {}	; <UNPREDICTABLE>
    3758:	0500001d 	streq	r0, [r0, #-29]	; 0xffffffe3
    375c:	1d0c07ae 	stcne	7, cr0, [ip, #-696]	; 0xfffffd48
    3760:	04000000 	streq	r0, [r0], #-0
    3764:	000020d1 	ldrdeq	r2, [r0], -r1
    3768:	93167b06 	tstls	r6, #6144	; 0x1800
    376c:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    3770:	0000087e 	andeq	r0, r0, lr, ror r8
    3774:	b9050203 	stmdblt	r5, {r0, r1, r9}
    3778:	03000001 	movweq	r0, #1
    377c:	16f00708 	ldrbtne	r0, [r0], r8, lsl #14
    3780:	04030000 	streq	r0, [r3], #-0
    3784:	0014c404 	andseq	ip, r4, r4, lsl #8
    3788:	03080300 	movweq	r0, #33536	; 0x8300
    378c:	000014bc 			; <UNDEFINED> instruction: 0x000014bc
    3790:	d1040803 	tstle	r4, r3, lsl #16
    3794:	0300001d 	movweq	r0, #29
    3798:	1e830310 	mcrne	3, 4, r0, cr3, cr0, {0}
    379c:	8a0c0000 	bhi	3037a4 <_bss_end+0x2fa140>
    37a0:	c9000008 	stmdbgt	r0, {r3}
    37a4:	15000008 	strne	r0, [r0, #-8]
    37a8:	00000024 	andeq	r0, r0, r4, lsr #32
    37ac:	b90e00ff 	stmdblt	lr, {r0, r1, r2, r3, r4, r5, r6, r7}
    37b0:	0f000008 	svceq	0x00000008
    37b4:	00001ce2 	andeq	r1, r0, r2, ror #25
    37b8:	1601fc06 	strne	pc, [r1], -r6, lsl #24
    37bc:	000008c9 	andeq	r0, r0, r9, asr #17
    37c0:	0014730f 	andseq	r7, r4, pc, lsl #6
    37c4:	02020600 	andeq	r0, r2, #0, 12
    37c8:	0008c916 	andeq	ip, r8, r6, lsl r9
    37cc:	21040400 	tstcs	r4, r0, lsl #8
    37d0:	2a070000 	bcs	1c37d8 <_bss_end+0x1ba174>
    37d4:	0004f910 	andeq	pc, r4, r0, lsl r9	; <UNPREDICTABLE>
    37d8:	08e80c00 	stmiaeq	r8!, {sl, fp}^
    37dc:	08ff0000 	ldmeq	pc!, {}^	; <UNPREDICTABLE>
    37e0:	000d0000 	andeq	r0, sp, r0
    37e4:	00113309 	andseq	r3, r1, r9, lsl #6
    37e8:	112f0700 			; <UNDEFINED> instruction: 0x112f0700
    37ec:	000008f4 	strdeq	r0, [r0], -r4
    37f0:	00116509 	andseq	r6, r1, r9, lsl #10
    37f4:	11300700 	teqne	r0, r0, lsl #14
    37f8:	000008f4 	strdeq	r0, [r0], -r4
    37fc:	0008ff17 	andeq	pc, r8, r7, lsl pc	; <UNPREDICTABLE>
    3800:	09330800 	ldmdbeq	r3!, {fp}
    3804:	2403050a 	strcs	r0, [r3], #-1290	; 0xfffffaf6
    3808:	17000096 			; <UNDEFINED> instruction: 0x17000096
    380c:	0000090b 	andeq	r0, r0, fp, lsl #18
    3810:	0a093408 	beq	250838 <_bss_end+0x2471d4>
    3814:	96380305 	ldrtls	r0, [r8], -r5, lsl #6
    3818:	Address 0x0000000000003818 is out of bounds.


Disassembly of section .debug_abbrev:

00000000 <.debug_abbrev>:
       0:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
       4:	030b130e 	movweq	r1, #45838	; 0xb30e
       8:	110e1b0e 	tstne	lr, lr, lsl #22
       c:	10061201 	andne	r1, r6, r1, lsl #4
      10:	02000017 	andeq	r0, r0, #23
      14:	193f002e 	ldmdbne	pc!, {r1, r2, r3, r5}	; <UNPREDICTABLE>
      18:	0b3a0e03 	bleq	e8382c <_bss_end+0xe7a1c8>
      1c:	0b390b3b 	bleq	e42d10 <_bss_end+0xe396ac>
      20:	06120111 			; <UNDEFINED> instruction: 0x06120111
      24:	42971840 	addsmi	r1, r7, #64, 16	; 0x400000
      28:	03000019 	movweq	r0, #25
      2c:	0e030139 	mcreq	1, 0, r0, cr3, cr9, {1}
      30:	0b3b0b3a 	bleq	ec2d20 <_bss_end+0xeb96bc>
      34:	00001301 	andeq	r1, r0, r1, lsl #6
      38:	3f012e04 	svccc	0x00012e04
      3c:	3a0e0319 	bcc	380ca8 <_bss_end+0x377644>
      40:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
      44:	01193c0b 	tsteq	r9, fp, lsl #24
      48:	05000013 	streq	r0, [r0, #-19]	; 0xffffffed
      4c:	13490005 	movtne	r0, #36869	; 0x9005
      50:	16060000 	strne	r0, [r6], -r0
      54:	3a0e0300 	bcc	380c5c <_bss_end+0x3775f8>
      58:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
      5c:	0013490b 	andseq	r4, r3, fp, lsl #18
      60:	012e0700 			; <UNDEFINED> instruction: 0x012e0700
      64:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
      68:	0b3b0b3a 	bleq	ec2d58 <_bss_end+0xeb96f4>
      6c:	13490b39 	movtne	r0, #39737	; 0x9b39
      70:	0000193c 	andeq	r1, r0, ip, lsr r9
      74:	0b000f08 	bleq	3c9c <CPSR_IRQ_INHIBIT+0x3c1c>
      78:	0013490b 	andseq	r4, r3, fp, lsl #18
      7c:	00240900 	eoreq	r0, r4, r0, lsl #18
      80:	0b3e0b0b 	bleq	f82cb4 <_bss_end+0xf79650>
      84:	00000e03 	andeq	r0, r0, r3, lsl #28
      88:	47012e0a 	strmi	r2, [r1, -sl, lsl #28]
      8c:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
      90:	97184006 	ldrls	r4, [r8, -r6]
      94:	13011942 	movwne	r1, #6466	; 0x1942
      98:	050b0000 	streq	r0, [fp, #-0]
      9c:	02134900 	andseq	r4, r3, #0, 18
      a0:	0c000018 	stceq	0, cr0, [r0], {24}
      a4:	08030005 	stmdaeq	r3, {r0, r2}
      a8:	0b3b0b3a 	bleq	ec2d98 <_bss_end+0xeb9734>
      ac:	13490b39 	movtne	r0, #39737	; 0x9b39
      b0:	00001802 	andeq	r1, r0, r2, lsl #16
      b4:	0b00240d 	bleq	90f0 <_kernel_main+0x7c>
      b8:	030b3e0b 	movweq	r3, #48651	; 0xbe0b
      bc:	0e000008 	cdpeq	0, 0, cr0, cr0, cr8, {0}
      c0:	1347012e 	movtne	r0, #28974	; 0x712e
      c4:	06120111 			; <UNDEFINED> instruction: 0x06120111
      c8:	42971840 	addsmi	r1, r7, #64, 16	; 0x400000
      cc:	00000019 	andeq	r0, r0, r9, lsl r0
      d0:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
      d4:	030b130e 	movweq	r1, #45838	; 0xb30e
      d8:	110e1b0e 	tstne	lr, lr, lsl #22
      dc:	10061201 	andne	r1, r6, r1, lsl #4
      e0:	02000017 	andeq	r0, r0, #23
      e4:	0b0b0024 	bleq	2c017c <_bss_end+0x2b6b18>
      e8:	0e030b3e 	vmoveq.16	d3[0], r0
      ec:	24030000 	strcs	r0, [r3], #-0
      f0:	3e0b0b00 	vmlacc.f64	d0, d11, d0
      f4:	0008030b 	andeq	r0, r8, fp, lsl #6
      f8:	00160400 	andseq	r0, r6, r0, lsl #8
      fc:	0b3a0e03 	bleq	e83910 <_bss_end+0xe7a2ac>
     100:	0b390b3b 	bleq	e42df4 <_bss_end+0xe39790>
     104:	00001349 	andeq	r1, r0, r9, asr #6
     108:	49002605 	stmdbmi	r0, {r0, r2, r9, sl, sp}
     10c:	06000013 			; <UNDEFINED> instruction: 0x06000013
     110:	08030139 	stmdaeq	r3, {r0, r3, r4, r5, r8}
     114:	0b3b0b3a 	bleq	ec2e04 <_bss_end+0xeb97a0>
     118:	13010b39 	movwne	r0, #6969	; 0x1b39
     11c:	34070000 	strcc	r0, [r7], #-0
     120:	3a0e0300 	bcc	380d28 <_bss_end+0x3776c4>
     124:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     128:	3c13490b 			; <UNDEFINED> instruction: 0x3c13490b
     12c:	6c061c19 	stcvs	12, cr1, [r6], {25}
     130:	08000019 	stmdaeq	r0, {r0, r3, r4}
     134:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     138:	0b3b0b3a 	bleq	ec2e28 <_bss_end+0xeb97c4>
     13c:	13490b39 	movtne	r0, #39737	; 0x9b39
     140:	0b1c193c 	bleq	706638 <_bss_end+0x6fcfd4>
     144:	0000196c 	andeq	r1, r0, ip, ror #18
     148:	03010409 	movweq	r0, #5129	; 0x1409
     14c:	3e196d0e 	cdpcc	13, 1, cr6, cr9, cr14, {0}
     150:	490b0b0b 	stmdbmi	fp, {r0, r1, r3, r8, r9, fp}
     154:	3b0b3a13 	blcc	2ce9a8 <_bss_end+0x2c5344>
     158:	010b390b 	tsteq	fp, fp, lsl #18
     15c:	0a000013 	beq	1b0 <CPSR_IRQ_INHIBIT+0x130>
     160:	08030028 	stmdaeq	r3, {r3, r5}
     164:	00000b1c 	andeq	r0, r0, ip, lsl fp
     168:	0300280b 	movweq	r2, #2059	; 0x80b
     16c:	000b1c0e 	andeq	r1, fp, lr, lsl #24
     170:	00340c00 	eorseq	r0, r4, r0, lsl #24
     174:	00001347 	andeq	r1, r0, r7, asr #6
     178:	0301020d 	movweq	r0, #4621	; 0x120d
     17c:	3a0b0b0e 	bcc	2c2dbc <_bss_end+0x2b9758>
     180:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     184:	0013010b 	andseq	r0, r3, fp, lsl #2
     188:	000d0e00 	andeq	r0, sp, r0, lsl #28
     18c:	0b3a0e03 	bleq	e839a0 <_bss_end+0xe7a33c>
     190:	0b390b3b 	bleq	e42e84 <_bss_end+0xe39820>
     194:	0b381349 	bleq	e04ec0 <_bss_end+0xdfb85c>
     198:	2e0f0000 	cdpcs	0, 0, cr0, cr15, cr0, {0}
     19c:	03193f01 	tsteq	r9, #1, 30
     1a0:	3b0b3a0e 	blcc	2ce9e0 <_bss_end+0x2c537c>
     1a4:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     1a8:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     1ac:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     1b0:	00130113 	andseq	r0, r3, r3, lsl r1
     1b4:	00051000 	andeq	r1, r5, r0
     1b8:	19341349 	ldmdbne	r4!, {r0, r3, r6, r8, r9, ip}
     1bc:	05110000 	ldreq	r0, [r1, #-0]
     1c0:	00134900 	andseq	r4, r3, r0, lsl #18
     1c4:	012e1200 			; <UNDEFINED> instruction: 0x012e1200
     1c8:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     1cc:	0b3b0b3a 	bleq	ec2ebc <_bss_end+0xeb9858>
     1d0:	0e6e0b39 	vmoveq.8	d14[5], r0
     1d4:	193c0b32 	ldmdbne	ip!, {r1, r4, r5, r8, r9, fp}
     1d8:	13011364 	movwne	r1, #4964	; 0x1364
     1dc:	2e130000 	cdpcs	0, 1, cr0, cr3, cr0, {0}
     1e0:	03193f01 	tsteq	r9, #1, 30
     1e4:	3b0b3a0e 	blcc	2cea24 <_bss_end+0x2c53c0>
     1e8:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     1ec:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     1f0:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     1f4:	14000013 	strne	r0, [r0], #-19	; 0xffffffed
     1f8:	0b0b000f 	bleq	2c023c <_bss_end+0x2b6bd8>
     1fc:	00001349 	andeq	r1, r0, r9, asr #6
     200:	03003415 	movweq	r3, #1045	; 0x415
     204:	3b0b3a0e 	blcc	2cea44 <_bss_end+0x2c53e0>
     208:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     20c:	3c193f13 	ldccc	15, cr3, [r9], {19}
     210:	16000019 			; <UNDEFINED> instruction: 0x16000019
     214:	13470034 	movtne	r0, #28724	; 0x7034
     218:	0b3b0b3a 	bleq	ec2f08 <_bss_end+0xeb98a4>
     21c:	18020b39 	stmdane	r2, {r0, r3, r4, r5, r8, r9, fp}
     220:	2e170000 	cdpcs	0, 1, cr0, cr7, cr0, {0}
     224:	340e0300 	strcc	r0, [lr], #-768	; 0xfffffd00
     228:	12011119 	andne	r1, r1, #1073741830	; 0x40000006
     22c:	96184006 	ldrls	r4, [r8], -r6
     230:	00001942 	andeq	r1, r0, r2, asr #18
     234:	03012e18 	movweq	r2, #7704	; 0x1e18
     238:	1119340e 	tstne	r9, lr, lsl #8
     23c:	40061201 	andmi	r1, r6, r1, lsl #4
     240:	19429618 	stmdbne	r2, {r3, r4, r9, sl, ip, pc}^
     244:	00001301 	andeq	r1, r0, r1, lsl #6
     248:	03000519 	movweq	r0, #1305	; 0x519
     24c:	3b0b3a0e 	blcc	2cea8c <_bss_end+0x2c5428>
     250:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     254:	00180213 	andseq	r0, r8, r3, lsl r2
     258:	012e1a00 			; <UNDEFINED> instruction: 0x012e1a00
     25c:	0b3a1347 	bleq	e84f80 <_bss_end+0xe7b91c>
     260:	0b390b3b 	bleq	e42f54 <_bss_end+0xe398f0>
     264:	01111364 	tsteq	r1, r4, ror #6
     268:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     26c:	01194297 			; <UNDEFINED> instruction: 0x01194297
     270:	1b000013 	blne	2c4 <CPSR_IRQ_INHIBIT+0x244>
     274:	0e030005 	cdpeq	0, 0, cr0, cr3, cr5, {0}
     278:	19341349 	ldmdbne	r4!, {r0, r3, r6, r8, r9, ip}
     27c:	00001802 	andeq	r1, r0, r2, lsl #16
     280:	47012e1c 	smladmi	r1, ip, lr, r2
     284:	390b3a13 	stmdbcc	fp, {r0, r1, r4, r9, fp, ip, sp}
     288:	1113640b 	tstne	r3, fp, lsl #8
     28c:	40061201 	andmi	r1, r6, r1, lsl #4
     290:	19429718 	stmdbne	r2, {r3, r4, r8, r9, sl, ip, pc}^
     294:	00001301 	andeq	r1, r0, r1, lsl #6
     298:	47012e1d 	smladmi	r1, sp, lr, r2
     29c:	3b0b3a13 	blcc	2ceaf0 <_bss_end+0x2c548c>
     2a0:	640b390b 	strvs	r3, [fp], #-2315	; 0xfffff6f5
     2a4:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     2a8:	96184006 	ldrls	r4, [r8], -r6
     2ac:	13011942 	movwne	r1, #6466	; 0x1942
     2b0:	2e1e0000 	cdpcs	0, 1, cr0, cr14, cr0, {0}
     2b4:	3a134701 	bcc	4d1ec0 <_bss_end+0x4c885c>
     2b8:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     2bc:	2013640b 	andscs	r6, r3, fp, lsl #8
     2c0:	0013010b 	andseq	r0, r3, fp, lsl #2
     2c4:	00051f00 	andeq	r1, r5, r0, lsl #30
     2c8:	13490e03 	movtne	r0, #40451	; 0x9e03
     2cc:	00001934 	andeq	r1, r0, r4, lsr r9
     2d0:	03000520 	movweq	r0, #1312	; 0x520
     2d4:	3b0b3a0e 	blcc	2ceb14 <_bss_end+0x2c54b0>
     2d8:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     2dc:	21000013 	tstcs	r0, r3, lsl r0
     2e0:	1331012e 	teqne	r1, #-2147483637	; 0x8000000b
     2e4:	13640e6e 	cmnne	r4, #1760	; 0x6e0
     2e8:	06120111 			; <UNDEFINED> instruction: 0x06120111
     2ec:	42971840 	addsmi	r1, r7, #64, 16	; 0x400000
     2f0:	22000019 	andcs	r0, r0, #25
     2f4:	13310005 	teqne	r1, #5
     2f8:	00001802 	andeq	r1, r0, r2, lsl #16
     2fc:	01110100 	tsteq	r1, r0, lsl #2
     300:	0b130e25 	bleq	4c3b9c <_bss_end+0x4ba538>
     304:	0e1b0e03 	cdpeq	14, 1, cr0, cr11, cr3, {0}
     308:	06120111 			; <UNDEFINED> instruction: 0x06120111
     30c:	00001710 	andeq	r1, r0, r0, lsl r7
     310:	0b002402 	bleq	9320 <__udivsi3+0xe8>
     314:	030b3e0b 	movweq	r3, #48651	; 0xbe0b
     318:	0300000e 	movweq	r0, #14
     31c:	0b0b0024 	bleq	2c03b4 <_bss_end+0x2b6d50>
     320:	08030b3e 	stmdaeq	r3, {r1, r2, r3, r4, r5, r8, r9, fp}
     324:	16040000 	strne	r0, [r4], -r0
     328:	3a0e0300 	bcc	380f30 <_bss_end+0x3778cc>
     32c:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     330:	0013490b 	andseq	r4, r3, fp, lsl #18
     334:	00260500 	eoreq	r0, r6, r0, lsl #10
     338:	00001349 	andeq	r1, r0, r9, asr #6
     33c:	03013906 	movweq	r3, #6406	; 0x1906
     340:	3b0b3a08 	blcc	2ceb68 <_bss_end+0x2c5504>
     344:	010b390b 	tsteq	fp, fp, lsl #18
     348:	07000013 	smladeq	r0, r3, r0, r0
     34c:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     350:	0b3b0b3a 	bleq	ec3040 <_bss_end+0xeb99dc>
     354:	13490b39 	movtne	r0, #39737	; 0x9b39
     358:	061c193c 			; <UNDEFINED> instruction: 0x061c193c
     35c:	0000196c 	andeq	r1, r0, ip, ror #18
     360:	03003408 	movweq	r3, #1032	; 0x408
     364:	3b0b3a0e 	blcc	2ceba4 <_bss_end+0x2c5540>
     368:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     36c:	1c193c13 	ldcne	12, cr3, [r9], {19}
     370:	00196c0b 	andseq	r6, r9, fp, lsl #24
     374:	01040900 	tsteq	r4, r0, lsl #18
     378:	196d0e03 	stmdbne	sp!, {r0, r1, r9, sl, fp}^
     37c:	0b0b0b3e 	bleq	2c307c <_bss_end+0x2b9a18>
     380:	0b3a1349 	bleq	e850ac <_bss_end+0xe7ba48>
     384:	0b390b3b 	bleq	e43078 <_bss_end+0xe39a14>
     388:	00001301 	andeq	r1, r0, r1, lsl #6
     38c:	0300280a 	movweq	r2, #2058	; 0x80a
     390:	000b1c0e 	andeq	r1, fp, lr, lsl #24
     394:	00340b00 	eorseq	r0, r4, r0, lsl #22
     398:	00001347 	andeq	r1, r0, r7, asr #6
     39c:	0301020c 	movweq	r0, #4620	; 0x120c
     3a0:	3a0b0b0e 	bcc	2c2fe0 <_bss_end+0x2b997c>
     3a4:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     3a8:	0013010b 	andseq	r0, r3, fp, lsl #2
     3ac:	000d0d00 	andeq	r0, sp, r0, lsl #26
     3b0:	0b3a0e03 	bleq	e83bc4 <_bss_end+0xe7a560>
     3b4:	0b390b3b 	bleq	e430a8 <_bss_end+0xe39a44>
     3b8:	0b381349 	bleq	e050e4 <_bss_end+0xdfba80>
     3bc:	2e0e0000 	cdpcs	0, 0, cr0, cr14, cr0, {0}
     3c0:	03193f01 	tsteq	r9, #1, 30
     3c4:	3b0b3a0e 	blcc	2cec04 <_bss_end+0x2c55a0>
     3c8:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     3cc:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     3d0:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     3d4:	00130113 	andseq	r0, r3, r3, lsl r1
     3d8:	00050f00 	andeq	r0, r5, r0, lsl #30
     3dc:	19341349 	ldmdbne	r4!, {r0, r3, r6, r8, r9, ip}
     3e0:	05100000 	ldreq	r0, [r0, #-0]
     3e4:	00134900 	andseq	r4, r3, r0, lsl #18
     3e8:	012e1100 			; <UNDEFINED> instruction: 0x012e1100
     3ec:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     3f0:	0b3b0b3a 	bleq	ec30e0 <_bss_end+0xeb9a7c>
     3f4:	0e6e0b39 	vmoveq.8	d14[5], r0
     3f8:	193c0b32 	ldmdbne	ip!, {r1, r4, r5, r8, r9, fp}
     3fc:	13011364 	movwne	r1, #4964	; 0x1364
     400:	2e120000 	cdpcs	0, 1, cr0, cr2, cr0, {0}
     404:	03193f01 	tsteq	r9, #1, 30
     408:	3b0b3a0e 	blcc	2cec48 <_bss_end+0x2c55e4>
     40c:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     410:	3c0b320e 	sfmcc	f3, 4, [fp], {14}
     414:	00136419 	andseq	r6, r3, r9, lsl r4
     418:	000f1300 	andeq	r1, pc, r0, lsl #6
     41c:	13490b0b 	movtne	r0, #39691	; 0x9b0b
     420:	10140000 	andsne	r0, r4, r0
     424:	490b0b00 	stmdbmi	fp, {r8, r9, fp}
     428:	15000013 	strne	r0, [r0, #-19]	; 0xffffffed
     42c:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     430:	0b3b0b3a 	bleq	ec3120 <_bss_end+0xeb9abc>
     434:	13490b39 	movtne	r0, #39737	; 0x9b39
     438:	193c193f 	ldmdbne	ip!, {r0, r1, r2, r3, r4, r5, r8, fp, ip}
     43c:	34160000 	ldrcc	r0, [r6], #-0
     440:	3a134700 	bcc	4d2048 <_bss_end+0x4c89e4>
     444:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     448:	0018020b 	andseq	r0, r8, fp, lsl #4
     44c:	002e1700 	eoreq	r1, lr, r0, lsl #14
     450:	19340e03 	ldmdbne	r4!, {r0, r1, r9, sl, fp}
     454:	06120111 			; <UNDEFINED> instruction: 0x06120111
     458:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     45c:	18000019 	stmdane	r0, {r0, r3, r4}
     460:	0e03012e 	adfeqsp	f0, f3, #0.5
     464:	01111934 	tsteq	r1, r4, lsr r9
     468:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     46c:	01194296 			; <UNDEFINED> instruction: 0x01194296
     470:	19000013 	stmdbne	r0, {r0, r1, r4}
     474:	0e030005 	cdpeq	0, 0, cr0, cr3, cr5, {0}
     478:	0b3b0b3a 	bleq	ec3168 <_bss_end+0xeb9b04>
     47c:	13490b39 	movtne	r0, #39737	; 0x9b39
     480:	00001802 	andeq	r1, r0, r2, lsl #16
     484:	47012e1a 	smladmi	r1, sl, lr, r2
     488:	3b0b3a13 	blcc	2cecdc <_bss_end+0x2c5678>
     48c:	640b390b 	strvs	r3, [fp], #-2315	; 0xfffff6f5
     490:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     494:	96184006 	ldrls	r4, [r8], -r6
     498:	13011942 	movwne	r1, #6466	; 0x1942
     49c:	051b0000 	ldreq	r0, [fp, #-0]
     4a0:	490e0300 	stmdbmi	lr, {r8, r9}
     4a4:	02193413 	andseq	r3, r9, #318767104	; 0x13000000
     4a8:	1c000018 	stcne	0, cr0, [r0], {24}
     4ac:	08030005 	stmdaeq	r3, {r0, r2}
     4b0:	0b3b0b3a 	bleq	ec31a0 <_bss_end+0xeb9b3c>
     4b4:	13490b39 	movtne	r0, #39737	; 0x9b39
     4b8:	00001802 	andeq	r1, r0, r2, lsl #16
     4bc:	0300341d 	movweq	r3, #1053	; 0x41d
     4c0:	3b0b3a08 	blcc	2cece8 <_bss_end+0x2c5684>
     4c4:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     4c8:	00180213 	andseq	r0, r8, r3, lsl r2
     4cc:	012e1e00 			; <UNDEFINED> instruction: 0x012e1e00
     4d0:	0b3a1347 	bleq	e851f4 <_bss_end+0xe7bb90>
     4d4:	0b390b3b 	bleq	e431c8 <_bss_end+0xe39b64>
     4d8:	01111364 	tsteq	r1, r4, ror #6
     4dc:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     4e0:	01194297 			; <UNDEFINED> instruction: 0x01194297
     4e4:	1f000013 	svcne	0x00000013
     4e8:	1347012e 	movtne	r0, #28974	; 0x712e
     4ec:	0b3b0b3a 	bleq	ec31dc <_bss_end+0xeb9b78>
     4f0:	13640b39 	cmnne	r4, #58368	; 0xe400
     4f4:	13010b20 	movwne	r0, #6944	; 0x1b20
     4f8:	05200000 	streq	r0, [r0, #-0]!
     4fc:	490e0300 	stmdbmi	lr, {r8, r9}
     500:	00193413 	andseq	r3, r9, r3, lsl r4
     504:	00052100 	andeq	r2, r5, r0, lsl #2
     508:	0b3a0e03 	bleq	e83d1c <_bss_end+0xe7a6b8>
     50c:	0b390b3b 	bleq	e43200 <_bss_end+0xe39b9c>
     510:	00001349 	andeq	r1, r0, r9, asr #6
     514:	31012e22 	tstcc	r1, r2, lsr #28
     518:	640e6e13 	strvs	r6, [lr], #-3603	; 0xfffff1ed
     51c:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     520:	97184006 	ldrls	r4, [r8, -r6]
     524:	00001942 	andeq	r1, r0, r2, asr #18
     528:	31000523 	tstcc	r0, r3, lsr #10
     52c:	00180213 	andseq	r0, r8, r3, lsl r2
     530:	11010000 	mrsne	r0, (UNDEF: 1)
     534:	130e2501 	movwne	r2, #58625	; 0xe501
     538:	1b0e030b 	blne	38116c <_bss_end+0x377b08>
     53c:	1201110e 	andne	r1, r1, #-2147483645	; 0x80000003
     540:	00171006 	andseq	r1, r7, r6
     544:	00240200 	eoreq	r0, r4, r0, lsl #4
     548:	0b3e0b0b 	bleq	f8317c <_bss_end+0xf79b18>
     54c:	00000e03 	andeq	r0, r0, r3, lsl #28
     550:	49002603 	stmdbmi	r0, {r0, r1, r9, sl, sp}
     554:	04000013 	streq	r0, [r0], #-19	; 0xffffffed
     558:	0b0b0024 	bleq	2c05f0 <_bss_end+0x2b6f8c>
     55c:	08030b3e 	stmdaeq	r3, {r1, r2, r3, r4, r5, r8, r9, fp}
     560:	16050000 	strne	r0, [r5], -r0
     564:	3a0e0300 	bcc	38116c <_bss_end+0x377b08>
     568:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     56c:	0013490b 	andseq	r4, r3, fp, lsl #18
     570:	00350600 	eorseq	r0, r5, r0, lsl #12
     574:	00001349 	andeq	r1, r0, r9, asr #6
     578:	03013907 	movweq	r3, #6407	; 0x1907
     57c:	3b0b3a08 	blcc	2ceda4 <_bss_end+0x2c5740>
     580:	010b390b 	tsteq	fp, fp, lsl #18
     584:	08000013 	stmdaeq	r0, {r0, r1, r4}
     588:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     58c:	0b3b0b3a 	bleq	ec327c <_bss_end+0xeb9c18>
     590:	13490b39 	movtne	r0, #39737	; 0x9b39
     594:	061c193c 			; <UNDEFINED> instruction: 0x061c193c
     598:	0000196c 	andeq	r1, r0, ip, ror #18
     59c:	03003409 	movweq	r3, #1033	; 0x409
     5a0:	3b0b3a0e 	blcc	2cede0 <_bss_end+0x2c577c>
     5a4:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     5a8:	1c193c13 	ldcne	12, cr3, [r9], {19}
     5ac:	00196c0b 	andseq	r6, r9, fp, lsl #24
     5b0:	01040a00 	tsteq	r4, r0, lsl #20
     5b4:	196d0e03 	stmdbne	sp!, {r0, r1, r9, sl, fp}^
     5b8:	0b0b0b3e 	bleq	2c32b8 <_bss_end+0x2b9c54>
     5bc:	0b3a1349 	bleq	e852e8 <_bss_end+0xe7bc84>
     5c0:	0b390b3b 	bleq	e432b4 <_bss_end+0xe39c50>
     5c4:	00001301 	andeq	r1, r0, r1, lsl #6
     5c8:	0300280b 	movweq	r2, #2059	; 0x80b
     5cc:	000b1c08 	andeq	r1, fp, r8, lsl #24
     5d0:	00280c00 	eoreq	r0, r8, r0, lsl #24
     5d4:	0b1c0e03 	bleq	703de8 <_bss_end+0x6fa784>
     5d8:	040d0000 	streq	r0, [sp], #-0
     5dc:	6d0e0301 	stcvs	3, cr0, [lr, #-4]
     5e0:	0b0b3e19 	bleq	2cfe4c <_bss_end+0x2c67e8>
     5e4:	3a13490b 	bcc	4d2a18 <_bss_end+0x4c93b4>
     5e8:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     5ec:	0e00000b 	cdpeq	0, 0, cr0, cr0, cr11, {0}
     5f0:	13470034 	movtne	r0, #28724	; 0x7034
     5f4:	020f0000 	andeq	r0, pc, #0
     5f8:	0b0e0301 	bleq	381204 <_bss_end+0x377ba0>
     5fc:	3b0b3a0b 	blcc	2cee30 <_bss_end+0x2c57cc>
     600:	010b390b 	tsteq	fp, fp, lsl #18
     604:	10000013 	andne	r0, r0, r3, lsl r0
     608:	0e03000d 	cdpeq	0, 0, cr0, cr3, cr13, {0}
     60c:	0b3b0b3a 	bleq	ec32fc <_bss_end+0xeb9c98>
     610:	13490b39 	movtne	r0, #39737	; 0x9b39
     614:	00000b38 	andeq	r0, r0, r8, lsr fp
     618:	03001611 	movweq	r1, #1553	; 0x611
     61c:	3b0b3a0e 	blcc	2cee5c <_bss_end+0x2c57f8>
     620:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     624:	000b3213 	andeq	r3, fp, r3, lsl r2
     628:	012e1200 			; <UNDEFINED> instruction: 0x012e1200
     62c:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     630:	0b3b0b3a 	bleq	ec3320 <_bss_end+0xeb9cbc>
     634:	0e6e0b39 	vmoveq.8	d14[5], r0
     638:	0b321349 	bleq	c85364 <_bss_end+0xc7bd00>
     63c:	1364193c 	cmnne	r4, #60, 18	; 0xf0000
     640:	00001301 	andeq	r1, r0, r1, lsl #6
     644:	49000513 	stmdbmi	r0, {r0, r1, r4, r8, sl}
     648:	00193413 	andseq	r3, r9, r3, lsl r4
     64c:	00051400 	andeq	r1, r5, r0, lsl #8
     650:	00001349 	andeq	r1, r0, r9, asr #6
     654:	3f012e15 	svccc	0x00012e15
     658:	3a0e0319 	bcc	3812c4 <_bss_end+0x377c60>
     65c:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     660:	320e6e0b 	andcc	r6, lr, #11, 28	; 0xb0
     664:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     668:	00130113 	andseq	r0, r3, r3, lsl r1
     66c:	012e1600 			; <UNDEFINED> instruction: 0x012e1600
     670:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     674:	0b3b0b3a 	bleq	ec3364 <_bss_end+0xeb9d00>
     678:	0e6e0b39 	vmoveq.8	d14[5], r0
     67c:	0b321349 	bleq	c853a8 <_bss_end+0xc7bd44>
     680:	1364193c 	cmnne	r4, #60, 18	; 0xf0000
     684:	0f170000 	svceq	0x00170000
     688:	490b0b00 	stmdbmi	fp, {r8, r9, fp}
     68c:	18000013 	stmdane	r0, {r0, r1, r4}
     690:	00000015 	andeq	r0, r0, r5, lsl r0
     694:	0b001019 	bleq	4700 <CPSR_IRQ_INHIBIT+0x4680>
     698:	0013490b 	andseq	r4, r3, fp, lsl #18
     69c:	00341a00 	eorseq	r1, r4, r0, lsl #20
     6a0:	0b3a0e03 	bleq	e83eb4 <_bss_end+0xe7a850>
     6a4:	0b390b3b 	bleq	e43398 <_bss_end+0xe39d34>
     6a8:	193f1349 	ldmdbne	pc!, {r0, r3, r6, r8, r9, ip}	; <UNPREDICTABLE>
     6ac:	0000193c 	andeq	r1, r0, ip, lsr r9
     6b0:	0300281b 	movweq	r2, #2075	; 0x81b
     6b4:	00051c0e 	andeq	r1, r5, lr, lsl #24
     6b8:	00281c00 	eoreq	r1, r8, r0, lsl #24
     6bc:	061c0e03 	ldreq	r0, [ip], -r3, lsl #28
     6c0:	2e1d0000 	cdpcs	0, 1, cr0, cr13, cr0, {0}
     6c4:	03193f01 	tsteq	r9, #1, 30
     6c8:	3b0b3a0e 	blcc	2cef08 <_bss_end+0x2c58a4>
     6cc:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     6d0:	3c0b320e 	sfmcc	f3, 4, [fp], {14}
     6d4:	00136419 	andseq	r6, r3, r9, lsl r4
     6d8:	00341e00 	eorseq	r1, r4, r0, lsl #28
     6dc:	0b3a1347 	bleq	e85400 <_bss_end+0xe7bd9c>
     6e0:	0b390b3b 	bleq	e433d4 <_bss_end+0xe39d70>
     6e4:	00001802 	andeq	r1, r0, r2, lsl #16
     6e8:	0301131f 	movweq	r1, #4895	; 0x131f
     6ec:	3a0b0b0e 	bcc	2c332c <_bss_end+0x2b9cc8>
     6f0:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     6f4:	0013010b 	andseq	r0, r3, fp, lsl #2
     6f8:	000d2000 	andeq	r2, sp, r0
     6fc:	0b3a0e03 	bleq	e83f10 <_bss_end+0xe7a8ac>
     700:	0b390b3b 	bleq	e433f4 <_bss_end+0xe39d90>
     704:	0b0b1349 	bleq	2c5430 <_bss_end+0x2bbdcc>
     708:	0b0c0b0d 	bleq	303344 <_bss_end+0x2f9ce0>
     70c:	00000b38 	andeq	r0, r0, r8, lsr fp
     710:	03000d21 	movweq	r0, #3361	; 0xd21
     714:	3b0b3a0e 	blcc	2cef54 <_bss_end+0x2c58f0>
     718:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     71c:	0d0b0b13 	vstreq	d0, [fp, #-76]	; 0xffffffb4
     720:	380d0c0b 	stmdacc	sp, {r0, r1, r3, sl, fp}
     724:	2200000b 	andcs	r0, r0, #11
     728:	0e03002e 	cdpeq	0, 0, cr0, cr3, cr14, {1}
     72c:	01111934 	tsteq	r1, r4, lsr r9
     730:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     734:	00194296 	mulseq	r9, r6, r2
     738:	012e2300 			; <UNDEFINED> instruction: 0x012e2300
     73c:	19340e03 	ldmdbne	r4!, {r0, r1, r9, sl, fp}
     740:	06120111 			; <UNDEFINED> instruction: 0x06120111
     744:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     748:	00130119 	andseq	r0, r3, r9, lsl r1
     74c:	00052400 	andeq	r2, r5, r0, lsl #8
     750:	0b3a0e03 	bleq	e83f64 <_bss_end+0xe7a900>
     754:	0b390b3b 	bleq	e43448 <_bss_end+0xe39de4>
     758:	18021349 	stmdane	r2, {r0, r3, r6, r8, r9, ip}
     75c:	2e250000 	cdpcs	0, 2, cr0, cr5, cr0, {0}
     760:	3a134701 	bcc	4d236c <_bss_end+0x4c8d08>
     764:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     768:	1113640b 	tstne	r3, fp, lsl #8
     76c:	40061201 	andmi	r1, r6, r1, lsl #4
     770:	19429618 	stmdbne	r2, {r3, r4, r9, sl, ip, pc}^
     774:	00001301 	andeq	r1, r0, r1, lsl #6
     778:	03000526 	movweq	r0, #1318	; 0x526
     77c:	3413490e 	ldrcc	r4, [r3], #-2318	; 0xfffff6f2
     780:	00180219 	andseq	r0, r8, r9, lsl r2
     784:	00342700 	eorseq	r2, r4, r0, lsl #14
     788:	0b3a0803 	bleq	e8279c <_bss_end+0xe79138>
     78c:	0b390b3b 	bleq	e43480 <_bss_end+0xe39e1c>
     790:	18021349 	stmdane	r2, {r0, r3, r6, r8, r9, ip}
     794:	2e280000 	cdpcs	0, 2, cr0, cr8, cr0, {0}
     798:	3a134701 	bcc	4d23a4 <_bss_end+0x4c8d40>
     79c:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     7a0:	1113640b 	tstne	r3, fp, lsl #8
     7a4:	40061201 	andmi	r1, r6, r1, lsl #4
     7a8:	19429718 	stmdbne	r2, {r3, r4, r8, r9, sl, ip, pc}^
     7ac:	00001301 	andeq	r1, r0, r1, lsl #6
     7b0:	03000529 	movweq	r0, #1321	; 0x529
     7b4:	3b0b3a08 	blcc	2cefdc <_bss_end+0x2c5978>
     7b8:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     7bc:	00180213 	andseq	r0, r8, r3, lsl r2
     7c0:	012e2a00 			; <UNDEFINED> instruction: 0x012e2a00
     7c4:	0b3a1347 	bleq	e854e8 <_bss_end+0xe7be84>
     7c8:	0b390b3b 	bleq	e434bc <_bss_end+0xe39e58>
     7cc:	0b201364 	bleq	805564 <_bss_end+0x7fbf00>
     7d0:	00001301 	andeq	r1, r0, r1, lsl #6
     7d4:	0300052b 	movweq	r0, #1323	; 0x52b
     7d8:	3413490e 	ldrcc	r4, [r3], #-2318	; 0xfffff6f2
     7dc:	2c000019 	stccs	0, cr0, [r0], {25}
     7e0:	0e030005 	cdpeq	0, 0, cr0, cr3, cr5, {0}
     7e4:	0b3b0b3a 	bleq	ec34d4 <_bss_end+0xeb9e70>
     7e8:	13490b39 	movtne	r0, #39737	; 0x9b39
     7ec:	2e2d0000 	cdpcs	0, 2, cr0, cr13, cr0, {0}
     7f0:	6e133101 	mufvss	f3, f3, f1
     7f4:	1113640e 	tstne	r3, lr, lsl #8
     7f8:	40061201 	andmi	r1, r6, r1, lsl #4
     7fc:	19429718 	stmdbne	r2, {r3, r4, r8, r9, sl, ip, pc}^
     800:	052e0000 	streq	r0, [lr, #-0]!
     804:	02133100 	andseq	r3, r3, #0, 2
     808:	00000018 	andeq	r0, r0, r8, lsl r0
     80c:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
     810:	030b130e 	movweq	r1, #45838	; 0xb30e
     814:	110e1b0e 	tstne	lr, lr, lsl #22
     818:	10061201 	andne	r1, r6, r1, lsl #4
     81c:	02000017 	andeq	r0, r0, #23
     820:	0b0b0024 	bleq	2c08b8 <_bss_end+0x2b7254>
     824:	0e030b3e 	vmoveq.16	d3[0], r0
     828:	26030000 	strcs	r0, [r3], -r0
     82c:	00134900 	andseq	r4, r3, r0, lsl #18
     830:	00240400 	eoreq	r0, r4, r0, lsl #8
     834:	0b3e0b0b 	bleq	f83468 <_bss_end+0xf79e04>
     838:	00000803 	andeq	r0, r0, r3, lsl #16
     83c:	03001605 	movweq	r1, #1541	; 0x605
     840:	3b0b3a0e 	blcc	2cf080 <_bss_end+0x2c5a1c>
     844:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     848:	06000013 			; <UNDEFINED> instruction: 0x06000013
     84c:	08030139 	stmdaeq	r3, {r0, r3, r4, r5, r8}
     850:	0b3b0b3a 	bleq	ec3540 <_bss_end+0xeb9edc>
     854:	13010b39 	movwne	r0, #6969	; 0x1b39
     858:	34070000 	strcc	r0, [r7], #-0
     85c:	3a0e0300 	bcc	381464 <_bss_end+0x377e00>
     860:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     864:	3c13490b 			; <UNDEFINED> instruction: 0x3c13490b
     868:	6c061c19 	stcvs	12, cr1, [r6], {25}
     86c:	08000019 	stmdaeq	r0, {r0, r3, r4}
     870:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     874:	0b3b0b3a 	bleq	ec3564 <_bss_end+0xeb9f00>
     878:	13490b39 	movtne	r0, #39737	; 0x9b39
     87c:	0b1c193c 	bleq	706d74 <_bss_end+0x6fd710>
     880:	0000196c 	andeq	r1, r0, ip, ror #18
     884:	03010409 	movweq	r0, #5129	; 0x1409
     888:	3e196d0e 	cdpcc	13, 1, cr6, cr9, cr14, {0}
     88c:	490b0b0b 	stmdbmi	fp, {r0, r1, r3, r8, r9, fp}
     890:	3b0b3a13 	blcc	2cf0e4 <_bss_end+0x2c5a80>
     894:	010b390b 	tsteq	fp, fp, lsl #18
     898:	0a000013 	beq	8ec <CPSR_IRQ_INHIBIT+0x86c>
     89c:	08030028 	stmdaeq	r3, {r3, r5}
     8a0:	00000b1c 	andeq	r0, r0, ip, lsl fp
     8a4:	0300280b 	movweq	r2, #2059	; 0x80b
     8a8:	000b1c0e 	andeq	r1, fp, lr, lsl #24
     8ac:	00340c00 	eorseq	r0, r4, r0, lsl #24
     8b0:	00001347 	andeq	r1, r0, r7, asr #6
     8b4:	0301020d 	movweq	r0, #4621	; 0x120d
     8b8:	3a0b0b0e 	bcc	2c34f8 <_bss_end+0x2b9e94>
     8bc:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     8c0:	0013010b 	andseq	r0, r3, fp, lsl #2
     8c4:	000d0e00 	andeq	r0, sp, r0, lsl #28
     8c8:	0b3a0e03 	bleq	e840dc <_bss_end+0xe7aa78>
     8cc:	0b390b3b 	bleq	e435c0 <_bss_end+0xe39f5c>
     8d0:	0b381349 	bleq	e055fc <_bss_end+0xdfbf98>
     8d4:	2e0f0000 	cdpcs	0, 0, cr0, cr15, cr0, {0}
     8d8:	03193f01 	tsteq	r9, #1, 30
     8dc:	3b0b3a0e 	blcc	2cf11c <_bss_end+0x2c5ab8>
     8e0:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     8e4:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     8e8:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     8ec:	00130113 	andseq	r0, r3, r3, lsl r1
     8f0:	00051000 	andeq	r1, r5, r0
     8f4:	19341349 	ldmdbne	r4!, {r0, r3, r6, r8, r9, ip}
     8f8:	05110000 	ldreq	r0, [r1, #-0]
     8fc:	00134900 	andseq	r4, r3, r0, lsl #18
     900:	012e1200 			; <UNDEFINED> instruction: 0x012e1200
     904:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     908:	0b3b0b3a 	bleq	ec35f8 <_bss_end+0xeb9f94>
     90c:	0e6e0b39 	vmoveq.8	d14[5], r0
     910:	193c0b32 	ldmdbne	ip!, {r1, r4, r5, r8, r9, fp}
     914:	13011364 	movwne	r1, #4964	; 0x1364
     918:	2e130000 	cdpcs	0, 1, cr0, cr3, cr0, {0}
     91c:	03193f01 	tsteq	r9, #1, 30
     920:	3b0b3a0e 	blcc	2cf160 <_bss_end+0x2c5afc>
     924:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     928:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     92c:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     930:	14000013 	strne	r0, [r0], #-19	; 0xffffffed
     934:	0b0b000f 	bleq	2c0978 <_bss_end+0x2b7314>
     938:	00001349 	andeq	r1, r0, r9, asr #6
     93c:	03003415 	movweq	r3, #1045	; 0x415
     940:	3b0b3a0e 	blcc	2cf180 <_bss_end+0x2c5b1c>
     944:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     948:	3c193f13 	ldccc	15, cr3, [r9], {19}
     94c:	16000019 			; <UNDEFINED> instruction: 0x16000019
     950:	0e030028 	cdpeq	0, 0, cr0, cr3, cr8, {1}
     954:	0000051c 	andeq	r0, r0, ip, lsl r5
     958:	03002817 	movweq	r2, #2071	; 0x817
     95c:	00061c0e 	andeq	r1, r6, lr, lsl #24
     960:	012e1800 			; <UNDEFINED> instruction: 0x012e1800
     964:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     968:	0b3b0b3a 	bleq	ec3658 <_bss_end+0xeb9ff4>
     96c:	0e6e0b39 	vmoveq.8	d14[5], r0
     970:	193c0b32 	ldmdbne	ip!, {r1, r4, r5, r8, r9, fp}
     974:	00001364 	andeq	r1, r0, r4, ror #6
     978:	0b001019 	bleq	49e4 <CPSR_IRQ_INHIBIT+0x4964>
     97c:	0013490b 	andseq	r4, r3, fp, lsl #18
     980:	00341a00 	eorseq	r1, r4, r0, lsl #20
     984:	0b3a1347 	bleq	e856a8 <_bss_end+0xe7c044>
     988:	0b390b3b 	bleq	e4367c <_bss_end+0xe3a018>
     98c:	00001802 	andeq	r1, r0, r2, lsl #16
     990:	03002e1b 	movweq	r2, #3611	; 0xe1b
     994:	1119340e 	tstne	r9, lr, lsl #8
     998:	40061201 	andmi	r1, r6, r1, lsl #4
     99c:	19429618 	stmdbne	r2, {r3, r4, r9, sl, ip, pc}^
     9a0:	2e1c0000 	cdpcs	0, 1, cr0, cr12, cr0, {0}
     9a4:	340e0301 	strcc	r0, [lr], #-769	; 0xfffffcff
     9a8:	12011119 	andne	r1, r1, #1073741830	; 0x40000006
     9ac:	96184006 	ldrls	r4, [r8], -r6
     9b0:	13011942 	movwne	r1, #6466	; 0x1942
     9b4:	051d0000 	ldreq	r0, [sp, #-0]
     9b8:	3a0e0300 	bcc	3815c0 <_bss_end+0x377f5c>
     9bc:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     9c0:	0213490b 	andseq	r4, r3, #180224	; 0x2c000
     9c4:	1e000018 	mcrne	0, 0, r0, cr0, cr8, {0}
     9c8:	1347012e 	movtne	r0, #28974	; 0x712e
     9cc:	0b3b0b3a 	bleq	ec36bc <_bss_end+0xeba058>
     9d0:	13640b39 	cmnne	r4, #58368	; 0xe400
     9d4:	06120111 			; <UNDEFINED> instruction: 0x06120111
     9d8:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     9dc:	00130119 	andseq	r0, r3, r9, lsl r1
     9e0:	00051f00 	andeq	r1, r5, r0, lsl #30
     9e4:	13490e03 	movtne	r0, #40451	; 0x9e03
     9e8:	18021934 	stmdane	r2, {r2, r4, r5, r8, fp, ip}
     9ec:	05200000 	streq	r0, [r0, #-0]!
     9f0:	3a080300 	bcc	2015f8 <_bss_end+0x1f7f94>
     9f4:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     9f8:	0213490b 	andseq	r4, r3, #180224	; 0x2c000
     9fc:	21000018 	tstcs	r0, r8, lsl r0
     a00:	08030034 	stmdaeq	r3, {r2, r4, r5}
     a04:	0b3b0b3a 	bleq	ec36f4 <_bss_end+0xeba090>
     a08:	13490b39 	movtne	r0, #39737	; 0x9b39
     a0c:	00001802 	andeq	r1, r0, r2, lsl #16
     a10:	47012e22 	strmi	r2, [r1, -r2, lsr #28]
     a14:	3b0b3a13 	blcc	2cf268 <_bss_end+0x2c5c04>
     a18:	640b390b 	strvs	r3, [fp], #-2315	; 0xfffff6f5
     a1c:	010b2013 	tsteq	fp, r3, lsl r0
     a20:	23000013 	movwcs	r0, #19
     a24:	0e030005 	cdpeq	0, 0, cr0, cr3, cr5, {0}
     a28:	19341349 	ldmdbne	r4!, {r0, r3, r6, r8, r9, ip}
     a2c:	05240000 	streq	r0, [r4, #-0]!
     a30:	3a080300 	bcc	201638 <_bss_end+0x1f7fd4>
     a34:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     a38:	0013490b 	andseq	r4, r3, fp, lsl #18
     a3c:	012e2500 			; <UNDEFINED> instruction: 0x012e2500
     a40:	0e6e1331 	mcreq	3, 3, r1, cr14, cr1, {1}
     a44:	01111364 	tsteq	r1, r4, ror #6
     a48:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     a4c:	00194296 	mulseq	r9, r6, r2
     a50:	00052600 	andeq	r2, r5, r0, lsl #12
     a54:	18021331 	stmdane	r2, {r0, r4, r5, r8, r9, ip}
     a58:	01000000 	mrseq	r0, (UNDEF: 0)
     a5c:	0e250111 	mcreq	1, 1, r0, cr5, cr1, {0}
     a60:	0e030b13 	vmoveq.32	d3[0], r0
     a64:	01110e1b 	tsteq	r1, fp, lsl lr
     a68:	17100612 			; <UNDEFINED> instruction: 0x17100612
     a6c:	24020000 	strcs	r0, [r2], #-0
     a70:	3e0b0b00 	vmlacc.f64	d0, d11, d0
     a74:	000e030b 	andeq	r0, lr, fp, lsl #6
     a78:	00260300 	eoreq	r0, r6, r0, lsl #6
     a7c:	00001349 	andeq	r1, r0, r9, asr #6
     a80:	0b002404 	bleq	9a98 <_bss_end+0x434>
     a84:	030b3e0b 	movweq	r3, #48651	; 0xbe0b
     a88:	05000008 	streq	r0, [r0, #-8]
     a8c:	0e030016 	mcreq	0, 0, r0, cr3, cr6, {0}
     a90:	0b3b0b3a 	bleq	ec3780 <_bss_end+0xeba11c>
     a94:	13490b39 	movtne	r0, #39737	; 0x9b39
     a98:	35060000 	strcc	r0, [r6, #-0]
     a9c:	00134900 	andseq	r4, r3, r0, lsl #18
     aa0:	01390700 	teqeq	r9, r0, lsl #14
     aa4:	0b3a0803 	bleq	e82ab8 <_bss_end+0xe79454>
     aa8:	0b390b3b 	bleq	e4379c <_bss_end+0xe3a138>
     aac:	00001301 	andeq	r1, r0, r1, lsl #6
     ab0:	03003408 	movweq	r3, #1032	; 0x408
     ab4:	3b0b3a0e 	blcc	2cf2f4 <_bss_end+0x2c5c90>
     ab8:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     abc:	1c193c13 	ldcne	12, cr3, [r9], {19}
     ac0:	00196c06 	andseq	r6, r9, r6, lsl #24
     ac4:	00340900 	eorseq	r0, r4, r0, lsl #18
     ac8:	0b3a0e03 	bleq	e842dc <_bss_end+0xe7ac78>
     acc:	0b390b3b 	bleq	e437c0 <_bss_end+0xe3a15c>
     ad0:	193c1349 	ldmdbne	ip!, {r0, r3, r6, r8, r9, ip}
     ad4:	196c0b1c 	stmdbne	ip!, {r2, r3, r4, r8, r9, fp}^
     ad8:	040a0000 	streq	r0, [sl], #-0
     adc:	6d0e0301 	stcvs	3, cr0, [lr, #-4]
     ae0:	0b0b3e19 	bleq	2d034c <_bss_end+0x2c6ce8>
     ae4:	3a13490b 	bcc	4d2f18 <_bss_end+0x4c98b4>
     ae8:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     aec:	0013010b 	andseq	r0, r3, fp, lsl #2
     af0:	00280b00 	eoreq	r0, r8, r0, lsl #22
     af4:	0b1c0803 	bleq	702b08 <_bss_end+0x6f94a4>
     af8:	280c0000 	stmdacs	ip, {}	; <UNPREDICTABLE>
     afc:	1c0e0300 	stcne	3, cr0, [lr], {-0}
     b00:	0d00000b 	stceq	0, cr0, [r0, #-44]	; 0xffffffd4
     b04:	0e030104 	adfeqs	f0, f3, f4
     b08:	0b3e196d 	bleq	f870c4 <_bss_end+0xf7da60>
     b0c:	13490b0b 	movtne	r0, #39691	; 0x9b0b
     b10:	0b3b0b3a 	bleq	ec3800 <_bss_end+0xeba19c>
     b14:	00000b39 	andeq	r0, r0, r9, lsr fp
     b18:	4700340e 	strmi	r3, [r0, -lr, lsl #8]
     b1c:	0f000013 	svceq	0x00000013
     b20:	0e030102 	adfeqs	f0, f3, f2
     b24:	0b3a0b0b 	bleq	e83758 <_bss_end+0xe7a0f4>
     b28:	0b390b3b 	bleq	e4381c <_bss_end+0xe3a1b8>
     b2c:	00001301 	andeq	r1, r0, r1, lsl #6
     b30:	03000d10 	movweq	r0, #3344	; 0xd10
     b34:	3b0b3a0e 	blcc	2cf374 <_bss_end+0x2c5d10>
     b38:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     b3c:	000b3813 	andeq	r3, fp, r3, lsl r8
     b40:	012e1100 			; <UNDEFINED> instruction: 0x012e1100
     b44:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     b48:	0b3b0b3a 	bleq	ec3838 <_bss_end+0xeba1d4>
     b4c:	0e6e0b39 	vmoveq.8	d14[5], r0
     b50:	0b321349 	bleq	c8587c <_bss_end+0xc7c218>
     b54:	1364193c 	cmnne	r4, #60, 18	; 0xf0000
     b58:	00001301 	andeq	r1, r0, r1, lsl #6
     b5c:	49000512 	stmdbmi	r0, {r1, r4, r8, sl}
     b60:	00193413 	andseq	r3, r9, r3, lsl r4
     b64:	00051300 	andeq	r1, r5, r0, lsl #6
     b68:	00001349 	andeq	r1, r0, r9, asr #6
     b6c:	3f012e14 	svccc	0x00012e14
     b70:	3a0e0319 	bcc	3817dc <_bss_end+0x378178>
     b74:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     b78:	320e6e0b 	andcc	r6, lr, #11, 28	; 0xb0
     b7c:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     b80:	00130113 	andseq	r0, r3, r3, lsl r1
     b84:	012e1500 			; <UNDEFINED> instruction: 0x012e1500
     b88:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     b8c:	0b3b0b3a 	bleq	ec387c <_bss_end+0xeba218>
     b90:	0e6e0b39 	vmoveq.8	d14[5], r0
     b94:	0b321349 	bleq	c858c0 <_bss_end+0xc7c25c>
     b98:	1364193c 	cmnne	r4, #60, 18	; 0xf0000
     b9c:	0f160000 	svceq	0x00160000
     ba0:	490b0b00 	stmdbmi	fp, {r8, r9, fp}
     ba4:	17000013 	smladne	r0, r3, r0, r0
     ba8:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     bac:	0b3b0b3a 	bleq	ec389c <_bss_end+0xeba238>
     bb0:	13490b39 	movtne	r0, #39737	; 0x9b39
     bb4:	193c193f 	ldmdbne	ip!, {r0, r1, r2, r3, r4, r5, r8, fp, ip}
     bb8:	28180000 	ldmdacs	r8, {}	; <UNPREDICTABLE>
     bbc:	1c0e0300 	stcne	3, cr0, [lr], {-0}
     bc0:	19000005 	stmdbne	r0, {r0, r2}
     bc4:	0e030028 	cdpeq	0, 0, cr0, cr3, cr8, {1}
     bc8:	0000061c 	andeq	r0, r0, ip, lsl r6
     bcc:	3f012e1a 	svccc	0x00012e1a
     bd0:	3a0e0319 	bcc	38183c <_bss_end+0x3781d8>
     bd4:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     bd8:	320e6e0b 	andcc	r6, lr, #11, 28	; 0xb0
     bdc:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     be0:	1b000013 	blne	c34 <CPSR_IRQ_INHIBIT+0xbb4>
     be4:	0b0b0010 	bleq	2c0c2c <_bss_end+0x2b75c8>
     be8:	00001349 	andeq	r1, r0, r9, asr #6
     bec:	0300161c 	movweq	r1, #1564	; 0x61c
     bf0:	3b0b3a0e 	blcc	2cf430 <_bss_end+0x2c5dcc>
     bf4:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     bf8:	000b3213 	andeq	r3, fp, r3, lsl r2
     bfc:	00151d00 	andseq	r1, r5, r0, lsl #26
     c00:	341e0000 	ldrcc	r0, [lr], #-0
     c04:	3a134700 	bcc	4d280c <_bss_end+0x4c91a8>
     c08:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     c0c:	0018020b 	andseq	r0, r8, fp, lsl #4
     c10:	002e1f00 	eoreq	r1, lr, r0, lsl #30
     c14:	19340e03 	ldmdbne	r4!, {r0, r1, r9, sl, fp}
     c18:	06120111 			; <UNDEFINED> instruction: 0x06120111
     c1c:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     c20:	20000019 	andcs	r0, r0, r9, lsl r0
     c24:	0e03012e 	adfeqsp	f0, f3, #0.5
     c28:	01111934 	tsteq	r1, r4, lsr r9
     c2c:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
     c30:	01194296 			; <UNDEFINED> instruction: 0x01194296
     c34:	21000013 	tstcs	r0, r3, lsl r0
     c38:	0e030005 	cdpeq	0, 0, cr0, cr3, cr5, {0}
     c3c:	0b3b0b3a 	bleq	ec392c <_bss_end+0xeba2c8>
     c40:	13490b39 	movtne	r0, #39737	; 0x9b39
     c44:	00001802 	andeq	r1, r0, r2, lsl #16
     c48:	47012e22 	strmi	r2, [r1, -r2, lsr #28]
     c4c:	3b0b3a13 	blcc	2cf4a0 <_bss_end+0x2c5e3c>
     c50:	640b390b 	strvs	r3, [fp], #-2315	; 0xfffff6f5
     c54:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     c58:	96184006 	ldrls	r4, [r8], -r6
     c5c:	13011942 	movwne	r1, #6466	; 0x1942
     c60:	05230000 	streq	r0, [r3, #-0]!
     c64:	490e0300 	stmdbmi	lr, {r8, r9}
     c68:	02193413 	andseq	r3, r9, #318767104	; 0x13000000
     c6c:	24000018 	strcs	r0, [r0], #-24	; 0xffffffe8
     c70:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     c74:	0b3b0b3a 	bleq	ec3964 <_bss_end+0xeba300>
     c78:	13490b39 	movtne	r0, #39737	; 0x9b39
     c7c:	00001802 	andeq	r1, r0, r2, lsl #16
     c80:	47012e25 	strmi	r2, [r1, -r5, lsr #28]
     c84:	3b0b3a13 	blcc	2cf4d8 <_bss_end+0x2c5e74>
     c88:	640b390b 	strvs	r3, [fp], #-2315	; 0xfffff6f5
     c8c:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     c90:	97184006 	ldrls	r4, [r8, -r6]
     c94:	13011942 	movwne	r1, #6466	; 0x1942
     c98:	05260000 	streq	r0, [r6, #-0]!
     c9c:	3a080300 	bcc	2018a4 <_bss_end+0x1f8240>
     ca0:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     ca4:	0213490b 	andseq	r4, r3, #180224	; 0x2c000
     ca8:	27000018 	smladcs	r0, r8, r0, r0
     cac:	1347012e 	movtne	r0, #28974	; 0x712e
     cb0:	0b3b0b3a 	bleq	ec39a0 <_bss_end+0xeba33c>
     cb4:	13640b39 	cmnne	r4, #58368	; 0xe400
     cb8:	13010b20 	movwne	r0, #6944	; 0x1b20
     cbc:	05280000 	streq	r0, [r8, #-0]!
     cc0:	490e0300 	stmdbmi	lr, {r8, r9}
     cc4:	00193413 	andseq	r3, r9, r3, lsl r4
     cc8:	00052900 	andeq	r2, r5, r0, lsl #18
     ccc:	0b3a0e03 	bleq	e844e0 <_bss_end+0xe7ae7c>
     cd0:	0b390b3b 	bleq	e439c4 <_bss_end+0xe3a360>
     cd4:	00001349 	andeq	r1, r0, r9, asr #6
     cd8:	31012e2a 	tstcc	r1, sl, lsr #28
     cdc:	640e6e13 	strvs	r6, [lr], #-3603	; 0xfffff1ed
     ce0:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     ce4:	97184006 	ldrls	r4, [r8, -r6]
     ce8:	13011942 	movwne	r1, #6466	; 0x1942
     cec:	052b0000 	streq	r0, [fp, #-0]!
     cf0:	02133100 	andseq	r3, r3, #0, 2
     cf4:	2c000018 	stccs	0, cr0, [r0], {24}
     cf8:	193f002e 	ldmdbne	pc!, {r1, r2, r3, r5}	; <UNPREDICTABLE>
     cfc:	0b3a0e03 	bleq	e84510 <_bss_end+0xe7aeac>
     d00:	0b390b3b 	bleq	e439f4 <_bss_end+0xe3a390>
     d04:	06120111 			; <UNDEFINED> instruction: 0x06120111
     d08:	42971840 	addsmi	r1, r7, #64, 16	; 0x400000
     d0c:	2d000019 	stccs	0, cr0, [r0, #-100]	; 0xffffff9c
     d10:	193f002e 	ldmdbne	pc!, {r1, r2, r3, r5}	; <UNPREDICTABLE>
     d14:	0b3a0e03 	bleq	e84528 <_bss_end+0xe7aec4>
     d18:	0b390b3b 	bleq	e43a0c <_bss_end+0xe3a3a8>
     d1c:	06120111 			; <UNDEFINED> instruction: 0x06120111
     d20:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     d24:	00000019 	andeq	r0, r0, r9, lsl r0
     d28:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
     d2c:	030b130e 	movweq	r1, #45838	; 0xb30e
     d30:	110e1b0e 	tstne	lr, lr, lsl #22
     d34:	10061201 	andne	r1, r6, r1, lsl #4
     d38:	02000017 	andeq	r0, r0, #23
     d3c:	0b0b0024 	bleq	2c0dd4 <_bss_end+0x2b7770>
     d40:	0e030b3e 	vmoveq.16	d3[0], r0
     d44:	26030000 	strcs	r0, [r3], -r0
     d48:	00134900 	andseq	r4, r3, r0, lsl #18
     d4c:	00240400 	eoreq	r0, r4, r0, lsl #8
     d50:	0b3e0b0b 	bleq	f83984 <_bss_end+0xf7a320>
     d54:	00000803 	andeq	r0, r0, r3, lsl #16
     d58:	49003505 	stmdbmi	r0, {r0, r2, r8, sl, ip, sp}
     d5c:	06000013 			; <UNDEFINED> instruction: 0x06000013
     d60:	0e030016 	mcreq	0, 0, r0, cr3, cr6, {0}
     d64:	0b3b0b3a 	bleq	ec3a54 <_bss_end+0xeba3f0>
     d68:	13490b39 	movtne	r0, #39737	; 0x9b39
     d6c:	04070000 	streq	r0, [r7], #-0
     d70:	6d0e0301 	stcvs	3, cr0, [lr, #-4]
     d74:	0b0b3e19 	bleq	2d05e0 <_bss_end+0x2c6f7c>
     d78:	3a13490b 	bcc	4d31ac <_bss_end+0x4c9b48>
     d7c:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     d80:	0013010b 	andseq	r0, r3, fp, lsl #2
     d84:	00280800 	eoreq	r0, r8, r0, lsl #16
     d88:	0b1c0e03 	bleq	70459c <_bss_end+0x6faf38>
     d8c:	02090000 	andeq	r0, r9, #0
     d90:	0b0e0301 	bleq	38199c <_bss_end+0x378338>
     d94:	3b0b3a0b 	blcc	2cf5c8 <_bss_end+0x2c5f64>
     d98:	010b390b 	tsteq	fp, fp, lsl #18
     d9c:	0a000013 	beq	df0 <CPSR_IRQ_INHIBIT+0xd70>
     da0:	0e03000d 	cdpeq	0, 0, cr0, cr3, cr13, {0}
     da4:	0b3b0b3a 	bleq	ec3a94 <_bss_end+0xeba430>
     da8:	13490b39 	movtne	r0, #39737	; 0x9b39
     dac:	00000b38 	andeq	r0, r0, r8, lsr fp
     db0:	3f012e0b 	svccc	0x00012e0b
     db4:	3a0e0319 	bcc	381a20 <_bss_end+0x3783bc>
     db8:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     dbc:	490e6e0b 	stmdbmi	lr, {r0, r1, r3, r9, sl, fp, sp, lr}
     dc0:	3c0b3213 	sfmcc	f3, 4, [fp], {19}
     dc4:	01136419 	tsteq	r3, r9, lsl r4
     dc8:	0c000013 	stceq	0, cr0, [r0], {19}
     dcc:	13490005 	movtne	r0, #36869	; 0x9005
     dd0:	00001934 	andeq	r1, r0, r4, lsr r9
     dd4:	4900050d 	stmdbmi	r0, {r0, r2, r3, r8, sl}
     dd8:	0e000013 	mcreq	0, 0, r0, cr0, cr3, {0}
     ddc:	193f012e 	ldmdbne	pc!, {r1, r2, r3, r5, r8}	; <UNPREDICTABLE>
     de0:	0b3a0e03 	bleq	e845f4 <_bss_end+0xe7af90>
     de4:	0b390b3b 	bleq	e43ad8 <_bss_end+0xe3a474>
     de8:	0b320e6e 	bleq	c847a8 <_bss_end+0xc7b144>
     dec:	1364193c 	cmnne	r4, #60, 18	; 0xf0000
     df0:	00001301 	andeq	r1, r0, r1, lsl #6
     df4:	3f012e0f 	svccc	0x00012e0f
     df8:	3a0e0319 	bcc	381a64 <_bss_end+0x378400>
     dfc:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     e00:	320e6e0b 	andcc	r6, lr, #11, 28	; 0xb0
     e04:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     e08:	10000013 	andne	r0, r0, r3, lsl r0
     e0c:	0b0b000f 	bleq	2c0e50 <_bss_end+0x2b77ec>
     e10:	00001349 	andeq	r1, r0, r9, asr #6
     e14:	0b001011 	bleq	4e60 <CPSR_IRQ_INHIBIT+0x4de0>
     e18:	0013490b 	andseq	r4, r3, fp, lsl #18
     e1c:	00341200 	eorseq	r1, r4, r0, lsl #4
     e20:	0b3a0e03 	bleq	e84634 <_bss_end+0xe7afd0>
     e24:	0b390b3b 	bleq	e43b18 <_bss_end+0xe3a4b4>
     e28:	193f1349 	ldmdbne	pc!, {r0, r3, r6, r8, r9, ip}	; <UNPREDICTABLE>
     e2c:	0000193c 	andeq	r1, r0, ip, lsr r9
     e30:	03013913 	movweq	r3, #6419	; 0x1913
     e34:	3b0b3a08 	blcc	2cf65c <_bss_end+0x2c5ff8>
     e38:	010b390b 	tsteq	fp, fp, lsl #18
     e3c:	14000013 	strne	r0, [r0], #-19	; 0xffffffed
     e40:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     e44:	0b3b0b3a 	bleq	ec3b34 <_bss_end+0xeba4d0>
     e48:	13490b39 	movtne	r0, #39737	; 0x9b39
     e4c:	061c193c 			; <UNDEFINED> instruction: 0x061c193c
     e50:	0000196c 	andeq	r1, r0, ip, ror #18
     e54:	03003415 	movweq	r3, #1045	; 0x415
     e58:	3b0b3a0e 	blcc	2cf698 <_bss_end+0x2c6034>
     e5c:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     e60:	1c193c13 	ldcne	12, cr3, [r9], {19}
     e64:	00196c0b 	andseq	r6, r9, fp, lsl #24
     e68:	00281600 	eoreq	r1, r8, r0, lsl #12
     e6c:	0b1c0803 	bleq	702e80 <_bss_end+0x6f981c>
     e70:	04170000 	ldreq	r0, [r7], #-0
     e74:	6d0e0301 	stcvs	3, cr0, [lr, #-4]
     e78:	0b0b3e19 	bleq	2d06e4 <_bss_end+0x2c7080>
     e7c:	3a13490b 	bcc	4d32b0 <_bss_end+0x4c9c4c>
     e80:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     e84:	1800000b 	stmdane	r0, {r0, r1, r3}
     e88:	13470034 	movtne	r0, #28724	; 0x7034
     e8c:	2e190000 	cdpcs	0, 1, cr0, cr9, cr0, {0}
     e90:	03193f01 	tsteq	r9, #1, 30
     e94:	3b0b3a0e 	blcc	2cf6d4 <_bss_end+0x2c6070>
     e98:	6e0b390b 	vmlavs.f16	s6, s22, s22	; <UNPREDICTABLE>
     e9c:	3213490e 	andscc	r4, r3, #229376	; 0x38000
     ea0:	64193c0b 	ldrvs	r3, [r9], #-3083	; 0xfffff3f5
     ea4:	1a000013 	bne	ef8 <CPSR_IRQ_INHIBIT+0xe78>
     ea8:	0e030028 	cdpeq	0, 0, cr0, cr3, cr8, {1}
     eac:	0000051c 	andeq	r0, r0, ip, lsl r5
     eb0:	0300281b 	movweq	r2, #2075	; 0x81b
     eb4:	00061c0e 	andeq	r1, r6, lr, lsl #24
     eb8:	00161c00 	andseq	r1, r6, r0, lsl #24
     ebc:	0b3a0e03 	bleq	e846d0 <_bss_end+0xe7b06c>
     ec0:	0b390b3b 	bleq	e43bb4 <_bss_end+0xe3a550>
     ec4:	0b321349 	bleq	c85bf0 <_bss_end+0xc7c58c>
     ec8:	151d0000 	ldrne	r0, [sp, #-0]
     ecc:	1e000000 	cdpne	0, 0, cr0, cr0, cr0, {0}
     ed0:	0e030034 	mcreq	0, 0, r0, cr3, cr4, {1}
     ed4:	0b3b0b3a 	bleq	ec3bc4 <_bss_end+0xeba560>
     ed8:	13490b39 	movtne	r0, #39737	; 0x9b39
     edc:	1802196c 	stmdane	r2, {r2, r3, r5, r6, r8, fp, ip}
     ee0:	341f0000 	ldrcc	r0, [pc], #-0	; ee8 <CPSR_IRQ_INHIBIT+0xe68>
     ee4:	3a0e0300 	bcc	381aec <_bss_end+0x378488>
     ee8:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     eec:	3f13490b 	svccc	0x0013490b
     ef0:	00180219 	andseq	r0, r8, r9, lsl r2
     ef4:	002e2000 	eoreq	r2, lr, r0
     ef8:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     efc:	0b3b0b3a 	bleq	ec3bec <_bss_end+0xeba588>
     f00:	13490b39 	movtne	r0, #39737	; 0x9b39
     f04:	06120111 			; <UNDEFINED> instruction: 0x06120111
     f08:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     f0c:	21000019 	tstcs	r0, r9, lsl r0
     f10:	193f002e 	ldmdbne	pc!, {r1, r2, r3, r5}	; <UNPREDICTABLE>
     f14:	0b3a0e03 	bleq	e84728 <_bss_end+0xe7b0c4>
     f18:	0b390b3b 	bleq	e43c0c <_bss_end+0xe3a5a8>
     f1c:	06120111 			; <UNDEFINED> instruction: 0x06120111
     f20:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     f24:	00000019 	andeq	r0, r0, r9, lsl r0
     f28:	10001101 	andne	r1, r0, r1, lsl #2
     f2c:	03065506 	movweq	r5, #25862	; 0x6506
     f30:	250e1b0e 	strcs	r1, [lr, #-2830]	; 0xfffff4f2
     f34:	0005130e 	andeq	r1, r5, lr, lsl #6
     f38:	11010000 	mrsne	r0, (UNDEF: 1)
     f3c:	130e2501 	movwne	r2, #58625	; 0xe501
     f40:	1b0e030b 	blne	381b74 <_bss_end+0x378510>
     f44:	1201110e 	andne	r1, r1, #-2147483645	; 0x80000003
     f48:	00171006 	andseq	r1, r7, r6
     f4c:	00160200 	andseq	r0, r6, r0, lsl #4
     f50:	0b3a0e03 	bleq	e84764 <_bss_end+0xe7b100>
     f54:	0b390b3b 	bleq	e43c48 <_bss_end+0xe3a5e4>
     f58:	00001349 	andeq	r1, r0, r9, asr #6
     f5c:	0b000f03 	bleq	4b70 <CPSR_IRQ_INHIBIT+0x4af0>
     f60:	0013490b 	andseq	r4, r3, fp, lsl #18
     f64:	00150400 	andseq	r0, r5, r0, lsl #8
     f68:	34050000 	strcc	r0, [r5], #-0
     f6c:	3a0e0300 	bcc	381b74 <_bss_end+0x378510>
     f70:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     f74:	3f13490b 	svccc	0x0013490b
     f78:	00193c19 	andseq	r3, r9, r9, lsl ip
     f7c:	00240600 	eoreq	r0, r4, r0, lsl #12
     f80:	0b3e0b0b 	bleq	f83bb4 <_bss_end+0xf7a550>
     f84:	00000803 	andeq	r0, r0, r3, lsl #16
     f88:	49010107 	stmdbmi	r1, {r0, r1, r2, r8}
     f8c:	00130113 	andseq	r0, r3, r3, lsl r1
     f90:	00210800 	eoreq	r0, r1, r0, lsl #16
     f94:	062f1349 	strteq	r1, [pc], -r9, asr #6
     f98:	24090000 	strcs	r0, [r9], #-0
     f9c:	3e0b0b00 	vmlacc.f64	d0, d11, d0
     fa0:	000e030b 	andeq	r0, lr, fp, lsl #6
     fa4:	012e0a00 			; <UNDEFINED> instruction: 0x012e0a00
     fa8:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
     fac:	0b3b0b3a 	bleq	ec3c9c <_bss_end+0xeba638>
     fb0:	13490b39 	movtne	r0, #39737	; 0x9b39
     fb4:	06120111 			; <UNDEFINED> instruction: 0x06120111
     fb8:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
     fbc:	00130119 	andseq	r0, r3, r9, lsl r1
     fc0:	00340b00 	eorseq	r0, r4, r0, lsl #22
     fc4:	0b3a0e03 	bleq	e847d8 <_bss_end+0xe7b174>
     fc8:	0b390b3b 	bleq	e43cbc <_bss_end+0xe3a658>
     fcc:	18021349 	stmdane	r2, {r0, r3, r6, r8, r9, ip}
     fd0:	2e0c0000 	cdpcs	0, 0, cr0, cr12, cr0, {0}
     fd4:	03193f01 	tsteq	r9, #1, 30
     fd8:	3b0b3a0e 	blcc	2cf818 <_bss_end+0x2c61b4>
     fdc:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
     fe0:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
     fe4:	97184006 	ldrls	r4, [r8, -r6]
     fe8:	13011942 	movwne	r1, #6466	; 0x1942
     fec:	340d0000 	strcc	r0, [sp], #-0
     ff0:	3a080300 	bcc	201bf8 <_bss_end+0x1f8594>
     ff4:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
     ff8:	0213490b 	andseq	r4, r3, #180224	; 0x2c000
     ffc:	00000018 	andeq	r0, r0, r8, lsl r0
    1000:	10001101 	andne	r1, r0, r1, lsl #2
    1004:	12011106 	andne	r1, r1, #-2147483647	; 0x80000001
    1008:	1b0e0301 	blne	381c14 <_bss_end+0x3785b0>
    100c:	130e250e 	movwne	r2, #58638	; 0xe50e
    1010:	00000005 	andeq	r0, r0, r5
    1014:	10001101 	andne	r1, r0, r1, lsl #2
    1018:	12011106 	andne	r1, r1, #-2147483647	; 0x80000001
    101c:	1b0e0301 	blne	381c28 <_bss_end+0x3785c4>
    1020:	130e250e 	movwne	r2, #58638	; 0xe50e
    1024:	00000005 	andeq	r0, r0, r5
    1028:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
    102c:	030b130e 	movweq	r1, #45838	; 0xb30e
    1030:	100e1b0e 	andne	r1, lr, lr, lsl #22
    1034:	02000017 	andeq	r0, r0, #23
    1038:	0b0b0024 	bleq	2c10d0 <_bss_end+0x2b7a6c>
    103c:	08030b3e 	stmdaeq	r3, {r1, r2, r3, r4, r5, r8, r9, fp}
    1040:	24030000 	strcs	r0, [r3], #-0
    1044:	3e0b0b00 	vmlacc.f64	d0, d11, d0
    1048:	000e030b 	andeq	r0, lr, fp, lsl #6
    104c:	00160400 	andseq	r0, r6, r0, lsl #8
    1050:	0b3a0e03 	bleq	e84864 <_bss_end+0xe7b200>
    1054:	0b390b3b 	bleq	e43d48 <_bss_end+0xe3a6e4>
    1058:	00001349 	andeq	r1, r0, r9, asr #6
    105c:	0b000f05 	bleq	4c78 <CPSR_IRQ_INHIBIT+0x4bf8>
    1060:	0013490b 	andseq	r4, r3, fp, lsl #18
    1064:	01150600 	tsteq	r5, r0, lsl #12
    1068:	13491927 	movtne	r1, #39207	; 0x9927
    106c:	00001301 	andeq	r1, r0, r1, lsl #6
    1070:	49000507 	stmdbmi	r0, {r0, r1, r2, r8, sl}
    1074:	08000013 	stmdaeq	r0, {r0, r1, r4}
    1078:	00000026 	andeq	r0, r0, r6, lsr #32
    107c:	03003409 	movweq	r3, #1033	; 0x409
    1080:	3b0b3a0e 	blcc	2cf8c0 <_bss_end+0x2c625c>
    1084:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
    1088:	3c193f13 	ldccc	15, cr3, [r9], {19}
    108c:	0a000019 	beq	10f8 <CPSR_IRQ_INHIBIT+0x1078>
    1090:	0e030104 	adfeqs	f0, f3, f4
    1094:	0b0b0b3e 	bleq	2c3d94 <_bss_end+0x2ba730>
    1098:	0b3a1349 	bleq	e85dc4 <_bss_end+0xe7c760>
    109c:	0b390b3b 	bleq	e43d90 <_bss_end+0xe3a72c>
    10a0:	00001301 	andeq	r1, r0, r1, lsl #6
    10a4:	0300280b 	movweq	r2, #2059	; 0x80b
    10a8:	000b1c0e 	andeq	r1, fp, lr, lsl #24
    10ac:	01010c00 	tsteq	r1, r0, lsl #24
    10b0:	13011349 	movwne	r1, #4937	; 0x1349
    10b4:	210d0000 	mrscs	r0, (UNDEF: 13)
    10b8:	0e000000 	cdpeq	0, 0, cr0, cr0, cr0, {0}
    10bc:	13490026 	movtne	r0, #36902	; 0x9026
    10c0:	340f0000 	strcc	r0, [pc], #-0	; 10c8 <CPSR_IRQ_INHIBIT+0x1048>
    10c4:	3a0e0300 	bcc	381ccc <_bss_end+0x378668>
    10c8:	39053b0b 	stmdbcc	r5, {r0, r1, r3, r8, r9, fp, ip, sp}
    10cc:	3f13490b 	svccc	0x0013490b
    10d0:	00193c19 	andseq	r3, r9, r9, lsl ip
    10d4:	00131000 	andseq	r1, r3, r0
    10d8:	193c0e03 	ldmdbne	ip!, {r0, r1, r9, sl, fp}
    10dc:	15110000 	ldrne	r0, [r1, #-0]
    10e0:	00192700 	andseq	r2, r9, r0, lsl #14
    10e4:	00171200 	andseq	r1, r7, r0, lsl #4
    10e8:	193c0e03 	ldmdbne	ip!, {r0, r1, r9, sl, fp}
    10ec:	13130000 	tstne	r3, #0
    10f0:	0b0e0301 	bleq	381cfc <_bss_end+0x378698>
    10f4:	3b0b3a0b 	blcc	2cf928 <_bss_end+0x2c62c4>
    10f8:	010b3905 	tsteq	fp, r5, lsl #18
    10fc:	14000013 	strne	r0, [r0], #-19	; 0xffffffed
    1100:	0e03000d 	cdpeq	0, 0, cr0, cr3, cr13, {0}
    1104:	053b0b3a 	ldreq	r0, [fp, #-2874]!	; 0xfffff4c6
    1108:	13490b39 	movtne	r0, #39737	; 0x9b39
    110c:	00000b38 	andeq	r0, r0, r8, lsr fp
    1110:	49002115 	stmdbmi	r0, {r0, r2, r4, r8, sp}
    1114:	000b2f13 	andeq	r2, fp, r3, lsl pc
    1118:	01041600 	tsteq	r4, r0, lsl #12
    111c:	0b3e0e03 	bleq	f84930 <_bss_end+0xf7b2cc>
    1120:	13490b0b 	movtne	r0, #39691	; 0x9b0b
    1124:	053b0b3a 	ldreq	r0, [fp, #-2874]!	; 0xfffff4c6
    1128:	13010b39 	movwne	r0, #6969	; 0x1b39
    112c:	34170000 	ldrcc	r0, [r7], #-0
    1130:	3a134700 	bcc	4d2d38 <_bss_end+0x4c96d4>
    1134:	39053b0b 	stmdbcc	r5, {r0, r1, r3, r8, r9, fp, ip, sp}
    1138:	0018020b 	andseq	r0, r8, fp, lsl #4
	...

Disassembly of section .debug_aranges:

00000000 <.debug_aranges>:
   0:	0000001c 	andeq	r0, r0, ip, lsl r0
   4:	00000002 	andeq	r0, r0, r2
   8:	00040000 	andeq	r0, r4, r0
   c:	00000000 	andeq	r0, r0, r0
  10:	00008094 	muleq	r0, r4, r0
  14:	000000d8 	ldrdeq	r0, [r0], -r8
	...
  20:	0000001c 	andeq	r0, r0, ip, lsl r0
  24:	012a0002 			; <UNDEFINED> instruction: 0x012a0002
  28:	00040000 	andeq	r0, r4, r0
  2c:	00000000 	andeq	r0, r0, r0
  30:	0000816c 	andeq	r8, r0, ip, ror #2
  34:	000001c8 	andeq	r0, r0, r8, asr #3
	...
  40:	0000001c 	andeq	r0, r0, ip, lsl r0
  44:	05650002 	strbeq	r0, [r5, #-2]!
  48:	00040000 	andeq	r0, r4, r0
  4c:	00000000 	andeq	r0, r0, r0
  50:	00008334 	andeq	r8, r0, r4, lsr r3
  54:	00000500 	andeq	r0, r0, r0, lsl #10
	...
  60:	0000001c 	andeq	r0, r0, ip, lsl r0
  64:	0c320002 	ldceq	0, cr0, [r2], #-8
  68:	00040000 	andeq	r0, r4, r0
  6c:	00000000 	andeq	r0, r0, r0
  70:	00008834 	andeq	r8, r0, r4, lsr r8
  74:	00000290 	muleq	r0, r0, r2
	...
  80:	0000001c 	andeq	r0, r0, ip, lsl r0
  84:	14720002 	ldrbtne	r0, [r2], #-2
  88:	00040000 	andeq	r0, r4, r0
  8c:	00000000 	andeq	r0, r0, r0
  90:	00008ac4 	andeq	r8, r0, r4, asr #21
  94:	000002d8 	ldrdeq	r0, [r0], -r8
	...
  a0:	0000001c 	andeq	r0, r0, ip, lsl r0
  a4:	1a030002 	bne	c00b4 <_bss_end+0xb6a50>
  a8:	00040000 	andeq	r0, r4, r0
  ac:	00000000 	andeq	r0, r0, r0
  b0:	00008d9c 	muleq	r0, ip, sp
  b4:	00000264 	andeq	r0, r0, r4, ror #4
	...
  c0:	0000001c 	andeq	r0, r0, ip, lsl r0
  c4:	238b0002 	orrcs	r0, fp, #2
  c8:	00040000 	andeq	r0, r4, r0
  cc:	00000000 	andeq	r0, r0, r0
  d0:	00009000 	andeq	r9, r0, r0
  d4:	00000100 	andeq	r0, r0, r0, lsl #2
	...
  e0:	00000024 	andeq	r0, r0, r4, lsr #32
  e4:	2d280002 	stccs	0, cr0, [r8, #-8]!
  e8:	00040000 	andeq	r0, r4, r0
  ec:	00000000 	andeq	r0, r0, r0
  f0:	00008000 	andeq	r8, r0, r0
  f4:	00000094 	muleq	r0, r4, r0
  f8:	00009100 	andeq	r9, r0, r0, lsl #2
  fc:	00000020 	andeq	r0, r0, r0, lsr #32
	...
 108:	0000001c 	andeq	r0, r0, ip, lsl r0
 10c:	2d4a0002 	stclcs	0, cr0, [sl, #-8]
 110:	00040000 	andeq	r0, r4, r0
 114:	00000000 	andeq	r0, r0, r0
 118:	00009120 	andeq	r9, r0, r0, lsr #2
 11c:	00000118 	andeq	r0, r0, r8, lsl r1
	...
 128:	0000001c 	andeq	r0, r0, ip, lsl r0
 12c:	2e990002 	cdpcs	0, 9, cr0, cr9, cr2, {0}
 130:	00040000 	andeq	r0, r4, r0
 134:	00000000 	andeq	r0, r0, r0
 138:	00009238 	andeq	r9, r0, r8, lsr r2
 13c:	0000020c 	andeq	r0, r0, ip, lsl #4
	...
 148:	0000001c 	andeq	r0, r0, ip, lsl r0
 14c:	2ebf0002 	cdpcs	0, 11, cr0, cr15, cr2, {0}
 150:	00040000 	andeq	r0, r4, r0
 154:	00000000 	andeq	r0, r0, r0
 158:	00009444 	andeq	r9, r0, r4, asr #8
 15c:	00000004 	andeq	r0, r0, r4
	...
 168:	00000014 	andeq	r0, r0, r4, lsl r0
 16c:	2ee50002 	cdpcs	0, 14, cr0, cr5, cr2, {0}
 170:	00040000 	andeq	r0, r4, r0
	...

Disassembly of section .debug_line:

00000000 <.debug_line>:
   0:	00000090 	muleq	r0, r0, r0
   4:	00570003 	subseq	r0, r7, r3
   8:	01020000 	mrseq	r0, (UNDEF: 2)
   c:	000d0efb 	strdeq	r0, [sp], -fp
  10:	01010101 	tsteq	r1, r1, lsl #2
  14:	01000000 	mrseq	r0, (UNDEF: 0)
  18:	2f010000 	svccs	0x00010000
  1c:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
  20:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
  24:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
  28:	6b532f61 	blvs	14cbdb4 <_bss_end+0x14c2750>
  2c:	2f616c6f 	svccs	0x00616c6f
  30:	6f706572 	svcvs	0x00706572
  34:	736f2f73 	cmnvc	pc, #460	; 0x1cc
  38:	6b2f352d 	blvs	bcd4f4 <_bss_end+0xbc3e90>
  3c:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
  40:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
  44:	63000063 	movwvs	r0, #99	; 0x63
  48:	632e7878 			; <UNDEFINED> instruction: 0x632e7878
  4c:	01007070 	tsteq	r0, r0, ror r0
  50:	623c0000 	eorsvs	r0, ip, #0
  54:	746c6975 	strbtvc	r6, [ip], #-2421	; 0xfffff68b
  58:	3e6e692d 	vmulcc.f16	s13, s28, s27	; <UNPREDICTABLE>
  5c:	00000000 	andeq	r0, r0, r0
  60:	00020500 	andeq	r0, r2, r0, lsl #10
  64:	80940205 	addshi	r0, r4, r5, lsl #4
  68:	0a030000 	beq	c0070 <_bss_end+0xb6a0c>
  6c:	830b0501 	movwhi	r0, #46337	; 0xb501
  70:	054a0a05 	strbeq	r0, [sl, #-2565]	; 0xfffff5fb
  74:	05858302 	streq	r8, [r5, #770]	; 0x302
  78:	0205830e 	andeq	r8, r5, #939524096	; 0x38000000
  7c:	05848567 	streq	r8, [r4, #1383]	; 0x567
  80:	854c8601 	strbhi	r8, [ip, #-1537]	; 0xfffff9ff
  84:	854c854c 	strbhi	r8, [ip, #-1356]	; 0xfffffab4
  88:	02000205 	andeq	r0, r0, #1342177280	; 0x50000000
  8c:	024b0104 	subeq	r0, fp, #4, 2
  90:	01010002 	tsteq	r1, r2
  94:	00000164 	andeq	r0, r0, r4, ror #2
  98:	00f40003 	rscseq	r0, r4, r3
  9c:	01020000 	mrseq	r0, (UNDEF: 2)
  a0:	000d0efb 	strdeq	r0, [sp], -fp
  a4:	01010101 	tsteq	r1, r1, lsl #2
  a8:	01000000 	mrseq	r0, (UNDEF: 0)
  ac:	2f010000 	svccs	0x00010000
  b0:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
  b4:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
  b8:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
  bc:	6b532f61 	blvs	14cbe48 <_bss_end+0x14c27e4>
  c0:	2f616c6f 	svccs	0x00616c6f
  c4:	6f706572 	svcvs	0x00706572
  c8:	736f2f73 	cmnvc	pc, #460	; 0x1cc
  cc:	6b2f352d 	blvs	bcd588 <_bss_end+0xbc3f24>
  d0:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
  d4:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
  d8:	72642f63 	rsbvc	r2, r4, #396	; 0x18c
  dc:	72657669 	rsbvc	r7, r5, #110100480	; 0x6900000
  e0:	682f0073 	stmdavs	pc!, {r0, r1, r4, r5, r6}	; <UNPREDICTABLE>
  e4:	2f656d6f 	svccs	0x00656d6f
  e8:	6b72616d 	blvs	1c986a4 <_bss_end+0x1c8f040>
  ec:	6164766f 	cmnvs	r4, pc, ror #12
  f0:	6f6b532f 	svcvs	0x006b532f
  f4:	722f616c 	eorvc	r6, pc, #108, 2
  f8:	736f7065 	cmnvc	pc, #101	; 0x65
  fc:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 100:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 104:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 108:	636e692f 	cmnvs	lr, #770048	; 0xbc000
 10c:	6564756c 	strbvs	r7, [r4, #-1388]!	; 0xfffffa94
 110:	616f622f 	cmnvs	pc, pc, lsr #4
 114:	722f6472 	eorvc	r6, pc, #1912602624	; 0x72000000
 118:	2f306970 	svccs	0x00306970
 11c:	006c6168 	rsbeq	r6, ip, r8, ror #2
 120:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; 6c <CPSR_FIQ_INHIBIT+0x2c>
 124:	616d2f65 	cmnvs	sp, r5, ror #30
 128:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
 12c:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
 130:	616c6f6b 	cmnvs	ip, fp, ror #30
 134:	7065722f 	rsbvc	r7, r5, pc, lsr #4
 138:	6f2f736f 	svcvs	0x002f736f
 13c:	2f352d73 	svccs	0x00352d73
 140:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
 144:	692f6c65 	stmdbvs	pc!, {r0, r2, r5, r6, sl, fp, sp, lr}	; <UNPREDICTABLE>
 148:	756c636e 	strbvc	r6, [ip, #-878]!	; 0xfffffc92
 14c:	642f6564 	strtvs	r6, [pc], #-1380	; 154 <CPSR_IRQ_INHIBIT+0xd4>
 150:	65766972 	ldrbvs	r6, [r6, #-2418]!	; 0xfffff68e
 154:	00007372 	andeq	r7, r0, r2, ror r3
 158:	5f6d6362 	svcpl	0x006d6362
 15c:	2e787561 	cdpcs	5, 7, cr7, cr8, cr1, {3}
 160:	00707063 	rsbseq	r7, r0, r3, rrx
 164:	70000001 	andvc	r0, r0, r1
 168:	70697265 	rsbvc	r7, r9, r5, ror #4
 16c:	61726568 	cmnvs	r2, r8, ror #10
 170:	682e736c 	stmdavs	lr!, {r2, r3, r5, r6, r8, r9, ip, sp, lr}
 174:	00000200 	andeq	r0, r0, r0, lsl #4
 178:	5f6d6362 	svcpl	0x006d6362
 17c:	2e787561 	cdpcs	5, 7, cr7, cr8, cr1, {3}
 180:	00030068 	andeq	r0, r3, r8, rrx
 184:	746e6900 	strbtvc	r6, [lr], #-2304	; 0xfffff700
 188:	2e666564 	cdpcs	5, 6, cr6, cr6, cr4, {3}
 18c:	00020068 	andeq	r0, r2, r8, rrx
 190:	01050000 	mrseq	r0, (UNDEF: 5)
 194:	6c020500 	cfstr32vs	mvfx0, [r2], {-0}
 198:	16000081 	strne	r0, [r0], -r1, lsl #1
 19c:	059f3905 	ldreq	r3, [pc, #2309]	; aa9 <CPSR_IRQ_INHIBIT+0xa29>
 1a0:	05a16901 	streq	r6, [r1, #2305]!	; 0x901
 1a4:	55059f35 	strpl	r9, [r5, #-3893]	; 0xfffff0cb
 1a8:	2e520582 	cdpcs	5, 5, cr0, cr2, cr2, {4}
 1ac:	054a1105 	strbeq	r1, [sl, #-261]	; 0xfffffefb
 1b0:	05699f01 	strbeq	r9, [r9, #-3841]!	; 0xfffff0ff
 1b4:	56059f35 			; <UNDEFINED> instruction: 0x56059f35
 1b8:	2e530582 	cdpcs	5, 5, cr0, cr3, cr2, {4}
 1bc:	054a4f05 	strbeq	r4, [sl, #-3845]	; 0xfffff0fb
 1c0:	01052e11 	tsteq	r5, r1, lsl lr
 1c4:	0505699f 	streq	r6, [r5, #-2463]	; 0xfffff661
 1c8:	4a0e05bb 	bmi	3818bc <_bss_end+0x378258>
 1cc:	052e3005 	streq	r3, [lr, #-5]!
 1d0:	01054a32 	tsteq	r5, r2, lsr sl
 1d4:	0c05854b 	cfstr32eq	mvfx8, [r5], {75}	; 0x4b
 1d8:	4a15059f 	bmi	54185c <_bss_end+0x5381f8>
 1dc:	052e3705 	streq	r3, [lr, #-1797]!	; 0xfffff8fb
 1e0:	9e826701 	cdpls	7, 8, cr6, cr2, cr1, {0}
 1e4:	01040200 	mrseq	r0, R12_usr
 1e8:	18056606 	stmdane	r5, {r1, r2, r9, sl, sp, lr}
 1ec:	82660306 	rsbhi	r0, r6, #402653184	; 0x18000000
 1f0:	1a030105 	bne	c060c <_bss_end+0xb6fa8>
 1f4:	024aba66 	subeq	fp, sl, #417792	; 0x66000
 1f8:	0101000a 	tsteq	r1, sl
 1fc:	000002a0 	andeq	r0, r0, r0, lsr #5
 200:	00ee0003 	rsceq	r0, lr, r3
 204:	01020000 	mrseq	r0, (UNDEF: 2)
 208:	000d0efb 	strdeq	r0, [sp], -fp
 20c:	01010101 	tsteq	r1, r1, lsl #2
 210:	01000000 	mrseq	r0, (UNDEF: 0)
 214:	2f010000 	svccs	0x00010000
 218:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
 21c:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
 220:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
 224:	6b532f61 	blvs	14cbfb0 <_bss_end+0x14c294c>
 228:	2f616c6f 	svccs	0x00616c6f
 22c:	6f706572 	svcvs	0x00706572
 230:	736f2f73 	cmnvc	pc, #460	; 0x1cc
 234:	6b2f352d 	blvs	bcd6f0 <_bss_end+0xbc408c>
 238:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
 23c:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
 240:	72642f63 	rsbvc	r2, r4, #396	; 0x18c
 244:	72657669 	rsbvc	r7, r5, #110100480	; 0x6900000
 248:	682f0073 	stmdavs	pc!, {r0, r1, r4, r5, r6}	; <UNPREDICTABLE>
 24c:	2f656d6f 	svccs	0x00656d6f
 250:	6b72616d 	blvs	1c9880c <_bss_end+0x1c8f1a8>
 254:	6164766f 	cmnvs	r4, pc, ror #12
 258:	6f6b532f 	svcvs	0x006b532f
 25c:	722f616c 	eorvc	r6, pc, #108, 2
 260:	736f7065 	cmnvc	pc, #101	; 0x65
 264:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 268:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 26c:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 270:	636e692f 	cmnvs	lr, #770048	; 0xbc000
 274:	6564756c 	strbvs	r7, [r4, #-1388]!	; 0xfffffa94
 278:	616f622f 	cmnvs	pc, pc, lsr #4
 27c:	722f6472 	eorvc	r6, pc, #1912602624	; 0x72000000
 280:	2f306970 	svccs	0x00306970
 284:	006c6168 	rsbeq	r6, ip, r8, ror #2
 288:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; 1d4 <CPSR_IRQ_INHIBIT+0x154>
 28c:	616d2f65 	cmnvs	sp, r5, ror #30
 290:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
 294:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
 298:	616c6f6b 	cmnvs	ip, fp, ror #30
 29c:	7065722f 	rsbvc	r7, r5, pc, lsr #4
 2a0:	6f2f736f 	svcvs	0x002f736f
 2a4:	2f352d73 	svccs	0x00352d73
 2a8:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
 2ac:	692f6c65 	stmdbvs	pc!, {r0, r2, r5, r6, sl, fp, sp, lr}	; <UNPREDICTABLE>
 2b0:	756c636e 	strbvc	r6, [ip, #-878]!	; 0xfffffc92
 2b4:	642f6564 	strtvs	r6, [pc], #-1380	; 2bc <CPSR_IRQ_INHIBIT+0x23c>
 2b8:	65766972 	ldrbvs	r6, [r6, #-2418]!	; 0xfffff68e
 2bc:	00007372 	andeq	r7, r0, r2, ror r3
 2c0:	6f697067 	svcvs	0x00697067
 2c4:	7070632e 	rsbsvc	r6, r0, lr, lsr #6
 2c8:	00000100 	andeq	r0, r0, r0, lsl #2
 2cc:	64746e69 	ldrbtvs	r6, [r4], #-3689	; 0xfffff197
 2d0:	682e6665 	stmdavs	lr!, {r0, r2, r5, r6, r9, sl, sp, lr}
 2d4:	00000200 	andeq	r0, r0, r0, lsl #4
 2d8:	69726570 	ldmdbvs	r2!, {r4, r5, r6, r8, sl, sp, lr}^
 2dc:	72656870 	rsbvc	r6, r5, #112, 16	; 0x700000
 2e0:	2e736c61 	cdpcs	12, 7, cr6, cr3, cr1, {3}
 2e4:	00020068 	andeq	r0, r2, r8, rrx
 2e8:	69706700 	ldmdbvs	r0!, {r8, r9, sl, sp, lr}^
 2ec:	00682e6f 	rsbeq	r2, r8, pc, ror #28
 2f0:	00000003 	andeq	r0, r0, r3
 2f4:	05000105 	streq	r0, [r0, #-261]	; 0xfffffefb
 2f8:	00833402 	addeq	r3, r3, r2, lsl #8
 2fc:	39051700 	stmdbcc	r5, {r8, r9, sl, ip}
 300:	6901059f 	stmdbvs	r1, {r0, r1, r2, r3, r4, r7, r8, sl}
 304:	d70205a1 	strle	r0, [r2, -r1, lsr #11]
 308:	05670a05 	strbeq	r0, [r7, #-2565]!	; 0xfffff5fb
 30c:	0f054c0e 	svceq	0x00054c0e
 310:	40059208 	andmi	r9, r5, r8, lsl #4
 314:	2f0f0566 	svccs	0x000f0566
 318:	05664005 	strbeq	r4, [r6, #-5]!
 31c:	40052f0f 	andmi	r2, r5, pc, lsl #30
 320:	2f0f0566 	svccs	0x000f0566
 324:	05664005 	strbeq	r4, [r6, #-5]!
 328:	40052f0f 	andmi	r2, r5, pc, lsl #30
 32c:	2f0f0566 	svccs	0x000f0566
 330:	05664005 	strbeq	r4, [r6, #-5]!
 334:	17053111 	smladne	r5, r1, r1, r3
 338:	0a052008 	beq	148360 <_bss_end+0x13ecfc>
 33c:	4c090566 	cfstr32mi	mvfx0, [r9], {102}	; 0x66
 340:	a12f0105 			; <UNDEFINED> instruction: 0xa12f0105
 344:	05d70205 	ldrbeq	r0, [r7, #517]	; 0x205
 348:	0805670a 	stmdaeq	r5, {r1, r3, r8, r9, sl, sp, lr}
 34c:	0402004c 	streq	r0, [r2], #-76	; 0xffffffb4
 350:	00660601 	rsbeq	r0, r6, r1, lsl #12
 354:	4a020402 	bmi	81364 <_bss_end+0x77d00>
 358:	02000605 	andeq	r0, r0, #5242880	; 0x500000
 35c:	2e060404 	cdpcs	4, 0, cr0, cr6, cr4, {0}
 360:	02001005 	andeq	r1, r0, #5
 364:	054b0404 	strbeq	r0, [fp, #-1028]	; 0xfffffbfc
 368:	0402000a 	streq	r0, [r2], #-10
 36c:	09054a04 	stmdbeq	r5, {r2, r9, fp, lr}
 370:	04040200 	streq	r0, [r4], #-512	; 0xfffffe00
 374:	2f01054c 	svccs	0x0001054c
 378:	d7020585 	strle	r0, [r2, -r5, lsl #11]
 37c:	05670a05 	strbeq	r0, [r7, #-2565]!	; 0xfffff5fb
 380:	02004c08 	andeq	r4, r0, #8, 24	; 0x800
 384:	66060104 	strvs	r0, [r6], -r4, lsl #2
 388:	02040200 	andeq	r0, r4, #0, 4
 38c:	0006054a 	andeq	r0, r6, sl, asr #10
 390:	06040402 	streq	r0, [r4], -r2, lsl #8
 394:	0010052e 	andseq	r0, r0, lr, lsr #10
 398:	4b040402 	blmi	1013a8 <_bss_end+0xf7d44>
 39c:	02000a05 	andeq	r0, r0, #20480	; 0x5000
 3a0:	054a0404 	strbeq	r0, [sl, #-1028]	; 0xfffffbfc
 3a4:	04020009 	streq	r0, [r2], #-9
 3a8:	01054c04 	tsteq	r5, r4, lsl #24
 3ac:	0205852f 	andeq	r8, r5, #197132288	; 0xbc00000
 3b0:	670a05d7 			; <UNDEFINED> instruction: 0x670a05d7
 3b4:	004c0805 	subeq	r0, ip, r5, lsl #16
 3b8:	06010402 	streq	r0, [r1], -r2, lsl #8
 3bc:	04020066 	streq	r0, [r2], #-102	; 0xffffff9a
 3c0:	06054a02 	streq	r4, [r5], -r2, lsl #20
 3c4:	04040200 	streq	r0, [r4], #-512	; 0xfffffe00
 3c8:	10052e06 	andne	r2, r5, r6, lsl #28
 3cc:	04040200 	streq	r0, [r4], #-512	; 0xfffffe00
 3d0:	000a054b 	andeq	r0, sl, fp, asr #10
 3d4:	4a040402 	bmi	1013e4 <_bss_end+0xf7d80>
 3d8:	02000905 	andeq	r0, r0, #81920	; 0x14000
 3dc:	054c0404 	strbeq	r0, [ip, #-1028]	; 0xfffffbfc
 3e0:	05852f01 	streq	r2, [r5, #3841]	; 0xf01
 3e4:	0205d819 	andeq	sp, r5, #1638400	; 0x190000
 3e8:	4d1005ba 	cfldr32mi	mvfx0, [r0, #-744]	; 0xfffffd18
 3ec:	054a1905 	strbeq	r1, [sl, #-2309]	; 0xfffff6fb
 3f0:	1e05823b 	mcrne	2, 0, r8, cr5, cr11, {1}
 3f4:	2e1b0566 	cfmsc32cs	mvfx0, mvfx11, mvfx6
 3f8:	052f0805 	streq	r0, [pc, #-2053]!	; fffffbfb <_bss_end+0xffff6597>
 3fc:	02052e28 	andeq	r2, r5, #40, 28	; 0x280
 400:	4a0b0549 	bmi	2c192c <_bss_end+0x2b82c8>
 404:	05670505 	strbeq	r0, [r7, #-1285]!	; 0xfffffafb
 408:	03052d0d 	movweq	r2, #23821	; 0x5d0d
 40c:	32010548 	andcc	r0, r1, #72, 10	; 0x12000000
 410:	a019054d 	andsge	r0, r9, sp, asr #10
 414:	05ba0205 	ldreq	r0, [sl, #517]!	; 0x205
 418:	26054b1a 			; <UNDEFINED> instruction: 0x26054b1a
 41c:	4a2f054c 	bmi	bc1954 <_bss_end+0xbb82f0>
 420:	05823105 	streq	r3, [r2, #261]	; 0x105
 424:	3c054a09 			; <UNDEFINED> instruction: 0x3c054a09
 428:	0001052e 	andeq	r0, r1, lr, lsr #10
 42c:	4b010402 	blmi	4143c <_bss_end+0x37dd8>
 430:	d8080569 	stmdale	r8, {r0, r3, r5, r6, r8, sl}
 434:	05663205 	strbeq	r3, [r6, #-517]!	; 0xfffffdfb
 438:	04020021 	streq	r0, [r2], #-33	; 0xffffffdf
 43c:	06054a02 	streq	r4, [r5], -r2, lsl #20
 440:	02040200 	andeq	r0, r4, #0, 4
 444:	003205f2 	ldrshteq	r0, [r2], -r2
 448:	4a030402 	bmi	c1458 <_bss_end+0xb7df4>
 44c:	02005105 	andeq	r5, r0, #1073741825	; 0x40000001
 450:	05660604 	strbeq	r0, [r6, #-1540]!	; 0xfffff9fc
 454:	04020035 	streq	r0, [r2], #-53	; 0xffffffcb
 458:	3205f206 	andcc	pc, r5, #1610612736	; 0x60000000
 45c:	07040200 	streq	r0, [r4, -r0, lsl #4]
 460:	0402004a 	streq	r0, [r2], #-74	; 0xffffffb6
 464:	054a0608 	strbeq	r0, [sl, #-1544]	; 0xfffff9f8
 468:	04020002 	streq	r0, [r2], #-2
 46c:	052e060a 	streq	r0, [lr, #-1546]!	; 0xfffff9f6
 470:	02054d12 	andeq	r4, r5, #1152	; 0x480
 474:	4a0b0566 	bmi	2c1a14 <_bss_end+0x2b83b0>
 478:	05661205 	strbeq	r1, [r6, #-517]!	; 0xfffffdfb
 47c:	03052e0d 	movweq	r2, #24077	; 0x5e0d
 480:	31010548 	tstcc	r1, r8, asr #10
 484:	02009e4a 	andeq	r9, r0, #1184	; 0x4a0
 488:	66060104 	strvs	r0, [r6], -r4, lsl #2
 48c:	03062305 	movweq	r2, #25349	; 0x6305
 490:	05827fa9 	streq	r7, [r2, #4009]	; 0xfa9
 494:	00d70301 	sbcseq	r0, r7, r1, lsl #6
 498:	024aba66 	subeq	fp, sl, #417792	; 0x66000
 49c:	0101000a 	tsteq	r1, sl
 4a0:	000001a7 	andeq	r0, r0, r7, lsr #3
 4a4:	01070003 	tsteq	r7, r3
 4a8:	01020000 	mrseq	r0, (UNDEF: 2)
 4ac:	000d0efb 	strdeq	r0, [sp], -fp
 4b0:	01010101 	tsteq	r1, r1, lsl #2
 4b4:	01000000 	mrseq	r0, (UNDEF: 0)
 4b8:	2f010000 	svccs	0x00010000
 4bc:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
 4c0:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
 4c4:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
 4c8:	6b532f61 	blvs	14cc254 <_bss_end+0x14c2bf0>
 4cc:	2f616c6f 	svccs	0x00616c6f
 4d0:	6f706572 	svcvs	0x00706572
 4d4:	736f2f73 	cmnvc	pc, #460	; 0x1cc
 4d8:	6b2f352d 	blvs	bcd994 <_bss_end+0xbc4330>
 4dc:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
 4e0:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
 4e4:	72642f63 	rsbvc	r2, r4, #396	; 0x18c
 4e8:	72657669 	rsbvc	r7, r5, #110100480	; 0x6900000
 4ec:	682f0073 	stmdavs	pc!, {r0, r1, r4, r5, r6}	; <UNPREDICTABLE>
 4f0:	2f656d6f 	svccs	0x00656d6f
 4f4:	6b72616d 	blvs	1c98ab0 <_bss_end+0x1c8f44c>
 4f8:	6164766f 	cmnvs	r4, pc, ror #12
 4fc:	6f6b532f 	svcvs	0x006b532f
 500:	722f616c 	eorvc	r6, pc, #108, 2
 504:	736f7065 	cmnvc	pc, #101	; 0x65
 508:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 50c:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 510:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 514:	636e692f 	cmnvs	lr, #770048	; 0xbc000
 518:	6564756c 	strbvs	r7, [r4, #-1388]!	; 0xfffffa94
 51c:	616f622f 	cmnvs	pc, pc, lsr #4
 520:	722f6472 	eorvc	r6, pc, #1912602624	; 0x72000000
 524:	2f306970 	svccs	0x00306970
 528:	006c6168 	rsbeq	r6, ip, r8, ror #2
 52c:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; 478 <CPSR_IRQ_INHIBIT+0x3f8>
 530:	616d2f65 	cmnvs	sp, r5, ror #30
 534:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
 538:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
 53c:	616c6f6b 	cmnvs	ip, fp, ror #30
 540:	7065722f 	rsbvc	r7, r5, pc, lsr #4
 544:	6f2f736f 	svcvs	0x002f736f
 548:	2f352d73 	svccs	0x00352d73
 54c:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
 550:	692f6c65 	stmdbvs	pc!, {r0, r2, r5, r6, sl, fp, sp, lr}	; <UNPREDICTABLE>
 554:	756c636e 	strbvc	r6, [ip, #-878]!	; 0xfffffc92
 558:	642f6564 	strtvs	r6, [pc], #-1380	; 560 <CPSR_IRQ_INHIBIT+0x4e0>
 55c:	65766972 	ldrbvs	r6, [r6, #-2418]!	; 0xfffff68e
 560:	00007372 	andeq	r7, r0, r2, ror r3
 564:	656d6974 	strbvs	r6, [sp, #-2420]!	; 0xfffff68c
 568:	70632e72 	rsbvc	r2, r3, r2, ror lr
 56c:	00010070 	andeq	r0, r1, r0, ror r0
 570:	746e6900 	strbtvc	r6, [lr], #-2304	; 0xfffff700
 574:	2e666564 	cdpcs	5, 6, cr6, cr6, cr4, {3}
 578:	00020068 	andeq	r0, r2, r8, rrx
 57c:	72657000 	rsbvc	r7, r5, #0
 580:	65687069 	strbvs	r7, [r8, #-105]!	; 0xffffff97
 584:	736c6172 	cmnvc	ip, #-2147483620	; 0x8000001c
 588:	0200682e 	andeq	r6, r0, #3014656	; 0x2e0000
 58c:	69740000 	ldmdbvs	r4!, {}^	; <UNPREDICTABLE>
 590:	2e72656d 	cdpcs	5, 7, cr6, cr2, cr13, {3}
 594:	00030068 	andeq	r0, r3, r8, rrx
 598:	6d636200 	sfmvs	f6, 2, [r3, #-0]
 59c:	7875615f 	ldmdavc	r5!, {r0, r1, r2, r3, r4, r6, r8, sp, lr}^
 5a0:	0300682e 	movweq	r6, #2094	; 0x82e
 5a4:	61750000 	cmnvs	r5, r0
 5a8:	682e7472 	stmdavs	lr!, {r1, r4, r5, r6, sl, ip, sp, lr}
 5ac:	00000300 	andeq	r0, r0, r0, lsl #6
 5b0:	00010500 	andeq	r0, r1, r0, lsl #10
 5b4:	88340205 	ldmdahi	r4!, {r0, r2, r9}
 5b8:	1b030000 	blne	c05c0 <_bss_end+0xb6f5c>
 5bc:	9f130501 	svcls	0x00130501
 5c0:	052e5605 	streq	r5, [lr, #-1541]!	; 0xfffff9fb
 5c4:	05a1a101 	streq	sl, [r1, #257]!	; 0x101
 5c8:	18059f0c 	stmdane	r5, {r2, r3, r8, r9, sl, fp, ip, pc}
 5cc:	2e36054a 	cdpcs	5, 3, cr0, cr6, cr10, {2}
 5d0:	854b0105 	strbhi	r0, [fp, #-261]	; 0xfffffefb
 5d4:	05d71e05 	ldrbeq	r1, [r7, #3589]	; 0xe05
 5d8:	15058220 	strne	r8, [r5, #-544]	; 0xfffffde0
 5dc:	671b054d 	ldrvs	r0, [fp, -sp, asr #10]
 5e0:	05671705 	strbeq	r1, [r7, #-1797]!	; 0xfffff8fb
 5e4:	13056715 	movwne	r6, #22293	; 0x5715
 5e8:	d8460566 	stmdale	r6, {r1, r2, r5, r6, r8, sl}^
 5ec:	052e2105 	streq	r2, [lr, #-261]!	; 0xfffffefb
 5f0:	23058225 	movwcs	r8, #21029	; 0x5225
 5f4:	2505302e 	strcs	r3, [r5, #-46]	; 0xffffffd2
 5f8:	4c0f0582 	cfstr32mi	mvfx0, [pc], {130}	; 0x82
 5fc:	69670105 	stmdbvs	r7!, {r0, r2, r8}^
 600:	05836f05 	streq	r6, [r3, #3845]	; 0xf05
 604:	1705841b 	smladne	r5, fp, r4, r8
 608:	83010583 	movwhi	r0, #5507	; 0x1583
 60c:	83230569 			; <UNDEFINED> instruction: 0x83230569
 610:	05822505 	streq	r2, [r2, #1285]	; 0x505
 614:	05054c09 	streq	r4, [r5, #-3081]	; 0xfffff3f7
 618:	4b09054a 	blmi	241b48 <_bss_end+0x2384e4>
 61c:	054a1205 	strbeq	r1, [sl, #-517]	; 0xfffffdfb
 620:	05692f01 	strbeq	r2, [r9, #-3841]!	; 0xfffff0ff
 624:	1005832b 	andne	r8, r5, fp, lsr #6
 628:	2e2b0582 	cfsh64cs	mvdx0, mvdx11, #-62
 62c:	66830105 	strvs	r0, [r3], r5, lsl #2
 630:	0402009e 	streq	r0, [r2], #-158	; 0xffffff62
 634:	05660601 	strbeq	r0, [r6, #-1537]!	; 0xfffff9ff
 638:	bb03061e 	bllt	c1eb8 <_bss_end+0xb8854>
 63c:	0105827f 	tsteq	r5, pc, ror r2
 640:	6600c503 	strvs	ip, [r0], -r3, lsl #10
 644:	0a024aba 	beq	93134 <_bss_end+0x89ad0>
 648:	cd010100 	stfgts	f0, [r1, #-0]
 64c:	03000001 	movweq	r0, #1
 650:	0000fb00 	andeq	pc, r0, r0, lsl #22
 654:	fb010200 	blx	40e5e <_bss_end+0x377fa>
 658:	01000d0e 	tsteq	r0, lr, lsl #26
 65c:	00010101 	andeq	r0, r1, r1, lsl #2
 660:	00010000 	andeq	r0, r1, r0
 664:	682f0100 	stmdavs	pc!, {r8}	; <UNPREDICTABLE>
 668:	2f656d6f 	svccs	0x00656d6f
 66c:	6b72616d 	blvs	1c98c28 <_bss_end+0x1c8f5c4>
 670:	6164766f 	cmnvs	r4, pc, ror #12
 674:	6f6b532f 	svcvs	0x006b532f
 678:	722f616c 	eorvc	r6, pc, #108, 2
 67c:	736f7065 	cmnvc	pc, #101	; 0x65
 680:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 684:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 688:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 68c:	6372732f 	cmnvs	r2, #-1140850688	; 0xbc000000
 690:	6972642f 	ldmdbvs	r2!, {r0, r1, r2, r3, r5, sl, sp, lr}^
 694:	73726576 	cmnvc	r2, #494927872	; 0x1d800000
 698:	6f682f00 	svcvs	0x00682f00
 69c:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; 4f0 <CPSR_IRQ_INHIBIT+0x470>
 6a0:	6f6b7261 	svcvs	0x006b7261
 6a4:	2f616476 	svccs	0x00616476
 6a8:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
 6ac:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
 6b0:	2f736f70 	svccs	0x00736f70
 6b4:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
 6b8:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
 6bc:	2f6c656e 	svccs	0x006c656e
 6c0:	6c636e69 	stclvs	14, cr6, [r3], #-420	; 0xfffffe5c
 6c4:	2f656475 	svccs	0x00656475
 6c8:	72616f62 	rsbvc	r6, r1, #392	; 0x188
 6cc:	70722f64 	rsbsvc	r2, r2, r4, ror #30
 6d0:	682f3069 	stmdavs	pc!, {r0, r3, r5, r6, ip, sp}	; <UNPREDICTABLE>
 6d4:	2f006c61 	svccs	0x00006c61
 6d8:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
 6dc:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
 6e0:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
 6e4:	6b532f61 	blvs	14cc470 <_bss_end+0x14c2e0c>
 6e8:	2f616c6f 	svccs	0x00616c6f
 6ec:	6f706572 	svcvs	0x00706572
 6f0:	736f2f73 	cmnvc	pc, #460	; 0x1cc
 6f4:	6b2f352d 	blvs	bcdbb0 <_bss_end+0xbc454c>
 6f8:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
 6fc:	6e692f6c 	cdpvs	15, 6, cr2, cr9, cr12, {3}
 700:	64756c63 	ldrbtvs	r6, [r5], #-3171	; 0xfffff39d
 704:	72642f65 	rsbvc	r2, r4, #404	; 0x194
 708:	72657669 	rsbvc	r7, r5, #110100480	; 0x6900000
 70c:	75000073 	strvc	r0, [r0, #-115]	; 0xffffff8d
 710:	2e747261 	cdpcs	2, 7, cr7, cr4, cr1, {3}
 714:	00707063 	rsbseq	r7, r0, r3, rrx
 718:	70000001 	andvc	r0, r0, r1
 71c:	70697265 	rsbvc	r7, r9, r5, ror #4
 720:	61726568 	cmnvs	r2, r8, ror #10
 724:	682e736c 	stmdavs	lr!, {r2, r3, r5, r6, r8, r9, ip, sp, lr}
 728:	00000200 	andeq	r0, r0, r0, lsl #4
 72c:	5f6d6362 	svcpl	0x006d6362
 730:	2e787561 	cdpcs	5, 7, cr7, cr8, cr1, {3}
 734:	00030068 	andeq	r0, r3, r8, rrx
 738:	72617500 	rsbvc	r7, r1, #0, 10
 73c:	00682e74 	rsbeq	r2, r8, r4, ror lr
 740:	69000003 	stmdbvs	r0, {r0, r1}
 744:	6564746e 	strbvs	r7, [r4, #-1134]!	; 0xfffffb92
 748:	00682e66 	rsbeq	r2, r8, r6, ror #28
 74c:	00000002 	andeq	r0, r0, r2
 750:	05000105 	streq	r0, [r0, #-261]	; 0xfffffefb
 754:	008ac402 	addeq	ip, sl, r2, lsl #8
 758:	0f051700 	svceq	0x00051700
 75c:	6805059f 	stmdavs	r5, {r0, r1, r2, r3, r4, r7, r8, sl}
 760:	054a1005 	strbeq	r1, [sl, #-5]
 764:	16056705 	strne	r6, [r5], -r5, lsl #14
 768:	8305054a 	movwhi	r0, #21834	; 0x554a
 76c:	054a1605 	strbeq	r1, [sl, #-1541]	; 0xfffff9fb
 770:	16058305 	strne	r8, [r5], -r5, lsl #6
 774:	8305054a 	movwhi	r0, #21834	; 0x554a
 778:	054a1605 	strbeq	r1, [sl, #-1541]	; 0xfffff9fb
 77c:	05858301 	streq	r8, [r5, #769]	; 0x301
 780:	2e059f05 	cdpcs	15, 0, cr9, cr5, cr5, {0}
 784:	4a3f054a 	bmi	fc1cb4 <_bss_end+0xfb8650>
 788:	05825605 	streq	r5, [r2, #1541]	; 0x605
 78c:	16052e66 	strne	r2, [r5], -r6, ror #28
 790:	9f01052e 	svcls	0x0001052e
 794:	9f3a0569 	svcls	0x003a0569
 798:	052e6005 	streq	r6, [lr, #-5]!
 79c:	05058218 	streq	r8, [r5, #-536]	; 0xfffffde8
 7a0:	4a16054c 	bmi	581cd8 <_bss_end+0x578674>
 7a4:	05840505 	streq	r0, [r4, #1285]	; 0x505
 7a8:	05054a16 	streq	r4, [r5, #-2582]	; 0xfffff5ea
 7ac:	4a160584 	bmi	581dc4 <_bss_end+0x578760>
 7b0:	85830105 	strhi	r0, [r3, #261]	; 0x105
 7b4:	05bc0e05 	ldreq	r0, [ip, #3589]!	; 0xe05
 7b8:	36054a1f 			; <UNDEFINED> instruction: 0x36054a1f
 7bc:	2e0c0582 	cfsh32cs	mvfx0, mvfx12, #-62
 7c0:	31ba0505 			; <UNDEFINED> instruction: 0x31ba0505
 7c4:	054a1605 	strbeq	r1, [sl, #-1541]	; 0xfffff9fb
 7c8:	05698301 	strbeq	r8, [r9, #-769]!	; 0xfffffcff
 7cc:	1505a10c 	strne	sl, [r5, #-268]	; 0xfffffef4
 7d0:	03040200 	movweq	r0, #16896	; 0x4200
 7d4:	0016054a 	andseq	r0, r6, sl, asr #10
 7d8:	2e030402 	cdpcs	4, 0, cr0, cr3, cr2, {0}
 7dc:	02001805 	andeq	r1, r0, #327680	; 0x50000
 7e0:	05660304 	strbeq	r0, [r6, #-772]!	; 0xfffffcfc
 7e4:	04020013 	streq	r0, [r2], #-19	; 0xffffffed
 7e8:	14054b02 	strne	r4, [r5], #-2818	; 0xfffff4fe
 7ec:	02040200 	andeq	r0, r4, #0, 4
 7f0:	000e052e 	andeq	r0, lr, lr, lsr #10
 7f4:	4a020402 	bmi	81804 <_bss_end+0x781a0>
 7f8:	02000505 	andeq	r0, r0, #20971520	; 0x1400000
 7fc:	05810204 	streq	r0, [r1, #516]	; 0x204
 800:	9e668401 	cdpls	4, 6, cr8, cr6, cr1, {0}
 804:	01040200 	mrseq	r0, R12_usr
 808:	12056606 	andne	r6, r5, #6291456	; 0x600000
 80c:	82550306 	subshi	r0, r5, #402653184	; 0x18000000
 810:	2b030105 	blcs	c0c2c <_bss_end+0xb75c8>
 814:	024aba66 	subeq	fp, sl, #417792	; 0x66000
 818:	0101000a 	tsteq	r1, sl
 81c:	000001fe 	strdeq	r0, [r0], -lr
 820:	01570003 	cmpeq	r7, r3
 824:	01020000 	mrseq	r0, (UNDEF: 2)
 828:	000d0efb 	strdeq	r0, [sp], -fp
 82c:	01010101 	tsteq	r1, r1, lsl #2
 830:	01000000 	mrseq	r0, (UNDEF: 0)
 834:	2f010000 	svccs	0x00010000
 838:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
 83c:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
 840:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
 844:	6b532f61 	blvs	14cc5d0 <_bss_end+0x14c2f6c>
 848:	2f616c6f 	svccs	0x00616c6f
 84c:	6f706572 	svcvs	0x00706572
 850:	736f2f73 	cmnvc	pc, #460	; 0x1cc
 854:	6b2f352d 	blvs	bcdd10 <_bss_end+0xbc46ac>
 858:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
 85c:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
 860:	682f0063 	stmdavs	pc!, {r0, r1, r5, r6}	; <UNPREDICTABLE>
 864:	2f656d6f 	svccs	0x00656d6f
 868:	6b72616d 	blvs	1c98e24 <_bss_end+0x1c8f7c0>
 86c:	6164766f 	cmnvs	r4, pc, ror #12
 870:	6f6b532f 	svcvs	0x006b532f
 874:	722f616c 	eorvc	r6, pc, #108, 2
 878:	736f7065 	cmnvc	pc, #101	; 0x65
 87c:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 880:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 884:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 888:	636e692f 	cmnvs	lr, #770048	; 0xbc000
 88c:	6564756c 	strbvs	r7, [r4, #-1388]!	; 0xfffffa94
 890:	616f622f 	cmnvs	pc, pc, lsr #4
 894:	722f6472 	eorvc	r6, pc, #1912602624	; 0x72000000
 898:	2f306970 	svccs	0x00306970
 89c:	006c6168 	rsbeq	r6, ip, r8, ror #2
 8a0:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; 7ec <CPSR_IRQ_INHIBIT+0x76c>
 8a4:	616d2f65 	cmnvs	sp, r5, ror #30
 8a8:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
 8ac:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
 8b0:	616c6f6b 	cmnvs	ip, fp, ror #30
 8b4:	7065722f 	rsbvc	r7, r5, pc, lsr #4
 8b8:	6f2f736f 	svcvs	0x002f736f
 8bc:	2f352d73 	svccs	0x00352d73
 8c0:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
 8c4:	692f6c65 	stmdbvs	pc!, {r0, r2, r5, r6, sl, fp, sp, lr}	; <UNPREDICTABLE>
 8c8:	756c636e 	strbvc	r6, [ip, #-878]!	; 0xfffffc92
 8cc:	642f6564 	strtvs	r6, [pc], #-1380	; 8d4 <CPSR_IRQ_INHIBIT+0x854>
 8d0:	65766972 	ldrbvs	r6, [r6, #-2418]!	; 0xfffff68e
 8d4:	2f007372 	svccs	0x00007372
 8d8:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
 8dc:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
 8e0:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
 8e4:	6b532f61 	blvs	14cc670 <_bss_end+0x14c300c>
 8e8:	2f616c6f 	svccs	0x00616c6f
 8ec:	6f706572 	svcvs	0x00706572
 8f0:	736f2f73 	cmnvc	pc, #460	; 0x1cc
 8f4:	6b2f352d 	blvs	bcddb0 <_bss_end+0xbc474c>
 8f8:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
 8fc:	6e692f6c 	cdpvs	15, 6, cr2, cr9, cr12, {3}
 900:	64756c63 	ldrbtvs	r6, [r5], #-3171	; 0xfffff39d
 904:	69000065 	stmdbvs	r0, {r0, r2, r5, r6}
 908:	7265746e 	rsbvc	r7, r5, #1845493760	; 0x6e000000
 90c:	74707572 	ldrbtvc	r7, [r0], #-1394	; 0xfffffa8e
 910:	6e6f635f 	mcrvs	3, 3, r6, cr15, cr15, {2}
 914:	6c6f7274 	sfmvs	f7, 2, [pc], #-464	; 74c <CPSR_IRQ_INHIBIT+0x6cc>
 918:	2e72656c 	cdpcs	5, 7, cr6, cr2, cr12, {3}
 91c:	00707063 	rsbseq	r7, r0, r3, rrx
 920:	69000001 	stmdbvs	r0, {r0}
 924:	6564746e 	strbvs	r7, [r4, #-1134]!	; 0xfffffb92
 928:	00682e66 	rsbeq	r2, r8, r6, ror #28
 92c:	70000002 	andvc	r0, r0, r2
 930:	70697265 	rsbvc	r7, r9, r5, ror #4
 934:	61726568 	cmnvs	r2, r8, ror #10
 938:	682e736c 	stmdavs	lr!, {r2, r3, r5, r6, r8, r9, ip, sp, lr}
 93c:	00000200 	andeq	r0, r0, r0, lsl #4
 940:	5f6d6362 	svcpl	0x006d6362
 944:	2e787561 	cdpcs	5, 7, cr7, cr8, cr1, {3}
 948:	00030068 	andeq	r0, r3, r8, rrx
 94c:	72617500 	rsbvc	r7, r1, #0, 10
 950:	00682e74 	rsbeq	r2, r8, r4, ror lr
 954:	69000003 	stmdbvs	r0, {r0, r1}
 958:	7265746e 	rsbvc	r7, r5, #1845493760	; 0x6e000000
 95c:	74707572 	ldrbtvc	r7, [r0], #-1394	; 0xfffffa8e
 960:	6e6f635f 	mcrvs	3, 3, r6, cr15, cr15, {2}
 964:	6c6f7274 	sfmvs	f7, 2, [pc], #-464	; 79c <CPSR_IRQ_INHIBIT+0x71c>
 968:	2e72656c 	cdpcs	5, 7, cr6, cr2, cr12, {3}
 96c:	00040068 	andeq	r0, r4, r8, rrx
 970:	6d697400 	cfstrdvs	mvd7, [r9, #-0]
 974:	682e7265 	stmdavs	lr!, {r0, r2, r5, r6, r9, ip, sp, lr}
 978:	00000300 	andeq	r0, r0, r0, lsl #6
 97c:	00010500 	andeq	r0, r1, r0, lsl #10
 980:	8d9c0205 	lfmhi	f0, 4, [ip, #20]
 984:	0f030000 	svceq	0x00030000
 988:	05854c01 	streq	r4, [r5, #3073]	; 0xc01
 98c:	05056b24 	streq	r6, [r5, #-2852]	; 0xfffff4dc
 990:	4b1c0566 	blmi	701f30 <_bss_end+0x6f88cc>
 994:	854b0105 	strbhi	r0, [fp, #-261]	; 0xfffffefb
 998:	17056a30 	smladxne	r5, r0, sl, r6
 99c:	2e3c059f 	mrccs	5, 1, r0, cr12, cr15, {4}
 9a0:	a14d0105 	cmpge	sp, r5, lsl #2
 9a4:	059f0c05 	ldreq	r0, [pc, #3077]	; 15b1 <CPSR_IRQ_INHIBIT+0x1531>
 9a8:	3a054a1c 	bcc	153220 <_bss_end+0x149bbc>
 9ac:	4b01052e 	blmi	41e6c <_bss_end+0x38808>
 9b0:	9f430585 	svcls	0x00430585
 9b4:	052e4005 	streq	r4, [lr, #-5]!
 9b8:	40054a39 	andmi	r4, r5, r9, lsr sl
 9bc:	2e3b0582 	cfadd32cs	mvfx0, mvfx11, mvfx2
 9c0:	692f0105 	stmdbvs	pc!, {r0, r2, r8}	; <UNPREDICTABLE>
 9c4:	059f4405 	ldreq	r4, [pc, #1029]	; dd1 <CPSR_IRQ_INHIBIT+0xd51>
 9c8:	3a052e41 	bcc	14c2d4 <_bss_end+0x142c70>
 9cc:	8241054a 	subhi	r0, r1, #310378496	; 0x12800000
 9d0:	052e3c05 	streq	r3, [lr, #-3077]!	; 0xfffff3fb
 9d4:	05692f01 	strbeq	r2, [r9, #-3841]!	; 0xfffff0ff
 9d8:	87059f18 	smladhi	r5, r8, pc, r9	; <UNPREDICTABLE>
 9dc:	7a054c01 	bvc	1539e8 <_bss_end+0x14a384>
 9e0:	4a73054a 	bmi	1cc1f10 <_bss_end+0x1cb88ac>
 9e4:	05827a05 	streq	r7, [r2, #2565]	; 0xa05
 9e8:	01052e75 	tsteq	r5, r5, ror lr
 9ec:	1805692f 	stmdane	r5, {r0, r1, r2, r3, r5, r8, fp, sp, lr}
 9f0:	0189059f 			; <UNDEFINED> instruction: 0x0189059f
 9f4:	4a7c054c 	bmi	1f01f2c <_bss_end+0x1ef88c8>
 9f8:	054a7505 	strbeq	r7, [sl, #-1285]	; 0xfffffafb
 9fc:	7705827c 	smlsdxvc	r5, ip, r2, r8
 a00:	2f01052e 	svccs	0x0001052e
 a04:	02009e66 	andeq	r9, r0, #1632	; 0x660
 a08:	66060104 	strvs	r0, [r6], -r4, lsl #2
 a0c:	03064305 	movweq	r4, #25349	; 0x6305
 a10:	01058245 	tsteq	r5, r5, asr #4
 a14:	ba663b03 	blt	198f628 <_bss_end+0x1985fc4>
 a18:	000a024a 	andeq	r0, sl, sl, asr #4
 a1c:	01a00101 	lsleq	r0, r1, #2
 a20:	00030000 	andeq	r0, r3, r0
 a24:	00000151 	andeq	r0, r0, r1, asr r1
 a28:	0efb0102 	cdpeq	1, 15, cr0, cr11, cr2, {0}
 a2c:	0101000d 	tsteq	r1, sp
 a30:	00000101 	andeq	r0, r0, r1, lsl #2
 a34:	00000100 	andeq	r0, r0, r0, lsl #2
 a38:	6f682f01 	svcvs	0x00682f01
 a3c:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; 890 <CPSR_IRQ_INHIBIT+0x810>
 a40:	6f6b7261 	svcvs	0x006b7261
 a44:	2f616476 	svccs	0x00616476
 a48:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
 a4c:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
 a50:	2f736f70 	svccs	0x00736f70
 a54:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
 a58:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
 a5c:	2f6c656e 	svccs	0x006c656e
 a60:	00637273 	rsbeq	r7, r3, r3, ror r2
 a64:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; 9b0 <CPSR_IRQ_INHIBIT+0x930>
 a68:	616d2f65 	cmnvs	sp, r5, ror #30
 a6c:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
 a70:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
 a74:	616c6f6b 	cmnvs	ip, fp, ror #30
 a78:	7065722f 	rsbvc	r7, r5, pc, lsr #4
 a7c:	6f2f736f 	svcvs	0x002f736f
 a80:	2f352d73 	svccs	0x00352d73
 a84:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
 a88:	692f6c65 	stmdbvs	pc!, {r0, r2, r5, r6, sl, fp, sp, lr}	; <UNPREDICTABLE>
 a8c:	756c636e 	strbvc	r6, [ip, #-878]!	; 0xfffffc92
 a90:	622f6564 	eorvs	r6, pc, #100, 10	; 0x19000000
 a94:	6472616f 	ldrbtvs	r6, [r2], #-367	; 0xfffffe91
 a98:	6970722f 	ldmdbvs	r0!, {r0, r1, r2, r3, r5, r9, ip, sp, lr}^
 a9c:	61682f30 	cmnvs	r8, r0, lsr pc
 aa0:	682f006c 	stmdavs	pc!, {r2, r3, r5, r6}	; <UNPREDICTABLE>
 aa4:	2f656d6f 	svccs	0x00656d6f
 aa8:	6b72616d 	blvs	1c99064 <_bss_end+0x1c8fa00>
 aac:	6164766f 	cmnvs	r4, pc, ror #12
 ab0:	6f6b532f 	svcvs	0x006b532f
 ab4:	722f616c 	eorvc	r6, pc, #108, 2
 ab8:	736f7065 	cmnvc	pc, #101	; 0x65
 abc:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
 ac0:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
 ac4:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
 ac8:	636e692f 	cmnvs	lr, #770048	; 0xbc000
 acc:	6564756c 	strbvs	r7, [r4, #-1388]!	; 0xfffffa94
 ad0:	6972642f 	ldmdbvs	r2!, {r0, r1, r2, r3, r5, sl, sp, lr}^
 ad4:	73726576 	cmnvc	r2, #494927872	; 0x1d800000
 ad8:	6f682f00 	svcvs	0x00682f00
 adc:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; 930 <CPSR_IRQ_INHIBIT+0x8b0>
 ae0:	6f6b7261 	svcvs	0x006b7261
 ae4:	2f616476 	svccs	0x00616476
 ae8:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
 aec:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
 af0:	2f736f70 	svccs	0x00736f70
 af4:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
 af8:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
 afc:	2f6c656e 	svccs	0x006c656e
 b00:	6c636e69 	stclvs	14, cr6, [r3], #-420	; 0xfffffe5c
 b04:	00656475 	rsbeq	r6, r5, r5, ror r4
 b08:	69616d00 	stmdbvs	r1!, {r8, sl, fp, sp, lr}^
 b0c:	70632e6e 	rsbvc	r2, r3, lr, ror #28
 b10:	00010070 	andeq	r0, r1, r0, ror r0
 b14:	746e6900 	strbtvc	r6, [lr], #-2304	; 0xfffff700
 b18:	2e666564 	cdpcs	5, 6, cr6, cr6, cr4, {3}
 b1c:	00020068 	andeq	r0, r2, r8, rrx
 b20:	69706700 	ldmdbvs	r0!, {r8, r9, sl, sp, lr}^
 b24:	00682e6f 	rsbeq	r2, r8, pc, ror #28
 b28:	70000003 	andvc	r0, r0, r3
 b2c:	70697265 	rsbvc	r7, r9, r5, ror #4
 b30:	61726568 	cmnvs	r2, r8, ror #10
 b34:	682e736c 	stmdavs	lr!, {r2, r3, r5, r6, r8, r9, ip, sp, lr}
 b38:	00000200 	andeq	r0, r0, r0, lsl #4
 b3c:	5f6d6362 	svcpl	0x006d6362
 b40:	2e787561 	cdpcs	5, 7, cr7, cr8, cr1, {3}
 b44:	00030068 	andeq	r0, r3, r8, rrx
 b48:	72617500 	rsbvc	r7, r1, #0, 10
 b4c:	00682e74 	rsbeq	r2, r8, r4, ror lr
 b50:	74000003 	strvc	r0, [r0], #-3
 b54:	72656d69 	rsbvc	r6, r5, #6720	; 0x1a40
 b58:	0300682e 	movweq	r6, #2094	; 0x82e
 b5c:	6e690000 	cdpvs	0, 6, cr0, cr9, cr0, {0}
 b60:	72726574 	rsbsvc	r6, r2, #116, 10	; 0x1d000000
 b64:	5f747075 	svcpl	0x00747075
 b68:	746e6f63 	strbtvc	r6, [lr], #-3939	; 0xfffff09d
 b6c:	6c6c6f72 	stclvs	15, cr6, [ip], #-456	; 0xfffffe38
 b70:	682e7265 	stmdavs	lr!, {r0, r2, r5, r6, r9, ip, sp, lr}
 b74:	00000400 	andeq	r0, r0, r0, lsl #8
 b78:	00010500 	andeq	r0, r1, r0, lsl #10
 b7c:	90000205 	andls	r0, r0, r5, lsl #4
 b80:	0f030000 	svceq	0x00030000
 b84:	4d0f0501 	cfstr32mi	mvfx0, [pc, #-4]	; b88 <CPSR_IRQ_INHIBIT+0xb08>
 b88:	05ba0205 	ldreq	r0, [sl, #517]!	; 0x205
 b8c:	13054c0d 	movwne	r4, #23565	; 0x5c0d
 b90:	89010567 	stmdbhi	r1, {r0, r1, r2, r5, r6, r8, sl}
 b94:	052b0d05 	streq	r0, [fp, #-3333]!	; 0xfffff2fb
 b98:	01056713 	tsteq	r5, r3, lsl r7
 b9c:	19058584 	stmdbne	r5, {r2, r7, r8, sl, pc}
 ba0:	8516054c 	ldrhi	r0, [r6, #-1356]	; 0xfffffab4
 ba4:	05671805 	strbeq	r1, [r7, #-2053]!	; 0xfffff7fb
 ba8:	2105690e 	tstcs	r5, lr, lsl #18
 bac:	690f0569 	stmdbvs	pc, {r0, r3, r5, r6, r8, sl}	; <UNPREDICTABLE>
 bb0:	05a12005 	streq	r2, [r1, #5]!
 bb4:	0505680c 	streq	r6, [r5, #-2060]	; 0xfffff7f4
 bb8:	01040200 	mrseq	r0, R12_usr
 bbc:	00100231 	andseq	r0, r0, r1, lsr r2
 bc0:	00940101 	addseq	r0, r4, r1, lsl #2
 bc4:	00030000 	andeq	r0, r3, r0
 bc8:	00000049 	andeq	r0, r0, r9, asr #32
 bcc:	0efb0102 	cdpeq	1, 15, cr0, cr11, cr2, {0}
 bd0:	0101000d 	tsteq	r1, sp
 bd4:	00000101 	andeq	r0, r0, r1, lsl #2
 bd8:	00000100 	andeq	r0, r0, r0, lsl #2
 bdc:	6f682f01 	svcvs	0x00682f01
 be0:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; a34 <CPSR_IRQ_INHIBIT+0x9b4>
 be4:	6f6b7261 	svcvs	0x006b7261
 be8:	2f616476 	svccs	0x00616476
 bec:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
 bf0:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
 bf4:	2f736f70 	svccs	0x00736f70
 bf8:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
 bfc:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
 c00:	2f6c656e 	svccs	0x006c656e
 c04:	00637273 	rsbeq	r7, r3, r3, ror r2
 c08:	61747300 	cmnvs	r4, r0, lsl #6
 c0c:	732e7472 			; <UNDEFINED> instruction: 0x732e7472
 c10:	00000100 	andeq	r0, r0, r0, lsl #2
 c14:	02050000 	andeq	r0, r5, #0
 c18:	00008000 	andeq	r8, r0, r0
 c1c:	2f010d03 	svccs	0x00010d03
 c20:	2f2f2f2f 	svccs	0x002f2f2f
 c24:	1d032f2f 	stcne	15, cr2, [r3, #-188]	; 0xffffff44
 c28:	2f312008 	svccs	0x00312008
 c2c:	2f2f2f32 	svccs	0x002f2f32
 c30:	312f2f31 			; <UNDEFINED> instruction: 0x312f2f31
 c34:	2f312f2f 	svccs	0x00312f2f
 c38:	2f2f302f 	svccs	0x002f302f
 c3c:	0202302f 	andeq	r3, r2, #47	; 0x2f
 c40:	00010100 	andeq	r0, r1, r0, lsl #2
 c44:	91000205 	tstls	r0, r5, lsl #4
 c48:	d9030000 	stmdble	r3, {}	; <UNPREDICTABLE>
 c4c:	2f2f0100 	svccs	0x002f0100
 c50:	33312f2f 	teqcc	r1, #47, 30	; 0xbc
 c54:	00020233 	andeq	r0, r2, r3, lsr r2
 c58:	00ce0101 	sbceq	r0, lr, r1, lsl #2
 c5c:	00030000 	andeq	r0, r3, r0
 c60:	0000004d 	andeq	r0, r0, sp, asr #32
 c64:	0efb0102 	cdpeq	1, 15, cr0, cr11, cr2, {0}
 c68:	0101000d 	tsteq	r1, sp
 c6c:	00000101 	andeq	r0, r0, r1, lsl #2
 c70:	00000100 	andeq	r0, r0, r0, lsl #2
 c74:	6f682f01 	svcvs	0x00682f01
 c78:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; acc <CPSR_IRQ_INHIBIT+0xa4c>
 c7c:	6f6b7261 	svcvs	0x006b7261
 c80:	2f616476 	svccs	0x00616476
 c84:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
 c88:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
 c8c:	2f736f70 	svccs	0x00736f70
 c90:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
 c94:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
 c98:	2f6c656e 	svccs	0x006c656e
 c9c:	00637273 	rsbeq	r7, r3, r3, ror r2
 ca0:	61747300 	cmnvs	r4, r0, lsl #6
 ca4:	70757472 	rsbsvc	r7, r5, r2, ror r4
 ca8:	7070632e 	rsbsvc	r6, r0, lr, lsr #6
 cac:	00000100 	andeq	r0, r0, r0, lsl #2
 cb0:	00010500 	andeq	r0, r1, r0, lsl #10
 cb4:	91200205 			; <UNDEFINED> instruction: 0x91200205
 cb8:	14030000 	strne	r0, [r3], #-0
 cbc:	6a090501 	bvs	2420c8 <_bss_end+0x238a64>
 cc0:	02001f05 	andeq	r1, r0, #5, 30
 cc4:	05660304 	strbeq	r0, [r6, #-772]!	; 0xfffffcfc
 cc8:	04020006 	streq	r0, [r2], #-6
 ccc:	0205bb02 	andeq	fp, r5, #2048	; 0x800
 cd0:	02040200 	andeq	r0, r4, #0, 4
 cd4:	85090565 	strhi	r0, [r9, #-1381]	; 0xfffffa9b
 cd8:	bd2f0105 	stflts	f0, [pc, #-20]!	; ccc <CPSR_IRQ_INHIBIT+0xc4c>
 cdc:	056b0d05 	strbeq	r0, [fp, #-3333]!	; 0xfffff2fb
 ce0:	04020024 	streq	r0, [r2], #-36	; 0xffffffdc
 ce4:	04054a03 	streq	r4, [r5], #-2563	; 0xfffff5fd
 ce8:	02040200 	andeq	r0, r4, #0, 4
 cec:	000b0583 	andeq	r0, fp, r3, lsl #11
 cf0:	4a020402 	bmi	81d00 <_bss_end+0x7869c>
 cf4:	02000205 	andeq	r0, r0, #1342177280	; 0x50000000
 cf8:	052d0204 	streq	r0, [sp, #-516]!	; 0xfffffdfc
 cfc:	01058509 	tsteq	r5, r9, lsl #10
 d00:	0d05a12f 	stfeqd	f2, [r5, #-188]	; 0xffffff44
 d04:	0024056a 	eoreq	r0, r4, sl, ror #10
 d08:	4a030402 	bmi	c1d18 <_bss_end+0xb86b4>
 d0c:	02000405 	andeq	r0, r0, #83886080	; 0x5000000
 d10:	05830204 	streq	r0, [r3, #516]	; 0x204
 d14:	0402000b 	streq	r0, [r2], #-11
 d18:	02054a02 	andeq	r4, r5, #8192	; 0x2000
 d1c:	02040200 	andeq	r0, r4, #0, 4
 d20:	8509052d 	strhi	r0, [r9, #-1325]	; 0xfffffad3
 d24:	022f0105 	eoreq	r0, pc, #1073741825	; 0x40000001
 d28:	0101000a 	tsteq	r1, sl
 d2c:	00000079 	andeq	r0, r0, r9, ror r0
 d30:	00460003 	subeq	r0, r6, r3
 d34:	01020000 	mrseq	r0, (UNDEF: 2)
 d38:	000d0efb 	strdeq	r0, [sp], -fp
 d3c:	01010101 	tsteq	r1, r1, lsl #2
 d40:	01000000 	mrseq	r0, (UNDEF: 0)
 d44:	2e010000 	cdpcs	0, 0, cr0, cr1, cr0, {0}
 d48:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 d4c:	2f2e2e2f 	svccs	0x002e2e2f
 d50:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 d54:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 d58:	62696c2f 	rsbvs	r6, r9, #12032	; 0x2f00
 d5c:	2f636367 	svccs	0x00636367
 d60:	666e6f63 	strbtvs	r6, [lr], -r3, ror #30
 d64:	612f6769 			; <UNDEFINED> instruction: 0x612f6769
 d68:	00006d72 	andeq	r6, r0, r2, ror sp
 d6c:	3162696c 	cmncc	r2, ip, ror #18
 d70:	636e7566 	cmnvs	lr, #427819008	; 0x19800000
 d74:	00532e73 	subseq	r2, r3, r3, ror lr
 d78:	00000001 	andeq	r0, r0, r1
 d7c:	38020500 	stmdacc	r2, {r8, sl}
 d80:	03000092 	movweq	r0, #146	; 0x92
 d84:	300108ca 	andcc	r0, r1, sl, asr #17
 d88:	2f2f2f2f 	svccs	0x002f2f2f
 d8c:	d002302f 	andle	r3, r2, pc, lsr #32
 d90:	312f1401 			; <UNDEFINED> instruction: 0x312f1401
 d94:	4c302f2f 	ldcmi	15, cr2, [r0], #-188	; 0xffffff44
 d98:	1f03322f 	svcne	0x0003322f
 d9c:	2f2f2f66 	svccs	0x002f2f66
 da0:	2f2f2f2f 	svccs	0x002f2f2f
 da4:	01000202 	tsteq	r0, r2, lsl #4
 da8:	00005c01 	andeq	r5, r0, r1, lsl #24
 dac:	46000300 	strmi	r0, [r0], -r0, lsl #6
 db0:	02000000 	andeq	r0, r0, #0
 db4:	0d0efb01 	vstreq	d15, [lr, #-4]
 db8:	01010100 	mrseq	r0, (UNDEF: 17)
 dbc:	00000001 	andeq	r0, r0, r1
 dc0:	01000001 	tsteq	r0, r1
 dc4:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 dc8:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 dcc:	2f2e2e2f 	svccs	0x002e2e2f
 dd0:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 dd4:	696c2f2e 	stmdbvs	ip!, {r1, r2, r3, r5, r8, r9, sl, fp, sp}^
 dd8:	63636762 	cmnvs	r3, #25690112	; 0x1880000
 ddc:	6e6f632f 	cdpvs	3, 6, cr6, cr15, cr15, {1}
 de0:	2f676966 	svccs	0x00676966
 de4:	006d7261 	rsbeq	r7, sp, r1, ror #4
 de8:	62696c00 	rsbvs	r6, r9, #0, 24
 dec:	6e756631 	mrcvs	6, 3, r6, cr5, cr1, {1}
 df0:	532e7363 			; <UNDEFINED> instruction: 0x532e7363
 df4:	00000100 	andeq	r0, r0, r0, lsl #2
 df8:	02050000 	andeq	r0, r5, #0
 dfc:	00009444 	andeq	r9, r0, r4, asr #8
 e00:	010bb403 	tsteq	fp, r3, lsl #8
 e04:	01000202 	tsteq	r0, r2, lsl #4
 e08:	00010301 	andeq	r0, r1, r1, lsl #6
 e0c:	fd000300 	stc2	3, cr0, [r0, #-0]
 e10:	02000000 	andeq	r0, r0, #0
 e14:	0d0efb01 	vstreq	d15, [lr, #-4]
 e18:	01010100 	mrseq	r0, (UNDEF: 17)
 e1c:	00000001 	andeq	r0, r0, r1
 e20:	01000001 	tsteq	r0, r1
 e24:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e28:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 e2c:	2f2e2e2f 	svccs	0x002e2e2f
 e30:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e34:	696c2f2e 	stmdbvs	ip!, {r1, r2, r3, r5, r8, r9, sl, fp, sp}^
 e38:	63636762 	cmnvs	r3, #25690112	; 0x1880000
 e3c:	2f2e2e2f 	svccs	0x002e2e2f
 e40:	6c636e69 	stclvs	14, cr6, [r3], #-420	; 0xfffffe5c
 e44:	00656475 	rsbeq	r6, r5, r5, ror r4
 e48:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e4c:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 e50:	2f2e2e2f 	svccs	0x002e2e2f
 e54:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e58:	6363672f 	cmnvs	r3, #12320768	; 0xbc0000
 e5c:	2f2e2e00 	svccs	0x002e2e00
 e60:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e64:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 e68:	2f2e2e2f 	svccs	0x002e2e2f
 e6c:	6c2f2e2e 	stcvs	14, cr2, [pc], #-184	; dbc <CPSR_IRQ_INHIBIT+0xd3c>
 e70:	63676269 	cmnvs	r7, #-1879048186	; 0x90000006
 e74:	2e2e2f63 	cdpcs	15, 2, cr2, cr14, cr3, {3}
 e78:	6363672f 	cmnvs	r3, #12320768	; 0xbc0000
 e7c:	6e6f632f 	cdpvs	3, 6, cr6, cr15, cr15, {1}
 e80:	2f676966 	svccs	0x00676966
 e84:	006d7261 	rsbeq	r7, sp, r1, ror #4
 e88:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e8c:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
 e90:	2f2e2e2f 	svccs	0x002e2e2f
 e94:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
 e98:	696c2f2e 	stmdbvs	ip!, {r1, r2, r3, r5, r8, r9, sl, fp, sp}^
 e9c:	63636762 	cmnvs	r3, #25690112	; 0x1880000
 ea0:	61680000 	cmnvs	r8, r0
 ea4:	61746873 	cmnvs	r4, r3, ror r8
 ea8:	00682e62 	rsbeq	r2, r8, r2, ror #28
 eac:	61000001 	tstvs	r0, r1
 eb0:	692d6d72 	pushvs	{r1, r4, r5, r6, r8, sl, fp, sp, lr}
 eb4:	682e6173 	stmdavs	lr!, {r0, r1, r4, r5, r6, r8, sp, lr}
 eb8:	00000200 	andeq	r0, r0, r0, lsl #4
 ebc:	2d6d7261 	sfmcs	f7, 2, [sp, #-388]!	; 0xfffffe7c
 ec0:	2e757063 	cdpcs	0, 7, cr7, cr5, cr3, {3}
 ec4:	00020068 	andeq	r0, r2, r8, rrx
 ec8:	736e6900 	cmnvc	lr, #0, 18
 ecc:	6f632d6e 	svcvs	0x00632d6e
 ed0:	6174736e 	cmnvs	r4, lr, ror #6
 ed4:	2e73746e 	cdpcs	4, 7, cr7, cr3, cr14, {3}
 ed8:	00020068 	andeq	r0, r2, r8, rrx
 edc:	6d726100 	ldfvse	f6, [r2, #-0]
 ee0:	0300682e 	movweq	r6, #2094	; 0x82e
 ee4:	696c0000 	stmdbvs	ip!, {}^	; <UNPREDICTABLE>
 ee8:	63636762 	cmnvs	r3, #25690112	; 0x1880000
 eec:	00682e32 	rsbeq	r2, r8, r2, lsr lr
 ef0:	67000004 	strvs	r0, [r0, -r4]
 ef4:	632d6c62 			; <UNDEFINED> instruction: 0x632d6c62
 ef8:	73726f74 	cmnvc	r2, #116, 30	; 0x1d0
 efc:	0400682e 	streq	r6, [r0], #-2094	; 0xfffff7d2
 f00:	696c0000 	stmdbvs	ip!, {}^	; <UNPREDICTABLE>
 f04:	63636762 	cmnvs	r3, #25690112	; 0x1880000
 f08:	00632e32 	rsbeq	r2, r3, r2, lsr lr
 f0c:	00000004 	andeq	r0, r0, r4

Disassembly of section .debug_str:

00000000 <.debug_str>:
       0:	78635f5f 	stmdavc	r3!, {r0, r1, r2, r3, r4, r6, r8, r9, sl, fp, ip, lr}^
       4:	75675f61 	strbvc	r5, [r7, #-3937]!	; 0xfffff09f
       8:	5f647261 	svcpl	0x00647261
       c:	75716361 	ldrbvc	r6, [r1, #-865]!	; 0xfffffc9f
      10:	00657269 	rsbeq	r7, r5, r9, ror #4
      14:	20554e47 	subscs	r4, r5, r7, asr #28
      18:	312b2b43 			; <UNDEFINED> instruction: 0x312b2b43
      1c:	2e392034 	mrccs	0, 1, r2, cr9, cr4, {1}
      20:	20312e32 	eorscs	r2, r1, r2, lsr lr
      24:	39313032 	ldmdbcc	r1!, {r1, r4, r5, ip, sp}
      28:	35323031 	ldrcc	r3, [r2, #-49]!	; 0xffffffcf
      2c:	65722820 	ldrbvs	r2, [r2, #-2080]!	; 0xfffff7e0
      30:	7361656c 	cmnvc	r1, #108, 10	; 0x1b000000
      34:	5b202965 	blpl	80a5d0 <_bss_end+0x800f6c>
      38:	2f4d5241 	svccs	0x004d5241
      3c:	2d6d7261 	sfmcs	f7, 2, [sp, #-388]!	; 0xfffffe7c
      40:	72622d39 	rsbvc	r2, r2, #3648	; 0xe40
      44:	68636e61 	stmdavs	r3!, {r0, r5, r6, r9, sl, fp, sp, lr}^
      48:	76657220 	strbtvc	r7, [r5], -r0, lsr #4
      4c:	6f697369 	svcvs	0x00697369
      50:	3732206e 	ldrcc	r2, [r2, -lr, rrx]!
      54:	39393537 	ldmdbcc	r9!, {r0, r1, r2, r4, r5, r8, sl, ip, sp}
      58:	6d2d205d 	stcvs	0, cr2, [sp, #-372]!	; 0xfffffe8c
      5c:	616f6c66 	cmnvs	pc, r6, ror #24
      60:	62612d74 	rsbvs	r2, r1, #116, 26	; 0x1d00
      64:	61683d69 	cmnvs	r8, r9, ror #26
      68:	2d206472 	cfstrscs	mvf6, [r0, #-456]!	; 0xfffffe38
      6c:	7570666d 	ldrbvc	r6, [r0, #-1645]!	; 0xfffff993
      70:	7066763d 	rsbvc	r7, r6, sp, lsr r6
      74:	666d2d20 	strbtvs	r2, [sp], -r0, lsr #26
      78:	74616f6c 	strbtvc	r6, [r1], #-3948	; 0xfffff094
      7c:	6962612d 	stmdbvs	r2!, {r0, r2, r3, r5, r8, sp, lr}^
      80:	7261683d 	rsbvc	r6, r1, #3997696	; 0x3d0000
      84:	6d2d2064 	stcvs	0, cr2, [sp, #-400]!	; 0xfffffe70
      88:	3d757066 	ldclcc	0, cr7, [r5, #-408]!	; 0xfffffe68
      8c:	20706676 	rsbscs	r6, r0, r6, ror r6
      90:	75746d2d 	ldrbvc	r6, [r4, #-3373]!	; 0xfffff2d3
      94:	613d656e 	teqvs	sp, lr, ror #10
      98:	31316d72 	teqcc	r1, r2, ror sp
      9c:	7a6a3637 	bvc	1a8d980 <_bss_end+0x1a8431c>
      a0:	20732d66 	rsbscs	r2, r3, r6, ror #26
      a4:	72616d2d 	rsbvc	r6, r1, #2880	; 0xb40
      a8:	6d2d206d 	stcvs	0, cr2, [sp, #-436]!	; 0xfffffe4c
      ac:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
      b0:	6d72613d 	ldfvse	f6, [r2, #-244]!	; 0xffffff0c
      b4:	6b7a3676 	blvs	1e8da94 <_bss_end+0x1e84430>
      b8:	2070662b 	rsbscs	r6, r0, fp, lsr #12
      bc:	2d20672d 	stccs	7, cr6, [r0, #-180]!	; 0xffffff4c
      c0:	4f2d2067 	svcmi	0x002d2067
      c4:	4f2d2030 	svcmi	0x002d2030
      c8:	682f0030 	stmdavs	pc!, {r4, r5}	; <UNPREDICTABLE>
      cc:	2f656d6f 	svccs	0x00656d6f
      d0:	6b72616d 	blvs	1c9868c <_bss_end+0x1c8f028>
      d4:	6164766f 	cmnvs	r4, pc, ror #12
      d8:	6f6b532f 	svcvs	0x006b532f
      dc:	722f616c 	eorvc	r6, pc, #108, 2
      e0:	736f7065 	cmnvc	pc, #101	; 0x65
      e4:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
      e8:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
      ec:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
      f0:	6372732f 	cmnvs	r2, #-1140850688	; 0xbc000000
      f4:	7878632f 	ldmdavc	r8!, {r0, r1, r2, r3, r5, r8, r9, sp, lr}^
      f8:	7070632e 	rsbsvc	r6, r0, lr, lsr #6
      fc:	635f5f00 	cmpvs	pc, #0, 30
     100:	675f6178 			; <UNDEFINED> instruction: 0x675f6178
     104:	64726175 	ldrbtvs	r6, [r2], #-373	; 0xfffffe8b
     108:	6c65725f 	sfmvs	f7, 2, [r5], #-380	; 0xfffffe84
     10c:	65736165 	ldrbvs	r6, [r3, #-357]!	; 0xfffffe9b
     110:	635f5f00 	cmpvs	pc, #0, 30
     114:	675f6178 			; <UNDEFINED> instruction: 0x675f6178
     118:	64726175 	ldrbtvs	r6, [r2], #-373	; 0xfffffe8b
     11c:	6f62615f 	svcvs	0x0062615f
     120:	5f007472 	svcpl	0x00007472
     124:	6f73645f 	svcvs	0x0073645f
     128:	6e61685f 	mcrvs	8, 3, r6, cr1, cr15, {2}
     12c:	00656c64 	rsbeq	r6, r5, r4, ror #24
     130:	78635f5f 	stmdavc	r3!, {r0, r1, r2, r3, r4, r6, r8, r9, sl, fp, ip, lr}^
     134:	74615f61 	strbtvc	r5, [r1], #-3937	; 0xfffff09f
     138:	74697865 	strbtvc	r7, [r9], #-2149	; 0xfffff79b
     13c:	6f682f00 	svcvs	0x00682f00
     140:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; ffffff94 <_bss_end+0xffff6930>
     144:	6f6b7261 	svcvs	0x006b7261
     148:	2f616476 	svccs	0x00616476
     14c:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
     150:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
     154:	2f736f70 	svccs	0x00736f70
     158:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
     15c:	6975622f 	ldmdbvs	r5!, {r0, r1, r2, r3, r5, r9, sp, lr}^
     160:	5f00646c 	svcpl	0x0000646c
     164:	7878635f 	ldmdavc	r8!, {r0, r1, r2, r3, r4, r6, r8, r9, sp, lr}^
     168:	76696261 	strbtvc	r6, [r9], -r1, ror #4
     16c:	5f5f0031 	svcpl	0x005f0031
     170:	5f617863 	svcpl	0x00617863
     174:	65727570 	ldrbvs	r7, [r2, #-1392]!	; 0xfffffa90
     178:	7269765f 	rsbvc	r7, r9, #99614720	; 0x5f00000
     17c:	6c617574 	cfstr64vs	mvdx7, [r1], #-464	; 0xfffffe30
     180:	615f5f00 	cmpvs	pc, r0, lsl #30
     184:	69626165 	stmdbvs	r2!, {r0, r2, r5, r6, r8, sp, lr}^
     188:	776e755f 			; <UNDEFINED> instruction: 0x776e755f
     18c:	5f646e69 	svcpl	0x00646e69
     190:	5f707063 	svcpl	0x00707063
     194:	00317270 	eorseq	r7, r1, r0, ror r2
     198:	75675f5f 	strbvc	r5, [r7, #-3935]!	; 0xfffff0a1
     19c:	00647261 	rsbeq	r7, r4, r1, ror #4
     1a0:	676e6f6c 	strbvs	r6, [lr, -ip, ror #30]!
     1a4:	6e6f6c20 	cdpvs	12, 6, cr6, cr15, cr0, {1}
     1a8:	6e692067 	cdpvs	0, 6, cr2, cr9, cr7, {3}
     1ac:	69540074 	ldmdbvs	r4, {r2, r4, r5, r6}^
     1b0:	5f72656d 	svcpl	0x0072656d
     1b4:	65736142 	ldrbvs	r6, [r3, #-322]!	; 0xfffffebe
     1b8:	6f687300 	svcvs	0x00687300
     1bc:	69207472 	stmdbvs	r0!, {r1, r4, r5, r6, sl, ip, sp, lr}
     1c0:	5300746e 	movwpl	r7, #1134	; 0x46e
     1c4:	5f314950 	svcpl	0x00314950
     1c8:	4b454550 	blmi	1151710 <_bss_end+0x11480ac>
     1cc:	49505300 	ldmdbmi	r0, {r8, r9, ip, lr}^
     1d0:	54535f31 	ldrbpl	r5, [r3], #-3889	; 0xfffff0cf
     1d4:	6d005441 	cfstrsvs	mvf5, [r0, #-260]	; 0xfffffefc
     1d8:	5f585541 	svcpl	0x00585541
     1dc:	00676552 	rsbeq	r6, r7, r2, asr r5
     1e0:	344e5a5f 	strbcc	r5, [lr], #-2655	; 0xfffff5a1
     1e4:	58554143 	ldmdapl	r5, {r0, r1, r6, r8, lr}^
     1e8:	65473231 	strbvs	r3, [r7, #-561]	; 0xfffffdcf
     1ec:	65525f74 	ldrbvs	r5, [r2, #-3956]	; 0xfffff08c
     1f0:	74736967 	ldrbtvc	r6, [r3], #-2407	; 0xfffff699
     1f4:	4e457265 	cdpmi	2, 4, cr7, cr5, cr5, {3}
     1f8:	6c616833 	stclvs	8, cr6, [r1], #-204	; 0xffffff34
     1fc:	58554137 	ldmdapl	r5, {r0, r1, r2, r4, r5, r8, lr}^
     200:	6765525f 			; <UNDEFINED> instruction: 0x6765525f
     204:	61760045 	cmnvs	r6, r5, asr #32
     208:	0065756c 	rsbeq	r7, r5, ip, ror #10
     20c:	5f787561 	svcpl	0x00787561
     210:	65736162 	ldrbvs	r6, [r3, #-354]!	; 0xfffffe9e
     214:	6e697500 	cdpvs	5, 6, cr7, cr9, cr0, {0}
     218:	5f323374 	svcpl	0x00323374
     21c:	4e450074 	mcrmi	0, 2, r0, cr5, cr4, {3}
     220:	454c4241 	strbmi	r4, [ip, #-577]	; 0xfffffdbf
     224:	5a5f0053 	bpl	17c0378 <_bss_end+0x17b6d14>
     228:	4143344e 	cmpmi	r3, lr, asr #8
     22c:	45365855 	ldrmi	r5, [r6, #-2133]!	; 0xfffff7ab
     230:	6c62616e 	stfvse	f6, [r2], #-440	; 0xfffffe48
     234:	334e4565 	movtcc	r4, #58725	; 0xe565
     238:	316c6168 	cmncc	ip, r8, ror #2
     23c:	58554135 	ldmdapl	r5, {r0, r2, r4, r5, r8, lr}^
     240:	7265505f 	rsbvc	r5, r5, #95	; 0x5f
     244:	65687069 	strbvs	r7, [r8, #-105]!	; 0xffffff97
     248:	736c6172 	cmnvc	ip, #-2147483620	; 0x8000001c
     24c:	55410045 	strbpl	r0, [r1, #-69]	; 0xffffffbb
     250:	65505f58 	ldrbvs	r5, [r0, #-3928]	; 0xfffff0a8
     254:	68706972 	ldmdavs	r0!, {r1, r4, r5, r6, r8, fp, sp, lr}^
     258:	6c617265 	sfmvs	f7, 2, [r1], #-404	; 0xfffffe6c
     25c:	5a5f0073 	bpl	17c0430 <_bss_end+0x17b6dcc>
     260:	4143344e 	cmpmi	r3, lr, asr #8
     264:	34435855 	strbcc	r5, [r3], #-2133	; 0xfffff7ab
     268:	5f006a45 	svcpl	0x00006a45
     26c:	43344e5a 	teqmi	r4, #1440	; 0x5a0
     270:	31585541 	cmpcc	r8, r1, asr #10
     274:	74655332 	strbtvc	r5, [r5], #-818	; 0xfffffcce
     278:	6765525f 			; <UNDEFINED> instruction: 0x6765525f
     27c:	65747369 	ldrbvs	r7, [r4, #-873]!	; 0xfffffc97
     280:	334e4572 	movtcc	r4, #58738	; 0xe572
     284:	376c6168 	strbcc	r6, [ip, -r8, ror #2]!
     288:	5f585541 	svcpl	0x00585541
     28c:	45676552 	strbmi	r6, [r7, #-1362]!	; 0xfffffaae
     290:	5053006a 	subspl	r0, r3, sl, rrx
     294:	495f3049 	ldmdbmi	pc, {r0, r3, r6, ip, sp}^	; <UNPREDICTABLE>
     298:	475f004f 	ldrbmi	r0, [pc, -pc, asr #32]
     29c:	41424f4c 	cmpmi	r2, ip, asr #30
     2a0:	735f5f4c 	cmpvc	pc, #76, 30	; 0x130
     2a4:	495f6275 	ldmdbmi	pc, {r0, r2, r4, r5, r6, r9, sp, lr}^	; <UNPREDICTABLE>
     2a8:	5541735f 	strbpl	r7, [r1, #-863]	; 0xfffffca1
     2ac:	50530058 	subspl	r0, r3, r8, asr r0
     2b0:	435f3049 	cmpmi	pc, #73	; 0x49
     2b4:	304c544e 	subcc	r5, ip, lr, asr #8
     2b8:	49505300 	ldmdbmi	r0, {r8, r9, ip, lr}^
     2bc:	4e435f30 	mcrmi	15, 2, r5, cr3, cr0, {1}
     2c0:	00314c54 	eorseq	r4, r1, r4, asr ip
     2c4:	69726550 	ldmdbvs	r2!, {r4, r6, r8, sl, sp, lr}^
     2c8:	72656870 	rsbvc	r6, r5, #112, 16	; 0x700000
     2cc:	425f6c61 	subsmi	r6, pc, #24832	; 0x6100
     2d0:	00657361 	rsbeq	r7, r5, r1, ror #6
     2d4:	6e695f5f 	mcrvs	15, 3, r5, cr9, cr15, {2}
     2d8:	61697469 	cmnvs	r9, r9, ror #8
     2dc:	657a696c 	ldrbvs	r6, [sl, #-2412]!	; 0xfffff694
     2e0:	5300705f 	movwpl	r7, #95	; 0x5f
     2e4:	5f314950 	svcpl	0x00314950
     2e8:	4c544e43 	mrrcmi	14, 4, r4, r4, cr3	; <UNPREDICTABLE>
     2ec:	50530030 	subspl	r0, r3, r0, lsr r0
     2f0:	435f3149 	cmpmi	pc, #1073741842	; 0x40000012
     2f4:	314c544e 	cmpcc	ip, lr, asr #8
     2f8:	736e7500 	cmnvc	lr, #0, 10
     2fc:	656e6769 	strbvs	r6, [lr, #-1897]!	; 0xfffff897
     300:	68632064 	stmdavs	r3!, {r2, r5, r6, sp}^
     304:	2f007261 	svccs	0x00007261
     308:	656d6f68 	strbvs	r6, [sp, #-3944]!	; 0xfffff098
     30c:	72616d2f 	rsbvc	r6, r1, #3008	; 0xbc0
     310:	64766f6b 	ldrbtvs	r6, [r6], #-3947	; 0xfffff095
     314:	6b532f61 	blvs	14cc0a0 <_bss_end+0x14c2a3c>
     318:	2f616c6f 	svccs	0x00616c6f
     31c:	6f706572 	svcvs	0x00706572
     320:	736f2f73 	cmnvc	pc, #460	; 0x1cc
     324:	6b2f352d 	blvs	bcd7e0 <_bss_end+0xbc417c>
     328:	656e7265 	strbvs	r7, [lr, #-613]!	; 0xfffffd9b
     32c:	72732f6c 	rsbsvc	r2, r3, #108, 30	; 0x1b0
     330:	72642f63 	rsbvc	r2, r4, #396	; 0x18c
     334:	72657669 	rsbvc	r7, r5, #110100480	; 0x6900000
     338:	63622f73 	cmnvs	r2, #460	; 0x1cc
     33c:	75615f6d 	strbvc	r5, [r1, #-3949]!	; 0xfffff093
     340:	70632e78 	rsbvc	r2, r3, r8, ror lr
     344:	554d0070 	strbpl	r0, [sp, #-112]	; 0xffffff90
     348:	5245495f 	subpl	r4, r5, #1556480	; 0x17c000
     34c:	5f554d00 	svcpl	0x00554d00
     350:	0052434d 	subseq	r4, r2, sp, asr #6
     354:	4f495047 	svcmi	0x00495047
     358:	7361425f 	cmnvc	r1, #-268435451	; 0xf0000005
     35c:	5a5f0065 	bpl	17c04f8 <_bss_end+0x17b6e94>
     360:	4143344e 	cmpmi	r3, lr, asr #8
     364:	32435855 	subcc	r5, r3, #5570560	; 0x550000
     368:	47006a45 	strmi	r6, [r0, -r5, asr #20]
     36c:	525f7465 	subspl	r7, pc, #1694498816	; 0x65000000
     370:	73696765 	cmnvc	r9, #26476544	; 0x1940000
     374:	00726574 	rsbseq	r6, r2, r4, ror r5
     378:	495f554d 	ldmdbmi	pc, {r0, r2, r3, r6, r8, sl, ip, lr}^	; <UNPREDICTABLE>
     37c:	4d005249 	sfmmi	f5, 4, [r0, #-292]	; 0xfffffedc
     380:	4e435f55 	mcrmi	15, 2, r5, cr3, cr5, {2}
     384:	53004c54 	movwpl	r4, #3156	; 0xc54
     388:	00314950 	eorseq	r4, r1, r0, asr r9
     38c:	32495053 	subcc	r5, r9, #83	; 0x53
     390:	6f687300 	svcvs	0x00687300
     394:	75207472 	strvc	r7, [r0, #-1138]!	; 0xfffffb8e
     398:	6769736e 	strbvs	r7, [r9, -lr, ror #6]!
     39c:	2064656e 	rsbcs	r6, r4, lr, ror #10
     3a0:	00746e69 	rsbseq	r6, r4, r9, ror #28
     3a4:	31495053 	qdaddcc	r5, r3, r9
     3a8:	004f495f 	subeq	r4, pc, pc, asr r9	; <UNPREDICTABLE>
     3ac:	4c5f554d 	cfldr64mi	mvdx5, [pc], {77}	; 0x4d
     3b0:	53005243 	movwpl	r5, #579	; 0x243
     3b4:	5f304950 	svcpl	0x00304950
     3b8:	4b454550 	blmi	1151900 <_bss_end+0x114829c>
     3bc:	5f554d00 	svcpl	0x00554d00
     3c0:	44554142 	ldrbmi	r4, [r5], #-322	; 0xfffffebe
     3c4:	49505300 	ldmdbmi	r0, {r8, r9, ip, lr}^
     3c8:	54535f30 	ldrbpl	r5, [r3], #-3888	; 0xfffff0d0
     3cc:	47005441 	strmi	r5, [r0, -r1, asr #8]
     3d0:	5f4f4950 	svcpl	0x004f4950
     3d4:	5f6e6950 	svcpl	0x006e6950
     3d8:	6e756f43 	cdpvs	15, 7, cr6, cr5, cr3, {2}
     3dc:	5f5f0074 	svcpl	0x005f0074
     3e0:	74617473 	strbtvc	r7, [r1], #-1139	; 0xfffffb8d
     3e4:	695f6369 	ldmdbvs	pc, {r0, r3, r5, r6, r8, r9, sp, lr}^	; <UNPREDICTABLE>
     3e8:	6974696e 	ldmdbvs	r4!, {r1, r2, r3, r5, r6, r8, fp, sp, lr}^
     3ec:	7a696c61 	bvc	1a5b578 <_bss_end+0x1a51f14>
     3f0:	6f697461 	svcvs	0x00697461
     3f4:	6e615f6e 	cdpvs	15, 6, cr5, cr1, cr14, {3}
     3f8:	65645f64 	strbvs	r5, [r4, #-3940]!	; 0xfffff09c
     3fc:	75727473 	ldrbvc	r7, [r2, #-1139]!	; 0xfffffb8d
     400:	6f697463 	svcvs	0x00697463
     404:	00305f6e 	eorseq	r5, r0, lr, ror #30
     408:	72705f5f 	rsbsvc	r5, r0, #380	; 0x17c
     40c:	69726f69 	ldmdbvs	r2!, {r0, r3, r5, r6, r8, r9, sl, fp, sp, lr}^
     410:	74007974 	strvc	r7, [r0], #-2420	; 0xfffff68c
     414:	00736968 	rsbseq	r6, r3, r8, ror #18
     418:	65746e49 	ldrbvs	r6, [r4, #-3657]!	; 0xfffff1b7
     41c:	70757272 	rsbsvc	r7, r5, r2, ror r2
     420:	6f435f74 	svcvs	0x00435f74
     424:	6f72746e 	svcvs	0x0072746e
     428:	72656c6c 	rsbvc	r6, r5, #108, 24	; 0x6c00
     42c:	7361425f 	cmnvc	r1, #-268435451	; 0xf0000005
     430:	65530065 	ldrbvs	r0, [r3, #-101]	; 0xffffff9b
     434:	65525f74 	ldrbvs	r5, [r2, #-3956]	; 0xfffff08c
     438:	74736967 	ldrbtvc	r6, [r3], #-2407	; 0xfffff699
     43c:	4d007265 	sfmmi	f7, 4, [r0, #-404]	; 0xfffffe6c
     440:	534d5f55 	movtpl	r5, #57173	; 0xdf55
     444:	55410052 	strbpl	r0, [r1, #-82]	; 0xffffffae
     448:	61425f58 	cmpvs	r2, r8, asr pc
     44c:	4d006573 	cfstr32mi	mvfx6, [r0, #-460]	; 0xfffffe34
     450:	54535f55 	ldrbpl	r5, [r3], #-3925	; 0xfffff0ab
     454:	61005441 	tstvs	r0, r1, asr #8
     458:	705f7875 	subsvc	r7, pc, r5, ror r8	; <UNPREDICTABLE>
     45c:	70697265 	rsbvc	r7, r9, r5, ror #4
     460:	61726568 	cmnvs	r2, r8, ror #10
     464:	554d006c 	strbpl	r0, [sp, #-108]	; 0xffffff94
     468:	004f495f 	subeq	r4, pc, pc, asr r9	; <UNPREDICTABLE>
     46c:	696e694d 	stmdbvs	lr!, {r0, r2, r3, r6, r8, fp, sp, lr}^
     470:	54524155 	ldrbpl	r4, [r2], #-341	; 0xfffffeab
     474:	5f554d00 	svcpl	0x00554d00
     478:	0052534c 	subseq	r5, r2, ip, asr #6
     47c:	5f676572 	svcpl	0x00676572
     480:	00786469 	rsbseq	r6, r8, r9, ror #8
     484:	61666544 	cmnvs	r6, r4, asr #10
     488:	5f746c75 	svcpl	0x00746c75
     48c:	636f6c43 	cmnvs	pc, #17152	; 0x4300
     490:	61525f6b 	cmpvs	r2, fp, ror #30
     494:	5f006574 	svcpl	0x00006574
     498:	43344e5a 	teqmi	r4, #1440	; 0x5a0
     49c:	37585541 	ldrbcc	r5, [r8, -r1, asr #10]
     4a0:	61736944 	cmnvs	r3, r4, asr #18
     4a4:	45656c62 	strbmi	r6, [r5, #-3170]!	; 0xfffff39e
     4a8:	6168334e 	cmnvs	r8, lr, asr #6
     4ac:	4135316c 	teqmi	r5, ip, ror #2
     4b0:	505f5855 	subspl	r5, pc, r5, asr r8	; <UNPREDICTABLE>
     4b4:	70697265 	rsbvc	r7, r9, r5, ror #4
     4b8:	61726568 	cmnvs	r2, r8, ror #10
     4bc:	0045736c 	subeq	r7, r5, ip, ror #6
     4c0:	535f554d 	cmppl	pc, #322961408	; 0x13400000
     4c4:	54415243 	strbpl	r5, [r1], #-579	; 0xfffffdbd
     4c8:	5f004843 	svcpl	0x00004843
     4cc:	314b4e5a 	cmpcc	fp, sl, asr lr
     4d0:	50474333 	subpl	r4, r7, r3, lsr r3
     4d4:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     4d8:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     4dc:	38317265 	ldmdacc	r1!, {r0, r2, r5, r6, r9, ip, sp, lr}
     4e0:	5f746547 	svcpl	0x00746547
     4e4:	454c5047 	strbmi	r5, [ip, #-71]	; 0xffffffb9
     4e8:	6f4c5f56 	svcvs	0x004c5f56
     4ec:	69746163 	ldmdbvs	r4!, {r0, r1, r5, r6, r8, sp, lr}^
     4f0:	6a456e6f 	bvs	115beb4 <_bss_end+0x1152850>
     4f4:	30536a52 	subscc	r6, r3, r2, asr sl
     4f8:	5047005f 	subpl	r0, r7, pc, asr r0
     4fc:	30544553 	subscc	r4, r4, r3, asr r5
     500:	53504700 	cmppl	r0, #0, 14
     504:	00315445 	eorseq	r5, r1, r5, asr #8
     508:	53465047 	movtpl	r5, #24647	; 0x6047
     50c:	00304c45 	eorseq	r4, r0, r5, asr #24
     510:	53465047 	movtpl	r5, #24647	; 0x6047
     514:	00314c45 	eorseq	r4, r1, r5, asr #24
     518:	53465047 	movtpl	r5, #24647	; 0x6047
     51c:	00324c45 	eorseq	r4, r2, r5, asr #24
     520:	53465047 	movtpl	r5, #24647	; 0x6047
     524:	00334c45 	eorseq	r4, r3, r5, asr #24
     528:	53465047 	movtpl	r5, #24647	; 0x6047
     52c:	00344c45 	eorseq	r4, r4, r5, asr #24
     530:	53465047 	movtpl	r5, #24647	; 0x6047
     534:	00354c45 	eorseq	r4, r5, r5, asr #24
     538:	45465047 	strbmi	r5, [r6, #-71]	; 0xffffffb9
     53c:	4700314e 	strmi	r3, [r0, -lr, asr #2]
     540:	44555050 	ldrbmi	r5, [r5], #-80	; 0xffffffb0
     544:	304b4c43 	subcc	r4, fp, r3, asr #24
     548:	50504700 	subspl	r4, r0, r0, lsl #14
     54c:	4c434455 	cfstrdmi	mvd4, [r3], {85}	; 0x55
     550:	4700314b 	strmi	r3, [r0, -fp, asr #2]
     554:	4e455250 	mcrmi	2, 2, r5, cr5, cr0, {2}
     558:	50470030 	subpl	r0, r7, r0, lsr r0
     55c:	314e4552 	cmpcc	lr, r2, asr r5
     560:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     564:	47433331 	smlaldxmi	r3, r3, r1, r3	; <UNPREDICTABLE>
     568:	5f4f4950 	svcpl	0x004f4950
     56c:	646e6148 	strbtvs	r6, [lr], #-328	; 0xfffffeb8
     570:	3172656c 	cmncc	r2, ip, ror #10
     574:	74655330 	strbtvc	r5, [r5], #-816	; 0xfffffcd0
     578:	74754f5f 	ldrbtvc	r4, [r5], #-3935	; 0xfffff0a1
     57c:	45747570 	ldrbmi	r7, [r4, #-1392]!	; 0xfffffa90
     580:	5f00626a 	svcpl	0x0000626a
     584:	314b4e5a 	cmpcc	fp, sl, asr lr
     588:	50474333 	subpl	r4, r7, r3, lsr r3
     58c:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     590:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     594:	39317265 	ldmdbcc	r1!, {r0, r2, r5, r6, r9, ip, sp, lr}
     598:	5f746547 	svcpl	0x00746547
     59c:	53465047 	movtpl	r5, #24647	; 0x6047
     5a0:	4c5f4c45 	mrrcmi	12, 4, r4, pc, cr5	; <UNPREDICTABLE>
     5a4:	7461636f 	strbtvc	r6, [r1], #-879	; 0xfffffc91
     5a8:	456e6f69 	strbmi	r6, [lr, #-3945]!	; 0xfffff097
     5ac:	536a526a 	cmnpl	sl, #-1610612730	; 0xa0000006
     5b0:	6d005f30 	stcvs	15, cr5, [r0, #-192]	; 0xffffff40
     5b4:	4f495047 	svcmi	0x00495047
     5b8:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     5bc:	47433331 	smlaldxmi	r3, r3, r1, r3	; <UNPREDICTABLE>
     5c0:	5f4f4950 	svcpl	0x004f4950
     5c4:	646e6148 	strbtvs	r6, [lr], #-328	; 0xfffffeb8
     5c8:	4372656c 	cmnmi	r2, #108, 10	; 0x1b000000
     5cc:	006a4534 	rsbeq	r4, sl, r4, lsr r5
     5d0:	746e6975 	strbtvc	r6, [lr], #-2421	; 0xfffff68b
     5d4:	00745f38 	rsbseq	r5, r4, r8, lsr pc
     5d8:	44455047 	strbmi	r5, [r5], #-71	; 0xffffffb9
     5dc:	47003053 	smlsdmi	r0, r3, r0, r3
     5e0:	53444550 	movtpl	r4, #17744	; 0x4550
     5e4:	6f620031 	svcvs	0x00620031
     5e8:	55006c6f 	strpl	r6, [r0, #-3183]	; 0xfffff391
     5ec:	6570736e 	ldrbvs	r7, [r0, #-878]!	; 0xfffffc92
     5f0:	69666963 	stmdbvs	r6!, {r0, r1, r5, r6, r8, fp, sp, lr}^
     5f4:	47006465 	strmi	r6, [r0, -r5, ror #8]
     5f8:	4e454c50 	mcrmi	12, 2, r4, cr5, cr0, {2}
     5fc:	75660030 	strbvc	r0, [r6, #-48]!	; 0xffffffd0
     600:	4700636e 	strmi	r6, [r0, -lr, ror #6]
     604:	475f7465 	ldrbmi	r7, [pc, -r5, ror #8]
     608:	5f4f4950 	svcpl	0x004f4950
     60c:	636e7546 	cmnvs	lr, #293601280	; 0x11800000
     610:	6e6f6974 			; <UNDEFINED> instruction: 0x6e6f6974
     614:	50474300 	subpl	r4, r7, r0, lsl #6
     618:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     61c:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     620:	47007265 	strmi	r7, [r0, -r5, ror #4]
     624:	475f7465 	ldrbmi	r7, [pc, -r5, ror #8]
     628:	54455350 	strbpl	r5, [r5], #-848	; 0xfffffcb0
     62c:	636f4c5f 	cmnvs	pc, #24320	; 0x5f00
     630:	6f697461 	svcvs	0x00697461
     634:	682f006e 	stmdavs	pc!, {r1, r2, r3, r5, r6}	; <UNPREDICTABLE>
     638:	2f656d6f 	svccs	0x00656d6f
     63c:	6b72616d 	blvs	1c98bf8 <_bss_end+0x1c8f594>
     640:	6164766f 	cmnvs	r4, pc, ror #12
     644:	6f6b532f 	svcvs	0x006b532f
     648:	722f616c 	eorvc	r6, pc, #108, 2
     64c:	736f7065 	cmnvc	pc, #101	; 0x65
     650:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
     654:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
     658:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
     65c:	6372732f 	cmnvs	r2, #-1140850688	; 0xbc000000
     660:	6972642f 	ldmdbvs	r2!, {r0, r1, r2, r3, r5, sl, sp, lr}^
     664:	73726576 	cmnvc	r2, #494927872	; 0x1d800000
     668:	6970672f 	ldmdbvs	r0!, {r0, r1, r2, r3, r5, r8, r9, sl, sp, lr}^
     66c:	70632e6f 	rsbvc	r2, r3, pc, ror #28
     670:	50470070 	subpl	r0, r7, r0, ror r0
     674:	00445550 	subeq	r5, r4, r0, asr r5
     678:	5f746547 	svcpl	0x00746547
     67c:	454c5047 	strbmi	r5, [ip, #-71]	; 0xffffffb9
     680:	6f4c5f56 	svcvs	0x004c5f56
     684:	69746163 	ldmdbvs	r4!, {r0, r1, r5, r6, r8, sp, lr}^
     688:	5f006e6f 	svcpl	0x00006e6f
     68c:	314b4e5a 	cmpcc	fp, sl, asr lr
     690:	50474333 	subpl	r4, r7, r3, lsr r3
     694:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     698:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     69c:	37317265 	ldrcc	r7, [r1, -r5, ror #4]!
     6a0:	5f746547 	svcpl	0x00746547
     6a4:	4f495047 	svcmi	0x00495047
     6a8:	6e75465f 	mrcvs	6, 3, r4, cr5, cr15, {2}
     6ac:	6f697463 	svcvs	0x00697463
     6b0:	006a456e 	rsbeq	r4, sl, lr, ror #10
     6b4:	314e5a5f 	cmpcc	lr, pc, asr sl
     6b8:	50474333 	subpl	r4, r7, r3, lsr r3
     6bc:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     6c0:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     6c4:	32437265 	subcc	r7, r3, #1342177286	; 0x50000006
     6c8:	47006a45 	strmi	r6, [r0, -r5, asr #20]
     6cc:	45464150 	strbmi	r4, [r6, #-336]	; 0xfffffeb0
     6d0:	4700304e 	strmi	r3, [r0, -lr, asr #32]
     6d4:	45464150 	strbmi	r4, [r6, #-336]	; 0xfffffeb0
     6d8:	5f00314e 	svcpl	0x0000314e
     6dc:	314b4e5a 	cmpcc	fp, sl, asr lr
     6e0:	50474333 	subpl	r4, r7, r3, lsr r3
     6e4:	485f4f49 	ldmdami	pc, {r0, r3, r6, r8, r9, sl, fp, lr}^	; <UNPREDICTABLE>
     6e8:	6c646e61 	stclvs	14, cr6, [r4], #-388	; 0xfffffe7c
     6ec:	38317265 	ldmdacc	r1!, {r0, r2, r5, r6, r9, ip, sp, lr}
     6f0:	5f746547 	svcpl	0x00746547
     6f4:	4c435047 	mcrrmi	0, 4, r5, r3, cr7
     6f8:	6f4c5f52 	svcvs	0x004c5f52
     6fc:	69746163 	ldmdbvs	r4!, {r0, r1, r5, r6, r8, sp, lr}^
     700:	6a456e6f 	bvs	115c0c4 <_bss_end+0x1152a60>
     704:	30536a52 	subscc	r6, r3, r2, asr sl
     708:	475f005f 			; <UNDEFINED> instruction: 0x475f005f
     70c:	41424f4c 	cmpmi	r2, ip, asr #30
     710:	735f5f4c 	cmpvc	pc, #76, 30	; 0x130
     714:	495f6275 	ldmdbmi	pc, {r0, r2, r4, r5, r6, r9, sp, lr}^	; <UNPREDICTABLE>
     718:	5047735f 	subpl	r7, r7, pc, asr r3
     71c:	47004f49 	strmi	r4, [r0, -r9, asr #30]
     720:	524c4350 	subpl	r4, ip, #80, 6	; 0x40000001
     724:	50470031 	subpl	r0, r7, r1, lsr r0
     728:	4e455241 	cdpmi	2, 4, cr5, cr5, cr1, {2}
     72c:	50470030 	subpl	r0, r7, r0, lsr r0
     730:	4e455241 	cdpmi	2, 4, cr5, cr5, cr1, {2}
     734:	50470031 	subpl	r0, r7, r1, lsr r0
     738:	304e4548 	subcc	r4, lr, r8, asr #10
     73c:	48504700 	ldmdami	r0, {r8, r9, sl, lr}^
     740:	00314e45 	eorseq	r4, r1, r5, asr #28
     744:	6f697067 	svcvs	0x00697067
     748:	7361625f 	cmnvc	r1, #-268435451	; 0xf0000005
     74c:	64615f65 	strbtvs	r5, [r1], #-3941	; 0xfffff09b
     750:	47007264 	strmi	r7, [r0, -r4, ror #4]
     754:	4e454c50 	mcrmi	12, 2, r4, cr5, cr0, {2}
     758:	65470031 	strbvs	r0, [r7, #-49]	; 0xffffffcf
     75c:	50475f74 	subpl	r5, r7, r4, ror pc
     760:	4c455346 	mcrrmi	3, 4, r5, r5, cr6
     764:	636f4c5f 	cmnvs	pc, #24320	; 0x5f00
     768:	6f697461 	svcvs	0x00697461
     76c:	5047006e 	subpl	r0, r7, lr, rrx
     770:	525f4f49 	subspl	r4, pc, #292	; 0x124
     774:	5f006765 	svcpl	0x00006765
     778:	33314e5a 	teqcc	r1, #1440	; 0x5a0
     77c:	49504743 	ldmdbmi	r0, {r0, r1, r6, r8, r9, sl, lr}^
     780:	61485f4f 	cmpvs	r8, pc, asr #30
     784:	656c646e 	strbvs	r6, [ip, #-1134]!	; 0xfffffb92
     788:	53373172 	teqpl	r7, #-2147483620	; 0x8000001c
     78c:	475f7465 	ldrbmi	r7, [pc, -r5, ror #8]
     790:	5f4f4950 	svcpl	0x004f4950
     794:	636e7546 	cmnvs	lr, #293601280	; 0x11800000
     798:	6e6f6974 			; <UNDEFINED> instruction: 0x6e6f6974
     79c:	34316a45 	ldrtcc	r6, [r1], #-2629	; 0xfffff5bb
     7a0:	4950474e 	ldmdbmi	r0, {r1, r2, r3, r6, r8, r9, sl, lr}^
     7a4:	75465f4f 	strbvc	r5, [r6, #-3919]	; 0xfffff0b1
     7a8:	6974636e 	ldmdbvs	r4!, {r1, r2, r3, r5, r6, r8, r9, sp, lr}^
     7ac:	41006e6f 	tstmi	r0, pc, ror #28
     7b0:	325f746c 	subscc	r7, pc, #108, 8	; 0x6c000000
     7b4:	4c504700 	mrrcmi	7, 0, r4, r0, cr0
     7b8:	00305645 	eorseq	r5, r0, r5, asr #12
     7bc:	454c5047 	strbmi	r5, [ip, #-71]	; 0xffffffb9
     7c0:	53003156 	movwpl	r3, #342	; 0x156
     7c4:	475f7465 	ldrbmi	r7, [pc, -r5, ror #8]
     7c8:	5f4f4950 	svcpl	0x004f4950
     7cc:	636e7546 	cmnvs	lr, #293601280	; 0x11800000
     7d0:	6e6f6974 			; <UNDEFINED> instruction: 0x6e6f6974
     7d4:	74696200 	strbtvc	r6, [r9], #-512	; 0xfffffe00
     7d8:	7864695f 	stmdavc	r4!, {r0, r1, r2, r3, r4, r6, r8, fp, sp, lr}^
     7dc:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     7e0:	4333314b 	teqmi	r3, #-1073741806	; 0xc0000012
     7e4:	4f495047 	svcmi	0x00495047
     7e8:	6e61485f 	mcrvs	8, 3, r4, cr1, cr15, {2}
     7ec:	72656c64 	rsbvc	r6, r5, #100, 24	; 0x6400
     7f0:	65473831 	strbvs	r3, [r7, #-2097]	; 0xfffff7cf
     7f4:	50475f74 	subpl	r5, r7, r4, ror pc
     7f8:	5f544553 	svcpl	0x00544553
     7fc:	61636f4c 	cmnvs	r3, ip, asr #30
     800:	6e6f6974 			; <UNDEFINED> instruction: 0x6e6f6974
     804:	6a526a45 	bvs	149b120 <_bss_end+0x1491abc>
     808:	005f3053 	subseq	r3, pc, r3, asr r0	; <UNPREDICTABLE>
     80c:	5f746547 	svcpl	0x00746547
     810:	4c435047 	mcrrmi	0, 4, r5, r3, cr7
     814:	6f4c5f52 	svcvs	0x004c5f52
     818:	69746163 	ldmdbvs	r4!, {r0, r1, r5, r6, r8, sp, lr}^
     81c:	49006e6f 	stmdbmi	r0, {r0, r1, r2, r3, r5, r6, r9, sl, fp, sp, lr}
     820:	7475706e 	ldrbtvc	r7, [r5], #-110	; 0xffffff92
     824:	46504700 	ldrbmi	r4, [r0], -r0, lsl #14
     828:	00304e45 	eorseq	r4, r0, r5, asr #28
     82c:	5f746553 	svcpl	0x00746553
     830:	7074754f 	rsbsvc	r7, r4, pc, asr #10
     834:	41007475 	tstmi	r0, r5, ror r4
     838:	305f746c 	subscc	r7, pc, ip, ror #8
     83c:	746c4100 	strbtvc	r4, [ip], #-256	; 0xffffff00
     840:	4100315f 	tstmi	r0, pc, asr r1
     844:	335f746c 	cmpcc	pc, #108, 8	; 0x6c000000
     848:	746c4100 	strbtvc	r4, [ip], #-256	; 0xffffff00
     84c:	4100345f 	tstmi	r0, pc, asr r4
     850:	355f746c 	ldrbcc	r7, [pc, #-1132]	; 3ec <CPSR_IRQ_INHIBIT+0x36c>
     854:	43504700 	cmpmi	r0, #0, 14
     858:	0030524c 	eorseq	r5, r0, ip, asr #4
     85c:	6d69546d 	cfstrdvs	mvd5, [r9, #-436]!	; 0xfffffe4c
     860:	525f7265 	subspl	r7, pc, #1342177286	; 0x50000006
     864:	00736765 	rsbseq	r6, r3, r5, ror #14
     868:	364e5a5f 			; <UNDEFINED> instruction: 0x364e5a5f
     86c:	6d695443 	cfstrdvs	mvd5, [r9, #-268]!	; 0xfffffef4
     870:	34437265 	strbcc	r7, [r3], #-613	; 0xfffffd9b
     874:	5f006d45 	svcpl	0x00006d45
     878:	43364e5a 	teqmi	r6, #1440	; 0x5a0
     87c:	656d6954 	strbvs	r6, [sp, #-2388]!	; 0xfffff6ac
     880:	49303272 	ldmdbmi	r0!, {r1, r4, r5, r6, r9, ip, sp}
     884:	69545f73 	ldmdbvs	r4, {r0, r1, r4, r5, r6, r8, r9, sl, fp, ip, lr}^
     888:	5f72656d 	svcpl	0x0072656d
     88c:	5f515249 	svcpl	0x00515249
     890:	646e6550 	strbtvs	r6, [lr], #-1360	; 0xfffffab0
     894:	45676e69 	strbmi	r6, [r7, #-3689]!	; 0xfffff197
     898:	52420076 	subpl	r0, r2, #118	; 0x76
     89c:	3034325f 	eorscc	r3, r4, pc, asr r2
     8a0:	72570030 	subsvc	r0, r7, #48	; 0x30
     8a4:	00657469 	rsbeq	r7, r5, r9, ror #8
     8a8:	364e5a5f 			; <UNDEFINED> instruction: 0x364e5a5f
     8ac:	6d695443 	cfstrdvs	mvd5, [r9, #-268]!	; 0xfffffef4
     8b0:	45367265 	ldrmi	r7, [r6, #-613]!	; 0xfffffd9b
     8b4:	6c62616e 	stfvse	f6, [r2], #-440	; 0xfffffe48
     8b8:	46504565 	ldrbmi	r4, [r0], -r5, ror #10
     8bc:	6a457676 	bvs	115e29c <_bss_end+0x1154c38>
     8c0:	544e3631 	strbpl	r3, [lr], #-1585	; 0xfffff9cf
     8c4:	72656d69 	rsbvc	r6, r5, #6720	; 0x1a40
     8c8:	6572505f 	ldrbvs	r5, [r2, #-95]!	; 0xffffffa1
     8cc:	6c616373 	stclvs	3, cr6, [r1], #-460	; 0xfffffe34
     8d0:	49007265 	stmdbmi	r0, {r0, r2, r5, r6, r9, ip, sp, lr}
     8d4:	525f5152 	subspl	r5, pc, #-2147483628	; 0x80000014
     8d8:	6d007761 	stcvs	7, cr7, [r0, #-388]	; 0xfffffe7c
     8dc:	00585541 	subseq	r5, r8, r1, asr #10
     8e0:	5f657250 	svcpl	0x00657250
     8e4:	69766944 	ldmdbvs	r6!, {r2, r6, r8, fp, sp, lr}^
     8e8:	00726564 	rsbseq	r6, r2, r4, ror #10
     8ec:	6d695454 	cfstrdvs	mvd5, [r9, #-336]!	; 0xfffffeb0
     8f0:	435f7265 	cmpmi	pc, #1342177286	; 0x50000006
     8f4:	626c6c61 	rsbvs	r6, ip, #24832	; 0x6100
     8f8:	006b6361 	rsbeq	r6, fp, r1, ror #6
     8fc:	345f5242 	ldrbcc	r5, [pc], #-578	; 904 <CPSR_IRQ_INHIBIT+0x884>
     900:	00303038 	eorseq	r3, r0, r8, lsr r0
     904:	72616843 	rsbvc	r6, r1, #4390912	; 0x430000
     908:	7500375f 	strvc	r3, [r0, #-1887]	; 0xfffff8a1
     90c:	31746e69 	cmncc	r4, r9, ror #28
     910:	00745f36 	rsbseq	r5, r4, r6, lsr pc
     914:	65657266 	strbvs	r7, [r5, #-614]!	; 0xfffffd9a
     918:	6e75725f 	mrcvs	2, 3, r7, cr5, cr15, {2}
     91c:	676e696e 	strbvs	r6, [lr, -lr, ror #18]!
     920:	6572705f 	ldrbvs	r7, [r2, #-95]!	; 0xffffffa1
     924:	6c616373 	stclvs	3, cr6, [r1], #-460	; 0xfffffe34
     928:	53007265 	movwpl	r7, #613	; 0x265
     92c:	435f7465 	cmpmi	pc, #1694498816	; 0x65000000
     930:	5f726168 	svcpl	0x00726168
     934:	676e654c 	strbvs	r6, [lr, -ip, asr #10]!
     938:	43006874 	movwmi	r6, #2164	; 0x874
     93c:	5f726168 	svcpl	0x00726168
     940:	69740038 	ldmdbvs	r4!, {r3, r4, r5}^
     944:	5f72656d 	svcpl	0x0072656d
     948:	62616e65 	rsbvs	r6, r1, #1616	; 0x650
     94c:	0064656c 	rsbeq	r6, r4, ip, ror #10
     950:	73657250 	cmnvc	r5, #80, 4
     954:	656c6163 	strbvs	r6, [ip, #-355]!	; 0xfffffe9d
     958:	00315f72 	eorseq	r5, r1, r2, ror pc
     95c:	4f4c475f 	svcmi	0x004c475f
     960:	5f4c4142 	svcpl	0x004c4142
     964:	6275735f 	rsbsvs	r7, r5, #2080374785	; 0x7c000001
     968:	735f495f 	cmpvc	pc, #1556480	; 0x17c000
     96c:	656d6954 	strbvs	r6, [sp, #-2388]!	; 0xfffff6ac
     970:	55430072 	strbpl	r0, [r3, #-114]	; 0xffffff8e
     974:	00545241 	subseq	r5, r4, r1, asr #4
     978:	65746e69 	ldrbvs	r6, [r4, #-3689]!	; 0xfffff197
     97c:	70757272 	rsbsvc	r7, r5, r2, ror r2
     980:	6e655f74 	mcrvs	15, 3, r5, cr5, cr4, {3}
     984:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
     988:	73490064 	movtvc	r0, #36964	; 0x9064
     98c:	6d69545f 	cfstrdvs	mvd5, [r9, #-380]!	; 0xfffffe84
     990:	495f7265 	ldmdbmi	pc, {r0, r2, r5, r6, r9, ip, sp, lr}^	; <UNPREDICTABLE>
     994:	505f5152 	subspl	r5, pc, r2, asr r1	; <UNPREDICTABLE>
     998:	69646e65 	stmdbvs	r4!, {r0, r2, r5, r6, r9, sl, fp, sp, lr}^
     99c:	6800676e 	stmdavs	r0, {r1, r2, r3, r5, r6, r8, r9, sl, sp, lr}
     9a0:	5f746c61 	svcpl	0x00746c61
     9a4:	645f6e69 	ldrbvs	r6, [pc], #-3689	; 9ac <CPSR_IRQ_INHIBIT+0x92c>
     9a8:	67756265 	ldrbvs	r6, [r5, -r5, ror #4]!
     9ac:	6572625f 	ldrbvs	r6, [r2, #-607]!	; 0xfffffda1
     9b0:	63006b61 	movwvs	r6, #2913	; 0xb61
     9b4:	626c6c61 	rsbvs	r6, ip, #24832	; 0x6100
     9b8:	006b6361 	rsbeq	r6, fp, r1, ror #6
     9bc:	6f6c6552 	svcvs	0x006c6552
     9c0:	42006461 	andmi	r6, r0, #1627389952	; 0x61000000
     9c4:	39315f52 	ldmdbcc	r1!, {r1, r4, r6, r8, r9, sl, fp, ip, lr}
     9c8:	00303032 	eorseq	r3, r0, r2, lsr r0
     9cc:	335f5242 	cmpcc	pc, #536870916	; 0x20000004
     9d0:	30303438 	eorscc	r3, r0, r8, lsr r4
     9d4:	756e7500 	strbvc	r7, [lr, #-1280]!	; 0xfffffb00
     9d8:	5f646573 	svcpl	0x00646573
     9dc:	6e750033 	mrcvs	0, 3, r0, cr5, cr3, {1}
     9e0:	64657375 	strbtvs	r7, [r5], #-885	; 0xfffffc8b
     9e4:	4200345f 	andmi	r3, r0, #1593835520	; 0x5f000000
     9e8:	37355f52 			; <UNDEFINED> instruction: 0x37355f52
     9ec:	00303036 	eorseq	r3, r0, r6, lsr r0
     9f0:	656d6974 	strbvs	r6, [sp, #-2420]!	; 0xfffff68c
     9f4:	65725f72 	ldrbvs	r5, [r2, #-3954]!	; 0xfffff08e
     9f8:	61625f67 	cmnvs	r2, r7, ror #30
     9fc:	53006573 	movwpl	r6, #1395	; 0x573
     a00:	425f7465 	subsmi	r7, pc, #1694498816	; 0x65000000
     a04:	5f647561 	svcpl	0x00647561
     a08:	65746152 	ldrbvs	r6, [r4, #-338]!	; 0xfffffeae
     a0c:	756e7500 	strbvc	r7, [lr, #-1280]!	; 0xfffffb00
     a10:	5f646573 	svcpl	0x00646573
     a14:	52420030 	subpl	r0, r2, #48	; 0x30
     a18:	3531315f 	ldrcc	r3, [r1, #-351]!	; 0xfffffea1
     a1c:	00303032 	eorseq	r3, r0, r2, lsr r0
     a20:	73756e75 	cmnvc	r5, #1872	; 0x750
     a24:	315f6465 	cmpcc	pc, r5, ror #8
     a28:	756e7500 	strbvc	r7, [lr, #-1280]!	; 0xfffffb00
     a2c:	5f646573 	svcpl	0x00646573
     a30:	52490032 	subpl	r0, r9, #50	; 0x32
     a34:	614d5f51 	cmpvs	sp, r1, asr pc
     a38:	64656b73 	strbtvs	r6, [r5], #-2931	; 0xfffff48d
     a3c:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     a40:	69544336 	ldmdbvs	r4, {r1, r2, r4, r5, r8, r9, lr}^
     a44:	3172656d 	cmncc	r2, sp, ror #10
     a48:	51524932 	cmppl	r2, r2, lsr r9
     a4c:	6c61435f 	stclvs	3, cr4, [r1], #-380	; 0xfffffe84
     a50:	6361626c 	cmnvs	r1, #108, 4	; 0xc0000006
     a54:	0076456b 	rsbseq	r4, r6, fp, ror #10
     a58:	315f5242 	cmpcc	pc, r2, asr #4
     a5c:	00303032 	eorseq	r3, r0, r2, lsr r0
     a60:	354e5a5f 	strbcc	r5, [lr, #-2655]	; 0xfffff5a1
     a64:	52415543 	subpl	r5, r1, #281018368	; 0x10c00000
     a68:	53333154 	teqpl	r3, #84, 2
     a6c:	425f7465 	subsmi	r7, pc, #1694498816	; 0x65000000
     a70:	5f647561 	svcpl	0x00647561
     a74:	65746152 	ldrbvs	r6, [r4, #-338]!	; 0xfffffeae
     a78:	4e353145 	rsfmism	f3, f5, f5
     a7c:	54524155 	ldrbpl	r4, [r2], #-341	; 0xfffffeab
     a80:	7561425f 	strbvc	r4, [r1, #-607]!	; 0xfffffda1
     a84:	61525f64 	cmpvs	r2, r4, ror #30
     a88:	6d006574 	cfstr32vs	mvfx6, [r0, #-464]	; 0xfffffe30
     a8c:	6c6c6143 	stfvse	f6, [ip], #-268	; 0xfffffef4
     a90:	6b636162 	blvs	18d9020 <_bss_end+0x18cf9bc>
     a94:	6f682f00 	svcvs	0x00682f00
     a98:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; 8ec <CPSR_IRQ_INHIBIT+0x86c>
     a9c:	6f6b7261 	svcvs	0x006b7261
     aa0:	2f616476 	svccs	0x00616476
     aa4:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
     aa8:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
     aac:	2f736f70 	svccs	0x00736f70
     ab0:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
     ab4:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
     ab8:	2f6c656e 	svccs	0x006c656e
     abc:	2f637273 	svccs	0x00637273
     ac0:	76697264 	strbtvc	r7, [r9], -r4, ror #4
     ac4:	2f737265 	svccs	0x00737265
     ac8:	656d6974 	strbvs	r6, [sp, #-2420]!	; 0xfffff68c
     acc:	70632e72 	rsbvc	r2, r3, r2, ror lr
     ad0:	72660070 	rsbvc	r0, r6, #112	; 0x70
     ad4:	725f6565 	subsvc	r6, pc, #423624704	; 0x19400000
     ad8:	696e6e75 	stmdbvs	lr!, {r0, r2, r4, r5, r6, r9, sl, fp, sp, lr}^
     adc:	655f676e 	ldrbvs	r6, [pc, #-1902]	; 376 <CPSR_IRQ_INHIBIT+0x2f6>
     ae0:	6c62616e 	stfvse	f6, [r2], #-440	; 0xfffffe48
     ae4:	54430065 	strbpl	r0, [r3], #-101	; 0xffffff9b
     ae8:	72656d69 	rsbvc	r6, r5, #6720	; 0x1a40
     aec:	6c656400 	cfstrdvs	mvd6, [r5], #-0
     af0:	54007961 	strpl	r7, [r0], #-2401	; 0xfffff69f
     af4:	72656d69 	rsbvc	r6, r5, #6720	; 0x1a40
     af8:	6765525f 			; <UNDEFINED> instruction: 0x6765525f
     afc:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     b00:	69544336 	ldmdbvs	r4, {r1, r2, r4, r5, r8, r9, lr}^
     b04:	4372656d 	cmnmi	r2, #457179136	; 0x1b400000
     b08:	006d4532 	rsbeq	r4, sp, r2, lsr r5
     b0c:	354e5a5f 	strbcc	r5, [lr, #-2655]	; 0xfffff5a1
     b10:	52415543 	subpl	r5, r1, #281018368	; 0x10c00000
     b14:	72573554 	subsvc	r3, r7, #84, 10	; 0x15000000
     b18:	45657469 	strbmi	r7, [r5, #-1129]!	; 0xfffffb97
     b1c:	5a5f0063 	bpl	17c0cb0 <_bss_end+0x17b764c>
     b20:	5443364e 	strbpl	r3, [r3], #-1614	; 0xfffff9b2
     b24:	72656d69 	rsbvc	r6, r5, #6720	; 0x1a40
     b28:	73694437 	cmnvc	r9, #922746880	; 0x37000000
     b2c:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
     b30:	5f007645 	svcpl	0x00007645
     b34:	43354e5a 	teqmi	r5, #1440	; 0x5a0
     b38:	54524155 	ldrbpl	r4, [r2], #-341	; 0xfffffeab
     b3c:	69725735 	ldmdbvs	r2!, {r0, r2, r4, r5, r8, r9, sl, ip, lr}^
     b40:	50456574 	subpl	r6, r5, r4, ror r5
     b44:	4900634b 	stmdbmi	r0, {r0, r1, r3, r6, r8, r9, sp, lr}
     b48:	435f5152 	cmpmi	pc, #-2147483628	; 0x80000014
     b4c:	7261656c 	rsbvc	r6, r1, #108, 10	; 0x1b000000
     b50:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     b54:	41554335 	cmpmi	r5, r5, lsr r3
     b58:	35315452 	ldrcc	r5, [r1, #-1106]!	; 0xfffffbae
     b5c:	5f746553 	svcpl	0x00746553
     b60:	72616843 	rsbvc	r6, r1, #4390912	; 0x430000
     b64:	6e654c5f 	mcrvs	12, 3, r4, cr5, cr15, {2}
     b68:	45687467 	strbmi	r7, [r8, #-1127]!	; 0xfffffb99
     b6c:	554e3731 	strbpl	r3, [lr, #-1841]	; 0xfffff8cf
     b70:	5f545241 	svcpl	0x00545241
     b74:	72616843 	rsbvc	r6, r1, #4390912	; 0x430000
     b78:	6e654c5f 	mcrvs	12, 3, r4, cr5, cr15, {2}
     b7c:	00687467 	rsbeq	r7, r8, r7, ror #8
     b80:	354e5a5f 	strbcc	r5, [lr, #-2655]	; 0xfffff5a1
     b84:	52415543 	subpl	r5, r1, #281018368	; 0x10c00000
     b88:	45344354 	ldrmi	r4, [r4, #-852]!	; 0xfffffcac
     b8c:	41433452 	cmpmi	r3, r2, asr r4
     b90:	54005855 	strpl	r5, [r0], #-2133	; 0xfffff7ab
     b94:	656d6954 	strbvs	r6, [sp, #-2388]!	; 0xfffff6ac
     b98:	74435f72 	strbvc	r5, [r3], #-3954	; 0xfffff08e
     b9c:	6c465f6c 	mcrrvs	15, 6, r5, r6, cr12
     ba0:	00736761 	rsbseq	r6, r3, r1, ror #14
     ba4:	73657250 	cmnvc	r5, #80, 4
     ba8:	656c6163 	strbvs	r6, [ip, #-355]!	; 0xfffffe9d
     bac:	35325f72 	ldrcc	r5, [r2, #-3954]!	; 0xfffff08e
     bb0:	6f630036 	svcvs	0x00630036
     bb4:	65746e75 	ldrbvs	r6, [r4, #-3701]!	; 0xfffff18b
     bb8:	32335f72 	eorscc	r5, r3, #456	; 0x1c8
     bbc:	61560062 	cmpvs	r6, r2, rrx
     bc0:	0065756c 	rsbeq	r7, r5, ip, ror #10
     bc4:	73657250 	cmnvc	r5, #80, 4
     bc8:	656c6163 	strbvs	r6, [ip, #-355]!	; 0xfffffe9d
     bcc:	36315f72 	shsub16cc	r5, r1, r2
     bd0:	5f524200 	svcpl	0x00524200
     bd4:	30303639 	eorscc	r3, r0, r9, lsr r6
     bd8:	51524900 	cmppl	r2, r0, lsl #18
     bdc:	6c61435f 	stclvs	3, cr4, [r1], #-380	; 0xfffffe84
     be0:	6361626c 	cmnvs	r1, #108, 4	; 0xc0000006
     be4:	7246006b 	subvc	r0, r6, #107	; 0x6b
     be8:	525f6565 	subspl	r6, pc, #423624704	; 0x19400000
     bec:	696e6e75 	stmdbvs	lr!, {r0, r2, r4, r5, r6, r9, sl, fp, sp, lr}^
     bf0:	5f00676e 	svcpl	0x0000676e
     bf4:	43364e5a 	teqmi	r6, #1440	; 0x5a0
     bf8:	656d6954 	strbvs	r6, [sp, #-2388]!	; 0xfffff6ac
     bfc:	65523472 	ldrbvs	r3, [r2, #-1138]	; 0xfffffb8e
     c00:	4e457367 	cdpmi	3, 4, cr7, cr5, cr7, {3}
     c04:	6c616833 	stclvs	8, cr6, [r1], #-204	; 0xffffff34
     c08:	6d695439 	cfstrdvs	mvd5, [r9, #-228]!	; 0xffffff1c
     c0c:	525f7265 	subspl	r7, pc, #1342177286	; 0x50000006
     c10:	00456765 	subeq	r6, r5, r5, ror #14
     c14:	64616f4c 	strbtvs	r6, [r1], #-3916	; 0xfffff0b4
     c18:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     c1c:	41554335 	cmpmi	r5, r5, lsr r3
     c20:	32435452 	subcc	r5, r3, #1375731712	; 0x52000000
     c24:	43345245 	teqmi	r4, #1342177284	; 0x50000004
     c28:	00585541 	subseq	r5, r8, r1, asr #10
     c2c:	4f4c475f 	svcmi	0x004c475f
     c30:	5f4c4142 	svcpl	0x004c4142
     c34:	6275735f 	rsbsvs	r7, r5, #2080374785	; 0x7c000001
     c38:	735f495f 	cmpvc	pc, #1556480	; 0x17c000
     c3c:	54524155 	ldrbpl	r4, [r2], #-341	; 0xfffffeab
     c40:	682f0030 	stmdavs	pc!, {r4, r5}	; <UNPREDICTABLE>
     c44:	2f656d6f 	svccs	0x00656d6f
     c48:	6b72616d 	blvs	1c99204 <_bss_end+0x1c8fba0>
     c4c:	6164766f 	cmnvs	r4, pc, ror #12
     c50:	6f6b532f 	svcvs	0x006b532f
     c54:	722f616c 	eorvc	r6, pc, #108, 2
     c58:	736f7065 	cmnvc	pc, #101	; 0x65
     c5c:	2d736f2f 	ldclcs	15, cr6, [r3, #-188]!	; 0xffffff44
     c60:	656b2f35 	strbvs	r2, [fp, #-3893]!	; 0xfffff0cb
     c64:	6c656e72 	stclvs	14, cr6, [r5], #-456	; 0xfffffe38
     c68:	6372732f 	cmnvs	r2, #-1140850688	; 0xbc000000
     c6c:	6972642f 	ldmdbvs	r2!, {r0, r1, r2, r3, r5, sl, sp, lr}^
     c70:	73726576 	cmnvc	r2, #494927872	; 0x1d800000
     c74:	7261752f 	rsbvc	r7, r1, #197132288	; 0xbc00000
     c78:	70632e74 	rsbvc	r2, r3, r4, ror lr
     c7c:	61720070 	cmnvs	r2, r0, ror r0
     c80:	5f006574 	svcpl	0x00006574
     c84:	31324e5a 	teqcc	r2, sl, asr lr
     c88:	746e4943 	strbtvc	r4, [lr], #-2371	; 0xfffff6bd
     c8c:	75727265 	ldrbvc	r7, [r2, #-613]!	; 0xfffffd9b
     c90:	435f7470 	cmpmi	pc, #112, 8	; 0x70000000
     c94:	72746e6f 	rsbsvc	r6, r4, #1776	; 0x6f0
     c98:	656c6c6f 	strbvs	r6, [ip, #-3183]!	; 0xfffff391
     c9c:	45303172 	ldrmi	r3, [r0, #-370]!	; 0xfffffe8e
     ca0:	6c62616e 	stfvse	f6, [r2], #-440	; 0xfffffe48
     ca4:	52495f65 	subpl	r5, r9, #404	; 0x194
     ca8:	334e4551 	movtcc	r4, #58705	; 0xe551
     cac:	316c6168 	cmncc	ip, r8, ror #2
     cb0:	51524930 	cmppl	r2, r0, lsr r9
     cb4:	756f535f 	strbvc	r5, [pc, #-863]!	; 95d <CPSR_IRQ_INHIBIT+0x8dd>
     cb8:	45656372 	strbmi	r6, [r5, #-882]!	; 0xfffffc8e
     cbc:	78646900 	stmdavc	r4!, {r8, fp, sp, lr}^
     cc0:	7361625f 	cmnvc	r1, #-268435451	; 0xf0000005
     cc4:	5a5f0065 	bpl	17c0e60 <_bss_end+0x17b77fc>
     cc8:	4331324e 	teqmi	r1, #-536870908	; 0xe0000004
     ccc:	65746e49 	ldrbvs	r6, [r4, #-3657]!	; 0xfffff1b7
     cd0:	70757272 	rsbsvc	r7, r5, r2, ror r2
     cd4:	6f435f74 	svcvs	0x00435f74
     cd8:	6f72746e 	svcvs	0x0072746e
     cdc:	72656c6c 	rsbvc	r6, r5, #108, 24	; 0x6c00
     ce0:	67655234 			; <UNDEFINED> instruction: 0x67655234
     ce4:	334e4573 	movtcc	r4, #58739	; 0xe573
     ce8:	326c6168 	rsbcc	r6, ip, #104, 2
     cec:	746e4934 	strbtvc	r4, [lr], #-2356	; 0xfffff6cc
     cf0:	75727265 	ldrbvc	r7, [r2, #-613]!	; 0xfffffd9b
     cf4:	435f7470 	cmpmi	pc, #112, 8	; 0x70000000
     cf8:	72746e6f 	rsbsvc	r6, r4, #1776	; 0x6f0
     cfc:	656c6c6f 	strbvs	r6, [ip, #-3183]!	; 0xfffff391
     d00:	65525f72 	ldrbvs	r5, [r2, #-3954]	; 0xfffff08e
     d04:	47004567 	strmi	r4, [r0, -r7, ror #10]
     d08:	5f315550 	svcpl	0x00315550
     d0c:	746c6148 	strbtvc	r6, [ip], #-328	; 0xfffffeb8
     d10:	69614d00 	stmdbvs	r1!, {r8, sl, fp, lr}^
     d14:	786f626c 	stmdavc	pc!, {r2, r3, r5, r6, r9, sp, lr}^	; <UNPREDICTABLE>
     d18:	616e4500 	cmnvs	lr, r0, lsl #10
     d1c:	5f656c62 	svcpl	0x00656c62
     d20:	00515249 	subseq	r5, r1, r9, asr #4
     d24:	324e5a5f 	subcc	r5, lr, #389120	; 0x5f000
     d28:	6e494331 	mcrvs	3, 2, r4, cr9, cr1, {1}
     d2c:	72726574 	rsbsvc	r6, r2, #116, 10	; 0x1d000000
     d30:	5f747075 	svcpl	0x00747075
     d34:	746e6f43 	strbtvc	r6, [lr], #-3907	; 0xfffff0bd
     d38:	6c6c6f72 	stclvs	15, cr6, [ip], #-456	; 0xfffffe38
     d3c:	32437265 	subcc	r7, r3, #1342177286	; 0x50000006
     d40:	49006d45 	stmdbmi	r0, {r0, r2, r6, r8, sl, fp, sp, lr}
     d44:	455f5152 	ldrbmi	r5, [pc, #-338]	; bfa <CPSR_IRQ_INHIBIT+0xb7a>
     d48:	6c62616e 	stfvse	f6, [r2], #-440	; 0xfffffe48
     d4c:	00315f65 	eorseq	r5, r1, r5, ror #30
     d50:	5f515249 	svcpl	0x00515249
     d54:	62616e45 	rsbvs	r6, r1, #1104	; 0x450
     d58:	325f656c 	subscc	r6, pc, #108, 10	; 0x1b000000
     d5c:	4e5a5f00 	cdpmi	15, 5, cr5, cr10, cr0, {0}
     d60:	49433132 	stmdbmi	r3, {r1, r4, r5, r8, ip, sp}^
     d64:	7265746e 	rsbvc	r7, r5, #1845493760	; 0x6e000000
     d68:	74707572 	ldrbtvc	r7, [r0], #-1394	; 0xfffffa8e
     d6c:	6e6f435f 	mcrvs	3, 3, r4, cr15, cr15, {2}
     d70:	6c6f7274 	sfmvs	f7, 2, [pc], #-464	; ba8 <CPSR_IRQ_INHIBIT+0xb28>
     d74:	3172656c 	cmncc	r2, ip, ror #10
     d78:	73694437 	cmnvc	r9, #922746880	; 0x37000000
     d7c:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
     d80:	7361425f 	cmnvc	r1, #-268435451	; 0xf0000005
     d84:	495f6369 	ldmdbmi	pc, {r0, r3, r5, r6, r8, r9, sp, lr}^	; <UNPREDICTABLE>
     d88:	4e455152 	mcrmi	1, 2, r5, cr5, cr2, {2}
     d8c:	6c616833 	stclvs	8, cr6, [r1], #-204	; 0xffffff34
     d90:	52493631 	subpl	r3, r9, #51380224	; 0x3100000
     d94:	61425f51 	cmpvs	r2, r1, asr pc
     d98:	5f636973 	svcpl	0x00636973
     d9c:	72756f53 	rsbsvc	r6, r5, #332	; 0x14c
     da0:	00456563 	subeq	r6, r5, r3, ror #10
     da4:	65746e49 	ldrbvs	r6, [r4, #-3657]!	; 0xfffff1b7
     da8:	70757272 	rsbsvc	r7, r5, r2, ror r2
     dac:	6f435f74 	svcvs	0x00435f74
     db0:	6f72746e 	svcvs	0x0072746e
     db4:	72656c6c 	rsbvc	r6, r5, #108, 24	; 0x6c00
     db8:	6765525f 			; <UNDEFINED> instruction: 0x6765525f
     dbc:	73694400 	cmnvc	r9, #0, 8
     dc0:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
     dc4:	5152495f 	cmppl	r2, pc, asr r9
     dc8:	6c6c4900 			; <UNDEFINED> instruction: 0x6c6c4900
     dcc:	6c616765 	stclvs	7, cr6, [r1], #-404	; 0xfffffe6c
     dd0:	6363415f 	cmnvs	r3, #-1073741801	; 0xc0000017
     dd4:	5f737365 	svcpl	0x00737365
     dd8:	6c490031 	mcrrvs	0, 3, r0, r9, cr1
     ddc:	6167656c 	cmnvs	r7, ip, ror #10
     de0:	63415f6c 	movtvs	r5, #8044	; 0x1f6c
     de4:	73736563 	cmnvc	r3, #415236096	; 0x18c00000
     de8:	4900325f 	stmdbmi	r0, {r0, r1, r2, r3, r4, r6, r9, ip, sp}
     dec:	425f5152 	subsmi	r5, pc, #-2147483628	; 0x80000014
     df0:	63697361 	cmnvs	r9, #-2080374783	; 0x84000001
     df4:	756f535f 	strbvc	r5, [pc, #-863]!	; a9d <CPSR_IRQ_INHIBIT+0xa1d>
     df8:	00656372 	rsbeq	r6, r5, r2, ror r3
     dfc:	324e5a5f 	subcc	r5, lr, #389120	; 0x5f000
     e00:	6e494331 	mcrvs	3, 2, r4, cr9, cr1, {1}
     e04:	72726574 	rsbsvc	r6, r2, #116, 10	; 0x1d000000
     e08:	5f747075 	svcpl	0x00747075
     e0c:	746e6f43 	strbtvc	r6, [lr], #-3907	; 0xfffff0bd
     e10:	6c6c6f72 	stclvs	15, cr6, [ip], #-456	; 0xfffffe38
     e14:	31317265 	teqcc	r1, r5, ror #4
     e18:	61736944 	cmnvs	r3, r4, asr #18
     e1c:	5f656c62 	svcpl	0x00656c62
     e20:	45515249 	ldrbmi	r5, [r1, #-585]	; 0xfffffdb7
     e24:	6168334e 	cmnvs	r8, lr, asr #6
     e28:	4930316c 	ldmdbmi	r0!, {r2, r3, r5, r6, r8, ip, sp}
     e2c:	535f5152 	cmppl	pc, #-2147483628	; 0x80000014
     e30:	6372756f 	cmnvs	r2, #465567744	; 0x1bc00000
     e34:	66004565 	strvs	r4, [r0], -r5, ror #10
     e38:	5f747361 	svcpl	0x00747361
     e3c:	65746e69 	ldrbvs	r6, [r4, #-3689]!	; 0xfffff197
     e40:	70757272 	rsbsvc	r7, r5, r2, ror r2
     e44:	61685f74 	smcvs	34292	; 0x85f4
     e48:	656c646e 	strbvs	r6, [ip, #-1134]!	; 0xfffffb92
     e4c:	52490072 	subpl	r0, r9, #114	; 0x72
     e50:	61425f51 	cmpvs	r2, r1, asr pc
     e54:	5f636973 	svcpl	0x00636973
     e58:	646e6550 	strbtvs	r6, [lr], #-1360	; 0xfffffab0
     e5c:	00676e69 	rsbeq	r6, r7, r9, ror #28
     e60:	746e4943 	strbtvc	r4, [lr], #-2371	; 0xfffff6bd
     e64:	75727265 	ldrbvc	r7, [r2, #-613]!	; 0xfffffd9b
     e68:	435f7470 	cmpmi	pc, #112, 8	; 0x70000000
     e6c:	72746e6f 	rsbsvc	r6, r4, #1776	; 0x6f0
     e70:	656c6c6f 	strbvs	r6, [ip, #-3183]!	; 0xfffff391
     e74:	52490072 	subpl	r0, r9, #114	; 0x72
     e78:	61425f51 	cmpvs	r2, r1, asr pc
     e7c:	5f636973 	svcpl	0x00636973
     e80:	61736944 	cmnvs	r3, r4, asr #18
     e84:	00656c62 	rsbeq	r6, r5, r2, ror #24
     e88:	62616e45 	rsbvs	r6, r1, #1104	; 0x450
     e8c:	425f656c 	subsmi	r6, pc, #108, 10	; 0x1b000000
     e90:	63697361 	cmnvs	r9, #-2080374783	; 0x84000001
     e94:	5152495f 	cmppl	r2, pc, asr r9
     e98:	55504700 	ldrbpl	r4, [r0, #-1792]	; 0xfffff900
     e9c:	61485f30 	cmpvs	r8, r0, lsr pc
     ea0:	4400746c 	strmi	r7, [r0], #-1132	; 0xfffffb94
     ea4:	62726f6f 	rsbsvs	r6, r2, #444	; 0x1bc
     ea8:	5f6c6c65 	svcpl	0x006c6c65
     eac:	6f440030 	svcvs	0x00440030
     eb0:	6562726f 	strbvs	r7, [r2, #-623]!	; 0xfffffd91
     eb4:	315f6c6c 	cmpcc	pc, ip, ror #24
     eb8:	6e496d00 	cdpvs	13, 4, cr6, cr9, cr0, {0}
     ebc:	72726574 	rsbsvc	r6, r2, #116, 10	; 0x1d000000
     ec0:	5f747075 	svcpl	0x00747075
     ec4:	73676552 	cmnvc	r7, #343932928	; 0x14800000
     ec8:	4c475f00 	mcrrmi	15, 0, r5, r7, cr0
     ecc:	4c41424f 	sfmmi	f4, 2, [r1], {79}	; 0x4f
     ed0:	75735f5f 	ldrbvc	r5, [r3, #-3935]!	; 0xfffff0a1
     ed4:	5f495f62 	svcpl	0x00495f62
     ed8:	746e4973 	strbtvc	r4, [lr], #-2419	; 0xfffff68d
     edc:	75727265 	ldrbvc	r7, [r2, #-613]!	; 0xfffffd9b
     ee0:	74437470 	strbvc	r7, [r3], #-1136	; 0xfffffb90
     ee4:	5a5f006c 	bpl	17c109c <_bss_end+0x17b7a38>
     ee8:	4331324e 	teqmi	r1, #-536870908	; 0xe0000004
     eec:	65746e49 	ldrbvs	r6, [r4, #-3657]!	; 0xfffff1b7
     ef0:	70757272 	rsbsvc	r7, r5, r2, ror r2
     ef4:	6f435f74 	svcvs	0x00435f74
     ef8:	6f72746e 	svcvs	0x0072746e
     efc:	72656c6c 	rsbvc	r6, r5, #108, 24	; 0x6c00
     f00:	6e453631 	mcrvs	6, 2, r3, cr5, cr1, {1}
     f04:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
     f08:	7361425f 	cmnvc	r1, #-268435451	; 0xf0000005
     f0c:	495f6369 	ldmdbmi	pc, {r0, r3, r5, r6, r8, r9, sp, lr}^	; <UNPREDICTABLE>
     f10:	4e455152 	mcrmi	1, 2, r5, cr5, cr2, {2}
     f14:	6c616833 	stclvs	8, cr6, [r1], #-204	; 0xffffff34
     f18:	52493631 	subpl	r3, r9, #51380224	; 0x3100000
     f1c:	61425f51 	cmpvs	r2, r1, asr pc
     f20:	5f636973 	svcpl	0x00636973
     f24:	72756f53 	rsbsvc	r6, r5, #332	; 0x14c
     f28:	00456563 	subeq	r6, r5, r3, ror #10
     f2c:	324e5a5f 	subcc	r5, lr, #389120	; 0x5f000
     f30:	6e494331 	mcrvs	3, 2, r4, cr9, cr1, {1}
     f34:	72726574 	rsbsvc	r6, r2, #116, 10	; 0x1d000000
     f38:	5f747075 	svcpl	0x00747075
     f3c:	746e6f43 	strbtvc	r6, [lr], #-3907	; 0xfffff0bd
     f40:	6c6c6f72 	stclvs	15, cr6, [ip], #-456	; 0xfffffe38
     f44:	34437265 	strbcc	r7, [r3], #-613	; 0xfffffd9b
     f48:	49006d45 	stmdbmi	r0, {r0, r2, r6, r8, sl, fp, sp, lr}
     f4c:	535f4332 	cmppl	pc, #-939524096	; 0xc8000000
     f50:	535f4950 	cmppl	pc, #80, 18	; 0x140000
     f54:	4556414c 	ldrbmi	r4, [r6, #-332]	; 0xfffffeb4
     f58:	494e495f 	stmdbmi	lr, {r0, r1, r2, r3, r4, r6, r8, fp, lr}^
     f5c:	49460054 	stmdbmi	r6, {r2, r4, r6}^
     f60:	6f435f51 	svcvs	0x00435f51
     f64:	6f72746e 	svcvs	0x0072746e
     f68:	7269006c 	rsbvc	r0, r9, #108	; 0x6c
     f6c:	61685f71 	smcvs	34289	; 0x85f1
     f70:	656c646e 	strbvs	r6, [ip, #-1134]!	; 0xfffffb92
     f74:	52490072 	subpl	r0, r9, #114	; 0x72
     f78:	61425f51 	cmpvs	r2, r1, asr pc
     f7c:	5f636973 	svcpl	0x00636973
     f80:	62616e45 	rsbvs	r6, r1, #1104	; 0x450
     f84:	7300656c 	movwvc	r6, #1388	; 0x56c
     f88:	7774666f 	ldrbvc	r6, [r4, -pc, ror #12]!
     f8c:	5f657261 	svcpl	0x00657261
     f90:	65746e69 	ldrbvs	r6, [r4, #-3689]!	; 0xfffff197
     f94:	70757272 	rsbsvc	r7, r5, r2, ror r2
     f98:	61685f74 	smcvs	34292	; 0x85f4
     f9c:	656c646e 	strbvs	r6, [ip, #-1134]!	; 0xfffffb92
     fa0:	52490072 	subpl	r0, r9, #114	; 0x72
     fa4:	65505f51 	ldrbvs	r5, [r0, #-3921]	; 0xfffff0af
     fa8:	6e69646e 	cdpvs	4, 6, cr6, cr9, cr14, {3}
     fac:	00315f67 	eorseq	r5, r1, r7, ror #30
     fb0:	5f515249 	svcpl	0x00515249
     fb4:	72756f53 	rsbsvc	r6, r5, #332	; 0x14c
     fb8:	49006563 	stmdbmi	r0, {r0, r1, r5, r6, r8, sl, sp, lr}
     fbc:	505f5152 	subspl	r5, pc, r2, asr r1	; <UNPREDICTABLE>
     fc0:	69646e65 	stmdbvs	r4!, {r0, r2, r5, r6, r9, sl, fp, sp, lr}^
     fc4:	325f676e 	subscc	r6, pc, #28835840	; 0x1b80000
     fc8:	49504700 	ldmdbmi	r0, {r8, r9, sl, lr}^
     fcc:	00305f4f 	eorseq	r5, r0, pc, asr #30
     fd0:	4f495047 	svcmi	0x00495047
     fd4:	4700315f 	smlsdmi	r0, pc, r1, r3	; <UNPREDICTABLE>
     fd8:	5f4f4950 	svcpl	0x004f4950
     fdc:	50470032 	subpl	r0, r7, r2, lsr r0
     fe0:	335f4f49 	cmpcc	pc, #292	; 0x124
     fe4:	6f682f00 	svcvs	0x00682f00
     fe8:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; e3c <CPSR_IRQ_INHIBIT+0xdbc>
     fec:	6f6b7261 	svcvs	0x006b7261
     ff0:	2f616476 	svccs	0x00616476
     ff4:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
     ff8:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
     ffc:	2f736f70 	svccs	0x00736f70
    1000:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
    1004:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
    1008:	2f6c656e 	svccs	0x006c656e
    100c:	2f637273 	svccs	0x00637273
    1010:	65746e69 	ldrbvs	r6, [r4, #-3689]!	; 0xfffff197
    1014:	70757272 	rsbsvc	r7, r5, r2, ror r2
    1018:	6f635f74 	svcvs	0x00635f74
    101c:	6f72746e 	svcvs	0x0072746e
    1020:	72656c6c 	rsbvc	r6, r5, #108, 24	; 0x6c00
    1024:	7070632e 	rsbsvc	r6, r0, lr, lsr #6
    1028:	73694400 	cmnvc	r9, #0, 8
    102c:	656c6261 	strbvs	r6, [ip, #-609]!	; 0xfffffd9f
    1030:	7361425f 	cmnvc	r1, #-268435451	; 0xf0000005
    1034:	495f6369 	ldmdbmi	pc, {r0, r3, r5, r6, r8, r9, sp, lr}^	; <UNPREDICTABLE>
    1038:	73005152 	movwvc	r5, #338	; 0x152
    103c:	6372756f 	cmnvs	r2, #465567744	; 0x1bc00000
    1040:	64695f65 	strbtvs	r5, [r9], #-3941	; 0xfffff09b
    1044:	52490078 	subpl	r0, r9, #120	; 0x78
    1048:	69445f51 	stmdbvs	r4, {r0, r4, r6, r8, r9, sl, fp, ip, lr}^
    104c:	6c626173 	stfvse	f6, [r2], #-460	; 0xfffffe34
    1050:	00315f65 	eorseq	r5, r1, r5, ror #30
    1054:	5f515249 	svcpl	0x00515249
    1058:	61736944 	cmnvs	r3, r4, asr #18
    105c:	5f656c62 	svcpl	0x00656c62
    1060:	57500032 	smmlarpl	r0, r2, r0, r0
    1064:	00305f41 	eorseq	r5, r0, r1, asr #30
    1068:	5f415750 	svcpl	0x00415750
    106c:	454c0031 	strbmi	r0, [ip, #-49]	; 0xffffffcf
    1070:	74535f44 	ldrbvc	r5, [r3], #-3908	; 0xfffff0bc
    1074:	00657461 	rsbeq	r7, r5, r1, ror #8
    1078:	6d6f682f 	stclvs	8, cr6, [pc, #-188]!	; fc4 <CPSR_IRQ_INHIBIT+0xf44>
    107c:	616d2f65 	cmnvs	sp, r5, ror #30
    1080:	766f6b72 			; <UNDEFINED> instruction: 0x766f6b72
    1084:	532f6164 			; <UNDEFINED> instruction: 0x532f6164
    1088:	616c6f6b 	cmnvs	ip, fp, ror #30
    108c:	7065722f 	rsbvc	r7, r5, pc, lsr #4
    1090:	6f2f736f 	svcvs	0x002f736f
    1094:	2f352d73 	svccs	0x00352d73
    1098:	6e72656b 	cdpvs	5, 7, cr6, cr2, cr11, {3}
    109c:	732f6c65 			; <UNDEFINED> instruction: 0x732f6c65
    10a0:	6d2f6372 	stcvs	3, cr6, [pc, #-456]!	; ee0 <CPSR_IRQ_INHIBIT+0xe60>
    10a4:	2e6e6961 	vnmulcs.f16	s13, s28, s3	; <UNPREDICTABLE>
    10a8:	00707063 	rsbseq	r7, r0, r3, rrx
    10ac:	5f544341 	svcpl	0x00544341
    10b0:	5f44454c 	svcpl	0x0044454c
    10b4:	6e696c42 	cdpvs	12, 6, cr6, cr9, cr2, {2}
    10b8:	0072656b 	rsbseq	r6, r2, fp, ror #10
    10bc:	5f544341 	svcpl	0x00544341
    10c0:	006e6950 	rsbeq	r6, lr, r0, asr r9
    10c4:	72656b5f 	rsbvc	r6, r5, #97280	; 0x17c00
    10c8:	5f6c656e 	svcpl	0x006c656e
    10cc:	6e69616d 	powvsez	f6, f1, #5.0
    10d0:	6f682f00 	svcvs	0x00682f00
    10d4:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; f28 <CPSR_IRQ_INHIBIT+0xea8>
    10d8:	6f6b7261 	svcvs	0x006b7261
    10dc:	2f616476 	svccs	0x00616476
    10e0:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
    10e4:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
    10e8:	2f736f70 	svccs	0x00736f70
    10ec:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
    10f0:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
    10f4:	2f6c656e 	svccs	0x006c656e
    10f8:	2f637273 	svccs	0x00637273
    10fc:	72617473 	rsbvc	r7, r1, #1929379840	; 0x73000000
    1100:	00732e74 	rsbseq	r2, r3, r4, ror lr
    1104:	20554e47 	subscs	r4, r5, r7, asr #28
    1108:	32205341 	eorcc	r5, r0, #67108865	; 0x4000001
    110c:	0034332e 	eorseq	r3, r4, lr, lsr #6
    1110:	735f635f 	cmpvc	pc, #2080374785	; 0x7c000001
    1114:	74726174 	ldrbtvc	r6, [r2], #-372	; 0xfffffe8c
    1118:	5f007075 	svcpl	0x00007075
    111c:	5f737362 	svcpl	0x00737362
    1120:	72617473 	rsbvc	r7, r1, #1929379840	; 0x73000000
    1124:	5f5f0074 	svcpl	0x005f0074
    1128:	524f5443 	subpl	r5, pc, #1124073472	; 0x43000000
    112c:	444e455f 	strbmi	r4, [lr], #-1375	; 0xfffffaa1
    1130:	5f005f5f 	svcpl	0x00005f5f
    1134:	4f54435f 	svcmi	0x0054435f
    1138:	494c5f52 	stmdbmi	ip, {r1, r4, r6, r8, r9, sl, fp, ip, lr}^
    113c:	5f5f5453 	svcpl	0x005f5453
    1140:	445f5f00 	ldrbmi	r5, [pc], #-3840	; 1148 <CPSR_IRQ_INHIBIT+0x10c8>
    1144:	5f524f54 	svcpl	0x00524f54
    1148:	5f444e45 	svcpl	0x00444e45
    114c:	635f005f 	cmpvs	pc, #95	; 0x5f
    1150:	735f7070 	cmpvc	pc, #112	; 0x70
    1154:	64747568 	ldrbtvs	r7, [r4], #-1384	; 0xfffffa98
    1158:	006e776f 	rsbeq	r7, lr, pc, ror #14
    115c:	7373625f 	cmnvc	r3, #-268435451	; 0xf0000005
    1160:	646e655f 	strbtvs	r6, [lr], #-1375	; 0xfffffaa1
    1164:	445f5f00 	ldrbmi	r5, [pc], #-3840	; 116c <CPSR_IRQ_INHIBIT+0x10ec>
    1168:	5f524f54 	svcpl	0x00524f54
    116c:	5453494c 	ldrbpl	r4, [r3], #-2380	; 0xfffff6b4
    1170:	64005f5f 	strvs	r5, [r0], #-3935	; 0xfffff0a1
    1174:	5f726f74 	svcpl	0x00726f74
    1178:	00727470 	rsbseq	r7, r2, r0, ror r4
    117c:	726f7463 	rsbvc	r7, pc, #1660944384	; 0x63000000
    1180:	7274705f 	rsbsvc	r7, r4, #95	; 0x5f
    1184:	6f682f00 	svcvs	0x00682f00
    1188:	6d2f656d 	cfstr32vs	mvfx6, [pc, #-436]!	; fdc <CPSR_IRQ_INHIBIT+0xf5c>
    118c:	6f6b7261 	svcvs	0x006b7261
    1190:	2f616476 	svccs	0x00616476
    1194:	6c6f6b53 			; <UNDEFINED> instruction: 0x6c6f6b53
    1198:	65722f61 	ldrbvs	r2, [r2, #-3937]!	; 0xfffff09f
    119c:	2f736f70 	svccs	0x00736f70
    11a0:	352d736f 	strcc	r7, [sp, #-879]!	; 0xfffffc91
    11a4:	72656b2f 	rsbvc	r6, r5, #48128	; 0xbc00
    11a8:	2f6c656e 	svccs	0x006c656e
    11ac:	2f637273 	svccs	0x00637273
    11b0:	72617473 	rsbvc	r7, r1, #1929379840	; 0x73000000
    11b4:	2e707574 	mrccs	5, 3, r7, cr0, cr4, {3}
    11b8:	00707063 	rsbseq	r7, r0, r3, rrx
    11bc:	7070635f 	rsbsvc	r6, r0, pc, asr r3
    11c0:	6174735f 	cmnvs	r4, pc, asr r3
    11c4:	70757472 	rsbsvc	r7, r5, r2, ror r4
    11c8:	706e6600 	rsbvc	r6, lr, r0, lsl #12
    11cc:	2e007274 	mcrcs	2, 0, r7, cr0, cr4, {3}
    11d0:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
    11d4:	2f2e2e2f 	svccs	0x002e2e2f
    11d8:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
    11dc:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
    11e0:	62696c2f 	rsbvs	r6, r9, #12032	; 0x2f00
    11e4:	2f636367 	svccs	0x00636367
    11e8:	666e6f63 	strbtvs	r6, [lr], -r3, ror #30
    11ec:	612f6769 			; <UNDEFINED> instruction: 0x612f6769
    11f0:	6c2f6d72 	stcvs	13, cr6, [pc], #-456	; 1030 <CPSR_IRQ_INHIBIT+0xfb0>
    11f4:	66316269 	ldrtvs	r6, [r1], -r9, ror #4
    11f8:	73636e75 	cmnvc	r3, #1872	; 0x750
    11fc:	2f00532e 	svccs	0x0000532e
    1200:	6c697562 	cfstr64vs	mvdx7, [r9], #-392	; 0xfffffe78
    1204:	63672f64 	cmnvs	r7, #100, 30	; 0x190
    1208:	72612d63 	rsbvc	r2, r1, #6336	; 0x18c0
    120c:	6f6e2d6d 	svcvs	0x006e2d6d
    1210:	652d656e 	strvs	r6, [sp, #-1390]!	; 0xfffffa92
    1214:	2d696261 	sfmcs	f6, 2, [r9, #-388]!	; 0xfffffe7c
    1218:	6b396c47 	blvs	e5c33c <_bss_end+0xe52cd8>
    121c:	672f3954 			; <UNDEFINED> instruction: 0x672f3954
    1220:	612d6363 			; <UNDEFINED> instruction: 0x612d6363
    1224:	6e2d6d72 	mcrvs	13, 1, r6, cr13, cr2, {3}
    1228:	2d656e6f 	stclcs	14, cr6, [r5, #-444]!	; 0xfffffe44
    122c:	69626165 	stmdbvs	r2!, {r0, r2, r5, r6, r8, sp, lr}^
    1230:	322d392d 	eorcc	r3, sp, #737280	; 0xb4000
    1234:	2d393130 	ldfcss	f3, [r9, #-192]!	; 0xffffff40
    1238:	622f3471 	eorvs	r3, pc, #1895825408	; 0x71000000
    123c:	646c6975 	strbtvs	r6, [ip], #-2421	; 0xfffff68b
    1240:	6d72612f 	ldfvse	f6, [r2, #-188]!	; 0xffffff44
    1244:	6e6f6e2d 	cdpvs	14, 6, cr6, cr15, cr13, {1}
    1248:	61652d65 	cmnvs	r5, r5, ror #26
    124c:	612f6962 			; <UNDEFINED> instruction: 0x612f6962
    1250:	762f6d72 			; <UNDEFINED> instruction: 0x762f6d72
    1254:	2f657435 	svccs	0x00657435
    1258:	64726168 	ldrbtvs	r6, [r2], #-360	; 0xfffffe98
    125c:	62696c2f 	rsbvs	r6, r9, #12032	; 0x2f00
    1260:	00636367 	rsbeq	r6, r3, r7, ror #6
    1264:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1268:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    126c:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1270:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1274:	37316178 			; <UNDEFINED> instruction: 0x37316178
    1278:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    127c:	37617865 	strbcc	r7, [r1, -r5, ror #16]!
    1280:	61736900 	cmnvs	r3, r0, lsl #18
    1284:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1288:	5f70665f 	svcpl	0x0070665f
    128c:	006c6264 	rsbeq	r6, ip, r4, ror #4
    1290:	5f6d7261 	svcpl	0x006d7261
    1294:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    1298:	6d77695f 			; <UNDEFINED> instruction: 0x6d77695f
    129c:	0074786d 	rsbseq	r7, r4, sp, ror #16
    12a0:	47524154 			; <UNDEFINED> instruction: 0x47524154
    12a4:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    12a8:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    12ac:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    12b0:	33326d78 	teqcc	r2, #120, 26	; 0x1e00
    12b4:	4d524100 	ldfmie	f4, [r2, #-0]
    12b8:	0051455f 	subseq	r4, r1, pc, asr r5
    12bc:	47524154 			; <UNDEFINED> instruction: 0x47524154
    12c0:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    12c4:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    12c8:	31316d72 	teqcc	r1, r2, ror sp
    12cc:	32743635 	rsbscc	r3, r4, #55574528	; 0x3500000
    12d0:	69007366 	stmdbvs	r0, {r1, r2, r5, r6, r8, r9, ip, sp, lr}
    12d4:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    12d8:	745f7469 	ldrbvc	r7, [pc], #-1129	; 12e0 <CPSR_IRQ_INHIBIT+0x1260>
    12dc:	626d7568 	rsbvs	r7, sp, #104, 10	; 0x1a000000
    12e0:	52415400 	subpl	r5, r1, #0, 8
    12e4:	5f544547 	svcpl	0x00544547
    12e8:	5f555043 	svcpl	0x00555043
    12ec:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    12f0:	35617865 	strbcc	r7, [r1, #-2149]!	; 0xfffff79b
    12f4:	726f6337 	rsbvc	r6, pc, #-603979776	; 0xdc000000
    12f8:	61786574 	cmnvs	r8, r4, ror r5
    12fc:	42003335 	andmi	r3, r0, #-738197504	; 0xd4000000
    1300:	5f455341 	svcpl	0x00455341
    1304:	48435241 	stmdami	r3, {r0, r6, r9, ip, lr}^
    1308:	5f4d385f 	svcpl	0x004d385f
    130c:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1310:	52415400 	subpl	r5, r1, #0, 8
    1314:	5f544547 	svcpl	0x00544547
    1318:	5f555043 	svcpl	0x00555043
    131c:	386d7261 	stmdacc	sp!, {r0, r5, r6, r9, ip, sp, lr}^
    1320:	54003031 	strpl	r3, [r0], #-49	; 0xffffffcf
    1324:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1328:	50435f54 	subpl	r5, r3, r4, asr pc
    132c:	67785f55 			; <UNDEFINED> instruction: 0x67785f55
    1330:	31656e65 	cmncc	r5, r5, ror #28
    1334:	4d524100 	ldfmie	f4, [r2, #-0]
    1338:	5343505f 	movtpl	r5, #12383	; 0x305f
    133c:	5041415f 	subpl	r4, r1, pc, asr r1
    1340:	495f5343 	ldmdbmi	pc, {r0, r1, r6, r8, r9, ip, lr}^	; <UNPREDICTABLE>
    1344:	584d4d57 	stmdapl	sp, {r0, r1, r2, r4, r6, r8, sl, fp, lr}^
    1348:	41420054 	qdaddmi	r0, r4, r2
    134c:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1350:	5f484352 	svcpl	0x00484352
    1354:	41420030 	cmpmi	r2, r0, lsr r0
    1358:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    135c:	5f484352 	svcpl	0x00484352
    1360:	41420032 	cmpmi	r2, r2, lsr r0
    1364:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1368:	5f484352 	svcpl	0x00484352
    136c:	41420033 	cmpmi	r2, r3, lsr r0
    1370:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1374:	5f484352 	svcpl	0x00484352
    1378:	41420034 	cmpmi	r2, r4, lsr r0
    137c:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1380:	5f484352 	svcpl	0x00484352
    1384:	41420036 	cmpmi	r2, r6, lsr r0
    1388:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    138c:	5f484352 	svcpl	0x00484352
    1390:	41540037 	cmpmi	r4, r7, lsr r0
    1394:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1398:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    139c:	6373785f 	cmnvs	r3, #6225920	; 0x5f0000
    13a0:	00656c61 	rsbeq	r6, r5, r1, ror #24
    13a4:	5f617369 	svcpl	0x00617369
    13a8:	5f746962 	svcpl	0x00746962
    13ac:	64657270 	strbtvs	r7, [r5], #-624	; 0xfffffd90
    13b0:	00736572 	rsbseq	r6, r3, r2, ror r5
    13b4:	47524154 			; <UNDEFINED> instruction: 0x47524154
    13b8:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    13bc:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    13c0:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    13c4:	33336d78 	teqcc	r3, #120, 26	; 0x1e00
    13c8:	52415400 	subpl	r5, r1, #0, 8
    13cc:	5f544547 	svcpl	0x00544547
    13d0:	5f555043 	svcpl	0x00555043
    13d4:	376d7261 	strbcc	r7, [sp, -r1, ror #4]!
    13d8:	696d6474 	stmdbvs	sp!, {r2, r4, r5, r6, sl, sp, lr}^
    13dc:	61736900 	cmnvs	r3, r0, lsl #18
    13e0:	626f6e5f 	rsbvs	r6, pc, #1520	; 0x5f0
    13e4:	54007469 	strpl	r7, [r0], #-1129	; 0xfffffb97
    13e8:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    13ec:	50435f54 	subpl	r5, r3, r4, asr pc
    13f0:	72615f55 	rsbvc	r5, r1, #340	; 0x154
    13f4:	3731316d 	ldrcc	r3, [r1, -sp, ror #2]!
    13f8:	667a6a36 			; <UNDEFINED> instruction: 0x667a6a36
    13fc:	73690073 	cmnvc	r9, #115	; 0x73
    1400:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    1404:	66765f74 	uhsub16vs	r5, r6, r4
    1408:	00327670 	eorseq	r7, r2, r0, ror r6
    140c:	5f4d5241 	svcpl	0x004d5241
    1410:	5f534350 	svcpl	0x00534350
    1414:	4e4b4e55 	mcrmi	14, 2, r4, cr11, cr5, {2}
    1418:	004e574f 	subeq	r5, lr, pc, asr #14
    141c:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1420:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1424:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    1428:	65396d72 	ldrvs	r6, [r9, #-3442]!	; 0xfffff28e
    142c:	53414200 	movtpl	r4, #4608	; 0x1200
    1430:	52415f45 	subpl	r5, r1, #276	; 0x114
    1434:	355f4843 	ldrbcc	r4, [pc, #-2115]	; bf9 <CPSR_IRQ_INHIBIT+0xb79>
    1438:	004a4554 	subeq	r4, sl, r4, asr r5
    143c:	5f6d7261 	svcpl	0x006d7261
    1440:	73666363 	cmnvc	r6, #-1946157055	; 0x8c000001
    1444:	74735f6d 	ldrbtvc	r5, [r3], #-3949	; 0xfffff093
    1448:	00657461 	rsbeq	r7, r5, r1, ror #8
    144c:	5f6d7261 	svcpl	0x006d7261
    1450:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    1454:	00657435 	rsbeq	r7, r5, r5, lsr r4
    1458:	70736e75 	rsbsvc	r6, r3, r5, ror lr
    145c:	735f6365 	cmpvc	pc, #-1811939327	; 0x94000001
    1460:	6e697274 	mcrvs	2, 3, r7, cr9, cr4, {3}
    1464:	69007367 	stmdbvs	r0, {r0, r1, r2, r5, r6, r8, r9, ip, sp, lr}
    1468:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    146c:	735f7469 	cmpvc	pc, #1761607680	; 0x69000000
    1470:	5f006365 	svcpl	0x00006365
    1474:	7a6c635f 	bvc	1b1a1f8 <_bss_end+0x1b10b94>
    1478:	6261745f 	rsbvs	r7, r1, #1593835520	; 0x5f000000
    147c:	4d524100 	ldfmie	f4, [r2, #-0]
    1480:	0043565f 	subeq	r5, r3, pc, asr r6
    1484:	5f6d7261 	svcpl	0x006d7261
    1488:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    148c:	6373785f 	cmnvs	r3, #6225920	; 0x5f0000
    1490:	00656c61 	rsbeq	r6, r5, r1, ror #24
    1494:	5f4d5241 	svcpl	0x004d5241
    1498:	4100454c 	tstmi	r0, ip, asr #10
    149c:	565f4d52 			; <UNDEFINED> instruction: 0x565f4d52
    14a0:	52410053 	subpl	r0, r1, #83	; 0x53
    14a4:	45475f4d 	strbmi	r5, [r7, #-3917]	; 0xfffff0b3
    14a8:	6d726100 	ldfvse	f6, [r2, #-0]
    14ac:	6e75745f 	mrcvs	4, 3, r7, cr5, cr15, {2}
    14b0:	74735f65 	ldrbtvc	r5, [r3], #-3941	; 0xfffff09b
    14b4:	676e6f72 			; <UNDEFINED> instruction: 0x676e6f72
    14b8:	006d7261 	rsbeq	r7, sp, r1, ror #4
    14bc:	706d6f63 	rsbvc	r6, sp, r3, ror #30
    14c0:	2078656c 	rsbscs	r6, r8, ip, ror #10
    14c4:	616f6c66 	cmnvs	pc, r6, ror #24
    14c8:	41540074 	cmpmi	r4, r4, ror r0
    14cc:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    14d0:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    14d4:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    14d8:	61786574 	cmnvs	r8, r4, ror r5
    14dc:	54003531 	strpl	r3, [r0], #-1329	; 0xfffffacf
    14e0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    14e4:	50435f54 	subpl	r5, r3, r4, asr pc
    14e8:	61665f55 	cmnvs	r6, r5, asr pc
    14ec:	74363237 	ldrtvc	r3, [r6], #-567	; 0xfffffdc9
    14f0:	41540065 	cmpmi	r4, r5, rrx
    14f4:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    14f8:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    14fc:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1500:	61786574 	cmnvs	r8, r4, ror r5
    1504:	41003731 	tstmi	r0, r1, lsr r7
    1508:	475f4d52 			; <UNDEFINED> instruction: 0x475f4d52
    150c:	41540054 	cmpmi	r4, r4, asr r0
    1510:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1514:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1518:	6f656e5f 	svcvs	0x00656e5f
    151c:	73726576 	cmnvc	r2, #494927872	; 0x1d800000
    1520:	00316e65 	eorseq	r6, r1, r5, ror #28
    1524:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
    1528:	2e2e2f2e 	cdpcs	15, 2, cr2, cr14, cr14, {1}
    152c:	2f2e2e2f 	svccs	0x002e2e2f
    1530:	2e2f2e2e 	cdpcs	14, 2, cr2, cr15, cr14, {1}
    1534:	696c2f2e 	stmdbvs	ip!, {r1, r2, r3, r5, r8, r9, sl, fp, sp}^
    1538:	63636762 	cmnvs	r3, #25690112	; 0x1880000
    153c:	62696c2f 	rsbvs	r6, r9, #12032	; 0x2f00
    1540:	32636367 	rsbcc	r6, r3, #-1677721599	; 0x9c000001
    1544:	5400632e 	strpl	r6, [r0], #-814	; 0xfffffcd2
    1548:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    154c:	50435f54 	subpl	r5, r3, r4, asr pc
    1550:	6f635f55 	svcvs	0x00635f55
    1554:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1558:	00663472 	rsbeq	r3, r6, r2, ror r4
    155c:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1560:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    1564:	45375f48 	ldrmi	r5, [r7, #-3912]!	; 0xfffff0b8
    1568:	4154004d 	cmpmi	r4, sp, asr #32
    156c:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1570:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1574:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1578:	61786574 	cmnvs	r8, r4, ror r5
    157c:	68003231 	stmdavs	r0, {r0, r4, r5, r9, ip, sp}
    1580:	76687361 	strbtvc	r7, [r8], -r1, ror #6
    1584:	745f6c61 	ldrbvc	r6, [pc], #-3169	; 158c <CPSR_IRQ_INHIBIT+0x150c>
    1588:	53414200 	movtpl	r4, #4608	; 0x1200
    158c:	52415f45 	subpl	r5, r1, #276	; 0x114
    1590:	365f4843 	ldrbcc	r4, [pc], -r3, asr #16
    1594:	69005a4b 	stmdbvs	r0, {r0, r1, r3, r6, r9, fp, ip, lr}
    1598:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    159c:	00737469 	rsbseq	r7, r3, r9, ror #8
    15a0:	5f6d7261 	svcpl	0x006d7261
    15a4:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    15a8:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    15ac:	6477685f 	ldrbtvs	r6, [r7], #-2143	; 0xfffff7a1
    15b0:	61007669 	tstvs	r0, r9, ror #12
    15b4:	665f6d72 			; <UNDEFINED> instruction: 0x665f6d72
    15b8:	645f7570 	ldrbvs	r7, [pc], #-1392	; 15c0 <CPSR_IRQ_INHIBIT+0x1540>
    15bc:	00637365 	rsbeq	r7, r3, r5, ror #6
    15c0:	5f617369 	svcpl	0x00617369
    15c4:	5f746962 	svcpl	0x00746962
    15c8:	36317066 	ldrtcc	r7, [r1], -r6, rrx
    15cc:	554e4700 	strbpl	r4, [lr, #-1792]	; 0xfffff900
    15d0:	37314320 	ldrcc	r4, [r1, -r0, lsr #6]!
    15d4:	322e3920 	eorcc	r3, lr, #32, 18	; 0x80000
    15d8:	3220312e 	eorcc	r3, r0, #-2147483637	; 0x8000000b
    15dc:	31393130 	teqcc	r9, r0, lsr r1
    15e0:	20353230 	eorscs	r3, r5, r0, lsr r2
    15e4:	6c657228 	sfmvs	f7, 2, [r5], #-160	; 0xffffff60
    15e8:	65736165 	ldrbvs	r6, [r3, #-357]!	; 0xfffffe9b
    15ec:	415b2029 	cmpmi	fp, r9, lsr #32
    15f0:	612f4d52 			; <UNDEFINED> instruction: 0x612f4d52
    15f4:	392d6d72 	pushcc	{r1, r4, r5, r6, r8, sl, fp, sp, lr}
    15f8:	6172622d 	cmnvs	r2, sp, lsr #4
    15fc:	2068636e 	rsbcs	r6, r8, lr, ror #6
    1600:	69766572 	ldmdbvs	r6!, {r1, r4, r5, r6, r8, sl, sp, lr}^
    1604:	6e6f6973 			; <UNDEFINED> instruction: 0x6e6f6973
    1608:	37373220 	ldrcc	r3, [r7, -r0, lsr #4]!
    160c:	5d393935 			; <UNDEFINED> instruction: 0x5d393935
    1610:	616d2d20 	cmnvs	sp, r0, lsr #26
    1614:	2d206d72 	stccs	13, cr6, [r0, #-456]!	; 0xfffffe38
    1618:	6f6c666d 	svcvs	0x006c666d
    161c:	612d7461 			; <UNDEFINED> instruction: 0x612d7461
    1620:	683d6962 	ldmdavs	sp!, {r1, r5, r6, r8, fp, sp, lr}
    1624:	20647261 	rsbcs	r7, r4, r1, ror #4
    1628:	72616d2d 	rsbvc	r6, r1, #2880	; 0xb40
    162c:	613d6863 	teqvs	sp, r3, ror #16
    1630:	35766d72 	ldrbcc	r6, [r6, #-3442]!	; 0xfffff28e
    1634:	662b6574 			; <UNDEFINED> instruction: 0x662b6574
    1638:	672d2070 			; <UNDEFINED> instruction: 0x672d2070
    163c:	20672d20 	rsbcs	r2, r7, r0, lsr #26
    1640:	2d20672d 	stccs	7, cr6, [r0, #-180]!	; 0xffffff4c
    1644:	2d20324f 	sfmcs	f3, 4, [r0, #-316]!	; 0xfffffec4
    1648:	2d20324f 	sfmcs	f3, 4, [r0, #-316]!	; 0xfffffec4
    164c:	2d20324f 	sfmcs	f3, 4, [r0, #-316]!	; 0xfffffec4
    1650:	69756266 	ldmdbvs	r5!, {r1, r2, r5, r6, r9, sp, lr}^
    1654:	6e69646c 	cdpvs	4, 6, cr6, cr9, cr12, {3}
    1658:	696c2d67 	stmdbvs	ip!, {r0, r1, r2, r5, r6, r8, sl, fp, sp}^
    165c:	63636762 	cmnvs	r3, #25690112	; 0x1880000
    1660:	6e662d20 	cdpvs	13, 6, cr2, cr6, cr0, {1}
    1664:	74732d6f 	ldrbtvc	r2, [r3], #-3439	; 0xfffff291
    1668:	2d6b6361 	stclcs	3, cr6, [fp, #-388]!	; 0xfffffe7c
    166c:	746f7270 	strbtvc	r7, [pc], #-624	; 1674 <CPSR_IRQ_INHIBIT+0x15f4>
    1670:	6f746365 	svcvs	0x00746365
    1674:	662d2072 			; <UNDEFINED> instruction: 0x662d2072
    1678:	692d6f6e 	pushvs	{r1, r2, r3, r5, r6, r8, r9, sl, fp, sp, lr}
    167c:	6e696c6e 	cdpvs	12, 6, cr6, cr9, cr14, {3}
    1680:	662d2065 	strtvs	r2, [sp], -r5, rrx
    1684:	69736976 	ldmdbvs	r3!, {r1, r2, r4, r5, r6, r8, fp, sp, lr}^
    1688:	696c6962 	stmdbvs	ip!, {r1, r5, r6, r8, fp, sp, lr}^
    168c:	683d7974 	ldmdavs	sp!, {r2, r4, r5, r6, r8, fp, ip, sp, lr}
    1690:	65646469 	strbvs	r6, [r4, #-1129]!	; 0xfffffb97
    1694:	5241006e 	subpl	r0, r1, #110	; 0x6e
    1698:	49485f4d 	stmdbmi	r8, {r0, r2, r3, r6, r8, r9, sl, fp, ip, lr}^
    169c:	61736900 	cmnvs	r3, r0, lsl #18
    16a0:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    16a4:	6964615f 	stmdbvs	r4!, {r0, r1, r2, r3, r4, r6, r8, sp, lr}^
    16a8:	41540076 	cmpmi	r4, r6, ror r0
    16ac:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    16b0:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    16b4:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    16b8:	36333131 			; <UNDEFINED> instruction: 0x36333131
    16bc:	5400736a 	strpl	r7, [r0], #-874	; 0xfffffc96
    16c0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    16c4:	50435f54 	subpl	r5, r3, r4, asr pc
    16c8:	72615f55 	rsbvc	r5, r1, #340	; 0x154
    16cc:	5400386d 	strpl	r3, [r0], #-2157	; 0xfffff793
    16d0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    16d4:	50435f54 	subpl	r5, r3, r4, asr pc
    16d8:	72615f55 	rsbvc	r5, r1, #340	; 0x154
    16dc:	5400396d 	strpl	r3, [r0], #-2413	; 0xfffff693
    16e0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    16e4:	50435f54 	subpl	r5, r3, r4, asr pc
    16e8:	61665f55 	cmnvs	r6, r5, asr pc
    16ec:	00363236 	eorseq	r3, r6, r6, lsr r2
    16f0:	676e6f6c 	strbvs	r6, [lr, -ip, ror #30]!
    16f4:	6e6f6c20 	cdpvs	12, 6, cr6, cr15, cr0, {1}
    16f8:	6e752067 	cdpvs	0, 7, cr2, cr5, cr7, {3}
    16fc:	6e676973 			; <UNDEFINED> instruction: 0x6e676973
    1700:	69206465 	stmdbvs	r0!, {r0, r2, r5, r6, sl, sp, lr}
    1704:	6100746e 	tstvs	r0, lr, ror #8
    1708:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    170c:	5f686372 	svcpl	0x00686372
    1710:	65736d63 	ldrbvs	r6, [r3, #-3427]!	; 0xfffff29d
    1714:	52415400 	subpl	r5, r1, #0, 8
    1718:	5f544547 	svcpl	0x00544547
    171c:	5f555043 	svcpl	0x00555043
    1720:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1724:	346d7865 	strbtcc	r7, [sp], #-2149	; 0xfffff79b
    1728:	52415400 	subpl	r5, r1, #0, 8
    172c:	5f544547 	svcpl	0x00544547
    1730:	5f555043 	svcpl	0x00555043
    1734:	316d7261 	cmncc	sp, r1, ror #4
    1738:	54006530 	strpl	r6, [r0], #-1328	; 0xfffffad0
    173c:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1740:	50435f54 	subpl	r5, r3, r4, asr pc
    1744:	6f635f55 	svcvs	0x00635f55
    1748:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    174c:	6100376d 	tstvs	r0, sp, ror #14
    1750:	635f6d72 	cmpvs	pc, #7296	; 0x1c80
    1754:	5f646e6f 	svcpl	0x00646e6f
    1758:	65646f63 	strbvs	r6, [r4, #-3939]!	; 0xfffff09d
    175c:	4d524100 	ldfmie	f4, [r2, #-0]
    1760:	5343505f 	movtpl	r5, #12383	; 0x305f
    1764:	5041415f 	subpl	r4, r1, pc, asr r1
    1768:	69005343 	stmdbvs	r0, {r0, r1, r6, r8, r9, ip, lr}
    176c:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1770:	615f7469 	cmpvs	pc, r9, ror #8
    1774:	38766d72 	ldmdacc	r6!, {r1, r4, r5, r6, r8, sl, fp, sp, lr}^
    1778:	4200325f 	andmi	r3, r0, #-268435451	; 0xf0000005
    177c:	5f455341 	svcpl	0x00455341
    1780:	48435241 	stmdami	r3, {r0, r6, r9, ip, lr}^
    1784:	004d335f 	subeq	r3, sp, pc, asr r3
    1788:	47524154 			; <UNDEFINED> instruction: 0x47524154
    178c:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1790:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    1794:	31376d72 	teqcc	r7, r2, ror sp
    1798:	61007430 	tstvs	r0, r0, lsr r4
    179c:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    17a0:	5f686372 	svcpl	0x00686372
    17a4:	6d6d7769 	stclvs	7, cr7, [sp, #-420]!	; 0xfffffe5c
    17a8:	00327478 	eorseq	r7, r2, r8, ror r4
    17ac:	5f617369 	svcpl	0x00617369
    17b0:	5f6d756e 	svcpl	0x006d756e
    17b4:	73746962 	cmnvc	r4, #1605632	; 0x188000
    17b8:	52415400 	subpl	r5, r1, #0, 8
    17bc:	5f544547 	svcpl	0x00544547
    17c0:	5f555043 	svcpl	0x00555043
    17c4:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    17c8:	306d7865 	rsbcc	r7, sp, r5, ror #16
    17cc:	73756c70 	cmnvc	r5, #112, 24	; 0x7000
    17d0:	6c616d73 	stclvs	13, cr6, [r1], #-460	; 0xfffffe34
    17d4:	6c756d6c 	ldclvs	13, cr6, [r5], #-432	; 0xfffffe50
    17d8:	6c706974 			; <UNDEFINED> instruction: 0x6c706974
    17dc:	41540079 	cmpmi	r4, r9, ror r0
    17e0:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    17e4:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    17e8:	7978655f 	ldmdbvc	r8!, {r0, r1, r2, r3, r4, r6, r8, sl, sp, lr}^
    17ec:	6d736f6e 	ldclvs	15, cr6, [r3, #-440]!	; 0xfffffe48
    17f0:	41540031 	cmpmi	r4, r1, lsr r0
    17f4:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    17f8:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    17fc:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1800:	72786574 	rsbsvc	r6, r8, #116, 10	; 0x1d000000
    1804:	69003235 	stmdbvs	r0, {r0, r2, r4, r5, r9, ip, sp}
    1808:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    180c:	745f7469 	ldrbvc	r7, [pc], #-1129	; 1814 <CPSR_IRQ_INHIBIT+0x1794>
    1810:	00766964 	rsbseq	r6, r6, r4, ror #18
    1814:	66657270 			; <UNDEFINED> instruction: 0x66657270
    1818:	6e5f7265 	cdpvs	2, 5, cr7, cr15, cr5, {3}
    181c:	5f6e6f65 	svcpl	0x006e6f65
    1820:	5f726f66 	svcpl	0x00726f66
    1824:	69623436 	stmdbvs	r2!, {r1, r2, r4, r5, sl, ip, sp}^
    1828:	69007374 	stmdbvs	r0, {r2, r4, r5, r6, r8, r9, ip, sp, lr}
    182c:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1830:	665f7469 	ldrbvs	r7, [pc], -r9, ror #8
    1834:	66363170 			; <UNDEFINED> instruction: 0x66363170
    1838:	54006c6d 	strpl	r6, [r0], #-3181	; 0xfffff393
    183c:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1840:	50435f54 	subpl	r5, r3, r4, asr pc
    1844:	6f635f55 	svcvs	0x00635f55
    1848:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    184c:	00323361 	eorseq	r3, r2, r1, ror #6
    1850:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1854:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1858:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    185c:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1860:	35336178 	ldrcc	r6, [r3, #-376]!	; 0xfffffe88
    1864:	61736900 	cmnvs	r3, r0, lsl #18
    1868:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    186c:	3170665f 	cmncc	r0, pc, asr r6
    1870:	6e6f6336 	mcrvs	3, 3, r6, cr15, cr6, {1}
    1874:	6e750076 	mrcvs	0, 3, r0, cr5, cr6, {3}
    1878:	63657073 	cmnvs	r5, #115	; 0x73
    187c:	74735f76 	ldrbtvc	r5, [r3], #-3958	; 0xfffff08a
    1880:	676e6972 			; <UNDEFINED> instruction: 0x676e6972
    1884:	41540073 	cmpmi	r4, r3, ror r0
    1888:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    188c:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1890:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    1894:	36353131 			; <UNDEFINED> instruction: 0x36353131
    1898:	00733274 	rsbseq	r3, r3, r4, ror r2
    189c:	47524154 			; <UNDEFINED> instruction: 0x47524154
    18a0:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    18a4:	665f5550 			; <UNDEFINED> instruction: 0x665f5550
    18a8:	36303661 	ldrtcc	r3, [r0], -r1, ror #12
    18ac:	54006574 	strpl	r6, [r0], #-1396	; 0xfffffa8c
    18b0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    18b4:	50435f54 	subpl	r5, r3, r4, asr pc
    18b8:	72615f55 	rsbvc	r5, r1, #340	; 0x154
    18bc:	3632396d 	ldrtcc	r3, [r2], -sp, ror #18
    18c0:	00736a65 	rsbseq	r6, r3, r5, ror #20
    18c4:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    18c8:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    18cc:	54345f48 	ldrtpl	r5, [r4], #-3912	; 0xfffff0b8
    18d0:	61736900 	cmnvs	r3, r0, lsl #18
    18d4:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    18d8:	7972635f 	ldmdbvc	r2!, {r0, r1, r2, r3, r4, r6, r8, r9, sp, lr}^
    18dc:	006f7470 	rsbeq	r7, pc, r0, ror r4	; <UNPREDICTABLE>
    18e0:	5f6d7261 	svcpl	0x006d7261
    18e4:	73676572 	cmnvc	r7, #478150656	; 0x1c800000
    18e8:	5f6e695f 	svcpl	0x006e695f
    18ec:	75716573 	ldrbvc	r6, [r1, #-1395]!	; 0xfffffa8d
    18f0:	65636e65 	strbvs	r6, [r3, #-3685]!	; 0xfffff19b
    18f4:	61736900 	cmnvs	r3, r0, lsl #18
    18f8:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    18fc:	0062735f 	rsbeq	r7, r2, pc, asr r3
    1900:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1904:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    1908:	54355f48 	ldrtpl	r5, [r5], #-3912	; 0xfffff0b8
    190c:	73690045 	cmnvc	r9, #69	; 0x45
    1910:	65665f61 	strbvs	r5, [r6, #-3937]!	; 0xfffff09f
    1914:	72757461 	rsbsvc	r7, r5, #1627389952	; 0x61000000
    1918:	73690065 	cmnvc	r9, #101	; 0x65
    191c:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    1920:	6d735f74 	ldclvs	15, cr5, [r3, #-464]!	; 0xfffffe30
    1924:	6d6c6c61 	stclvs	12, cr6, [ip, #-388]!	; 0xfffffe7c
    1928:	61006c75 	tstvs	r0, r5, ror ip
    192c:	6c5f6d72 	mrrcvs	13, 7, r6, pc, cr2	; <UNPREDICTABLE>
    1930:	5f676e61 	svcpl	0x00676e61
    1934:	7074756f 	rsbsvc	r7, r4, pc, ror #10
    1938:	6f5f7475 	svcvs	0x005f7475
    193c:	63656a62 	cmnvs	r5, #401408	; 0x62000
    1940:	74615f74 	strbtvc	r5, [r1], #-3956	; 0xfffff08c
    1944:	62697274 	rsbvs	r7, r9, #116, 4	; 0x40000007
    1948:	73657475 	cmnvc	r5, #1962934272	; 0x75000000
    194c:	6f6f685f 	svcvs	0x006f685f
    1950:	7369006b 	cmnvc	r9, #107	; 0x6b
    1954:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    1958:	70665f74 	rsbvc	r5, r6, r4, ror pc
    195c:	3233645f 	eorscc	r6, r3, #1593835520	; 0x5f000000
    1960:	4d524100 	ldfmie	f4, [r2, #-0]
    1964:	00454e5f 	subeq	r4, r5, pc, asr lr
    1968:	5f617369 	svcpl	0x00617369
    196c:	5f746962 	svcpl	0x00746962
    1970:	00386562 	eorseq	r6, r8, r2, ror #10
    1974:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1978:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    197c:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    1980:	31316d72 	teqcc	r1, r2, ror sp
    1984:	7a6a3637 	bvc	1a8f268 <_bss_end+0x1a85c04>
    1988:	72700073 	rsbsvc	r0, r0, #115	; 0x73
    198c:	7365636f 	cmnvc	r5, #-1140850687	; 0xbc000001
    1990:	5f726f73 	svcpl	0x00726f73
    1994:	65707974 	ldrbvs	r7, [r0, #-2420]!	; 0xfffff68c
    1998:	6c6c6100 	stfvse	f6, [ip], #-0
    199c:	7570665f 	ldrbvc	r6, [r0, #-1631]!	; 0xfffff9a1
    19a0:	72610073 	rsbvc	r0, r1, #115	; 0x73
    19a4:	63705f6d 	cmnvs	r0, #436	; 0x1b4
    19a8:	41420073 	hvcmi	8195	; 0x2003
    19ac:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    19b0:	5f484352 	svcpl	0x00484352
    19b4:	61005435 	tstvs	r0, r5, lsr r4
    19b8:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    19bc:	34686372 	strbtcc	r6, [r8], #-882	; 0xfffffc8e
    19c0:	41540074 	cmpmi	r4, r4, ror r0
    19c4:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    19c8:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    19cc:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    19d0:	61786574 	cmnvs	r8, r4, ror r5
    19d4:	6f633637 	svcvs	0x00633637
    19d8:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    19dc:	00353561 	eorseq	r3, r5, r1, ror #10
    19e0:	5f6d7261 	svcpl	0x006d7261
    19e4:	656e7574 	strbvs	r7, [lr, #-1396]!	; 0xfffffa8c
    19e8:	7562775f 	strbvc	r7, [r2, #-1887]!	; 0xfffff8a1
    19ec:	74680066 	strbtvc	r0, [r8], #-102	; 0xffffff9a
    19f0:	685f6261 	ldmdavs	pc, {r0, r5, r6, r9, sp, lr}^	; <UNPREDICTABLE>
    19f4:	00687361 	rsbeq	r7, r8, r1, ror #6
    19f8:	5f617369 	svcpl	0x00617369
    19fc:	5f746962 	svcpl	0x00746962
    1a00:	72697571 	rsbvc	r7, r9, #473956352	; 0x1c400000
    1a04:	6f6e5f6b 	svcvs	0x006e5f6b
    1a08:	6c6f765f 	stclvs	6, cr7, [pc], #-380	; 1894 <CPSR_IRQ_INHIBIT+0x1814>
    1a0c:	6c697461 	cfstrdvs	mvd7, [r9], #-388	; 0xfffffe7c
    1a10:	65635f65 	strbvs	r5, [r3, #-3941]!	; 0xfffff09b
    1a14:	52415400 	subpl	r5, r1, #0, 8
    1a18:	5f544547 	svcpl	0x00544547
    1a1c:	5f555043 	svcpl	0x00555043
    1a20:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1a24:	306d7865 	rsbcc	r7, sp, r5, ror #16
    1a28:	52415400 	subpl	r5, r1, #0, 8
    1a2c:	5f544547 	svcpl	0x00544547
    1a30:	5f555043 	svcpl	0x00555043
    1a34:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1a38:	316d7865 	cmncc	sp, r5, ror #16
    1a3c:	52415400 	subpl	r5, r1, #0, 8
    1a40:	5f544547 	svcpl	0x00544547
    1a44:	5f555043 	svcpl	0x00555043
    1a48:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1a4c:	336d7865 	cmncc	sp, #6619136	; 0x650000
    1a50:	61736900 	cmnvs	r3, r0, lsl #18
    1a54:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1a58:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    1a5c:	315f3876 	cmpcc	pc, r6, ror r8	; <UNPREDICTABLE>
    1a60:	6d726100 	ldfvse	f6, [r2, #-0]
    1a64:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    1a68:	616e5f68 	cmnvs	lr, r8, ror #30
    1a6c:	6900656d 	stmdbvs	r0, {r0, r2, r3, r5, r6, r8, sl, sp, lr}
    1a70:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1a74:	615f7469 	cmpvs	pc, r9, ror #8
    1a78:	38766d72 	ldmdacc	r6!, {r1, r4, r5, r6, r8, sl, fp, sp, lr}^
    1a7c:	6900335f 	stmdbvs	r0, {r0, r1, r2, r3, r4, r6, r8, r9, ip, sp}
    1a80:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1a84:	615f7469 	cmpvs	pc, r9, ror #8
    1a88:	38766d72 	ldmdacc	r6!, {r1, r4, r5, r6, r8, sl, fp, sp, lr}^
    1a8c:	6900345f 	stmdbvs	r0, {r0, r1, r2, r3, r4, r6, sl, ip, sp}
    1a90:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1a94:	615f7469 	cmpvs	pc, r9, ror #8
    1a98:	38766d72 	ldmdacc	r6!, {r1, r4, r5, r6, r8, sl, fp, sp, lr}^
    1a9c:	5400355f 	strpl	r3, [r0], #-1375	; 0xfffffaa1
    1aa0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1aa4:	50435f54 	subpl	r5, r3, r4, asr pc
    1aa8:	6f635f55 	svcvs	0x00635f55
    1aac:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1ab0:	00333561 	eorseq	r3, r3, r1, ror #10
    1ab4:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1ab8:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1abc:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1ac0:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1ac4:	35356178 	ldrcc	r6, [r5, #-376]!	; 0xfffffe88
    1ac8:	52415400 	subpl	r5, r1, #0, 8
    1acc:	5f544547 	svcpl	0x00544547
    1ad0:	5f555043 	svcpl	0x00555043
    1ad4:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1ad8:	35617865 	strbcc	r7, [r1, #-2149]!	; 0xfffff79b
    1adc:	41540037 	cmpmi	r4, r7, lsr r0
    1ae0:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1ae4:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1ae8:	63706d5f 	cmnvs	r0, #6080	; 0x17c0
    1aec:	0065726f 	rsbeq	r7, r5, pc, ror #4
    1af0:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1af4:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1af8:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    1afc:	6e5f6d72 	mrcvs	13, 2, r6, cr15, cr2, {3}
    1b00:	00656e6f 	rsbeq	r6, r5, pc, ror #28
    1b04:	5f6d7261 	svcpl	0x006d7261
    1b08:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    1b0c:	746f6e5f 	strbtvc	r6, [pc], #-3679	; 1b14 <CPSR_IRQ_INHIBIT+0x1a94>
    1b10:	4154006d 	cmpmi	r4, sp, rrx
    1b14:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1b18:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1b1c:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    1b20:	36323031 			; <UNDEFINED> instruction: 0x36323031
    1b24:	00736a65 	rsbseq	r6, r3, r5, ror #20
    1b28:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1b2c:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    1b30:	4a365f48 	bmi	d99858 <_bss_end+0xd901f4>
    1b34:	53414200 	movtpl	r4, #4608	; 0x1200
    1b38:	52415f45 	subpl	r5, r1, #276	; 0x114
    1b3c:	365f4843 	ldrbcc	r4, [pc], -r3, asr #16
    1b40:	4142004b 	cmpmi	r2, fp, asr #32
    1b44:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1b48:	5f484352 	svcpl	0x00484352
    1b4c:	69004d36 	stmdbvs	r0, {r1, r2, r4, r5, r8, sl, fp, lr}
    1b50:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1b54:	695f7469 	ldmdbvs	pc, {r0, r3, r5, r6, sl, ip, sp, lr}^	; <UNPREDICTABLE>
    1b58:	786d6d77 	stmdavc	sp!, {r0, r1, r2, r4, r5, r6, r8, sl, fp, sp, lr}^
    1b5c:	41540074 	cmpmi	r4, r4, ror r0
    1b60:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1b64:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1b68:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    1b6c:	36333131 			; <UNDEFINED> instruction: 0x36333131
    1b70:	0073666a 	rsbseq	r6, r3, sl, ror #12
    1b74:	5f4d5241 	svcpl	0x004d5241
    1b78:	4100534c 	tstmi	r0, ip, asr #6
    1b7c:	4c5f4d52 	mrrcmi	13, 5, r4, pc, cr2	; <UNPREDICTABLE>
    1b80:	41420054 	qdaddmi	r0, r4, r2
    1b84:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1b88:	5f484352 	svcpl	0x00484352
    1b8c:	54005a36 	strpl	r5, [r0], #-2614	; 0xfffff5ca
    1b90:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1b94:	50435f54 	subpl	r5, r3, r4, asr pc
    1b98:	6f635f55 	svcvs	0x00635f55
    1b9c:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1ba0:	63353761 	teqvs	r5, #25427968	; 0x1840000
    1ba4:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1ba8:	35356178 	ldrcc	r6, [r5, #-376]!	; 0xfffffe88
    1bac:	4d524100 	ldfmie	f4, [r2, #-0]
    1bb0:	5343505f 	movtpl	r5, #12383	; 0x305f
    1bb4:	5041415f 	subpl	r4, r1, pc, asr r1
    1bb8:	565f5343 	ldrbpl	r5, [pc], -r3, asr #6
    1bbc:	54005046 	strpl	r5, [r0], #-70	; 0xffffffba
    1bc0:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1bc4:	50435f54 	subpl	r5, r3, r4, asr pc
    1bc8:	77695f55 			; <UNDEFINED> instruction: 0x77695f55
    1bcc:	74786d6d 	ldrbtvc	r6, [r8], #-3437	; 0xfffff293
    1bd0:	73690032 	cmnvc	r9, #50	; 0x32
    1bd4:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    1bd8:	656e5f74 	strbvs	r5, [lr, #-3956]!	; 0xfffff08c
    1bdc:	61006e6f 	tstvs	r0, pc, ror #28
    1be0:	665f6d72 			; <UNDEFINED> instruction: 0x665f6d72
    1be4:	615f7570 	cmpvs	pc, r0, ror r5	; <UNPREDICTABLE>
    1be8:	00727474 	rsbseq	r7, r2, r4, ror r4
    1bec:	5f617369 	svcpl	0x00617369
    1bf0:	5f746962 	svcpl	0x00746962
    1bf4:	766d7261 	strbtvc	r7, [sp], -r1, ror #4
    1bf8:	006d6537 	rsbeq	r6, sp, r7, lsr r5
    1bfc:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1c00:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1c04:	665f5550 			; <UNDEFINED> instruction: 0x665f5550
    1c08:	36323661 	ldrtcc	r3, [r2], -r1, ror #12
    1c0c:	54006574 	strpl	r6, [r0], #-1396	; 0xfffffa8c
    1c10:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1c14:	50435f54 	subpl	r5, r3, r4, asr pc
    1c18:	616d5f55 	cmnvs	sp, r5, asr pc
    1c1c:	6c657672 	stclvs	6, cr7, [r5], #-456	; 0xfffffe38
    1c20:	6a705f6c 	bvs	1c199d8 <_bss_end+0x1c10374>
    1c24:	74680034 	strbtvc	r0, [r8], #-52	; 0xffffffcc
    1c28:	685f6261 	ldmdavs	pc, {r0, r5, r6, r9, sp, lr}^	; <UNPREDICTABLE>
    1c2c:	5f687361 	svcpl	0x00687361
    1c30:	6e696f70 	mcrvs	15, 3, r6, cr9, cr0, {3}
    1c34:	00726574 	rsbseq	r6, r2, r4, ror r5
    1c38:	5f6d7261 	svcpl	0x006d7261
    1c3c:	656e7574 	strbvs	r7, [lr, #-1396]!	; 0xfffffa8c
    1c40:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1c44:	5f786574 	svcpl	0x00786574
    1c48:	69003961 	stmdbvs	r0, {r0, r5, r6, r8, fp, ip, sp}
    1c4c:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1c50:	695f7469 	ldmdbvs	pc, {r0, r3, r5, r6, sl, ip, sp, lr}^	; <UNPREDICTABLE>
    1c54:	786d6d77 	stmdavc	sp!, {r0, r1, r2, r4, r5, r6, r8, sl, fp, sp, lr}^
    1c58:	54003274 	strpl	r3, [r0], #-628	; 0xfffffd8c
    1c5c:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1c60:	50435f54 	subpl	r5, r3, r4, asr pc
    1c64:	6f635f55 	svcvs	0x00635f55
    1c68:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1c6c:	63323761 	teqvs	r2, #25427968	; 0x1840000
    1c70:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1c74:	33356178 	teqcc	r5, #120, 2
    1c78:	61736900 	cmnvs	r3, r0, lsl #18
    1c7c:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1c80:	7568745f 	strbvc	r7, [r8, #-1119]!	; 0xfffffba1
    1c84:	0032626d 	eorseq	r6, r2, sp, ror #4
    1c88:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1c8c:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    1c90:	41375f48 	teqmi	r7, r8, asr #30
    1c94:	61736900 	cmnvs	r3, r0, lsl #18
    1c98:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1c9c:	746f645f 	strbtvc	r6, [pc], #-1119	; 1ca4 <CPSR_IRQ_INHIBIT+0x1c24>
    1ca0:	646f7270 	strbtvs	r7, [pc], #-624	; 1ca8 <CPSR_IRQ_INHIBIT+0x1c28>
    1ca4:	6d726100 	ldfvse	f6, [r2, #-0]
    1ca8:	3170665f 	cmncc	r0, pc, asr r6
    1cac:	79745f36 	ldmdbvc	r4!, {r1, r2, r4, r5, r8, r9, sl, fp, ip, lr}^
    1cb0:	6e5f6570 	mrcvs	5, 2, r6, cr15, cr0, {3}
    1cb4:	0065646f 	rsbeq	r6, r5, pc, ror #8
    1cb8:	5f4d5241 	svcpl	0x004d5241
    1cbc:	6100494d 	tstvs	r0, sp, asr #18
    1cc0:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    1cc4:	36686372 			; <UNDEFINED> instruction: 0x36686372
    1cc8:	7261006b 	rsbvc	r0, r1, #107	; 0x6b
    1ccc:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    1cd0:	6d366863 	ldcvs	8, cr6, [r6, #-396]!	; 0xfffffe74
    1cd4:	53414200 	movtpl	r4, #4608	; 0x1200
    1cd8:	52415f45 	subpl	r5, r1, #276	; 0x114
    1cdc:	375f4843 	ldrbcc	r4, [pc, -r3, asr #16]
    1ce0:	5f5f0052 	svcpl	0x005f0052
    1ce4:	63706f70 	cmnvs	r0, #112, 30	; 0x1c0
    1ce8:	746e756f 	strbtvc	r7, [lr], #-1391	; 0xfffffa91
    1cec:	6261745f 	rsbvs	r7, r1, #1593835520	; 0x5f000000
    1cf0:	61736900 	cmnvs	r3, r0, lsl #18
    1cf4:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1cf8:	736d635f 	cmnvc	sp, #2080374785	; 0x7c000001
    1cfc:	41540065 	cmpmi	r4, r5, rrx
    1d00:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1d04:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1d08:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1d0c:	61786574 	cmnvs	r8, r4, ror r5
    1d10:	54003337 	strpl	r3, [r0], #-823	; 0xfffffcc9
    1d14:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1d18:	50435f54 	subpl	r5, r3, r4, asr pc
    1d1c:	65675f55 	strbvs	r5, [r7, #-3925]!	; 0xfffff0ab
    1d20:	6972656e 	ldmdbvs	r2!, {r1, r2, r3, r5, r6, r8, sl, sp, lr}^
    1d24:	61377663 	teqvs	r7, r3, ror #12
    1d28:	52415400 	subpl	r5, r1, #0, 8
    1d2c:	5f544547 	svcpl	0x00544547
    1d30:	5f555043 	svcpl	0x00555043
    1d34:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    1d38:	37617865 	strbcc	r7, [r1, -r5, ror #16]!
    1d3c:	72610036 	rsbvc	r0, r1, #54	; 0x36
    1d40:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    1d44:	6e5f6863 	cdpvs	8, 5, cr6, cr15, cr3, {3}
    1d48:	6f765f6f 	svcvs	0x00765f6f
    1d4c:	6974616c 	ldmdbvs	r4!, {r2, r3, r5, r6, r8, sp, lr}^
    1d50:	635f656c 	cmpvs	pc, #108, 10	; 0x1b000000
    1d54:	41420065 	cmpmi	r2, r5, rrx
    1d58:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1d5c:	5f484352 	svcpl	0x00484352
    1d60:	69004138 	stmdbvs	r0, {r3, r4, r5, r8, lr}
    1d64:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1d68:	615f7469 	cmpvs	pc, r9, ror #8
    1d6c:	35766d72 	ldrbcc	r6, [r6, #-3442]!	; 0xfffff28e
    1d70:	41420074 	hvcmi	8196	; 0x2004
    1d74:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1d78:	5f484352 	svcpl	0x00484352
    1d7c:	54005238 	strpl	r5, [r0], #-568	; 0xfffffdc8
    1d80:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1d84:	50435f54 	subpl	r5, r3, r4, asr pc
    1d88:	6f635f55 	svcvs	0x00635f55
    1d8c:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1d90:	63333761 	teqvs	r3, #25427968	; 0x1840000
    1d94:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1d98:	35336178 	ldrcc	r6, [r3, #-376]!	; 0xfffffe88
    1d9c:	4d524100 	ldfmie	f4, [r2, #-0]
    1da0:	00564e5f 	subseq	r4, r6, pc, asr lr
    1da4:	5f6d7261 	svcpl	0x006d7261
    1da8:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    1dac:	72610034 	rsbvc	r0, r1, #52	; 0x34
    1db0:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    1db4:	00366863 	eorseq	r6, r6, r3, ror #16
    1db8:	5f6d7261 	svcpl	0x006d7261
    1dbc:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    1dc0:	72610037 	rsbvc	r0, r1, #55	; 0x37
    1dc4:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    1dc8:	00386863 	eorseq	r6, r8, r3, ror #16
    1dcc:	676e6f6c 	strbvs	r6, [lr, -ip, ror #30]!
    1dd0:	756f6420 	strbvc	r6, [pc, #-1056]!	; 19b8 <CPSR_IRQ_INHIBIT+0x1938>
    1dd4:	00656c62 	rsbeq	r6, r5, r2, ror #24
    1dd8:	5f6d7261 	svcpl	0x006d7261
    1ddc:	656e7574 	strbvs	r7, [lr, #-1396]!	; 0xfffffa8c
    1de0:	6373785f 	cmnvs	r3, #6225920	; 0x5f0000
    1de4:	00656c61 	rsbeq	r6, r5, r1, ror #24
    1de8:	696b616d 	stmdbvs	fp!, {r0, r2, r3, r5, r6, r8, sp, lr}^
    1dec:	635f676e 	cmpvs	pc, #28835840	; 0x1b80000
    1df0:	74736e6f 	ldrbtvc	r6, [r3], #-3695	; 0xfffff191
    1df4:	6261745f 	rsbvs	r7, r1, #1593835520	; 0x5f000000
    1df8:	7400656c 	strvc	r6, [r0], #-1388	; 0xfffffa94
    1dfc:	626d7568 	rsbvs	r7, sp, #104, 10	; 0x1a000000
    1e00:	6c61635f 	stclvs	3, cr6, [r1], #-380	; 0xfffffe84
    1e04:	69765f6c 	ldmdbvs	r6!, {r2, r3, r5, r6, r8, r9, sl, fp, ip, lr}^
    1e08:	616c5f61 	cmnvs	ip, r1, ror #30
    1e0c:	006c6562 	rsbeq	r6, ip, r2, ror #10
    1e10:	5f617369 	svcpl	0x00617369
    1e14:	5f746962 	svcpl	0x00746962
    1e18:	35767066 	ldrbcc	r7, [r6, #-102]!	; 0xffffff9a
    1e1c:	61736900 	cmnvs	r3, r0, lsl #18
    1e20:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    1e24:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    1e28:	006b3676 	rsbeq	r3, fp, r6, ror r6
    1e2c:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1e30:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1e34:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1e38:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1e3c:	00376178 	eorseq	r6, r7, r8, ror r1
    1e40:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1e44:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1e48:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1e4c:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1e50:	00386178 	eorseq	r6, r8, r8, ror r1
    1e54:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1e58:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1e5c:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1e60:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1e64:	00396178 	eorseq	r6, r9, r8, ror r1
    1e68:	5f4d5241 	svcpl	0x004d5241
    1e6c:	5f534350 	svcpl	0x00534350
    1e70:	53435041 	movtpl	r5, #12353	; 0x3041
    1e74:	4d524100 	ldfmie	f4, [r2, #-0]
    1e78:	5343505f 	movtpl	r5, #12383	; 0x305f
    1e7c:	5054415f 	subspl	r4, r4, pc, asr r1
    1e80:	63005343 	movwvs	r5, #835	; 0x343
    1e84:	6c706d6f 	ldclvs	13, cr6, [r0], #-444	; 0xfffffe44
    1e88:	64207865 	strtvs	r7, [r0], #-2149	; 0xfffff79b
    1e8c:	6c62756f 	cfstr64vs	mvdx7, [r2], #-444	; 0xfffffe44
    1e90:	41540065 	cmpmi	r4, r5, rrx
    1e94:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    1e98:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    1e9c:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    1ea0:	61786574 	cmnvs	r8, r4, ror r5
    1ea4:	6f633337 	svcvs	0x00633337
    1ea8:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1eac:	00333561 	eorseq	r3, r3, r1, ror #10
    1eb0:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1eb4:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1eb8:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1ebc:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1ec0:	70306d78 	eorsvc	r6, r0, r8, ror sp
    1ec4:	0073756c 	rsbseq	r7, r3, ip, ror #10
    1ec8:	5f6d7261 	svcpl	0x006d7261
    1ecc:	69006363 	stmdbvs	r0, {r0, r1, r5, r6, r8, r9, sp, lr}
    1ed0:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1ed4:	785f7469 	ldmdavc	pc, {r0, r3, r5, r6, sl, ip, sp, lr}^	; <UNPREDICTABLE>
    1ed8:	6c616373 	stclvs	3, cr6, [r1], #-460	; 0xfffffe34
    1edc:	645f0065 	ldrbvs	r0, [pc], #-101	; 1ee4 <CPSR_IRQ_INHIBIT+0x1e64>
    1ee0:	5f746e6f 	svcpl	0x00746e6f
    1ee4:	5f657375 	svcpl	0x00657375
    1ee8:	65657274 	strbvs	r7, [r5, #-628]!	; 0xfffffd8c
    1eec:	7265685f 	rsbvc	r6, r5, #6225920	; 0x5f0000
    1ef0:	54005f65 	strpl	r5, [r0], #-3941	; 0xfffff09b
    1ef4:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1ef8:	50435f54 	subpl	r5, r3, r4, asr pc
    1efc:	72615f55 	rsbvc	r5, r1, #340	; 0x154
    1f00:	7430316d 	ldrtvc	r3, [r0], #-365	; 0xfffffe93
    1f04:	00696d64 	rsbeq	r6, r9, r4, ror #26
    1f08:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1f0c:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1f10:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    1f14:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    1f18:	00356178 	eorseq	r6, r5, r8, ror r1
    1f1c:	65736162 	ldrbvs	r6, [r3, #-354]!	; 0xfffffe9e
    1f20:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    1f24:	65746968 	ldrbvs	r6, [r4, #-2408]!	; 0xfffff698
    1f28:	72757463 	rsbsvc	r7, r5, #1660944384	; 0x63000000
    1f2c:	72610065 	rsbvc	r0, r1, #101	; 0x65
    1f30:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    1f34:	635f6863 	cmpvs	pc, #6488064	; 0x630000
    1f38:	54006372 	strpl	r6, [r0], #-882	; 0xfffffc8e
    1f3c:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    1f40:	50435f54 	subpl	r5, r3, r4, asr pc
    1f44:	6f635f55 	svcvs	0x00635f55
    1f48:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    1f4c:	6d73316d 	ldfvse	f3, [r3, #-436]!	; 0xfffffe4c
    1f50:	6d6c6c61 	stclvs	12, cr6, [ip, #-388]!	; 0xfffffe7c
    1f54:	69746c75 	ldmdbvs	r4!, {r0, r2, r4, r5, r6, sl, fp, sp, lr}^
    1f58:	00796c70 	rsbseq	r6, r9, r0, ror ip
    1f5c:	5f6d7261 	svcpl	0x006d7261
    1f60:	72727563 	rsbsvc	r7, r2, #415236096	; 0x18c00000
    1f64:	5f746e65 	svcpl	0x00746e65
    1f68:	69006363 	stmdbvs	r0, {r0, r1, r5, r6, r8, r9, sp, lr}
    1f6c:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    1f70:	635f7469 	cmpvs	pc, #1761607680	; 0x69000000
    1f74:	32336372 	eorscc	r6, r3, #-939524095	; 0xc8000001
    1f78:	4d524100 	ldfmie	f4, [r2, #-0]
    1f7c:	004c505f 	subeq	r5, ip, pc, asr r0
    1f80:	5f617369 	svcpl	0x00617369
    1f84:	5f746962 	svcpl	0x00746962
    1f88:	76706676 			; <UNDEFINED> instruction: 0x76706676
    1f8c:	73690033 	cmnvc	r9, #51	; 0x33
    1f90:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    1f94:	66765f74 	uhsub16vs	r5, r6, r4
    1f98:	00347670 	eorseq	r7, r4, r0, ror r6
    1f9c:	45534142 	ldrbmi	r4, [r3, #-322]	; 0xfffffebe
    1fa0:	4352415f 	cmpmi	r2, #-1073741801	; 0xc0000017
    1fa4:	54365f48 	ldrtpl	r5, [r6], #-3912	; 0xfffff0b8
    1fa8:	41420032 	cmpmi	r2, r2, lsr r0
    1fac:	415f4553 	cmpmi	pc, r3, asr r5	; <UNPREDICTABLE>
    1fb0:	5f484352 	svcpl	0x00484352
    1fb4:	4d5f4d38 	ldclmi	13, cr4, [pc, #-224]	; 1edc <CPSR_IRQ_INHIBIT+0x1e5c>
    1fb8:	004e4941 	subeq	r4, lr, r1, asr #18
    1fbc:	47524154 			; <UNDEFINED> instruction: 0x47524154
    1fc0:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    1fc4:	615f5550 	cmpvs	pc, r0, asr r5	; <UNPREDICTABLE>
    1fc8:	74396d72 	ldrtvc	r6, [r9], #-3442	; 0xfffff28e
    1fcc:	00696d64 	rsbeq	r6, r9, r4, ror #26
    1fd0:	5f4d5241 	svcpl	0x004d5241
    1fd4:	42004c41 	andmi	r4, r0, #16640	; 0x4100
    1fd8:	5f455341 	svcpl	0x00455341
    1fdc:	48435241 	stmdami	r3, {r0, r6, r9, ip, lr}^
    1fe0:	004d375f 	subeq	r3, sp, pc, asr r7
    1fe4:	5f6d7261 	svcpl	0x006d7261
    1fe8:	67726174 			; <UNDEFINED> instruction: 0x67726174
    1fec:	6c5f7465 	cfldrdvs	mvd7, [pc], {101}	; 0x65
    1ff0:	6c656261 	sfmvs	f6, 2, [r5], #-388	; 0xfffffe7c
    1ff4:	6d726100 	ldfvse	f6, [r2, #-0]
    1ff8:	7261745f 	rsbvc	r7, r1, #1593835520	; 0x5f000000
    1ffc:	5f746567 	svcpl	0x00746567
    2000:	6e736e69 	cdpvs	14, 7, cr6, cr3, cr9, {3}
    2004:	52415400 	subpl	r5, r1, #0, 8
    2008:	5f544547 	svcpl	0x00544547
    200c:	5f555043 	svcpl	0x00555043
    2010:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    2014:	34727865 	ldrbtcc	r7, [r2], #-2149	; 0xfffff79b
    2018:	52415400 	subpl	r5, r1, #0, 8
    201c:	5f544547 	svcpl	0x00544547
    2020:	5f555043 	svcpl	0x00555043
    2024:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    2028:	35727865 	ldrbcc	r7, [r2, #-2149]!	; 0xfffff79b
    202c:	52415400 	subpl	r5, r1, #0, 8
    2030:	5f544547 	svcpl	0x00544547
    2034:	5f555043 	svcpl	0x00555043
    2038:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    203c:	37727865 	ldrbcc	r7, [r2, -r5, ror #16]!
    2040:	52415400 	subpl	r5, r1, #0, 8
    2044:	5f544547 	svcpl	0x00544547
    2048:	5f555043 	svcpl	0x00555043
    204c:	74726f63 	ldrbtvc	r6, [r2], #-3939	; 0xfffff09d
    2050:	38727865 	ldmdacc	r2!, {r0, r2, r5, r6, fp, ip, sp, lr}^
    2054:	61736900 	cmnvs	r3, r0, lsl #18
    2058:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    205c:	61706c5f 	cmnvs	r0, pc, asr ip
    2060:	73690065 	cmnvc	r9, #101	; 0x65
    2064:	69625f61 	stmdbvs	r2!, {r0, r5, r6, r8, r9, sl, fp, ip, lr}^
    2068:	75715f74 	ldrbvc	r5, [r1, #-3956]!	; 0xfffff08c
    206c:	5f6b7269 	svcpl	0x006b7269
    2070:	766d7261 	strbtvc	r7, [sp], -r1, ror #4
    2074:	007a6b36 	rsbseq	r6, sl, r6, lsr fp
    2078:	5f617369 	svcpl	0x00617369
    207c:	5f746962 	svcpl	0x00746962
    2080:	6d746f6e 	ldclvs	15, cr6, [r4, #-440]!	; 0xfffffe48
    2084:	61736900 	cmnvs	r3, r0, lsl #18
    2088:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    208c:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    2090:	69003476 	stmdbvs	r0, {r1, r2, r4, r5, r6, sl, ip, sp}
    2094:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    2098:	615f7469 	cmpvs	pc, r9, ror #8
    209c:	36766d72 			; <UNDEFINED> instruction: 0x36766d72
    20a0:	61736900 	cmnvs	r3, r0, lsl #18
    20a4:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    20a8:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    20ac:	69003776 	stmdbvs	r0, {r1, r2, r4, r5, r6, r8, r9, sl, ip, sp}
    20b0:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    20b4:	615f7469 	cmpvs	pc, r9, ror #8
    20b8:	38766d72 	ldmdacc	r6!, {r1, r4, r5, r6, r8, sl, fp, sp, lr}^
    20bc:	6f645f00 	svcvs	0x00645f00
    20c0:	755f746e 	ldrbvc	r7, [pc, #-1134]	; 1c5a <CPSR_IRQ_INHIBIT+0x1bda>
    20c4:	725f6573 	subsvc	r6, pc, #482344960	; 0x1cc00000
    20c8:	685f7874 	ldmdavs	pc, {r2, r4, r5, r6, fp, ip, sp, lr}^	; <UNPREDICTABLE>
    20cc:	5f657265 	svcpl	0x00657265
    20d0:	49515500 	ldmdbmi	r1, {r8, sl, ip, lr}^
    20d4:	65707974 	ldrbvs	r7, [r0, #-2420]!	; 0xfffff68c
    20d8:	61736900 	cmnvs	r3, r0, lsl #18
    20dc:	7469625f 	strbtvc	r6, [r9], #-607	; 0xfffffda1
    20e0:	6d72615f 	ldfvse	f6, [r2, #-380]!	; 0xfffffe84
    20e4:	65743576 	ldrbvs	r3, [r4, #-1398]!	; 0xfffffa8a
    20e8:	6d726100 	ldfvse	f6, [r2, #-0]
    20ec:	6e75745f 	mrcvs	4, 3, r7, cr5, cr15, {2}
    20f0:	72610065 	rsbvc	r0, r1, #101	; 0x65
    20f4:	70635f6d 	rsbvc	r5, r3, sp, ror #30
    20f8:	6e695f70 	mcrvs	15, 3, r5, cr9, cr0, {3}
    20fc:	77726574 			; <UNDEFINED> instruction: 0x77726574
    2100:	006b726f 	rsbeq	r7, fp, pc, ror #4
    2104:	636e7566 	cmnvs	lr, #427819008	; 0x19800000
    2108:	7274705f 	rsbsvc	r7, r4, #95	; 0x5f
    210c:	52415400 	subpl	r5, r1, #0, 8
    2110:	5f544547 	svcpl	0x00544547
    2114:	5f555043 	svcpl	0x00555043
    2118:	396d7261 	stmdbcc	sp!, {r0, r5, r6, r9, ip, sp, lr}^
    211c:	00743032 	rsbseq	r3, r4, r2, lsr r0
    2120:	62617468 	rsbvs	r7, r1, #104, 8	; 0x68000000
    2124:	0071655f 	rsbseq	r6, r1, pc, asr r5
    2128:	47524154 			; <UNDEFINED> instruction: 0x47524154
    212c:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    2130:	665f5550 			; <UNDEFINED> instruction: 0x665f5550
    2134:	36323561 	ldrtcc	r3, [r2], -r1, ror #10
    2138:	6d726100 	ldfvse	f6, [r2, #-0]
    213c:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    2140:	68745f68 	ldmdavs	r4!, {r3, r5, r6, r8, r9, sl, fp, ip, lr}^
    2144:	5f626d75 	svcpl	0x00626d75
    2148:	69647768 	stmdbvs	r4!, {r3, r5, r6, r8, r9, sl, ip, sp, lr}^
    214c:	74680076 	strbtvc	r0, [r8], #-118	; 0xffffff8a
    2150:	655f6261 	ldrbvs	r6, [pc, #-609]	; 1ef7 <CPSR_IRQ_INHIBIT+0x1e77>
    2154:	6f705f71 	svcvs	0x00705f71
    2158:	65746e69 	ldrbvs	r6, [r4, #-3689]!	; 0xfffff197
    215c:	72610072 	rsbvc	r0, r1, #114	; 0x72
    2160:	69705f6d 	ldmdbvs	r0!, {r0, r2, r3, r5, r6, r8, r9, sl, fp, ip, lr}^
    2164:	65725f63 	ldrbvs	r5, [r2, #-3939]!	; 0xfffff09d
    2168:	74736967 	ldrbtvc	r6, [r3], #-2407	; 0xfffff699
    216c:	54007265 	strpl	r7, [r0], #-613	; 0xfffffd9b
    2170:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    2174:	50435f54 	subpl	r5, r3, r4, asr pc
    2178:	6f635f55 	svcvs	0x00635f55
    217c:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    2180:	6d73306d 	ldclvs	0, cr3, [r3, #-436]!	; 0xfffffe4c
    2184:	6d6c6c61 	stclvs	12, cr6, [ip, #-388]!	; 0xfffffe7c
    2188:	69746c75 	ldmdbvs	r4!, {r0, r2, r4, r5, r6, sl, fp, sp, lr}^
    218c:	00796c70 	rsbseq	r6, r9, r0, ror ip
    2190:	47524154 			; <UNDEFINED> instruction: 0x47524154
    2194:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    2198:	6d5f5550 	cfldr64vs	mvdx5, [pc, #-320]	; 2060 <CPSR_IRQ_INHIBIT+0x1fe0>
    219c:	726f6370 	rsbvc	r6, pc, #112, 6	; 0xc0000001
    21a0:	766f6e65 	strbtvc	r6, [pc], -r5, ror #28
    21a4:	69007066 	stmdbvs	r0, {r1, r2, r5, r6, ip, sp, lr}
    21a8:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    21ac:	715f7469 	cmpvc	pc, r9, ror #8
    21b0:	6b726975 	blvs	1c9c78c <_bss_end+0x1c93128>
    21b4:	336d635f 	cmncc	sp, #2080374785	; 0x7c000001
    21b8:	72646c5f 	rsbvc	r6, r4, #24320	; 0x5f00
    21bc:	52410064 	subpl	r0, r1, #100	; 0x64
    21c0:	43435f4d 	movtmi	r5, #16205	; 0x3f4d
    21c4:	6d726100 	ldfvse	f6, [r2, #-0]
    21c8:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    21cc:	325f3868 	subscc	r3, pc, #104, 16	; 0x680000
    21d0:	6d726100 	ldfvse	f6, [r2, #-0]
    21d4:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    21d8:	335f3868 	cmpcc	pc, #104, 16	; 0x680000
    21dc:	6d726100 	ldfvse	f6, [r2, #-0]
    21e0:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    21e4:	345f3868 	ldrbcc	r3, [pc], #-2152	; 21ec <CPSR_IRQ_INHIBIT+0x216c>
    21e8:	52415400 	subpl	r5, r1, #0, 8
    21ec:	5f544547 	svcpl	0x00544547
    21f0:	5f555043 	svcpl	0x00555043
    21f4:	36706d66 	ldrbtcc	r6, [r0], -r6, ror #26
    21f8:	41003632 	tstmi	r0, r2, lsr r6
    21fc:	435f4d52 	cmpmi	pc, #5248	; 0x1480
    2200:	72610053 	rsbvc	r0, r1, #83	; 0x53
    2204:	70665f6d 	rsbvc	r5, r6, sp, ror #30
    2208:	695f3631 	ldmdbvs	pc, {r0, r4, r5, r9, sl, ip, sp}^	; <UNPREDICTABLE>
    220c:	0074736e 	rsbseq	r7, r4, lr, ror #6
    2210:	5f6d7261 	svcpl	0x006d7261
    2214:	65736162 	ldrbvs	r6, [r3, #-354]!	; 0xfffffe9e
    2218:	6372615f 	cmnvs	r2, #-1073741801	; 0xc0000017
    221c:	41540068 	cmpmi	r4, r8, rrx
    2220:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    2224:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    2228:	726f635f 	rsbvc	r6, pc, #2080374785	; 0x7c000001
    222c:	61786574 	cmnvs	r8, r4, ror r5
    2230:	6f633531 	svcvs	0x00633531
    2234:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    2238:	61003761 	tstvs	r0, r1, ror #14
    223c:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    2240:	37686372 			; <UNDEFINED> instruction: 0x37686372
    2244:	54006d65 	strpl	r6, [r0], #-3429	; 0xfffff29b
    2248:	45475241 	strbmi	r5, [r7, #-577]	; 0xfffffdbf
    224c:	50435f54 	subpl	r5, r3, r4, asr pc
    2250:	6f635f55 	svcvs	0x00635f55
    2254:	78657472 	stmdavc	r5!, {r1, r4, r5, r6, sl, ip, sp, lr}^
    2258:	00323761 	eorseq	r3, r2, r1, ror #14
    225c:	5f6d7261 	svcpl	0x006d7261
    2260:	5f736370 	svcpl	0x00736370
    2264:	61666564 	cmnvs	r6, r4, ror #10
    2268:	00746c75 	rsbseq	r6, r4, r5, ror ip
    226c:	5f4d5241 	svcpl	0x004d5241
    2270:	5f534350 	svcpl	0x00534350
    2274:	43504141 	cmpmi	r0, #1073741840	; 0x40000010
    2278:	4f4c5f53 	svcmi	0x004c5f53
    227c:	004c4143 	subeq	r4, ip, r3, asr #2
    2280:	47524154 			; <UNDEFINED> instruction: 0x47524154
    2284:	435f5445 	cmpmi	pc, #1157627904	; 0x45000000
    2288:	635f5550 	cmpvs	pc, #80, 10	; 0x14000000
    228c:	6574726f 	ldrbvs	r7, [r4, #-623]!	; 0xfffffd91
    2290:	35376178 	ldrcc	r6, [r7, #-376]!	; 0xfffffe88
    2294:	52415400 	subpl	r5, r1, #0, 8
    2298:	5f544547 	svcpl	0x00544547
    229c:	5f555043 	svcpl	0x00555043
    22a0:	6f727473 	svcvs	0x00727473
    22a4:	7261676e 	rsbvc	r6, r1, #28835840	; 0x1b80000
    22a8:	7261006d 	rsbvc	r0, r1, #109	; 0x6d
    22ac:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    22b0:	745f6863 	ldrbvc	r6, [pc], #-2147	; 22b8 <CPSR_IRQ_INHIBIT+0x2238>
    22b4:	626d7568 	rsbvs	r7, sp, #104, 10	; 0x1a000000
    22b8:	72610031 	rsbvc	r0, r1, #49	; 0x31
    22bc:	72615f6d 	rsbvc	r5, r1, #436	; 0x1b4
    22c0:	745f6863 	ldrbvc	r6, [pc], #-2147	; 22c8 <CPSR_IRQ_INHIBIT+0x2248>
    22c4:	626d7568 	rsbvs	r7, sp, #104, 10	; 0x1a000000
    22c8:	41540032 	cmpmi	r4, r2, lsr r0
    22cc:	54454752 	strbpl	r4, [r5], #-1874	; 0xfffff8ae
    22d0:	5550435f 	ldrbpl	r4, [r0, #-863]	; 0xfffffca1
    22d4:	6d77695f 			; <UNDEFINED> instruction: 0x6d77695f
    22d8:	0074786d 	rsbseq	r7, r4, sp, ror #16
    22dc:	5f6d7261 	svcpl	0x006d7261
    22e0:	68637261 	stmdavs	r3!, {r0, r5, r6, r9, ip, sp, lr}^
    22e4:	69007435 	stmdbvs	r0, {r0, r2, r4, r5, sl, ip, sp, lr}
    22e8:	625f6173 	subsvs	r6, pc, #-1073741796	; 0xc000001c
    22ec:	6d5f7469 	cfldrdvs	mvd7, [pc, #-420]	; 2150 <CPSR_IRQ_INHIBIT+0x20d0>
    22f0:	72610070 	rsbvc	r0, r1, #112	; 0x70
    22f4:	646c5f6d 	strbtvs	r5, [ip], #-3949	; 0xfffff093
    22f8:	6863735f 	stmdavs	r3!, {r0, r1, r2, r3, r4, r6, r8, r9, ip, sp, lr}^
    22fc:	61006465 	tstvs	r0, r5, ror #8
    2300:	615f6d72 	cmpvs	pc, r2, ror sp	; <UNPREDICTABLE>
    2304:	38686372 	stmdacc	r8!, {r1, r4, r5, r6, r8, r9, sp, lr}^
    2308:	Address 0x0000000000002308 is out of bounds.


Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347 	bcc	10d0d24 <_bss_end+0x10c76c0>
   4:	35312820 	ldrcc	r2, [r1, #-2080]!	; 0xfffff7e0
   8:	322d393a 	eorcc	r3, sp, #950272	; 0xe8000
   c:	2d393130 	ldfcss	f3, [r9, #-192]!	; 0xffffff40
  10:	302d3471 	eorcc	r3, sp, r1, ror r4
  14:	6e756275 	mrcvs	2, 3, r6, cr5, cr5, {3}
  18:	29317574 	ldmdbcs	r1!, {r2, r4, r5, r6, r8, sl, ip, sp, lr}
  1c:	322e3920 	eorcc	r3, lr, #32, 18	; 0x80000
  20:	3220312e 	eorcc	r3, r0, #-2147483637	; 0x8000000b
  24:	31393130 	teqcc	r9, r0, lsr r1
  28:	20353230 	eorscs	r3, r5, r0, lsr r2
  2c:	6c657228 	sfmvs	f7, 2, [r5], #-160	; 0xffffff60
  30:	65736165 	ldrbvs	r6, [r3, #-357]!	; 0xfffffe9b
  34:	415b2029 	cmpmi	fp, r9, lsr #32
  38:	612f4d52 			; <UNDEFINED> instruction: 0x612f4d52
  3c:	392d6d72 	pushcc	{r1, r4, r5, r6, r8, sl, fp, sp, lr}
  40:	6172622d 	cmnvs	r2, sp, lsr #4
  44:	2068636e 	rsbcs	r6, r8, lr, ror #6
  48:	69766572 	ldmdbvs	r6!, {r1, r4, r5, r6, r8, sl, sp, lr}^
  4c:	6e6f6973 			; <UNDEFINED> instruction: 0x6e6f6973
  50:	37373220 	ldrcc	r3, [r7, -r0, lsr #4]!
  54:	5d393935 			; <UNDEFINED> instruction: 0x5d393935
	...

Disassembly of section .ARM.attributes:

00000000 <.ARM.attributes>:
   0:	00003041 	andeq	r3, r0, r1, asr #32
   4:	61656100 	cmnvs	r5, r0, lsl #2
   8:	01006962 	tsteq	r0, r2, ror #18
   c:	00000026 	andeq	r0, r0, r6, lsr #32
  10:	4b5a3605 	blmi	168d82c <_bss_end+0x16841c8>
  14:	08070600 	stmdaeq	r7, {r9, sl}
  18:	0a010901 	beq	42424 <_bss_end+0x38dc0>
  1c:	14041202 	strne	r1, [r4], #-514	; 0xfffffdfe
  20:	17011501 	strne	r1, [r1, -r1, lsl #10]
  24:	1a011803 	bne	46038 <_bss_end+0x3c9d4>
  28:	1e011c01 	cdpne	12, 0, cr1, cr1, cr1, {0}
  2c:	44012206 	strmi	r2, [r1], #-518	; 0xfffffdfa
  30:	Address 0x0000000000000030 is out of bounds.


Disassembly of section .debug_frame:

00000000 <.debug_frame>:
   0:	0000000c 	andeq	r0, r0, ip
   4:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
   8:	7c020001 	stcvc	0, cr0, [r2], {1}
   c:	000d0c0e 	andeq	r0, sp, lr, lsl #24
  10:	0000001c 	andeq	r0, r0, ip, lsl r0
  14:	00000000 	andeq	r0, r0, r0
  18:	00008094 	muleq	r0, r4, r0
  1c:	00000038 	andeq	r0, r0, r8, lsr r0
  20:	8b040e42 	blhi	103930 <_bss_end+0xfa2cc>
  24:	0b0d4201 	bleq	350830 <_bss_end+0x3471cc>
  28:	420d0d54 	andmi	r0, sp, #84, 26	; 0x1500
  2c:	00000ecb 	andeq	r0, r0, fp, asr #29
  30:	0000001c 	andeq	r0, r0, ip, lsl r0
  34:	00000000 	andeq	r0, r0, r0
  38:	000080cc 	andeq	r8, r0, ip, asr #1
  3c:	0000002c 	andeq	r0, r0, ip, lsr #32
  40:	8b040e42 	blhi	103950 <_bss_end+0xfa2ec>
  44:	0b0d4201 	bleq	350850 <_bss_end+0x3471ec>
  48:	420d0d4e 	andmi	r0, sp, #4992	; 0x1380
  4c:	00000ecb 	andeq	r0, r0, fp, asr #29
  50:	0000001c 	andeq	r0, r0, ip, lsl r0
  54:	00000000 	andeq	r0, r0, r0
  58:	000080f8 	strdeq	r8, [r0], -r8	; <UNPREDICTABLE>
  5c:	00000020 	andeq	r0, r0, r0, lsr #32
  60:	8b040e42 	blhi	103970 <_bss_end+0xfa30c>
  64:	0b0d4201 	bleq	350870 <_bss_end+0x34720c>
  68:	420d0d48 	andmi	r0, sp, #72, 26	; 0x1200
  6c:	00000ecb 	andeq	r0, r0, fp, asr #29
  70:	0000001c 	andeq	r0, r0, ip, lsl r0
  74:	00000000 	andeq	r0, r0, r0
  78:	00008118 	andeq	r8, r0, r8, lsl r1
  7c:	00000018 	andeq	r0, r0, r8, lsl r0
  80:	8b040e42 	blhi	103990 <_bss_end+0xfa32c>
  84:	0b0d4201 	bleq	350890 <_bss_end+0x34722c>
  88:	420d0d44 	andmi	r0, sp, #68, 26	; 0x1100
  8c:	00000ecb 	andeq	r0, r0, fp, asr #29
  90:	0000001c 	andeq	r0, r0, ip, lsl r0
  94:	00000000 	andeq	r0, r0, r0
  98:	00008130 	andeq	r8, r0, r0, lsr r1
  9c:	00000018 	andeq	r0, r0, r8, lsl r0
  a0:	8b040e42 	blhi	1039b0 <_bss_end+0xfa34c>
  a4:	0b0d4201 	bleq	3508b0 <_bss_end+0x34724c>
  a8:	420d0d44 	andmi	r0, sp, #68, 26	; 0x1100
  ac:	00000ecb 	andeq	r0, r0, fp, asr #29
  b0:	0000001c 	andeq	r0, r0, ip, lsl r0
  b4:	00000000 	andeq	r0, r0, r0
  b8:	00008148 	andeq	r8, r0, r8, asr #2
  bc:	00000018 	andeq	r0, r0, r8, lsl r0
  c0:	8b040e42 	blhi	1039d0 <_bss_end+0xfa36c>
  c4:	0b0d4201 	bleq	3508d0 <_bss_end+0x34726c>
  c8:	420d0d44 	andmi	r0, sp, #68, 26	; 0x1100
  cc:	00000ecb 	andeq	r0, r0, fp, asr #29
  d0:	00000014 	andeq	r0, r0, r4, lsl r0
  d4:	00000000 	andeq	r0, r0, r0
  d8:	00008160 	andeq	r8, r0, r0, ror #2
  dc:	0000000c 	andeq	r0, r0, ip
  e0:	8b040e42 	blhi	1039f0 <_bss_end+0xfa38c>
  e4:	0b0d4201 	bleq	3508f0 <_bss_end+0x34728c>
  e8:	0000000c 	andeq	r0, r0, ip
  ec:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
  f0:	7c020001 	stcvc	0, cr0, [r2], {1}
  f4:	000d0c0e 	andeq	r0, sp, lr, lsl #24
  f8:	0000001c 	andeq	r0, r0, ip, lsl r0
  fc:	000000e8 	andeq	r0, r0, r8, ror #1
 100:	0000816c 	andeq	r8, r0, ip, ror #2
 104:	00000034 	andeq	r0, r0, r4, lsr r0
 108:	8b040e42 	blhi	103a18 <_bss_end+0xfa3b4>
 10c:	0b0d4201 	bleq	350918 <_bss_end+0x3472b4>
 110:	420d0d52 	andmi	r0, sp, #5248	; 0x1480
 114:	00000ecb 	andeq	r0, r0, fp, asr #29
 118:	0000001c 	andeq	r0, r0, ip, lsl r0
 11c:	000000e8 	andeq	r0, r0, r8, ror #1
 120:	000081a0 	andeq	r8, r0, r0, lsr #3
 124:	00000050 	andeq	r0, r0, r0, asr r0
 128:	8b080e42 	blhi	203a38 <_bss_end+0x1fa3d4>
 12c:	42018e02 	andmi	r8, r1, #2, 28
 130:	62040b0c 	andvs	r0, r4, #12, 22	; 0x3000
 134:	00080d0c 	andeq	r0, r8, ip, lsl #26
 138:	0000001c 	andeq	r0, r0, ip, lsl r0
 13c:	000000e8 	andeq	r0, r0, r8, ror #1
 140:	000081f0 	strdeq	r8, [r0], -r0
 144:	00000054 	andeq	r0, r0, r4, asr r0
 148:	8b080e42 	blhi	203a58 <_bss_end+0x1fa3f4>
 14c:	42018e02 	andmi	r8, r1, #2, 28
 150:	64040b0c 	strvs	r0, [r4], #-2828	; 0xfffff4f4
 154:	00080d0c 	andeq	r0, r8, ip, lsl #26
 158:	0000001c 	andeq	r0, r0, ip, lsl r0
 15c:	000000e8 	andeq	r0, r0, r8, ror #1
 160:	00008244 	andeq	r8, r0, r4, asr #4
 164:	00000044 	andeq	r0, r0, r4, asr #32
 168:	8b040e42 	blhi	103a78 <_bss_end+0xfa414>
 16c:	0b0d4201 	bleq	350978 <_bss_end+0x347314>
 170:	420d0d5a 	andmi	r0, sp, #5760	; 0x1680
 174:	00000ecb 	andeq	r0, r0, fp, asr #29
 178:	0000001c 	andeq	r0, r0, ip, lsl r0
 17c:	000000e8 	andeq	r0, r0, r8, ror #1
 180:	00008288 	andeq	r8, r0, r8, lsl #5
 184:	0000003c 	andeq	r0, r0, ip, lsr r0
 188:	8b040e42 	blhi	103a98 <_bss_end+0xfa434>
 18c:	0b0d4201 	bleq	350998 <_bss_end+0x347334>
 190:	420d0d56 	andmi	r0, sp, #5504	; 0x1580
 194:	00000ecb 	andeq	r0, r0, fp, asr #29
 198:	0000001c 	andeq	r0, r0, ip, lsl r0
 19c:	000000e8 	andeq	r0, r0, r8, ror #1
 1a0:	000082c4 	andeq	r8, r0, r4, asr #5
 1a4:	00000054 	andeq	r0, r0, r4, asr r0
 1a8:	8b080e42 	blhi	203ab8 <_bss_end+0x1fa454>
 1ac:	42018e02 	andmi	r8, r1, #2, 28
 1b0:	5e040b0c 	vmlapl.f64	d0, d4, d12
 1b4:	00080d0c 	andeq	r0, r8, ip, lsl #26
 1b8:	00000018 	andeq	r0, r0, r8, lsl r0
 1bc:	000000e8 	andeq	r0, r0, r8, ror #1
 1c0:	00008318 	andeq	r8, r0, r8, lsl r3
 1c4:	0000001c 	andeq	r0, r0, ip, lsl r0
 1c8:	8b080e42 	blhi	203ad8 <_bss_end+0x1fa474>
 1cc:	42018e02 	andmi	r8, r1, #2, 28
 1d0:	00040b0c 	andeq	r0, r4, ip, lsl #22
 1d4:	0000000c 	andeq	r0, r0, ip
 1d8:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 1dc:	7c020001 	stcvc	0, cr0, [r2], {1}
 1e0:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 1e4:	0000001c 	andeq	r0, r0, ip, lsl r0
 1e8:	000001d4 	ldrdeq	r0, [r0], -r4
 1ec:	00008334 	andeq	r8, r0, r4, lsr r3
 1f0:	00000034 	andeq	r0, r0, r4, lsr r0
 1f4:	8b040e42 	blhi	103b04 <_bss_end+0xfa4a0>
 1f8:	0b0d4201 	bleq	350a04 <_bss_end+0x3473a0>
 1fc:	420d0d52 	andmi	r0, sp, #5248	; 0x1480
 200:	00000ecb 	andeq	r0, r0, fp, asr #29
 204:	0000001c 	andeq	r0, r0, ip, lsl r0
 208:	000001d4 	ldrdeq	r0, [r0], -r4
 20c:	00008368 	andeq	r8, r0, r8, ror #6
 210:	00000114 	andeq	r0, r0, r4, lsl r1
 214:	8b040e42 	blhi	103b24 <_bss_end+0xfa4c0>
 218:	0b0d4201 	bleq	350a24 <_bss_end+0x3473c0>
 21c:	0d0d8002 	stceq	0, cr8, [sp, #-8]
 220:	000ecb42 	andeq	ip, lr, r2, asr #22
 224:	0000001c 	andeq	r0, r0, ip, lsl r0
 228:	000001d4 	ldrdeq	r0, [r0], -r4
 22c:	0000847c 	andeq	r8, r0, ip, ror r4
 230:	00000074 	andeq	r0, r0, r4, ror r0
 234:	8b040e42 	blhi	103b44 <_bss_end+0xfa4e0>
 238:	0b0d4201 	bleq	350a44 <_bss_end+0x3473e0>
 23c:	420d0d72 	andmi	r0, sp, #7296	; 0x1c80
 240:	00000ecb 	andeq	r0, r0, fp, asr #29
 244:	0000001c 	andeq	r0, r0, ip, lsl r0
 248:	000001d4 	ldrdeq	r0, [r0], -r4
 24c:	000084f0 	strdeq	r8, [r0], -r0
 250:	00000074 	andeq	r0, r0, r4, ror r0
 254:	8b040e42 	blhi	103b64 <_bss_end+0xfa500>
 258:	0b0d4201 	bleq	350a64 <_bss_end+0x347400>
 25c:	420d0d72 	andmi	r0, sp, #7296	; 0x1c80
 260:	00000ecb 	andeq	r0, r0, fp, asr #29
 264:	0000001c 	andeq	r0, r0, ip, lsl r0
 268:	000001d4 	ldrdeq	r0, [r0], -r4
 26c:	00008564 	andeq	r8, r0, r4, ror #10
 270:	00000074 	andeq	r0, r0, r4, ror r0
 274:	8b040e42 	blhi	103b84 <_bss_end+0xfa520>
 278:	0b0d4201 	bleq	350a84 <_bss_end+0x347420>
 27c:	420d0d72 	andmi	r0, sp, #7296	; 0x1c80
 280:	00000ecb 	andeq	r0, r0, fp, asr #29
 284:	0000001c 	andeq	r0, r0, ip, lsl r0
 288:	000001d4 	ldrdeq	r0, [r0], -r4
 28c:	000085d8 	ldrdeq	r8, [r0], -r8	; <UNPREDICTABLE>
 290:	000000a0 	andeq	r0, r0, r0, lsr #1
 294:	8b080e42 	blhi	203ba4 <_bss_end+0x1fa540>
 298:	42018e02 	andmi	r8, r1, #2, 28
 29c:	02040b0c 	andeq	r0, r4, #12, 22	; 0x3000
 2a0:	080d0c4a 	stmdaeq	sp, {r1, r3, r6, sl, fp}
 2a4:	0000001c 	andeq	r0, r0, ip, lsl r0
 2a8:	000001d4 	ldrdeq	r0, [r0], -r4
 2ac:	00008678 	andeq	r8, r0, r8, ror r6
 2b0:	00000074 	andeq	r0, r0, r4, ror r0
 2b4:	8b080e42 	blhi	203bc4 <_bss_end+0x1fa560>
 2b8:	42018e02 	andmi	r8, r1, #2, 28
 2bc:	74040b0c 	strvc	r0, [r4], #-2828	; 0xfffff4f4
 2c0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 2c4:	0000001c 	andeq	r0, r0, ip, lsl r0
 2c8:	000001d4 	ldrdeq	r0, [r0], -r4
 2cc:	000086ec 	andeq	r8, r0, ip, ror #13
 2d0:	000000d8 	ldrdeq	r0, [r0], -r8
 2d4:	8b080e42 	blhi	203be4 <_bss_end+0x1fa580>
 2d8:	42018e02 	andmi	r8, r1, #2, 28
 2dc:	02040b0c 	andeq	r0, r4, #12, 22	; 0x3000
 2e0:	080d0c66 	stmdaeq	sp, {r1, r2, r5, r6, sl, fp}
 2e4:	0000001c 	andeq	r0, r0, ip, lsl r0
 2e8:	000001d4 	ldrdeq	r0, [r0], -r4
 2ec:	000087c4 	andeq	r8, r0, r4, asr #15
 2f0:	00000054 	andeq	r0, r0, r4, asr r0
 2f4:	8b080e42 	blhi	203c04 <_bss_end+0x1fa5a0>
 2f8:	42018e02 	andmi	r8, r1, #2, 28
 2fc:	5e040b0c 	vmlapl.f64	d0, d4, d12
 300:	00080d0c 	andeq	r0, r8, ip, lsl #26
 304:	00000018 	andeq	r0, r0, r8, lsl r0
 308:	000001d4 	ldrdeq	r0, [r0], -r4
 30c:	00008818 	andeq	r8, r0, r8, lsl r8
 310:	0000001c 	andeq	r0, r0, ip, lsl r0
 314:	8b080e42 	blhi	203c24 <_bss_end+0x1fa5c0>
 318:	42018e02 	andmi	r8, r1, #2, 28
 31c:	00040b0c 	andeq	r0, r4, ip, lsl #22
 320:	0000000c 	andeq	r0, r0, ip
 324:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 328:	7c020001 	stcvc	0, cr0, [r2], {1}
 32c:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 330:	0000001c 	andeq	r0, r0, ip, lsl r0
 334:	00000320 	andeq	r0, r0, r0, lsr #6
 338:	00008834 	andeq	r8, r0, r4, lsr r8
 33c:	00000040 	andeq	r0, r0, r0, asr #32
 340:	8b040e42 	blhi	103c50 <_bss_end+0xfa5ec>
 344:	0b0d4201 	bleq	350b50 <_bss_end+0x3474ec>
 348:	420d0d58 	andmi	r0, sp, #88, 26	; 0x1600
 34c:	00000ecb 	andeq	r0, r0, fp, asr #29
 350:	0000001c 	andeq	r0, r0, ip, lsl r0
 354:	00000320 	andeq	r0, r0, r0, lsr #6
 358:	00008874 	andeq	r8, r0, r4, ror r8
 35c:	00000038 	andeq	r0, r0, r8, lsr r0
 360:	8b040e42 	blhi	103c70 <_bss_end+0xfa60c>
 364:	0b0d4201 	bleq	350b70 <_bss_end+0x34750c>
 368:	420d0d54 	andmi	r0, sp, #84, 26	; 0x1500
 36c:	00000ecb 	andeq	r0, r0, fp, asr #29
 370:	00000020 	andeq	r0, r0, r0, lsr #32
 374:	00000320 	andeq	r0, r0, r0, lsr #6
 378:	000088ac 	andeq	r8, r0, ip, lsr #17
 37c:	000000cc 	andeq	r0, r0, ip, asr #1
 380:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 384:	8e028b03 	vmlahi.f64	d8, d2, d3
 388:	0b0c4201 	bleq	310b94 <_bss_end+0x307530>
 38c:	0c600204 	sfmeq	f0, 2, [r0], #-16
 390:	00000c0d 	andeq	r0, r0, sp, lsl #24
 394:	0000001c 	andeq	r0, r0, ip, lsl r0
 398:	00000320 	andeq	r0, r0, r0, lsr #6
 39c:	00008978 	andeq	r8, r0, r8, ror r9
 3a0:	0000004c 	andeq	r0, r0, ip, asr #32
 3a4:	8b080e42 	blhi	203cb4 <_bss_end+0x1fa650>
 3a8:	42018e02 	andmi	r8, r1, #2, 28
 3ac:	60040b0c 	andvs	r0, r4, ip, lsl #22
 3b0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 3b4:	0000001c 	andeq	r0, r0, ip, lsl r0
 3b8:	00000320 	andeq	r0, r0, r0, lsr #6
 3bc:	000089c4 	andeq	r8, r0, r4, asr #19
 3c0:	00000050 	andeq	r0, r0, r0, asr r0
 3c4:	8b080e42 	blhi	203cd4 <_bss_end+0x1fa670>
 3c8:	42018e02 	andmi	r8, r1, #2, 28
 3cc:	62040b0c 	andvs	r0, r4, #12, 22	; 0x3000
 3d0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 3d4:	0000001c 	andeq	r0, r0, ip, lsl r0
 3d8:	00000320 	andeq	r0, r0, r0, lsr #6
 3dc:	00008a14 	andeq	r8, r0, r4, lsl sl
 3e0:	00000040 	andeq	r0, r0, r0, asr #32
 3e4:	8b080e42 	blhi	203cf4 <_bss_end+0x1fa690>
 3e8:	42018e02 	andmi	r8, r1, #2, 28
 3ec:	5a040b0c 	bpl	103024 <_bss_end+0xf99c0>
 3f0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 3f4:	0000001c 	andeq	r0, r0, ip, lsl r0
 3f8:	00000320 	andeq	r0, r0, r0, lsr #6
 3fc:	00008a54 	andeq	r8, r0, r4, asr sl
 400:	00000054 	andeq	r0, r0, r4, asr r0
 404:	8b080e42 	blhi	203d14 <_bss_end+0x1fa6b0>
 408:	42018e02 	andmi	r8, r1, #2, 28
 40c:	5e040b0c 	vmlapl.f64	d0, d4, d12
 410:	00080d0c 	andeq	r0, r8, ip, lsl #26
 414:	00000018 	andeq	r0, r0, r8, lsl r0
 418:	00000320 	andeq	r0, r0, r0, lsr #6
 41c:	00008aa8 	andeq	r8, r0, r8, lsr #21
 420:	0000001c 	andeq	r0, r0, ip, lsl r0
 424:	8b080e42 	blhi	203d34 <_bss_end+0x1fa6d0>
 428:	42018e02 	andmi	r8, r1, #2, 28
 42c:	00040b0c 	andeq	r0, r4, ip, lsl #22
 430:	0000000c 	andeq	r0, r0, ip
 434:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 438:	7c020001 	stcvc	0, cr0, [r2], {1}
 43c:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 440:	0000001c 	andeq	r0, r0, ip, lsl r0
 444:	00000430 	andeq	r0, r0, r0, lsr r4
 448:	00008ac4 	andeq	r8, r0, r4, asr #21
 44c:	000000a4 	andeq	r0, r0, r4, lsr #1
 450:	8b080e42 	blhi	203d60 <_bss_end+0x1fa6fc>
 454:	42018e02 	andmi	r8, r1, #2, 28
 458:	02040b0c 	andeq	r0, r4, #12, 22	; 0x3000
 45c:	080d0c4c 	stmdaeq	sp, {r2, r3, r6, sl, fp}
 460:	00000020 	andeq	r0, r0, r0, lsr #32
 464:	00000430 	andeq	r0, r0, r0, lsr r4
 468:	00008b68 	andeq	r8, r0, r8, ror #22
 46c:	0000005c 	andeq	r0, r0, ip, asr r0
 470:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 474:	8e028b03 	vmlahi.f64	d8, d2, d3
 478:	0b0c4201 	bleq	310c84 <_bss_end+0x307620>
 47c:	0d0c6804 	stceq	8, cr6, [ip, #-16]
 480:	0000000c 	andeq	r0, r0, ip
 484:	0000001c 	andeq	r0, r0, ip, lsl r0
 488:	00000430 	andeq	r0, r0, r0, lsr r4
 48c:	00008bc4 	andeq	r8, r0, r4, asr #23
 490:	00000088 	andeq	r0, r0, r8, lsl #1
 494:	8b080e42 	blhi	203da4 <_bss_end+0x1fa740>
 498:	42018e02 	andmi	r8, r1, #2, 28
 49c:	7c040b0c 			; <UNDEFINED> instruction: 0x7c040b0c
 4a0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 4a4:	0000001c 	andeq	r0, r0, ip, lsl r0
 4a8:	00000430 	andeq	r0, r0, r0, lsr r4
 4ac:	00008c4c 	andeq	r8, r0, ip, asr #24
 4b0:	00000074 	andeq	r0, r0, r4, ror r0
 4b4:	8b080e42 	blhi	203dc4 <_bss_end+0x1fa760>
 4b8:	42018e02 	andmi	r8, r1, #2, 28
 4bc:	74040b0c 	strvc	r0, [r4], #-2828	; 0xfffff4f4
 4c0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 4c4:	0000001c 	andeq	r0, r0, ip, lsl r0
 4c8:	00000430 	andeq	r0, r0, r0, lsr r4
 4cc:	00008cc0 	andeq	r8, r0, r0, asr #25
 4d0:	0000006c 	andeq	r0, r0, ip, rrx
 4d4:	8b080e42 	blhi	203de4 <_bss_end+0x1fa780>
 4d8:	42018e02 	andmi	r8, r1, #2, 28
 4dc:	70040b0c 	andvc	r0, r4, ip, lsl #22
 4e0:	00080d0c 	andeq	r0, r8, ip, lsl #26
 4e4:	0000001c 	andeq	r0, r0, ip, lsl r0
 4e8:	00000430 	andeq	r0, r0, r0, lsr r4
 4ec:	00008d2c 	andeq	r8, r0, ip, lsr #26
 4f0:	00000054 	andeq	r0, r0, r4, asr r0
 4f4:	8b080e42 	blhi	203e04 <_bss_end+0x1fa7a0>
 4f8:	42018e02 	andmi	r8, r1, #2, 28
 4fc:	5e040b0c 	vmlapl.f64	d0, d4, d12
 500:	00080d0c 	andeq	r0, r8, ip, lsl #26
 504:	00000018 	andeq	r0, r0, r8, lsl r0
 508:	00000430 	andeq	r0, r0, r0, lsr r4
 50c:	00008d80 	andeq	r8, r0, r0, lsl #27
 510:	0000001c 	andeq	r0, r0, ip, lsl r0
 514:	8b080e42 	blhi	203e24 <_bss_end+0x1fa7c0>
 518:	42018e02 	andmi	r8, r1, #2, 28
 51c:	00040b0c 	andeq	r0, r4, ip, lsl #22
 520:	0000000c 	andeq	r0, r0, ip
 524:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 528:	7c020001 	stcvc	0, cr0, [r2], {1}
 52c:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 530:	0000001c 	andeq	r0, r0, ip, lsl r0
 534:	00000520 	andeq	r0, r0, r0, lsr #10
 538:	00008d9c 	muleq	r0, ip, sp
 53c:	00000018 	andeq	r0, r0, r8, lsl r0
 540:	8b040e42 	blhi	103e50 <_bss_end+0xfa7ec>
 544:	0b0d4201 	bleq	350d50 <_bss_end+0x3476ec>
 548:	420d0d44 	andmi	r0, sp, #68, 26	; 0x1100
 54c:	00000ecb 	andeq	r0, r0, fp, asr #29
 550:	00000028 	andeq	r0, r0, r8, lsr #32
 554:	00000520 	andeq	r0, r0, r0, lsr #10
 558:	00008db4 			; <UNDEFINED> instruction: 0x00008db4
 55c:	00000038 	andeq	r0, r0, r8, lsr r0
 560:	80200e44 	eorhi	r0, r0, r4, asr #28
 564:	82078108 	andhi	r8, r7, #8, 2
 568:	84058306 	strhi	r8, [r5], #-774	; 0xfffffcfa
 56c:	8c038b04 			; <UNDEFINED> instruction: 0x8c038b04
 570:	42018e02 	andmi	r8, r1, #2, 28
 574:	52040b0c 	andpl	r0, r4, #12, 22	; 0x3000
 578:	00200d0c 	eoreq	r0, r0, ip, lsl #26
 57c:	00000014 	andeq	r0, r0, r4, lsl r0
 580:	00000520 	andeq	r0, r0, r0, lsr #10
 584:	00008dec 	andeq	r8, r0, ip, ror #27
 588:	00000010 	andeq	r0, r0, r0, lsl r0
 58c:	040b0c42 	streq	r0, [fp], #-3138	; 0xfffff3be
 590:	000d0c44 	andeq	r0, sp, r4, asr #24
 594:	0000001c 	andeq	r0, r0, ip, lsl r0
 598:	00000520 	andeq	r0, r0, r0, lsr #10
 59c:	00008dfc 	strdeq	r8, [r0], -ip
 5a0:	00000034 	andeq	r0, r0, r4, lsr r0
 5a4:	8b040e42 	blhi	103eb4 <_bss_end+0xfa850>
 5a8:	0b0d4201 	bleq	350db4 <_bss_end+0x347750>
 5ac:	420d0d52 	andmi	r0, sp, #5248	; 0x1480
 5b0:	00000ecb 	andeq	r0, r0, fp, asr #29
 5b4:	0000001c 	andeq	r0, r0, ip, lsl r0
 5b8:	00000520 	andeq	r0, r0, r0, lsr #10
 5bc:	00008e30 	andeq	r8, r0, r0, lsr lr
 5c0:	00000038 	andeq	r0, r0, r8, lsr r0
 5c4:	8b040e42 	blhi	103ed4 <_bss_end+0xfa870>
 5c8:	0b0d4201 	bleq	350dd4 <_bss_end+0x347770>
 5cc:	420d0d54 	andmi	r0, sp, #84, 26	; 0x1500
 5d0:	00000ecb 	andeq	r0, r0, fp, asr #29
 5d4:	00000020 	andeq	r0, r0, r0, lsr #32
 5d8:	00000520 	andeq	r0, r0, r0, lsr #10
 5dc:	00008e68 	andeq	r8, r0, r8, ror #28
 5e0:	00000044 	andeq	r0, r0, r4, asr #32
 5e4:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 5e8:	8e028b03 	vmlahi.f64	d8, d2, d3
 5ec:	0b0c4201 	bleq	310df8 <_bss_end+0x307794>
 5f0:	0d0c5c04 	stceq	12, cr5, [ip, #-16]
 5f4:	0000000c 	andeq	r0, r0, ip
 5f8:	00000020 	andeq	r0, r0, r0, lsr #32
 5fc:	00000520 	andeq	r0, r0, r0, lsr #10
 600:	00008eac 	andeq	r8, r0, ip, lsr #29
 604:	00000044 	andeq	r0, r0, r4, asr #32
 608:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 60c:	8e028b03 	vmlahi.f64	d8, d2, d3
 610:	0b0c4201 	bleq	310e1c <_bss_end+0x3077b8>
 614:	0d0c5c04 	stceq	12, cr5, [ip, #-16]
 618:	0000000c 	andeq	r0, r0, ip
 61c:	00000020 	andeq	r0, r0, r0, lsr #32
 620:	00000520 	andeq	r0, r0, r0, lsr #10
 624:	00008ef0 	strdeq	r8, [r0], -r0
 628:	00000050 	andeq	r0, r0, r0, asr r0
 62c:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 630:	8e028b03 	vmlahi.f64	d8, d2, d3
 634:	0b0c4201 	bleq	310e40 <_bss_end+0x3077dc>
 638:	0d0c6204 	sfmeq	f6, 4, [ip, #-16]
 63c:	0000000c 	andeq	r0, r0, ip
 640:	00000020 	andeq	r0, r0, r0, lsr #32
 644:	00000520 	andeq	r0, r0, r0, lsr #10
 648:	00008f40 	andeq	r8, r0, r0, asr #30
 64c:	00000050 	andeq	r0, r0, r0, asr r0
 650:	840c0e42 	strhi	r0, [ip], #-3650	; 0xfffff1be
 654:	8e028b03 	vmlahi.f64	d8, d2, d3
 658:	0b0c4201 	bleq	310e64 <_bss_end+0x307800>
 65c:	0d0c6204 	sfmeq	f6, 4, [ip, #-16]
 660:	0000000c 	andeq	r0, r0, ip
 664:	0000001c 	andeq	r0, r0, ip, lsl r0
 668:	00000520 	andeq	r0, r0, r0, lsr #10
 66c:	00008f90 	muleq	r0, r0, pc	; <UNPREDICTABLE>
 670:	00000054 	andeq	r0, r0, r4, asr r0
 674:	8b080e42 	blhi	203f84 <_bss_end+0x1fa920>
 678:	42018e02 	andmi	r8, r1, #2, 28
 67c:	5e040b0c 	vmlapl.f64	d0, d4, d12
 680:	00080d0c 	andeq	r0, r8, ip, lsl #26
 684:	00000018 	andeq	r0, r0, r8, lsl r0
 688:	00000520 	andeq	r0, r0, r0, lsr #10
 68c:	00008fe4 	andeq	r8, r0, r4, ror #31
 690:	0000001c 	andeq	r0, r0, ip, lsl r0
 694:	8b080e42 	blhi	203fa4 <_bss_end+0x1fa940>
 698:	42018e02 	andmi	r8, r1, #2, 28
 69c:	00040b0c 	andeq	r0, r4, ip, lsl #22
 6a0:	0000000c 	andeq	r0, r0, ip
 6a4:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 6a8:	7c020001 	stcvc	0, cr0, [r2], {1}
 6ac:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 6b0:	00000018 	andeq	r0, r0, r8, lsl r0
 6b4:	000006a0 	andeq	r0, r0, r0, lsr #13
 6b8:	00009000 	andeq	r9, r0, r0
 6bc:	00000074 	andeq	r0, r0, r4, ror r0
 6c0:	8b080e42 	blhi	203fd0 <_bss_end+0x1fa96c>
 6c4:	42018e02 	andmi	r8, r1, #2, 28
 6c8:	00040b0c 	andeq	r0, r4, ip, lsl #22
 6cc:	00000018 	andeq	r0, r0, r8, lsl r0
 6d0:	000006a0 	andeq	r0, r0, r0, lsr #13
 6d4:	00009074 	andeq	r9, r0, r4, ror r0
 6d8:	0000008c 	andeq	r0, r0, ip, lsl #1
 6dc:	8b080e42 	blhi	203fec <_bss_end+0x1fa988>
 6e0:	42018e02 	andmi	r8, r1, #2, 28
 6e4:	00040b0c 	andeq	r0, r4, ip, lsl #22
 6e8:	0000000c 	andeq	r0, r0, ip
 6ec:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 6f0:	7c020001 	stcvc	0, cr0, [r2], {1}
 6f4:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 6f8:	0000001c 	andeq	r0, r0, ip, lsl r0
 6fc:	000006e8 	andeq	r0, r0, r8, ror #13
 700:	00009120 	andeq	r9, r0, r0, lsr #2
 704:	00000068 	andeq	r0, r0, r8, rrx
 708:	8b040e42 	blhi	104018 <_bss_end+0xfa9b4>
 70c:	0b0d4201 	bleq	350f18 <_bss_end+0x3478b4>
 710:	420d0d68 	andmi	r0, sp, #104, 26	; 0x1a00
 714:	00000ecb 	andeq	r0, r0, fp, asr #29
 718:	0000001c 	andeq	r0, r0, ip, lsl r0
 71c:	000006e8 	andeq	r0, r0, r8, ror #13
 720:	00009188 	andeq	r9, r0, r8, lsl #3
 724:	00000058 	andeq	r0, r0, r8, asr r0
 728:	8b080e42 	blhi	204038 <_bss_end+0x1fa9d4>
 72c:	42018e02 	andmi	r8, r1, #2, 28
 730:	62040b0c 	andvs	r0, r4, #12, 22	; 0x3000
 734:	00080d0c 	andeq	r0, r8, ip, lsl #26
 738:	0000001c 	andeq	r0, r0, ip, lsl r0
 73c:	000006e8 	andeq	r0, r0, r8, ror #13
 740:	000091e0 	andeq	r9, r0, r0, ror #3
 744:	00000058 	andeq	r0, r0, r8, asr r0
 748:	8b080e42 	blhi	204058 <_bss_end+0x1fa9f4>
 74c:	42018e02 	andmi	r8, r1, #2, 28
 750:	62040b0c 	andvs	r0, r4, #12, 22	; 0x3000
 754:	00080d0c 	andeq	r0, r8, ip, lsl #26
 758:	0000000c 	andeq	r0, r0, ip
 75c:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
 760:	7c010001 	stcvc	0, cr0, [r1], {1}
 764:	000d0c0e 	andeq	r0, sp, lr, lsl #24
 768:	0000000c 	andeq	r0, r0, ip
 76c:	00000758 	andeq	r0, r0, r8, asr r7
 770:	00009238 	andeq	r9, r0, r8, lsr r2
 774:	000001ec 	andeq	r0, r0, ip, ror #3

Disassembly of section .debug_ranges:

00000000 <.debug_ranges>:
   0:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
   4:	00000000 	andeq	r0, r0, r0
   8:	00008000 	andeq	r8, r0, r0
   c:	00008094 	muleq	r0, r4, r0
  10:	00009100 	andeq	r9, r0, r0, lsl #2
  14:	00009120 	andeq	r9, r0, r0, lsr #2
	...
